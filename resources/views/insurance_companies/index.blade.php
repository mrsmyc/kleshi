@extends ('layouts.master', ['title' => 'Страховые компании'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Страховые компании</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/insurance_companies/create">Добавить</a></div>
</div>
<br>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>Название</th>
			<th>Полное название</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($companies as $item)
		<tr>
			<th>{{ $item->id }}</th>
			<td>{{ $item->name }}</td>
			<td>{{ $item->full_name }}</td>
			<td><a href="/insurance_companies/{{ $item->id }}/edit">Редактировать</a></td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $companies->links() }}</div>

@endsection