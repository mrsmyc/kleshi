@extends ('layouts.master', ['title' => 'Новая страховая компания'])

@section ('content')

<br>
<h2>Новая страховая компания</h2>

<form action="/insurance_companies" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-6">
			<label for="name">Название (в списке)</label>
			<input type="text" class="form-control" id="name" name="name" required>
		</div>
		<div class="form-group col-md-6">
			<label for="full_name">Полное название (в документах)</label>
			<input type="text" class="form-control" id="full_name" name="full_name" required>
		</div>
		<div class="form-group col-md-4">
			<div class="form-check">
				<input type="hidden" name="enabled" value="0">
				<input class="form-check-input" type="checkbox" id="enabled" name="enabled" value="1" checked>
				<label class="form-check-label" for="enabled">Активная</label>
			</div>
		</div>
		<div class="form-group col-md-4">
			<div class="form-check">
				<input type="hidden" name="guarantee_letter" value="0">
				<input class="form-check-input" type="checkbox" id="guarantee_letter" name="guarantee_letter" value="1">
				<label class="form-check-label" for="guarantee_letter">Требуют гарантийное письмо</label>
			</div>
		</div>
		<div class="form-group col-md-4">
			<div class="form-check">
				<input type="hidden" name="new_policy" value="0">
				<input class="form-check-input" type="checkbox" id="new_policy" name="new_policy" value="1">
				<label class="form-check-label" for="new_policy">Работают по новому полису</label>
			</div>
		</div>		
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

@endsection