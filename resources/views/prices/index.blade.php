@extends ('layouts.master', ['title' => 'Цены'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Цены</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/prices/create">Добавить</a></div>
</div>
<br>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Название</th>
			<th>Полное название</th>
			<th>Код</th>
			<th>Стоимость</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($prices as $item)
		<tr>
			<td>{{ $item->name }}</td>
			<td>{{ $item->full_name }}</td>
			<td>{{ $item->code }}</td>
			<td>{{ $item->price }}</td>
			<td><a href="/prices/{{ $item->id }}/edit">Редактировать</a></td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection