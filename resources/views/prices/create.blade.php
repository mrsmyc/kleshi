@extends ('layouts.master', ['title' => 'Новая запись о цене'])

@section ('content')

<br>
<h2>Новая запись о цене</h2>

<form action="/prices" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-4">
			<label for="name">Название</label>
			<input type="text" class="form-control" id="name" name="name" required>
		</div>
		<div class="form-group col-md-4">
			<label for="code">Код</label>
			<input type="text" class="form-control" id="code" name="code" required>
		</div>
		<div class="form-group col-md-4">
			<label for="price">Цена</label>
			<input type="number" class="form-control" id="price" name="price" required>
		</div>
		<div class="form-group col-md-12">
			<label for="full_name">Развёрнутое название</label>
			<textarea class="form-control" id="full_name" name="full_name" required></textarea> 
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

@endsection