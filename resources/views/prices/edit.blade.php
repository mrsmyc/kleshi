@extends ('layouts.master', ['title' => 'Редактирование записи о цене'])

@section ('content')

<br>
<h2>Редактирование записи о цене</h2>

<form action="/prices/{{ $price->id }}" method="POST">
	<div class="row">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<div class="form-group col-md-4">
			<label for="name">Название</label>
			<input type="text" class="form-control" id="name" value="{{ $price->name }}" disabled>
		</div>
		<div class="form-group col-md-4">
			<label for="code">Код</label>
			<input type="text" class="form-control" id="code" value="{{ $price->code }}" disabled>
		</div>
		<div class="form-group col-md-4">
			<label for="price">Цена</label>
			<input type="number" class="form-control" id="price" name="price" value="{{ $price->price }}" required>
		</div>
		<div class="form-group col-md-12">
			<label for="full_name">Развёрнутое название</label>
			<textarea class="form-control" id="full_name" name="full_name" required>{{ $price->full_name }}</textarea> 
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

@endsection