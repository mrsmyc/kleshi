@extends ('layouts.master', ['title' => 'Новый договор на вакцинацию'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-9">Договор на вакцинацию</h3>
</div>
<br>

<form action="/vaccination_agreements" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-2">
			<label for="date">Дата</label>
			<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" name="date" required>
		</div>
		<div class="form-group col-md-2">
			<label for="payment_type">Вид оплаты</label>
			<select id="payment_type" class="form-control" name="payment_type_id" required>
				@foreach ($paymentTypes as $item)
				<option value="{{ $item->id }}" {{ ($item->id == 2) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		
		<div class="col-md-12 section-title">
			<hr>
			<span>Данные организации</span>
		</div>

		<div class="form-group col-md-6">
			<label for="company_name">Название организации</label>
			<input type="text" class="form-control" id="company_name" name="company_name" required>
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_amount">Количество вакцин</label>
			<input type="text" class="form-control" id="vaccine_amount" name="vaccine_amount">
		</div>
		<div class="form-group col-md-4">
			<label for="social_group">Группа</label>
			<select class="selectpicker" name="group_id" required>
				<option value=""></option>
				<optgroup label="Проф контингенты">
					@foreach ($profContingents as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Группы риска">
					@foreach ($riskGroups as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Соц.группы">
					@foreach ($socialGroups as $item)
					<option value="{{ $item->id }}">{{ $item->name }}</option>
					@endforeach
				</optgroup>
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные вакцинаций</span>
		</div>

		<div class="form-group col-md-4">
			<label for="vaccine">Вакцина</label>
			<select id="vaccine" class="form-control" name="vaccine_id">
				<option value=""></option>
				@foreach ($vaccines as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="vaccine_serie">Серия вакцины</label>
			<select id="vaccine_serie" class="form-control" name="vaccine_serie_id" required>
				<option value=""></option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="vaccine_spot">Место введения</label>
			<select id="vaccine_spot" class="form-control" name="vaccine_spot_id">
				<option value=""></option>
				@foreach ($vaccineSpots as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span">Другие данные</span>
		</div>

		<div class="form-group col-md-12">
			<label for="comment">Комментарий</label>
			<input type="text" class="form-control" id="comment" name="comment">
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<script type="text/javascript" src="/js/vaccinations.js"></script>

@endsection