@extends ('layouts.master', ['title' => 'Редактирование договора'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-6">Договор на вакцинацию</h3>
	@if ( !$agreement->is_finished )
	<div class="col-md-3"><a class="btn btn-primary float-right" href="/vaccinations/create/{{ $agreement->id }}">Вакцинация</a></div>
	@endif
	<div class="col-md-3"><a class="btn btn-primary float-right" href="/reports/generate_vaccination_agreement_report/{{ $agreement->id }}" target="_blank">Генерация реестра</a></div>
</div>

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#agreement">Договор</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#patients">Пациенты</a>
	</li>
</ul>
<br>

<div class="tab-content">
	<div class="tab-pane active" id="agreement">
		<form action="/vaccination_agreements/{{ $agreement->id }}" method="POST">
			<div class="row">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group col-md-2">
					<label for="date">Дата</label>
					<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::parse($agreement->date)->format('Y-m-d') }}" name="date" required>
				</div>
				<div class="form-group col-md-2">
					<label for="payment_type">Вид оплаты</label>
					<select id="payment_type" class="form-control" name="payment_type_id" required>
						@foreach ($paymentTypes as $item)
						<option value="{{ $item->id }}" {{ ($item->id == $agreement->payment_type_id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные организации</span>
				</div>

				<div class="form-group col-md-6">
					<label for="company_name">Название организации</label>
					<input type="text" class="form-control" id="company_name" value="{{ $agreement->company_name }}" name="company_name">
				</div>
				<div class="form-group col-md-2">
					<label for="vaccine_amount">Количество вакцин</label>
					<input type="text" class="form-control" id="vaccine_amount" value="{{ $agreement->vaccine_amount }}" name="vaccine_amount">
				</div>
				<div class="form-group col-md-4">
					<label for="social_group">Группа</label>
					<select class="selectpicker" name="group_id" required>
						<option value=""></option>
						<optgroup label="Проф контингенты">
							@foreach ($profContingents as $item)
							<option value="{{ $item->id }}" {{ ($item->id == $agreement->group_id) ? 'selected' : '' }}>{{ $item->name }}</option>
							@endforeach
						</optgroup>
						<optgroup label="Группы риска">
							@foreach ($riskGroups as $item)
							<option value="{{ $item->id }}" {{ ($item->id == $agreement->group_id) ? 'selected' : '' }}>{{ $item->name }}</option>
							@endforeach
						</optgroup>
						<optgroup label="Соц.группы">
							@foreach ($socialGroups as $item)
							<option value="{{ $item->id }}" {{ ($item->id == $agreement->group_id) ? 'selected' : '' }}>{{ $item->name }}</option>
							@endforeach
						</optgroup>
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные вакцинаций</span>
				</div>

				<div class="form-group col-md-4">
					<label for="vaccine">Вакцина</label>
					<select id="vaccine" class="form-control" name="vaccine_id">
						<option value=""></option>
						@foreach ($vaccines as $item)
						<option value="{{ $item->id }}" {{ ($item->id == $agreement->vaccine_id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-4">
					<label for="vaccine_serie">Серия вакцины</label>
					<select id="vaccine_serie" class="form-control" name="vaccine_serie_id" required>
						<option value=""></option>
						@foreach ($vaccineSeries as $item)
						<option value="{{ $item->id }}" {{ ($item->id == $agreement->vaccine_serie_id) ? 'selected' : ''}}>{{ $item->name }} до {{ Carbon\Carbon::parse($item->valid_to)->format('d/m/Y') }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-4">
					<label for="vaccine_spot">Место введения</label>
					<select id="vaccine_spot" class="form-control" name="vaccine_spot_id">
						<option value=""></option>
						@foreach ($vaccineSpots as $item)
						<option value="{{ $item->id }}" {{ ($item->id == $agreement->vaccine_spot_id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span">Другие данные</span>
				</div>

				<div class="form-group col-md-12">
					<label for="comment">Комментарий</label>
					<input type="text" class="form-control" id="comment" name="comment" value="{{ $agreement->comment }}">
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>
	</div>

	<div class="tab-pane" id="patients">
		Выполнено: {{ $vaccinations->count() }} из {{ $agreement->vaccine_amount }}

		<table class="table table-striped">
			<thead>
				<tr>
					<th>#</th>
					<th>ФИО</th>
					<th>Кратность</th>
					<th>Возраст</th>
					<th>Дата обращения</th>
					<th>Действия</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($vaccinations as $vac)
				<tr>
					<th>{{ $vac->id }}</th>
					<td>{{ $vac->name }}</td>
					<td>{{ $vac->vaccine_level->name }}</td>
					<td>{{ $vac->age }}</td>
					<td>{{ Carbon\Carbon::parse($vac->date)->format('d/m/Y') }}</td>
					<td><a href="/vaccinations/{{ $vac->id }}/edit">Просмотр</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript" src="/js/vaccinations.js"></script>

@endsection