@extends ('layouts.master', ['title' => 'Договоры на вакцинацию'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Договоры на вакцинацию</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccination_agreements/create">Добавить</a></div>
</div>
<br>

<form action="/vaccination_agreements/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-1">
			<label for="id">Номер</label>
			<input type="text" class="form-control" id="id" name="id" value="{{ (isset($filter)) ? $filter['id'] : ''}}" placeholder="ID">
		</div>
		<div class="form-group col-md-4">
			<label for="name">Название компании</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="Название">
		</div>

		<button type="submit" class="btn btn-primary mt-3">Найти</button>
	</div>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th width="35%">Название компании</th>
			<th>Вакцина</th>
			<th>Кол-во вакцин</th>
			<th>Вакцинировано</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($agreements as $item)
		<tr>
			<th>{{ $item->id }}</th>
			<td>{{ $item->company_name }}</td>
			<td>{{ $item->vaccine->name }}</td>
			<td>{{  $item->vaccine_amount }}</td>
			<td>
				@if ($item->vaccinations()->count())
				<a href="/vaccination_agreements/{{ $item->id }}/edit#patients">{{ $item->vaccinations()->count() }}</a><br>
				@else
				{{ $item->vaccinations()->count() }}
				@endif
			</td>
			<td>
				<a href="/vaccination_agreements/{{ $item->id }}/edit">Просмотр</a><br>
				@if ( !$item->is_finished )
				<a href="/vaccinations/create/{{ $item->id }}" target="_blank">Вакцинировать</a><br>
				@endif
				<a href="/reports/generate_vaccination_agreement_report/{{ $item->id }}">Генерация отчёта</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $agreements->links() }}</div>

@endsection