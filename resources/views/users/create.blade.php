@extends ('layouts.master', ['title' => 'Новый пользователь'])

@section ('content')

<br>
<h2>Новый пользователь</h2>

@include('layouts.errors')

<form action="/users" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="col-md-12 section-title">
			<hr>
			<span>Общие данные</span>
		</div>
		<div class="form-group col-md-12">
			<label for="name">ФИО*</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
		</div>
		<div class="form-group col-md-6">
			<label for="login">Логин*</label>
			<input type="text" class="form-control" id="login" name="login" value="{{ old('login') }}" required>
		</div>
		<div class="form-group col-md-6">
			<label for="password">Пароль*</label>
			<div class="input-group mb-3">
				<input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}" required>
				<div class="input-group-append">
					<span id="show-password" class="input-group-text"><img src="/img/eye-close-up.svg" height="20px"></span> 
				</div>
			</div>
		</div>
		<div class="form-group col-md-6">
			<label for="email">E-mail</label>
			<input type="email" class="form-control" id="email" value="{{ old('email') }}" name="email">
		</div>
		<div class="form-group col-md-6">
			<label for="phone">Телефон</label>
			<input type="text" class="form-control" id="phone" value="{{ old('phone') }}" name="phone">
		</div>
		
		<div class="col-md-12 section-title">
			<hr>
			<span>Роли</span>
		</div>
	
		@foreach ($roles as $role)
		<div class="form-group col-md-3">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="role_{{ $role->id }}" name="roles[]" value="{{ $role->id }}" {{ (old('roles') && in_array($role->id, old('roles'))) ? 'checked' : '' }}>
				<label class="form-check-label" for="role_{{ $role->id }}">{{ $role->tag }}</label>
			</div>
		</div>
		@endforeach
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 500px;"></div>

<script type="text/javascript" src="/js/admin.js"></script>

@endsection