@extends ('layouts.master', ['title' => 'Пользователи'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Пользователи</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/users/create">Добавить</a></div>
</div>
<br>

<table class="table table-striped">
	<thead>
		<tr>
			<th>ФИО</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($users as $user)
		<tr>
			<td>{{ $user->name }}</td>
			<td><a href="/users/{{ $user->id }}/edit">Просмотр</a></td>
		</tr>
		@endforeach
	</tbody>
</table>

@endsection