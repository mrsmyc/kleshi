@extends ('layouts.master', ['title' => 'Новый пациент'])

@section ('content')

<div style="height: 30px"></div>
<div class="row">
    <div class="col-md-12 greeting">
        <h4>Здравствуйте, {{ Auth::user()->name }}!</h4>
        <a class="btn btn-primary" href="/logout">Выйти</a>
    </div>
</div>
<div style="height: 30px"></div>
<div class="card-deck text-center">
    <div class="card">
        <a href="admissions/create"><img src="/img/mite.png" class="card-img-top"></a>
        <div class="card-body">
            <h5 class="card-title">Регистрация пострадавшего</h5>
            <a href="admissions/create" class="btn btn-primary">Перейти</a>
        </div>
    </div>
    <div class="card text-center">
        <a href="antibodies/create"><img src="/img/blood.png" class="card-img-top"></a>
        <div class="card-body">
            <h5 class="card-title">Регистрация крови на антитела</h5>
            <a href="antibodies/create" class="btn btn-primary">Перейти</a>
        </div>
    </div>
    <div class="card text-center">
        <a href="vaccinations/create"><img src="/img/injection.png" class="card-img-top"></a>
        <div class="card-body">
            <h5 class="card-title">Вакцинация пациента</h5>
            <a href="vaccinations/create" class="btn btn-primary">Перейти</a>
        </div>
    </div>
</div>

@endsection