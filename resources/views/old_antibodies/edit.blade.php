@extends ('layouts.master', ['title' => 'Регистрация крови на антитела'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-9">Карточка антител (давние укусы)</h3>
	<div class="col-md-3"><a class="btn btn-primary float-right" href="/old_antibodies/{{ $antibody->id }}/print" target="_blank">Распечатать</a></div>
</div>
<br>

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#patient-card">Карточка пациента</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#results">Результаты исследования</a>
	</li>
</ul>
<br>

<div class="tab-content">
	<div class="tab-pane active" id="patient-card">
		<form action="/old_antibodies/{{ $antibody->id}}" method="POST">
			<div class="row">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group col-md-2">
					<label for="id">П/п</label>
					<input type="number" class="form-control" id="id" value="{{ $antibody->id }}" disabled>
				</div>
				<div class="form-group col-md-2">
					<label for="date">Дата</label>
					<input type="date" class="form-control" id="date" value="{{ ($antibody->date) ? Carbon\Carbon::parse($antibody->date)->format('Y-m-d') : '' }}" name="date">
				</div>
				<div class="form-group col-md-4">
					<label for="responsible">Ответственный*</label>
					<select id="responsible" class="form-control" name="responsible_id" required>
						@foreach ($managers as $item)
							<option value="{{ $item->id }}" {{ ( $antibody->responsible_id == $item->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="col-md-12 section-title">
					<hr>
					<span>Личные данные</span>
				</div>

				<div class="form-group col-md-4">
					<label for="name">ФИО</label>
					<input type="text" class="form-control" id="name" value="{{ $antibody->name }}" name="name">
				</div>
				<div class="form-group col-md-2">
					<label for="birth_date">Дата рождения</label>
					<input type="date" class="form-control" id="birth_date" value="{{ ($antibody->birth_date) ? Carbon\Carbon::parse($antibody->birth_date)->format('Y-m-d') : '' }}" name="birth_date">
				</div>
				<div class="form-group col-md-2">
					<label for="age">Возраст</label>
					<input type="number" min="0" class="form-control" id="age" value="{{ $antibody->age }}" name="age">
				</div>
				<div class="form-group col-md-2">
					<label for="phone">Телефон</label>
					<input type="text" class="form-control" id="phone" value="{{ $antibody->phone }}" name="phone">
				</div>
				<div class="form-group col-md-2">
					<label for="bite_year">Год укуса</label>
					<input type="number" class="form-control" id="bite_year" value="{{ $antibody->bite_year }}" name="bite_year">
				</div>

				<div class="form-group col-md-12">
					<label for="anamnesis">Анамнез</label>
					<textarea class="form-control" id="anamnesis" name="anamnesis" rows="5">{{ $antibody->anamnesis }}</textarea>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Виды исследований</span>
				</div>
		
				@foreach ($researches as $item)
				<div class="form-group col-md-2">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}" {{ (in_array($item->id, $checkedResearches)) ? 'checked' : ''}}>
						<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
					</div>
				</div>
				@endforeach
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>
	</div>

	<div class="tab-pane" id="results">
		<form action="/old_antibodies/{{ $antibody->id }}/update_result" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="row" id="scroll-to-result">
				<div class="form-group col-md-2">
					<label for="">Номер: </label>
					<b>{{ $antibody->id }}</b>
				</div>
				<div class="form-group col-md-4">
					<label for="">ФИО: </label>
					<b>{{ $antibody->name }}</b>
				</div>
				<div class="form-group col-md-6">
					<label for="">Запрошенные исследования: </label>
					@foreach ($researches as $item)
						@if (in_array($item->id, $checkedResearches)) 
						<b>{{ $item->name }}</b> |
						@endif
					@endforeach
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 section-title">
					<span>Данные Ig</span>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_m_te">Ig M КЭ</label>
					<select id="ig_m_te" class="form-control {{ ($result->ig_m_te == '0') ? 'negative' : '' }} {{ ($result->ig_m_te == '1') ? 'positive' : '' }}" name="ig_m_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_te">Ig G КЭ</label>
					<select id="ig_g_te" class="form-control {{ ($result->ig_g_te == '0') ? 'negative' : '' }} {{ ($result->ig_g_te == '1') ? 'positive' : '' }}" name="ig_g_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_te">Титр Ig G КЭ</label>
					<input type="text" class="form-control" id="titr_ig_g_te" value="{{ $result->titr_ig_g_te }}" name="titr_ig_g_te">
				</div>
				<div class="form-group col-md-2">
					<label for="ig_m_tb">Ig M КБ</label>
					<select id="ig_m_tb" class="form-control {{ ($result->ig_m_tb == '0') ? 'negative' : '' }} {{ ($result->ig_m_tb == '1') ? 'positive' : '' }}" name="ig_m_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_tb">Ig G КБ</label>
					<select id="ig_g_tb" class="form-control {{ ($result->ig_g_tb == '0') ? 'negative' : '' }} {{ ($result->ig_g_tb == '1') ? 'positive' : '' }}" name="ig_g_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_tb">Титр Ig G КБ</label>
					<input type="text" class="form-control" id="titr_ig_g_tb" value="{{ $result->titr_ig_g_tb }}" name="titr_ig_g_tb">
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные Кровь</span>
				</div>

				<div class="form-group col-md-2">
					<label for="blood_pcr_vte">Кровь ПЦР ВКЭ</label>
					<select id="blood_pcr_vte" class="form-control" name="blood_pcr_vte">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_vte == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_vte == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tb">Кровь ПЦР КБ</label>
					<select id="blood_pcr_tb" class="form-control" name="blood_pcr_tb">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hme">Кровь ПЦР МЭЧ</label>
					<select id="blood_pcr_hme" class="form-control" name="blood_pcr_hme">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hme == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hme == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hga">Кровь ПЦР ГАЧ</label>
					<select id="blood_pcr_hga" class="form-control" name="blood_pcr_hga">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hga == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hga == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hrf">Кровь ПЦР КВЛ</label>
					<select id="blood_pcr_hrf" class="form-control" name="blood_pcr_trf">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_trf == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_trf == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tr">Кровь ПЦР КР</label>
					<select id="blood_pcr_tr" class="form-control" name="blood_pcr_tr">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tr == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tr == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Другие данные</span>
				</div>

				<div class="form-group col-md-12">
					<label for="comment_result">Комментарий</label>
					<textarea class="form-control" id="comment_result" name="comment">{{ $result->comment }}</textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>

		<div style="height: 200px;"></div>
	</div>
</div>

<script type="text/javascript" src="/js/antibody.js"></script>

@endsection