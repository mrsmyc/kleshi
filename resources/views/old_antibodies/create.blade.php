@extends ('layouts.master', ['title' => 'Регистрация крови на антитела'])

@section ('content')

<br>
<h2>Регистрация крови на антитела (давние укусы)</h2>

<form action="/old_antibodies" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-2">
			<label for="id">П/п</label>
			<input type="number" class="form-control" id="id" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="date">Дата</label>
			<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" max="2100-12-31" min="2010-01-01" name="date">
		</div>
		<div class="form-group col-md-4">
			<label for="responsible">Ответственный</label>
			<select id="responsible" class="form-control" name="responsible_id" required>
				@foreach ($managers as $item)
				<option value="{{ $item->id }}" {{ ( $item->id == auth()->user()->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Личные данные</span>
		</div>

		<div class="form-group col-md-4">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name">
		</div>
		<div class="form-group col-md-2">
			<label for="birth_date">Дата рождения</label>
			<input type="date" class="form-control" id="birth_date" name="birth_date">
		</div>
		<div class="form-group col-md-2">
			<label for="age">Возраст</label>
			<input type="number" min="0" class="form-control" id="age" name="age">
		</div>
		<div class="form-group col-md-2">
			<label for="phone">Телефон</label>
			<input type="text" class="form-control" id="phone" name="phone">
		</div>
		<div class="form-group col-md-2">
			<label for="bite_year">Год укуса</label>
			<input type="number" class="form-control" id="bite_year" name="bite_year">
		</div>

		<div class="form-group col-md-12">
			<label for="anamnesis">Анамнез</label>
			<textarea class="form-control" id="anamnesis" name="anamnesis" rows="5"></textarea>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Виды исследований</span>
		</div>


		@foreach ($researches as $item)
		<div class="form-group col-md-2">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}">
				<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
			</div>
		</div>
		@endforeach

	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 200px;"></div>

<script type="text/javascript" src="/js/antibody.js"></script>

@endsection