@extends ('layouts.master', ['title' => 'Давние укусы'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Давние укусы</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/old_antibodies/create">Добавить</a></div>
</div>
<br>

<form action="/old_antibodies/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-1">
			<label for="id">Номер</label>
			<input type="text" class="form-control" id="id" name="id" value="{{ (isset($filter)) ? $filter['id'] : ''}}" placeholder="ID">
		</div>
		<div class="form-group col-md-4">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="ФИО">
		</div>
		<div class="form-group col-md-2">
			<label for="date">Дата обращения</label>
			<input type="date" class="form-control" id="date" name="date" value="{{ (isset($filter)) ? $filter['date'] : ''}}" placeholder="Дата">
		</div>
		<button type="submit" class="btn btn-primary mt-3">Найти</button>
	</div>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>ФИО</th>
			<th>Возраст</th>
			<th>Дата обращения</th>
			<th>Телефон</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($antibodies as $ant)
		<tr>
			<th>{{ $ant->id }}</th>
			<td>{{ $ant->name }}</td>
			<td>{{ $ant->age }}</td>
			<td>{{ Carbon\Carbon::parse($ant->date)->format('d/m/Y') }}</td>
			<td>{{ $ant->phone }}</td>
			<td>
				<a href="/old_antibodies/{{ $ant->id }}/edit">Просмотр</a><br>
				<a href="/old_antibodies/{{ $ant->id }}/edit#results">Результаты</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $antibodies->links() }}</div>

@endsection