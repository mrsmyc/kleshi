@extends ('layouts.master', ['title' => 'Отчёты'])

@section ('content')

<br>
<h2>Выгрузка данных</h2>

<form action="/export/export_data" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-4">
			<label for="export_type">Тип</label>
			<select id="export_type" class="form-control" name="export_type" required>
				<option value="admission">Первичные приёмы</option>
				<option value="antibody">Антитела</option>
			</select>
		</div>
		<!--div class="form-group col-md-3">
			<label for="admin_territory">Админ. территория</label>
			<select id="admin_territory" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="admin_territory_id">
				<option value=""></option>
				@//foreach ($adminTerritories as $item)
				<option value="{/{ $item->id }}" {/{ (isset($filter) && ($filter['admin_territory_id'] == $item->id )) ? 'selected' : ''}}>{/{ $item->name }}</option>
				@//endforeach
			</select>
		</div-->
		<div class="form-group col-md-2 date-block">
			<label for="date_from">От</label>
			<input type="date" class="form-control" id="date_from" name="date_from">
		</div>
		<div class="form-group col-md-2 date-block">
			<label for="date_to">До</label>
			<input type="date" class="form-control" id="date_to" name="date_to">
		</div>
		
	</div>

	<button type="submit" class="btn btn-primary">Сформировать</button>
</form>

@endsection