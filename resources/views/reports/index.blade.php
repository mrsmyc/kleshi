@extends ('layouts.master', ['title' => 'Отчёты'])

@section ('content')

<br>
<h2>Отчёты</h2>

<form action="/reports/make_report" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-4">
			<label for="report_type">Тип отчёта</label>
			<select id="report_type" class="form-control" name="report_type" required>
				<option value="insurance_report">Реестр для страховой компании</option>
				<option value="all_reports">Все реестры</option>
				<option value="yearly_report">Годовой отчёт</option>
				<option value="weekly_report">Отчёт для Быковой Натальи</option>
				<option value="weekly_report_additional">Дополнение к отчёту для Быковой Натальи</option>
				<option value="weekly_report_rpn">Еженедельный отчёт в РПН 2020</option>
				<option value="vac_report_rpn">Отчёт по вакцинациям в РПН</option>
			</select>
		</div>
		<div class="form-group col-md-2 date-block">
			<label for="date_from">От</label>
			<input type="date" class="form-control" id="date_from" name="date_from" required>
		</div>
		<div class="form-group col-md-2 date-block">
			<label for="date_to">До</label>
			<input type="date" class="form-control" id="date_to" name="date_to" required>
		</div>
		<div class="form-group col-md-2" id="year-block">
			<label for="year">Год</label>
			<input type="number" class="form-control" id="year" name="year" value="{{ Carbon\Carbon::today()->format('Y')}}" required disabled>
		</div>
		<div class="form-group col-md-4" id="insurance_block">
			<label for="insurance_company_id">Страховая компания</label>
			<select id="insurance_company_id" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id" required>
				<option value=""></option>
				@foreach ($insuranceCompanies as $company)
				<option value="{{ $company->id }}">{{ $company->name }}</option>
				@endforeach
			</select>
		</div>
		
	</div>

	<button type="submit" class="btn btn-primary">Сформировать</button>
</form>

<script type="text/javascript" src="/js/report.js"></script>

@endsection