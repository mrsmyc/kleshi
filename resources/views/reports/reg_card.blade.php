<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no">
	<meta name="theme-color" content="#ffffff">
	<title>Карточка первичного приёма</title>
	<link rel="stylesheet" type="text/css" href="/css/app.css">

</head>
<body>

<br>
<div class="row">
	<h2 class="col-md-9">Первичный приём №{{ $admission->id }}</h2>
</div>
<hr>

<div>
	<div><b>Номер полиса:</b> {{ $admission->policy_num }}</div>
	<div><b>Срок действия:</b> с {{ Carbon\Carbon::parse($admission->policy_start)->format('d.m.Y') }} до {{ Carbon\Carbon::parse($admission->policy_end)->format('d.m.Y') }}</div>
	</br>
	<div><b>ФИО:</b> {{ $admission->name }}</div>
	<div><b>Пол:</b> {{ $admission->gender->name }}</div>
	<div><b>Возраст:</b> {{ $admission->age }}</div>
	
	<div><b>Дата приёма:</b> {{ Carbon\Carbon::parse($admission->date)->format('d.m.Y') }}</div>
	@if ($admission->refund)
	<div><b>Дата возврата:</b> {{ Carbon\Carbon::parse($admission->refund)->format('d.m.Y') }}</div>
	@endif

	<div><b>Дата укуса:</b> {{ Carbon\Carbon::parse($admission->bite_date)->format('d.m.Y') }}</div>
	<div><b>Дата удаления:</b> {{ Carbon\Carbon::parse($admission->removal_date)->format('d.m.Y') }}</div>
	@if ($admission->removal_institution)
	<div><b>Учреждение, удалившее клеща:</b> {{ $admission->removal_institution->name }}</div>
	@endif
	@if ($admission->bite_spot)
	<div><b>Место укуса:</b> {{ $admission->bite_spot->name }}</div>
	@endif

	@if ($admission->material)
		<div><b>Материал:</b> {{ $admission->material->name }}</div>
		@if ($admission->material_id == '1')
			<div><b>Количество клещей:</b> {{ $admission->material_amount }}</div>
		@endif
	@endif
	@if ($admission->ig_date)
		<div><b>Дата постановки иммуноглобулина:</b> {{ $admission->ig_date }}</div>
		<div><b>Количество доз иммуноглобулина:</b> {{ $admission->ig_dose }}</div>
	@endif
	@if ($admission->infect_consult)
		<div><b>Дата консультации инфекциониста:</b> {{ $admission->consult_date }}</div>
	@endif
	@if ($admission->measures_date)
		@if ($admission->encephalitis_method)
			<div><b>Профилактика клещевого энцефалита:</b> {{ $admission->encephalitis_method->name }}</div>
		@endif
		@if ($admission->bacterial_method)
			<div><b>Профилактика бактериальных инфекций:</b> {{ $admission->bacterial_method->name }}</div>
		@endif
		<div><b>Дата применения мер:</b> {{ Carbon\Carbon::parse($admission->measures_date)->format('d.m.Y') }}</div>
	@endif
	@if ($admission->researches)
		<div><b>Запрошенные исследования:</b>
		@foreach ($admission->researches as $research)
			{{ $research->name }}
		@endforeach
		</div>
	@endif
</div>

</body>
</html>
