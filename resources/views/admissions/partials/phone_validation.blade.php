<!-- Модальное окно валидации телефона -->
<div id="phone-validation-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			<h5 class="modal-title"><img class="modal-icon" src="/img/warning.svg">Внимание!</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			<div class="modal-body">
				Похоже, Вы допустили ошибку в номере телефона: <span id="check-phone"></span> (менее 11 цифр). <b>Всё равно продолжить?</b> 
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Закрыть</button>
			</div>
		</div>
	</div>
</div>