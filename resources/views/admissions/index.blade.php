@extends ('layouts.master', ['title' => 'Регистрации пациентов'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Пострадавшие от укуса клеща</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/admissions/create">Добавить</a></div>
</div>
<br>

<form action="/admissions/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-1">
			<label for="year_id">№-Г</label>
			<input type="number" class="form-control" id="year_id" name="year_id" value="{{ (isset($filter)) ? $filter['year_id'] : ''}}" placeholder="ID-Г">
		</div>
		<div class="form-group col-md-3">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="ФИО">
		</div>
		<div class="form-group col-md-2">
			<label for="date_from">Дата (от)</label>
			<input type="date" class="form-control" id="date_from" name="date_from" value="{{ (isset($filter)) ? $filter['date_from'] : ''}}" placeholder="Дата (от)">
		</div>
		<div class="form-group col-md-2">
			<label for="date_to">Дата (до)</label>
			<input type="date" class="form-control" id="date_to" name="date_to" value="{{ (isset($filter)) ? $filter['date_to'] : ''}}" placeholder="Дата (до)">
		</div>
		<div class="form-group col-md-3">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['insurance_company_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<button type="submit" class="btn btn-primary mt-3">Найти</button>
		<div class="form-group col-md-1">
			<label for="gender">Пол</label>
			<select id="gender" class="form-control" name="gender_id">
				<option value=""></option>
				@foreach ($genders as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['gender_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="gender">Материал</label>
			<select id="gender" class="form-control" name="material_id">
				<option value=""></option>
				@foreach ($materials as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['material_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="encephalitis_method">Профилактика КЭ</label>
			<select class="selectpicker" id="encephalitis_method" multiple name="encephalitis_method_ids[]">
				@foreach ($encephalitisMethods as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && $filter['encephalitis_method_ids'] && in_array($item->id, $filter['encephalitis_method_ids'])) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="bacterial_method">Профилактика БИ</label>
			<select class="selectpicker" multiple id="bacterial_method" name="bacterial_method_ids[]">
				@foreach ($bacterialMethods as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && $filter['bacterial_method_ids'] && in_array($item->id, $filter['bacterial_method_ids'])) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="iged">Поставлен Ig</label>
			<select id="iged" class="form-control" name="iged">
				<option value=""></option>
				<option value="1" {{ (isset($filter) && ($filter['iged'] == 1 )) ? 'selected' : ''}}>Да</option>
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="needs_attention">Требует внимания</label>
			<select id="needs_attention" class="form-control" name="needs_attention">
				<option value=""></option>
				<option value="1" {{ (isset($filter) && ($filter['needs_attention'] == 1 )) ? 'selected' : ''}}>Да</option>
			</select>
		</div>
		<div class="form-group col-md-1">
			<label for="id">№</label>
			<input type="text" class="form-control" id="id" name="id" value="{{ (isset($filter)) ? $filter['id'] : ''}}" placeholder="ID">
		</div>
		<div class="form-group col-md-3">
			<label for="permanent_residence">ПМЖ</label>
			<select class="selectpicker" multiple id="permanent_residence" name="permanent_residence_ids[]">
				@foreach ($permanentResidences as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && $filter['permanent_residence_ids'] && in_array($item->id, $filter['permanent_residence_ids'])) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="admin_territory">Админ. территория</label>
			<select class="selectpicker" multiple id="admin_territory" name="admin_territory_ids[]">
				@foreach ($adminTerritories as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && $filter['admin_territory_ids'] && in_array($item->id, $filter['admin_territory_ids'])) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
</form>
<p>Всего: {{ $total }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>ФИО</th>
			<th>Возраст</th>
			<th>Дата обращения</th>
			<th>Телефон</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>

		@if ($admissions)
			@foreach ($admissions as $adm)
			<tr>
				<th width="7%">
					@if ($adm->year_id) 
					{{ $adm->year_id }}-Г <br>
					@endif
					{{ $adm->id }}
				</th>
				<td width="33%">{{ $adm->name }}</td>
				<td width="5%">{{ $adm->age }}</td>
				<td width="20%">{{ Carbon\Carbon::parse($adm->date)->format('d/m/Y') }}</td>
				<td width="20%">{{ $adm->phone }}</td>
				<td width="15%">
					<a href="/admissions/{{ $adm->id }}/edit">Просмотр</a><br>
					<a href="/admissions/{{ $adm->id }}/edit#results">Результаты</a>
				</td>
			</tr>
			@endforeach
		@else
			<tr>Нет записей</tr>
		@endif
	</tbody>
</table>

<div class="pagination-center">{{ $admissions->links() }}</div>

@endsection