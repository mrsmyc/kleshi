@extends ('layouts.master', ['title' => 'Карточка пациента'])

@section ('content')

<br>
<div class="row">
	<h2 class="col-md-6">Первичный приём <b>№{{ $admission->year_id }}-Г</b> (№{{ $admission->id }})</h2>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/admissions/create/{{ $admission->id }}">Копировать</a></div>
	@if ($admission->antibody)
	<div class="col-md-2">
		<a class="btn btn-primary float-right" href="/antibodies/{{ $admission->antibody->id }}/edit">Просмотр АТ</a>
	</div>
	@else
	<div class="col-md-2">
		<a class="btn btn-primary float-right" href="/antibodies/create/{{ $admission->id }}">Добавить АТ</a>
	</div>
	@endif
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/admissions/{{ $admission->id }}/print" target="_blank">Распечатать</a></div>
</div>
<!--hr-->

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#patient-card">Карточка пациента</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#results">Результаты исследования</a>
	</li>
</ul>
<br>

<div class="tab-content">
	<div class="tab-pane active" id="patient-card">
		<form action="/admissions/{{ $admission->id }}" method="POST" enctype="multipart/form-data" id="form">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<div class="row">
				<div class="form-group col-md-1 extra-width">
					<label for="year_id">П/п №-Г</label>
					<input type="text" class="form-control" id="year_id" value="{{ ($admission->year_id) ? $admission->year_id . '-Г' : '' }}" disabled>
				</div>
				<div class="form-group col-md-1 extra-width">
					<label for="id">П/п №</label>
					<input type="number" class="form-control" id="id" value="{{ $admission->id }}" disabled>
				</div>
				<div class="form-group col-md-2">
					<label for="date">Дата приёма*</label>
					<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::parse($admission->date)->format('Y-m-d') }}" name="date" max="2100-12-31" min="2010-01-01" required>
				</div>
				<div class="form-group col-md-2">
					<label for="time">Время приёма*</label>
					<input type="time" class="form-control" id="time" value="{{ Carbon\Carbon::parse($admission->time)->format('H:i') }}" name="time" required>
				</div>
				<div class="form-group col-md-3">
					<label for="responsible">Ответственный*</label>
					<select id="responsible" class="form-control" name="responsible_id" required>
						@foreach ($managers as $item)
							<option value="{{ $item->id }}" {{ ( $admission->responsible_id == $item->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="refund">Возврат</label>
					<input type="date" class="form-control" id="refund" value="{{ ($admission->refund) ? Carbon\Carbon::parse($admission->refund)->format('Y-m-d') : ''}}" max="2100-12-31" min="2010-01-01" name="refund">
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Личные данные</span>
				</div>

				<div class="form-group col-md-6">
					<label for="name">ФИО*</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ $admission->name }}" required>
				</div>
				<div class="form-group col-md-2">
					<label for="gender">Пол*</label>
					<select id="gender" class="form-control" name="gender_id" required>
						<option value=""></option>
						@foreach ($genders as $item)
						<option value="{{ $item->id }}" {{ ($admission->gender_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="birth_date">Дата рождения*</label>
					<input type="date" min="1900-01-01" class="form-control" id="birth_date" name="birth_date" value="{{ ($admission->birth_date) ? Carbon\Carbon::parse($admission->birth_date)->format('Y-m-d') : '' }}" required>
				</div>
				<div class="form-group col-md-2">
					<label for="age">Возраст*</label>
					<input type="number" min="0" class="form-control" id="age" value="{{ $admission->age }}" name="age" required>
				</div>
				<div class="form-group col-md-6">
					<label for="phone">Телефон*</label>
					<input type="text" class="form-control" id="phone" value="{{ $admission->phone }}" name="phone" required>
				</div>
				<div class="form-group col-md-3">
					<label for="extended_permanent_residence">ПМЖ (расширенный)</label>
					<select id="extended_permanent_residence" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="extended_permanent_residence_id">
						<option value=""></option>
						@foreach ($residences as $item)
						<option value="{{ $item->id }}" {{ ($admission->extended_permanent_residence_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="vaccine_level">Сведения о вакцинации</label>
					<select id="vaccine_level" class="form-control" name="vaccine_level_id">
						<option value=""></option>
						@foreach ($vaccineLevels as $item)
						<option value="{{ $item->id }}" {{ ($admission->vaccine_level_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные страховки</span>
				</div>

				<div class="form-group col-md-12" id="guarantee-letter-reminder" {{ ($admission->insurance_company && $admission->insurance_company->guarantee_letter == 1) ? '' : 'style=display:none'}}>
					<img class="modal-icon" src="/img/warning.svg">Данная страховая компания требует гарантийное письмо. Свяжитесь с компанией для его получения.
				</div>

				<div class="form-group col-md-6">
					<label for="insurance_company">Страховая компания</label>
					<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
						<option value=""></option>
						@foreach ($insuranceCompanies as $item)
						<option value="{{ $item->id }}" {{ ($admission->insurance_company_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<input type="hidden" name="policy_num" value="">
					<label for="policy_num">Номер полиса</label>
					<input type="text" class="form-control" id="policy_num" placeholder="" value="{{ $admission->policy_num }}" name="policy_num" data-block="insurance_data" required>
				</div>
				<div class="form-group col-md-3">
					<input type="hidden" name="policy_research_num" value="">
					<label for="policy_research_num">Кол-во исследований</label>
					<span data-toggle="tooltip" data-placement="left" title="На основании этого поля будет выставлен счёт СК. Если Вы согласовали 6 инфекций, хотя полис покрывает 4, выбирайте 6.">
						<img src="/img/question.svg" height="15px">
					</span>
					<select id="policy_research_num" class="form-control" data-block="insurance_data" name="policy_research_num" required>
						<option value=""></option>
						@foreach ($researchNum as $key => $item)
						<option value="{{ $key }}" {{ ($admission->policy_research_num == $key) ? 'selected' : ''}}>{{ $item }}</option>
						@endforeach
					</select>
				</div>
				<input type="hidden" name="asko_ic_name" value="">
				<input type="hidden" name="asko_immunocard" value="">
				<div class="form-group col-md-6 {{ ($admission->insurance_company && in_array($admission->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_ic_name_block">
					<label for="asko_ic_name">Страховая компания (уточнение)</label>
					<input type="text" class="form-control" id="asko_ic_name" name="asko_ic_name" data-block="insurance_data" value="{{ $admission->asko_ic_name }}">
				</div>
				<div class="form-group col-md-6 {{ ($admission->insurance_company && in_array($admission->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_immunocard_block">
					<label for="immunocard">Номер иммунокарты (или ГП)</label>
					<input type="text" class="form-control" id="asko_immunocard" name="asko_immunocard" data-block="insurance_data" value="{{ $admission->asko_immunocard }}">
				</div>
				<div class="form-group col-md-4">
					<input type="hidden" name="policy_start" value="">
					<label for="policy_start">Срок действия (от)</label>
					<input type="date" class="form-control" id="policy_start" value="{{ ($admission->policy_start) ? Carbon\Carbon::parse($admission->policy_start)->format('Y-m-d') : ''}}" name="policy_start" data-block="insurance_data" max="2100-12-31" min="2010-01-01" required>
				</div>
				<div class="form-group col-md-4">
					<input type="hidden" name="policy_end" value="">
					<label for="policy_end">Срок действия (до)</label>
					<input type="date" class="form-control" id="policy_end" value="{{ ($admission->policy_end) ? Carbon\Carbon::parse($admission->policy_end)->format('Y-m-d') : ''}}" name="policy_end" data-block="insurance_data" max="2100-12-31" min="2010-01-01" required>
				</div>
				<div class="form-group col-md-4">
					<label for="policy_scan">Скан полиса</label>
					@if ($admission->policy_scan)
					<div class="policy-view">
						<a data-fancybox="gallery" href="{{ Storage::url($admission->policy_scan) }}"><i class="fas fa-search"></i> Посмотреть</a>
						<a id="policy-delete"><i class="fas fa-trash"></i> Удалить</a>
						<input type="hidden" name="uploaded_policy_scan" value="1">
					</div>
					<input id="policy-file" type="file" class="form-control-file" id="policy_scan" name="policy_scan">
					@else
					<input type="file" class="form-control-file" id="policy_scan" name="policy_scan" data-block="insurance_data">
					@endif
				</div>

				<div class="col-md-12">
					<hr>
				</div>

				<div class="form-group col-md-12">
					<label for="comment_insurance">Примечание (страховая)</label>
					<input type="text" class="form-control" id="comment_insurance" value="{{ $admission->comment_insurance }}" name="comment_insurance">
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные об укусе</span>
				</div>

				<div class="form-group col-md-2">
					<label for="bite_date">Дата укуса</label>
					<input type="date" class="form-control" id="bite_date" value="{{ ($admission->bite_date) ? Carbon\Carbon::parse($admission->bite_date)->format('Y-m-d') : ''}}" max="2100-12-31" min="2010-01-01" name="bite_date">
				</div>
				<div class="form-group col-md-2">
					<label for="removal_date">Дата удаления</label>
					<input type="date" class="form-control" id="removal_date" value="{{ ($admission->removal_date) ? Carbon\Carbon::parse($admission->removal_date)->format('Y-m-d') : ''}}" max="2100-12-31" min="2010-01-01" name="removal_date">
				</div>
				<div class="form-group col-md-4">
					<label for="removal_institution">Учреждение, удалившее клеща</label>
					<select id="removal_institution" class="form-control" name="removal_institution_id">
						<option value=""></option>
						@foreach ($removalInstitutions as $item)
						<option value="{{ $item->id }}" {{ ($admission->removal_institution_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-4">
					<label for="bite_spot">Место укуса*</label>
					<select id="bite_spot" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="bite_spot_id" required>
						<option value=""></option>
						@foreach ($biteSpots as $item)
						<option value="{{ $item->id }}" {{ ($admission->bite_spot_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="bite_circumstance">Обстоятельства заражения*</label>
					<select id="bite_circumstance" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="bite_circumstance_id" required>
						<option value=""></option>
						@foreach ($biteCircumstances as $item)
						<option value="{{ $item->id }}" {{ ($admission->bite_circumstance_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="child_care_facility">Детское учреждение?</label>
					<select id="child_care_facility" class="form-control" data-live-search="true" data-none-selected-text name="child_care_facility_id">
						<option value=""></option>
						@foreach ($childCareFacilities as $item)
						<option value="{{ $item->id }}" {{ ($admission->child_care_facility_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="extended_admin_territory">Админ. территория</label>
					<select id="extended_admin_territory" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="extended_admin_territory_id">
						<option value=""></option>
						@foreach ($residences as $item)
						<option value="{{ $item->id }}" {{ ($admission->extended_admin_territory_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3" id="locality_clarification_block">
					<label for="locality_clarification">Уточнение местности</label>
					<input type="hidden" name="locality_clarification" value="">
					<input class="form-control" id="locality_clarification" type="text" name="locality_clarification" value="{{ $admission->locality_clarification }}">
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Меры</span>
				</div>

				<div class="form-group col-md-2">
					<label for="material">Материал</label>
					<select id="material" class="form-control" name="material_id">
						<option value=""></option>
						@foreach ($materials as $item)
						<option value="{{ $item->id }}" {{ ($admission->material_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="material_amount">Количество образцов</label>
					<input type="hidden" name="material_amount" value="">
					<input type="num" min="1" class="form-control" id="material_amount" value="{{ $admission->material_amount }}" name="material_amount">
				</div>
				<div class="form-group col-md-2">
					<label for="material_date">Дата забора</label>
					<input type="hidden" name="material_date" value="">
					<input type="date" min="{{ Carbon\Carbon::parse($admission->date)->format('Y-m-d') }}" class="form-control" id="material_date" value="{{ ($admission->material_date) ? Carbon\Carbon::parse($admission->material_date)->format('Y-m-d') : '' }}" name="material_date">
				</div>
				<div class="form-group col-md-3">
					<label for="infect_consult">Консультация инфекциониста</label>
					<select id="infect_consult" class="form-control" name="infect_consult" required>
						<option value="0" {{ ($admission->infect_consult == 0 ) ? 'selected' : ''}}>Нет</option>
						<option value="1" {{ ($admission->infect_consult == 1 ) ? 'selected' : ''}}>Да</option>
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="consult_date">Дата консультации</label>
					<input type="hidden" name="consult_date" value="">
					<input type="date" class="form-control" id="consult_date" placeholder="" max="2100-12-31" min="2010-01-01" name="consult_date" {{ ($admission->infect_consult == '1') ? 'value=' . $admission->consult_date : 'disabled'}}>
				</div>

				<div class="form-group col-md-2">
					<label for="ig_dose">Количество доз</label>
					<input type="num" min="0" class="form-control" id="ig_dose" value="{{ $admission->ig_dose }}" name="ig_dose">
				</div>
				<div class="form-group col-md-3">
					<label for="ig_date">Дата постановки Ig</label>
					<input type="hidden" name="ig_date" value="">
					<input type="date" class="form-control" id="ig_date" max="2100-12-31" min="2010-01-01" name="ig_date" {{ ($admission->ig_dose) ? 'value=' . $admission->ig_date : 'disabled'}}>
				</div>
				<div class="form-group col-md-2">
					<label for="encephalitis_method">Профилактика КЭ</label>
					<select id="encephalitis_method" class="form-control" name="encephalitis_method_id">
						<option value=""></option>
						@foreach ($encephalitisMethods as $item)
						<option value="{{ $item->id }}" {{ ($admission->encephalitis_method_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="bacterial_method">Профилактика БИ</label>
					<select id="bacterial_method" class="form-control" name="bacterial_method_id">
						<option value=""></option>
						@foreach ($bacterialMethods as $item)
						<option value="{{ $item->id }}" {{ ($admission->bacterial_method_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="measures_date">Дата применения мер</label>
					<input type="hidden" name="measures_date" value="">
					<input type="date" class="form-control" id="measures_date" max="2100-12-31" min="2010-01-01" name="measures_date" {{ ($admission->bacterial_method || $admission->encephalitis_method) ? 'value=' . $admission->measures_date : 'disabled'}}>
				</div>
				
				<div class="col-md-12 section-title">
					<hr>
					<span>Вид исследования</span>
				</div>

				<div class="form-group col-md-12" id="research_num_reminder" {{ ($admission->policy_research_num && ($admission->researches->count() > $admission->policy_research_num)) ? '' : 'style=display:none' }}>
					<img class="modal-icon" src="/img/warning.svg">Превышено количество исследований по страховке, требуется доплата.
				</div>

				@foreach ($researches as $item)
				<div class="form-group col-md-2">
					<div class="form-check">
						<input class="form-check-input research {{ ($item->id == '1' || $item->id == '2') ? 'research-te-tb' : ''}}{{ ($item->id == '3' || $item->id == '4') ? 'research-hga-hme' : ''}}" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}" {{ (in_array($item->id, $checkedResearches)) ? 'checked' : ''}}>
						<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
					</div>
				</div>
				@endforeach

				<div class="form-group col-md-12">
					<hr>
					<label for="comment">Примечание</label>
					<textarea class="form-control" id="comment" name="comment">{{ $admission->comment }}</textarea> 
				</div>

				<div class="form-group col-md-6">
					<div class="form-check">
						<input type="hidden" name="needs_attention" value="0">
						<input class="form-check-input" type="checkbox" id="needs_attention" name="needs_attention" {{ ($admission->needs_attention) ? 'checked' : ''}} value="1">
						<label class="form-check-label" for="needs_attention">Требует особого внимания или довнесения данных</label>
						<span data-toggle="tooltip" data-placement="right" title="Например, если необходимо добавить скан полиса, связаться с кем-то, или если это особый случай, который требует внимания при генерации реестров">
							<img src="/img/question.svg" height="15px">
						</span>
					</div>
				</div>
				<div class="form-group col-md-6">
					<div class="form-check">
						<input type="hidden" name="to_skip" value="0">
						<input class="form-check-input" type="checkbox" id="to_skip" name="to_skip" {{ ($admission->to_skip) ? 'checked' : ''}} value="1">
						<label class="form-check-label" for="to_skip">Не отображать в реестрах</label>
						<span data-toggle="tooltip" data-placement="right" title="Например, если человек не сдал кровь, или исследования не провели по другим причинам">
							<img src="/img/question.svg" height="15px">
						</span>
					</div>
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>

			<div style="height: 200px;"></div>
		</form>
	</div>

	<div class="tab-pane" id="results">
		<table class="table table-striped table-dark" id="scroll-to-result">
			<thead>
				<tr>
					<th scope="col" width="7%">#</th>
					<th scope="col" width="10%">ФИО</th>
					<th scope="col">Пол</th>
					<th scope="col">Возраст</th>
					<th scope="col">Вакц</th>
					<th scope="col">Укус</th>
					<th scope="col">Удаление</th>
					<th scope="col">Место</th>
					<th scope="col" width="15%">Локация</th>
					<th scope="col">Материал</th>
					<th scope="col">Исследования</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>{{ $admission->year_id }}-Г</b><br> {{ $admission->id }}</td>
					<td>{{ $admission->name }}</td>
					<td>{{ mb_substr($admission->gender->name, 0, 1) }}</td>
					<td>{{ $admission->age }}</td>
					<td>{{ ($admission->vaccine_level) ? mb_substr($admission->vaccine_level->name, 0, 6) : ''}}</td>
					<td>{{ ($admission->bite_date) ? Carbon\Carbon::parse($admission->bite_date)->format('d/m/Y') : '' }}</td>
					<td>{{ ($admission->removal_date) ? Carbon\Carbon::parse($admission->removal_date)->format('d/m/Y') : ''}}</td>
					<td>{{ ($admission->bite_spot) ? $admission->bite_spot->name : '' }}</td>
					<td>{{ $admission->full_location }}</td>
					<td>
						@if ($admission->material)
						{{ $admission->material->name }} 
						{{ ($admission->material_amount > 1 ) ? ' (' . $admission->material_amount . ')' : '' }}
						@endif
					</td>
					<td>@foreach ($researches as $item)
						@if (in_array($item->id, $checkedResearches)) 
						<b>{{ $item->name }}</b> |
						@endif
					@endforeach</td>
				</tr>
			</tbody>
		</table>
		@if ($admission->material_id && ($admission->material_id != 5 && $admission->material_id != 7))

		<form action="/admissions/{{ $admission->id }}/update_result" method="POST" id="result-form">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}

			@if ($admission->material_id == 1)
			@foreach ($tickResults as $key=>$tickResult)
			<div class="row">
				<div class="col-md-12 section-title">
					<hr>
					<span>Данные Клещ №{{ $key + 1 }}</span>
				</div>

				<div class="form-group col-md-1">
					<label for="tick_pcr_vte_{{ $key }}">ВКЭ</label>
					<select id="tick_pcr_vte_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_vte == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_vte == '1') ? 'positive' : '' }}" name="tick_pcr_vte[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_vte == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_vte == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-1">
					<label for="tick_pcr_tb_{{ $key }}">КБ</label>
					<select id="tick_pcr_tb_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_tb == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_tb == '1') ? 'positive' : '' }}" name="tick_pcr_tb[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-1">
					<label for="tick_pcr_hme_{{ $key }}">МЭЧ</label>
					<select id="tick_pcr_hme_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_hme == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_hme == '1') ? 'positive' : '' }}" name="tick_pcr_hme[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_hme == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_hme == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-1">
					<label for="tick_pcr_hga_{{ $key }}">ГАЧ</label>
					<select id="tick_pcr_hga_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_hga == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_hga == '1') ? 'positive' : '' }}" name="tick_pcr_hga[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_hga == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_hga == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-1">
					<label for="tick_pcr_trf_{{ $key }}">КВЛ</label>
					<select id="tick_pcr_trf_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_trf == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_trf == '1') ? 'positive' : '' }}" name="tick_pcr_trf[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_trf == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_trf == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-1">
					<label for="tick_pcr_tr_{{ $key }}">КР</label>
					<select id="tick_pcr_tr_{{ $key }}" class="form-control {{ ($tickResult->tick_pcr_tr == '0') ? 'negative' : '' }} {{ ($tickResult->tick_pcr_tr == '1') ? 'positive' : '' }}" name="tick_pcr_tr[{{ $key }}]">
						<option value=""></option>
						<option value="0" {{ ($tickResult->tick_pcr_tr == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($tickResult->tick_pcr_tr == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="tick_type_{{ $key }}">Вид клеща</label>
					<select id="tick_type_{{ $key }}" class="form-control" name="tick_type_id[{{ $key }}]">
						<option value=""></option>
						@foreach ($tickTypes as $item)
						<option value="{{ $item->id }}" {{ ($tickResult->tick_type_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="tick_gender_{{ $key }}">Пол клеща</label>
					<select id="tick_gender_{{ $key }}" class="form-control" name="tick_gender_id[{{ $key }}]">
						<option value=""></option>
						@foreach ($tickGenders as $item)
						<option value="{{ $item->id }}" {{ ($tickResult->tick_gender_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="tick_condition_{{ $key }}">Состояние клеща</label>
					<select id="tick_condition_{{ $key }}" class="form-control" name="tick_condition_id[{{ $key }}]">
						<option value=""></option>
						@foreach ($tickConditions as $item)
						<option value="{{ $item->id }}" {{ ($tickResult->tick_condition_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
			</div>
			@endforeach
			@endif

			@if ($admission->material_id != 1 && $admission->material_id != 6)
			<div class="row">
				<div class="col-md-12 section-title">
					<hr>
					<span>Данные Кровь</span>
				</div>

				<div class="form-group col-md-2">
					<label for="blood_pcr_vte">Кровь ПЦР ВКЭ</label>
					<select id="blood_pcr_vte" class="form-control {{ ($result->blood_pcr_vte == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_vte == '1') ? 'positive' : '' }}" name="blood_pcr_vte">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_vte == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_vte == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tb">Кровь ПЦР КБ</label>
					<select id="blood_pcr_tb" class="form-control  {{ ($result->blood_pcr_tb == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_tb == '1') ? 'positive' : '' }}" name="blood_pcr_tb">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hme">Кровь ПЦР МЭЧ</label>
					<select id="blood_pcr_hme" class="form-control  {{ ($result->blood_pcr_hme == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_hme == '1') ? 'positive' : '' }}" name="blood_pcr_hme">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hme == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hme == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hga">Кровь ПЦР ГАЧ</label>
					<select id="blood_pcr_hga" class="form-control  {{ ($result->blood_pcr_hga == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_hga == '1') ? 'positive' : '' }}" name="blood_pcr_hga">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hga == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hga == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tr">Кровь ПЦР КР</label>
					<select id="blood_pcr_tr" class="form-control  {{ ($result->blood_pcr_tr == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_tr == '1') ? 'positive' : '' }}" name="blood_pcr_tr">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tr == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tr == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_trf">Кровь ПЦР КВЛ</label>
					<select id="blood_pcr_trf" class="form-control  {{ ($result->blood_pcr_trf == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_trf == '1') ? 'positive' : '' }}" name="blood_pcr_trf">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_trf == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_trf == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
			</div>

			<div class="row">
				<div class="col-md-12 section-title">
					<hr>
					<span>Данные Ig</span>
				</div>

				<div class="form-group col-md-2">
					<label for="ig_m_te">Ig M КЭ</label>
					<select id="ig_m_te" class="form-control {{ ($result->ig_m_te == '0') ? 'negative' : '' }} {{ ($result->ig_m_te == '1') ? 'positive' : '' }}" name="ig_m_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_te">Ig G КЭ</label>
					<select id="ig_g_te" class="form-control {{ ($result->ig_g_te == '0') ? 'negative' : '' }} {{ ($result->ig_g_te == '1') ? 'positive' : '' }}" name="ig_g_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_te">Титр Ig G КЭ</label>
					<input type="text" class="form-control" id="titr_ig_g_te" value="{{ $result->titr_ig_g_te }}" name="titr_ig_g_te">
				</div>
				<div class="form-group col-md-2">
					<label for="ig_m_tb">Ig M КБ</label>
					<select id="ig_m_tb" class="form-control {{ ($result->ig_m_tb == '0') ? 'negative' : '' }} {{ ($result->ig_m_tb == '1') ? 'positive' : '' }}" name="ig_m_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_tb">Ig G КБ</label>
					<select id="ig_g_tb" class="form-control {{ ($result->ig_g_tb == '0') ? 'negative' : '' }} {{ ($result->ig_g_tb == '1') ? 'positive' : '' }}" name="ig_g_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_tb">Титр Ig G КБ</label>
					<input type="text" class="form-control" id="titr_ig_g_tb" value="{{ $result->titr_ig_g_tb }}" name="titr_ig_g_tb">
				</div>
			</div>
			@endif

			<div class="row">
				<div class="col-md-12 section-title">
					<hr>
					<span>Другие данные</span>
				</div>

				<div class="form-group col-md-3">
					<label for="antigen_te">Антиген КЭ</label>
					<select id="antigen_te" class="form-control {{ ($result->antigen_te == '0') ? 'negative' : '' }} {{ ($result->antigen_te == '1') ? 'positive' : '' }}" name="antigen_te">
						<option value=""></option>
						<option value="0" {{ ($result->antigen_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->antigen_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>

				<div class="form-group col-md-3">
					<label for="issue_date">Дата выдачи {{ (!$result->issue_date) ? '(не сохранено)' : '' }}</label>
					<input type="date" class="form-control" id="issue_date" value="{{ ($result->issue_date) ? Carbon\Carbon::parse($result->issue_date)->format('Y-m-d') : Carbon\Carbon::now()->format('Y-m-d') }}" max="2100-12-31" min="2010-01-01" name="issue_date" required>
				</div>

				<div class="form-group col-md-3">
					<label for="ready_to_notify">Результат готов к отправке (SMS)*</label>
					<select id="ready_to_notify" class="form-control" name="ready_to_notify" {{ ($result->sms_notification && (in_array($result->sms_notification->status, ['sent', 'failed']))) ? 'disabled' : ''}} required="">
						<option value=""></option>
						<option value="1" {{ ($result->ready_to_notify == '1') ? 'selected' : ''}}>Готов</option>
						<option value="0" {{ ($result->ready_to_notify == '0') ? 'selected' : ''}}>НЕ готов</option>
					</select>
				</div>
			</div>

			@if ($result->sms_notification)
			<table class="table table-dark" width="100%">
				<tr>
					<th width="60%">Текст уведомления</th>
					<th width="25%">Статус</th>
					<th width="15%">Дата отправки</th>
				</tr>
				<tr>
					<td>{{ $result->sms_notification->text }}</td>
					<td>{{ $smsStatuses[$result->sms_notification->status] }}</td>
					<td>{{ ($result->sms_notification->status == 'sent') ? Carbon\Carbon::parse($result->sms_notification->updated_at)->format('d.m.Y H:i') : ''}}</td>
				</tr>
			</table>
			@endif

			<div class="row">
				<div class="form-group col-md-12">
					<label for="comment_result">Комментарий</label>
					<textarea class="form-control" id="comment_result" name="comment" rows="3">{{ $result->comment }}</textarea>
				</div>
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>
			<!--select name="save_action" id="save_action">
				<option value="save_and_return">Сохранить и вернуться</option>
				<option value="save_and_next">Сохранить и к следующей</option>
				<option value="save_and_prev">Сохранить и к предыдущей</option>
			</select>
			<button type="submit" class="btn btn-primary">Сохранить и к следующей</button>
			<button type="submit" class="btn btn-primary">Сохранить и к предыдущей</button-->

			<div style="height: 20px;"></div>
		</form>
		@else
		Исследование не требуется
		@endif
	</div>
</div>

@include ('admissions.partials.guarantee_letter')

@include ('admissions.partials.phone_validation')

<script type="text/javascript" src="/js/admission.js"></script>

@endsection