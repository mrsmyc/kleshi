@extends ('layouts.master', ['title' => 'Новый пациент'])

@section ('content')

<br>
<div class="row">
	<h2 class="col-md-9">Карточка пациента</h2>
</div>
<hr>

<form action="/admissions" method="POST" enctype="multipart/form-data" id="form">
	{{ csrf_field() }}
	<div class="row">
		<div class="form-group col-md-1 extra-width">
			<label for="year_id">П/п №-Г</label>
			<input type="text" class="form-control" id="year_id" value="" disabled>
		</div>
		<div class="form-group col-md-1 extra-width">
			<label for="id">П/п №</label>
			<input type="number" class="form-control" id="id" value="" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="date">Дата приёма*</label>
			<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::now()->format('Y-m-d')}}" name="date" max="2100-12-31" min="2010-01-01" required>
		</div>
		<div class="form-group col-md-2">
			<label for="time">Время приёма*</label>
			<input type="time" class="form-control" id="time" value="{{ Carbon\Carbon::now()->format('H:i')}}" name="time" required>
		</div>
		<div class="form-group col-md-3">
			<label for="responsible">Ответственный*</label>
			<select id="responsible" class="form-control" name="responsible_id" required>
				@foreach ($managers as $item)
				<option value="{{ $item->id }}" {{ ( $item->id == auth()->user()->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="refund">Возврат</label>
			<input type="date" class="form-control" id="refund" max="2100-12-31" min="2010-01-01"name="refund">
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Личные данные</span>
		</div>

		<div class="form-group col-md-6">
			<label for="name">ФИО*</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ ($admission) ? $admission->name : ''}}" required>
		</div>
		<div class="form-group col-md-2">
			<label for="gender">Пол*</label>
			<select id="gender" class="form-control" name="gender_id">
				<option value=""></option>
				@foreach ($genders as $item)
				<option value="{{ $item->id }}" {{ ($admission && ($admission->gender_id == $item->id)) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="birth_date">Дата рождения*</label>
			<input type="date" min="1900-01-01" class="form-control" id="birth_date" name="birth_date" value="{{ ($admission) ? $admission->birth_date : ''}}" required>
		</div>
		<div class="form-group col-md-2">
			<label for="age">Возраст*</label>
			<input type="number" min="0" class="form-control" id="age" name="age" value="{{ ($admission) ? Carbon\Carbon::parse($admission->birth_date)->age : '' }}" required>
		</div>
		<div class="form-group col-md-6">
			<label for="phone">Телефон*</label>
			<input type="text" class="form-control" id="phone" name="phone" value="{{ ($admission) ? $admission->phone : ''}}" required>
		</div>
		<div class="form-group col-md-3">
			<label for="extended_permanent_residence">ПМЖ (расширенный)</label>
			<select id="extended_permanent_residence" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="extended_permanent_residence_id">
				<option value=""></option>
				@foreach ($residences as $item)
				<option value="{{ $item->id }}" data-residence="{{ $item->permanent_residence_id }}" {{ ($admission && ($admission->extended_permanent_residence_id == $item->id)) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="vaccine_level">Сведения о вакцинации</label>
			<select id="vaccine_level" class="form-control" name="vaccine_level_id">
				<option value=""></option>
				@foreach ($vaccineLevels as $item)
				<option value="{{ $item->id }}" {{ ($admission && ($admission->vaccine_level_id == $item->id)) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные страховки</span>
		</div>

		<div class="form-group col-md-12" id="guarantee-letter-reminder" style="display: none;">
			<img class="modal-icon" src="/img/warning.svg">Данная страховая компания требует гарантийное письмо. Свяжитесь с компанией для его получения.
		</div>
		<div class="form-group col-md-6">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}" {{ ($isPolicyValid && ($admission->insurance_company_id == $item->id)) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="policy_num">Номер полиса</label>
			<input type="text" class="form-control" id="policy_num" name="policy_num" data-block="insurance_data" value="{{ ($isPolicyValid) ? $admission->policy_num : '' }}" required>
		</div>
		<div class="form-group col-md-3">
			<label for="policy_research_num">Кол-во исследований</label>
			<span data-toggle="tooltip" data-placement="left" title="На основании этого поля будет выставлен счёт СК. Если Вы согласовали 6 инфекций, хотя полис покрывает 4, выбирайте 6.">
				<img src="/img/question.svg" height="15px">
			</span>
			<select id="policy_research_num" class="form-control" data-block="insurance_data" name="policy_research_num" required>
				<option value=""></option>
				@foreach ($researchNum as $key => $item)
				<option value="{{ $key }}" {{ ($isPolicyValid && ($admission->policy_research_num == $key)) ? 'selected' : '' }}>{{ $item }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-6 hidden" id="asko_ic_name_block">
			<label for="asko_ic_name">Страховая компания (уточнение)</label>
			<input type="text" class="form-control" id="asko_ic_name" name="asko_ic_name" data-block="insurance_data">
		</div>
		<div class="form-group col-md-6 hidden" id="asko_immunocard_block">
			<label for="immunocard">Номер иммунокарты (или ГП)</label>
			<input type="text" class="form-control" id="asko_immunocard" name="asko_immunocard" data-block="insurance_data">
		</div>
		<div class="form-group col-md-4">
			<label for="policy_start">Срок действия (от)</label>
			<input type="date" class="form-control" id="policy_start" max="2100-12-31" min="2010-01-01" name="policy_start" data-block="insurance_data" value="{{ ($isPolicyValid) ? $admission->policy_start : ''}}" required>
		</div>
		<div class="form-group col-md-4">
			<label for="policy_end">Срок действия (до)</label>
			<input type="date" class="form-control" id="policy_end" max="2100-12-31" min="2010-01-01" name="policy_end" data-block="insurance_data" value="{{ ($isPolicyValid) ? $admission->policy_end : ''}}" required>
		</div>
		<div class="form-group col-md-4">
			<label for="policy_scan">Скан полиса</label>
			<input type="file" class="form-control-file" id="policy_scan" name="policy_scan" onchange="readURL(this);" data-block="insurance_data">
		</div>

		<div class="col-md-12">
			<hr>
		</div>

		<div class="form-group col-md-12">
			<label for="comment_insurance">Примечание (страховая)</label>
			<input type="text" class="form-control" id="comment_insurance" name="comment_insurance">
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные об укусе</span>
		</div>

		<div class="form-group col-md-2">
			<label for="bite_date">Дата укуса</label>
			<input type="date" class="form-control" id="bite_date" max="2100-12-31" min="2010-01-01" name="bite_date">
		</div>
		<div class="form-group col-md-2">
			<label for="removal_date">Дата удаления</label>
			<input type="date" class="form-control" id="removal_date" max="2100-12-31" min="2010-01-01" name="removal_date">
		</div>
		<div class="form-group col-md-4">
			<label for="removal_institution">Учреждение, удалившее клеща</label>
			<select id="removal_institution" class="form-control" name="removal_institution_id">
				<option value=""></option>
				@foreach ($removalInstitutions as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="bite_spot">Место укуса*</label>
			<select id="bite_spot" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="bite_spot_id" required>
				<option value=""></option>
				@foreach ($biteSpots as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="bite_circumstance">Обстоятельства заражения*</label>
			<select id="bite_circumstance" class="form-control" name="bite_circumstance_id" required>
				<option value=""></option>
				@foreach ($biteCircumstances as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="child_care_facility">Детское учреждение?</label>
			<select id="child_care_facility" class="form-control" name="child_care_facility_id">
				<option value=""></option>
				@foreach ($childCareFacilities as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="extended_admin_territory">Админ. территория*</label>
			<select id="extended_admin_territory" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="extended_admin_territory_id" required>
				<option value=""></option>
				@foreach ($residences as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3" id="locality_clarification_block">
			<label for="locality_clarification">Уточнение местности</label>
			<input class="form-control" id="locality_clarification" type="text" name="locality_clarification">
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Меры</span>
		</div>
		<div class="form-group col-md-2">
			<label for="material">Материал</label>
			<select id="material" class="form-control" name="material_id">
				<option value=""></option>
				@foreach ($materials as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="material_amount">Количество образцов</label>
			<input type="number" min="1" class="form-control" id="material_amount" name="material_amount">
		</div>
		<div class="form-group col-md-2">
			<label for="material_date">Дата забора</label>
			<input type="date" class="form-control" id="material_date" name="material_date" disabled>
		</div>
		<div class="form-group col-md-3">
			<label for="infect_consult">Консультация инфекциониста</label>
			<select id="infect_consult" class="form-control" name="infect_consult" required>
				<option value="0">Нет</option>
				<option value="1">Да</option>
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="consult_date">Дата консультации</label>
			<input type="date" class="form-control" id="consult_date" placeholder="" max="2100-12-31" min="2010-01-01" name="consult_date" required disabled>
		</div>

		<div class="form-group col-md-2">
			<label for="ig_dose">Количество доз Ig</label>
			<input type="num" min="0" class="form-control" id="ig_dose" placeholder="" name="ig_dose">
		</div>
		<div class="form-group col-md-3">
			<label for="ig_date">Дата постановки Ig</label>
			<input type="date" class="form-control" id="ig_date" placeholder="" max="2100-12-31" min="2010-01-01" name="ig_date" required disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="encephalitis_method">Профилактика КЭ</label>
			<select id="encephalitis_method" class="form-control" name="encephalitis_method_id">
				<option value=""></option>
				@foreach ($encephalitisMethods as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="bacterial_method">Профилактика БИ</label>
			<select id="bacterial_method" class="form-control" name="bacterial_method_id">
				<option value=""></option>
				@foreach ($bacterialMethods as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="measures_date">Дата применения мер</label>
			<input type="date" class="form-control" id="measures_date" placeholder="" max="2100-12-31" min="2010-01-01" name="measures_date" required disabled>
		</div>
		
		<div class="col-md-12 section-title">
			<hr>
			<span>Вид исследования</span>
		</div>

		<div class="form-group col-md-12" id="research_num_reminder" style="display: none;">
			<img class="modal-icon" src="/img/warning.svg">Превышено количество исследований по страховке, требуется доплата.
		</div>

		@foreach ($researches as $item)
		<div class="form-group col-md-2">
			<div class="form-check">
				<input class="form-check-input research {{ ($item->id == '1' || $item->id == '2') ? 'research-te-tb' : ''}}{{ ($item->id == '3' || $item->id == '4') ? 'research-hga-hme' : ''}}" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}">
				<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
			</div>
		</div>
		@endforeach
		
		<div class="form-group col-md-12">
			<hr>
			<label for="comment">Примечание</label>
			<textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
		</div>

		<div class="form-group col-md-6">
			<div class="form-check">
				<input class="form-check-input" type="checkbox" id="needs_attention" name="needs_attention" value="1">
				<label class="form-check-label" for="needs_attention">Требует особого внимания или довнесения данных</label>
				<span data-toggle="tooltip" data-placement="right" title="Например, если необходимо добавить скан полиса, связаться с кем-то, или если это особый случай, который требует внимания при генерации реестров">
					<img src="/img/question.svg" height="15px">
				</span>
			</div>
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 200px;"></div>

@include ('admissions.partials.guarantee_letter')

@include ('admissions.partials.phone_validation')

<script type="text/javascript" src="/js/admission.js"></script>

@endsection