@extends ('layouts.master', ['title' => 'Результаты исследований'])

@section ('content')

<div class="no-print">
	<br>
	<div class="row">
		<h3 class="col-md-10">Результаты исследований первичных приёмов</h3>
	</div>
	<br>

	<form action="/results/admissions/filter" method="GET">
		<div class="form-row align-items-center">
			<div class="form-group col-md-2">
				<label for="date">Дата выдачи</label>
				<input type="date" class="form-control" id="date" name="date" value="{{ (isset($filter)) ? $filter['date'] : ''}}" placeholder="Дата" required>
			</div>
			<button type="submit" class="btn btn-primary mt-3">Найти</button>
			<div class="mt-3 btn">Для печати нажмите Ctrl+P</div>
		</div>
	</form>
</div>

@if ($results->count() > 0)
<table class="table table-striped results-check" border="1" style="position: absolute; left: 2.5vw; width: 95vw;">
	<thead>
		<tr align="center">
			<th rowspan="2">#</th>
			<th rowspan="2">ФИО</th>
			<th rowspan="2">Дата обр.</th>
			<th rowspan="2">Материал</th>
			<th colspan="6">Клещ</th>
			<!--th rowspan="2">Вид</th>
			<th rowspan="2">Пол</th>
			<th rowspan="2">Состояние</th-->
			<th rowspan="2">АГ КЭ</th>
			<th colspan="6">Кровь</th>
			<th colspan="6">Ig</th>
		</tr>
		<tr align="center">
			<th>КЭ</th>
			<th>КБ</th>
			<th>МЭЧ</th>
			<th>ГАЧ</th>
			<th>КР</th>
			<th>КВЛ</th>

			<th>КЭ</th>
			<th>КБ</th>
			<th>МЭЧ</th>
			<th>ГАЧ</th>
			<th>КР</th>
			<th>КВЛ</th>

			<th>M КЭ</th>
			<th>G КЭ</th>
			<th>Титр G КЭ</th>
			<th>M КБ</th>
			<th>G КБ</th>
			<th>Титр G КБ</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($results as $item)
		<tr align="center">
			<th>
				{{ $item->admission->year_id }}-Г<br>
				{{ $item->admission->id }}
			</th>
			<td><a href="/admissions/{{ $item->admission->id }}/edit#results" target="_blank">{{ $item->admission->name }}</a></td>
			<td>{{ Carbon\Carbon::parse($item->admission->date)->format('d/m') }}</td>
			<td>{{ $item->admission->material ? $item->admission->material->name : '' }}</td>

			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_vte))
					<b>{{ $tickResult->tick_pcr_vte == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_tb))
					<b>{{ $tickResult->tick_pcr_tb == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_hme))
					<b>{{ $tickResult->tick_pcr_hme == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_hga))
					<b>{{ $tickResult->tick_pcr_hga == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_tr))
					<b>{{ $tickResult->tick_pcr_tr == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
					@if (!is_null($tickResult->tick_pcr_trf))
					<b>{{ $tickResult->tick_pcr_trf == '1' ? 'пол' : 'отр' }}</b><br>
					@endif
					@endforeach
				@endif
			</td>

			<!--td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
						@if($tickResult->tick_type)
						{{ explode(" ", $tickResult->tick_type->name)[0] }}<br>
						@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
						@if($tickResult->tick_gender)
							{{ $tickResult->tick_gender->name }}<br>
						@endif
					@endforeach
				@endif
			</td>
			<td>
				@if ($item->admission->tick_results)
					@foreach($item->admission->tick_results as $tickResult)
						@if($tickResult->tick_condition)
							{{ $tickResult->tick_condition->name }}<br>
						@endif
					@endforeach
				@endif
			</td-->	

			<td>
				@if (!is_null($item->antigen_te))
					<b>{{ $item->antigen_te == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>

			<td>
				@if (!is_null($item->blood_pcr_vte))
					<b>{{ $item->blood_pcr_vte == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->blood_pcr_tb))
					<b>{{ $item->blood_pcr_tb == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->blood_pcr_hme))
					<b>{{ $item->blood_pcr_hme == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->blood_pcr_hga))
					<b>{{ $item->blood_pcr_hga == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->blood_pcr_tr))
					<b>{{ $item->blood_pcr_tr == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->blood_pcr_trf))
					<b>{{ $item->blood_pcr_trf == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>

			<td>
				@if (!is_null($item->ig_m_te))
					<b>{{ $item->ig_m_te == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->ig_g_te))
					<b>{{ $item->ig_g_te == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>{{ $item->titr_ig_g_te }}</td>
			<td>
				@if (!is_null($item->ig_m_tb))
					<b>{{ $item->ig_m_tb == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>
				@if (!is_null($item->ig_g_tb))
					<b>{{ $item->ig_g_tb == '1' ? 'пол' : 'отр' }}</b>
				@endif
			</td>
			<td>{{ $item->titr_ig_g_tb }}</td>
		</tr>
		@endforeach
	</tbody>
</table>
@else
<div>За выбранный день нет результатов для отображения</div>
@endif

@endsection