@extends ('layouts.master', ['title' => 'Отчёты'])

@section ('content')

<br>
<h2>тестовый отчет</h2>

<form action="/results/testReport" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-2 date-block">
			<label for="date_from">От</label>
			<input type="date" class="form-control" id="date_from" name="date_from" required>
		</div>
		<div class="form-group col-md-2 date-block">
			<label for="date_to">До</label>
			<input type="date" class="form-control" id="date_to" name="date_to" required>
		</div>
		
	</div>

	<button type="submit" class="btn btn-primary">Сформировать</button>
</form>

<script type="text/javascript" src="/js/report.js"></script>

@endsection