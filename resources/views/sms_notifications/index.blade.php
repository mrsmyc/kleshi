@extends ('layouts.master', ['title' => 'Отправка уведомлений'])

@section ('content')

<br>
<h2>Отправка sms-уведомлений</h2>

<form action="/sms_notifications/send" method="GET">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-6">
			Баланс: {{ $balance }} рублей
		</div>
		<div class="form-group col-md-6">
			Уведомлений к отправке: {{ $notificationsCount }}
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Отправить</button>
</form>

@endsection