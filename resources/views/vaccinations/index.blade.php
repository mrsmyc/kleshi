@extends ('layouts.master', ['title' => 'Вакцинации пациентов'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Вакцинация пациентов</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccinations/create">Добавить</a></div>
</div>
<br>

<form action="/vaccinations/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-1">
			<label for="id">Номер</label>
			<input type="text" class="form-control" id="id" name="id" value="{{ (isset($filter)) ? $filter['id'] : ''}}" placeholder="ID">
		</div>
		<div class="form-group col-md-2">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="ФИО">
		</div>
		<div class="form-group col-md-2">
			<label for="date_from">Дата (от)</label>
			<input type="date" class="form-control" id="date_from" name="date_from" value="{{ (isset($filter)) ? $filter['date_from'] : ''}}" placeholder="Дата (от)">
		</div>
		<div class="form-group col-md-2">
			<label for="date_to">Дата (до)</label>
			<input type="date" class="form-control" id="date_to" name="date_to" value="{{ (isset($filter)) ? $filter['date_to'] : ''}}" placeholder="Дата (до)">
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine">Вакцина</label>
			<select id="vaccine" class="form-control" name="vaccine_id">
				<option value=""></option>
				@foreach ($vaccines as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['vaccine_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_level">Уровень</label>
			<select id="vaccine_level" class="form-control" name="vaccine_level_id">
				<option value=""></option>
				@foreach ($vaccineLevels as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['vaccine_level_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<button type="submit" class="btn btn-primary mt-3">Найти</button>
	</div>
</form>
<p>Всего: {{ $total }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>ФИО</th>
			<th>Кратность</th>
			<th>Возраст</th>
			<th>Дата обращения</th>
			<th>Телефон</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($vaccinations as $vac)
		<tr>
			<th>{{ $vac->id }}</th>
			<td>{{ $vac->name }}</td>
			<td>{{ $vac->vaccine_level->name }}</td>
			<td>{{ $vac->age }}</td>
			<td>{{ Carbon\Carbon::parse($vac->date)->format('d/m/Y') }}</td>
			<td>{{ $vac->phone }}</td>
			<td><a href="/vaccinations/{{ $vac->id }}/edit">Просмотр</a></td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $vaccinations->links() }}</div>

@endsection