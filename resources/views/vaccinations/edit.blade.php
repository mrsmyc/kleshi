@extends ('layouts.master', ['title' => 'Вакцинация'])

@section ('content')

<br>

<div class="row">
	<h2 class="col-md-8">Карточка вакцинации</h2>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccinations/continue/{{ $vaccination->id }}">Копировать</a></div>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccinations/{{ $vaccination->id }}/print" target="_blank">Распечатать</a></div>
</div>

<form action="/vaccinations/{{ $vaccination->id }}" method="POST">
	<div class="row">
		{{ csrf_field() }}
		{{ method_field('PATCH') }}
		<input type="hidden" class="form-control" id="vaccination_id" value="{{ $vaccination->id }}">
		<div class="form-group col-md-2">
			<label for="date">Дата</label>
			<input type="date" class="form-control" id="date" value="{{ ($vaccination->date) ? Carbon\Carbon::parse($vaccination->date)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="date">
		</div>
		<div class="form-group col-md-2">
			<label for="payment_type">Вид оплаты</label>
			<select id="payment_type" class="form-control" name="payment_type_id" required>
				<option value=""></option>
				@foreach ($paymentTypes as $item)
				<option value="{{ $item->id }}" {{ ($vaccination->payment_type_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="responsible">Ответственный</label>
			<select id="responsible" class="form-control" name="responsible_id" required>
				@foreach ($managers as $item)
					<option value="{{ $item->id }}" {{ ( $vaccination->responsible_id == $item->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		
		<div class="col-md-12 section-title">
			<hr>
			<span>Личные данные</span>
		</div>

		<div class="form-group col-md-5">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" value="{{ $vaccination->name }}" name="name">
		</div>
		<div class="form-group col-md-3">
			<label for="phone">Телефон</label>
			<input type="text" class="form-control" id="phone" value="{{ $vaccination->phone }}" name="phone">
		</div>
		<div class="form-group col-md-2">
			<label for="birth_date">Дата рождения</label>
			<input type="date" min="0" class="form-control" id="birth_date" value="{{ $vaccination->birth_date }}"  name="birth_date">
		</div>
		<div class="form-group col-md-2">
			<label for="age">Возраст</label>
			<input type="number" min="0" class="form-control" id="age" value="{{ $vaccination->age }}" name="age">
		</div>
		<div class="form-group col-md-3">
			<label for="address">Домашний адрес</label>
			<input type="text" class="form-control" id="address" value="{{ $vaccination->address }}" name="address">
		</div>
		<div class="form-group col-md-2">
			<label for="organized">Организован</label>
			<select id="organized" class="form-control" name="organized">
				<option value="1" {{ ($vaccination->organized == 1) ? 'selected' : ''}}>Да</option>
				<option value="0" {{ ($vaccination->organized == 0) ? 'selected' : ''}}>Нет</option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="occupation_place">Место работы/учёбы/сад</label>
			<input type="text" class="form-control" id="occupation_place" value="{{ $vaccination->occupation_place }}" name="occupation_place">
		</div>
		<div class="form-group col-md-3">
			<label for="social_group">Группа</label>
			<select class="selectpicker" name="group_id" required>
				<option value=""></option>
				<optgroup label="Проф контингенты">
					@foreach ($profContingents as $item)
					<option value="{{ $item->id }}" {{ ($item->id == $vaccination->group_id) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Группы риска">
					@foreach ($riskGroups as $item)
					<option value="{{ $item->id }}" {{ ($item->id == $vaccination->group_id) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Соц.группы">
					@foreach ($socialGroups as $item)
					<option value="{{ $item->id }}" {{ ($item->id == $vaccination->group_id) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные вакцинации</span>
		</div>

		<div class="form-group col-md-4">
			<label for="vaccine">Вакцина</label>
			<select id="vaccine" class="form-control" name="vaccine_id">
				<option value=""></option>
				@foreach ($vaccines as $item)
				<option value="{{ $item->id }}" {{ ($vaccination->vaccine_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4" {{ ($vaccination->vaccine_id == '1') ? 'style=display:none' : ''}} id="our_series_block">
			<label for="vaccine_serie">Серия вакцины</label>
			<select id="vaccine_serie" class="form-control" name="vaccine_serie_id">
				<option value=""></option>
				@foreach ($vaccineSeries as $item)
				<option value="{{ $item->id }}" {{ ($item->id == $vaccination->vaccine_serie_id) ? 'selected' : '' }}>{{ $item->name }} до {{ Carbon\Carbon::parse($item->valid_to)->format('d/m/Y') }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4" id="own_vaccine_block" {{ ($vaccination->vaccine_id != '1') ? 'style=display:none' : ''}}>
			<label for="own_vaccine_info">Название, серия, срок</label>
			<input type="text" class="form-control" id="own_vaccine_info" name="own_vaccine_info" value="{{ $vaccination->own_vaccine_info }}">
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_spot">Место введения</label>
			<select id="vaccine_spot" class="form-control" name="vaccine_spot_id">
				@foreach ($vaccineSpots as $item)
				<option value="{{ $item->id }}" {{ ($vaccination->vaccine_spot_id == $item->id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_level">Кратность</label>
			<select id="vaccine_level" class="form-control" name="vaccine_level_id">
				<option value=""></option>
				@foreach ($vaccineLevels as $item)
				<option value="{{ $item->id }}" {{ ($item->id == $vaccination->vaccine_level_id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-10"></div>
		<div class="form-group col-md-12">
			<label for="comment">Примечание</label>
			<input type="text" class="form-control" id="comment" value="{{ $vaccination->comment }}" name="comment">
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 500px;"></div>

<script type="text/javascript" src="/js/vaccinations.js"></script>

@endsection