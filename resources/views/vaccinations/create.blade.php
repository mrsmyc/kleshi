@extends ('layouts.master', ['title' => 'Новый пациент'])

@section ('content')

<br>
<h2>Регистрация вакцинации</h2>

@if (!is_null($agreement) & empty($vaccination->id))

<!-- Modal -->
<div class="modal fade" id="vaccination_search" tabindex="-1" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form class="modal-content" id="vaccination_search_form" action="/vaccinations/create/{{ $agreement->id }}/" method="GET">
			<div class="modal-header">
				<h5 class="modal-title">Пациент обращался раньше?</h5>
				<button type="button" class="close" id="admission_entry_close" data-dismiss="modal" aria-label="Close">
		        	<span aria-hidden="true">&times;</span>
		        </button>
			</div>
			<div class="modal-body">
				<div class="form-group col-md-12">
					<input id="name_search" class="form-control" type="text" name="vaccination_name" placeholder="Начните вводить фамилию">
				</div>
				<div class="form-group col-md-12">
					<select id="vaccination_list" class="form-control" name="vaccination_id" size="6" required>
						<option disabled>Начните вводить имя</option>
					</select>
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-secondary" data-dismiss="modal">Нет</button>
				<button class="btn btn-primary">Да</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

	function formatDate(dateVal) {
		date = new Date(dateVal);
		var dd = date.getDate();
		if (dd < 10) dd = '0' + dd;

		var mm = date.getMonth() + 1;
		if (mm < 10) mm = '0' + mm;

		var yy = date.getFullYear() % 100;
		if (yy < 10) yy = '0' + yy;

		return dd + '.' + mm + '.' + yy;
	}

	$("#name_search").keyup(function() {
		//console.log($(this).val());
	    if ($(this).val()) {
	    	$.ajax
	        ({
	            type: "POST",
	            url: '/vaccinations/get_vaccination_list',
	            //dataType: 'json',
	            data: '_token=' + $("input[name=_token]").val() + '&name=' + $("#name_search").val(),
	            success: function (result) {
	            	//console.log(result.names);
	            	var options = '';
	            	$.each(result.names, function(key, value) {
					    options += "<option value='" + value.id +"'>" + value.name + ', ' + formatDate(value.date) + "</option>";
					});
					$('#vaccination_list').empty();
					$("#vaccination_list").append(options);
	            }
	        });
	    } else {
	    	$('#vaccination_list').empty();
	    	var options = '';
	    	options += "<option value='' disabled>Начните вводить фамилию</option>";
	    	$("#vaccination_list").append(options);
	    }
	});


	$('#vaccination_search').modal('toggle')

	var form = $('#vaccination_search_form'),
		input = $('#vaccination_list');

	form.submit(function(event){
		event.preventDefault();
		location.href = form.attr('action') + input.val();
	});
</script>

@endif 

<form action="/vaccinations" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-2">
			<label for="date">Дата</label>
			<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::now()->format('Y-m-d') }}" max="2100-12-31" min="2010-01-01" name="date">
		</div>
		<div class="form-group col-md-2">
			<label for="payment_type">Вид оплаты</label>
			<select id="payment_type" class="form-control" name="payment_type_id" required>
				<option value=""></option>
				@foreach ($paymentTypes as $item)
				<option value="{{ $item->id }}" {{ ($agreement && $item->id == $agreement->payment_type_id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="responsible">Ответственный</label>
			<select id="responsible" class="form-control" name="responsible_id" required>
				@foreach ($managers as $item)
				<option value="{{ $item->id }}" {{ ( $item->id == auth()->user()->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		@if ($agreement)
		<input type="hidden" name="vaccination_agreement_id" value="{{ $agreement->id }}">
		@endif
		
		<div class="col-md-12 section-title">
			<hr>
			<span>Личные данные</span>
		</div>

		@if ($agreement)
		<div class="col-md-12">
			<div class="vaccination-agreement-note">
				<img class="modal-icon" src="/img/warning.svg">Внимание! Данная вакцинация относится к договору {{ $agreement->company_name }}
			</div>
		</div>
		@endif

		<div class="form-group col-md-5">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ $vaccination ? $vaccination->name : '' }}" required>
		</div>
		<div class="form-group col-md-3">
			<label for="phone">Телефон</label>
			<input type="text" class="form-control" id="phone" name="phone" value="{{ $vaccination ? $vaccination->phone : '' }}">
		</div>
		<div class="form-group col-md-2">
			<label for="birth_date">Дата рождения</label>
			<input type="date" min="0" class="form-control" id="birth_date" name="birth_date" value="{{ $vaccination ? $vaccination->birth_date : '' }}" required>
		</div>
		<div class="form-group col-md-2">
			<label for="age">Возраст</label>
			<input type="number" min="0" class="form-control" id="age" name="age" value="{{ $vaccination ? Carbon\Carbon::parse($vaccination->birth_date)->age : '' }}" required>
		</div>
		<div class="form-group col-md-3">
			<label for="address">Домашний адрес</label>
			<input type="text" class="form-control" id="address" name="address" value="{{ $vaccination ? $vaccination->address : 'Иркутск' }}">
		</div>
		<div class="form-group col-md-2">
			<label for="organized">Организован</label>
			<select id="organized" class="form-control" name="organized">
				<option value="0">Нет</option>
				<option value="1" {{ ($agreement) ? 'selected' : ''}}>Да</option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="occupation_place">Место работы/учёбы/сад</label>
			<input type="text" class="form-control" id="occupation_place" value="{{ ($agreement) ? $agreement->company_name : '' }}" name="occupation_place" {{ ($agreement) ? 'disabled' : '' }}>
		</div>
		<div class="form-group col-md-3">
			<label for="social_group">Группа</label>
			<select class="selectpicker" name="group_id" required>
				<option value=""></option>
				<optgroup label="Проф контингенты">
					@foreach ($profContingents as $item)
					<option value="{{ $item->id }}" {{ ($agreement && ($item->id == $agreement->group_id)) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Группы риска">
					@foreach ($riskGroups as $item)
					<option value="{{ $item->id }}" {{ ($agreement && ($item->id == $agreement->group_id)) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
				<optgroup label="Соц.группы">
					@foreach ($socialGroups as $item)
					<option value="{{ $item->id }}" {{ ($agreement && ($item->id == $agreement->group_id)) ? 'selected' : ''}}>{{ $item->name }}</option>
					@endforeach
				</optgroup>
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные вакцинации</span>
		</div>

		<div class="form-group col-md-4">
			<label for="vaccine">Вакцина</label>
			<select id="vaccine" class="form-control" name="vaccine_id" required>
				<option value=""></option>
				@foreach ($vaccines as $item)
				<option value="{{ $item->id }}" {{ ($agreement && $item->id == $agreement->vaccine_id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4" id="our_series_block">
			<label for="vaccine_serie">Серия вакцины</label>
			<select id="vaccine_serie" class="form-control" name="vaccine_serie_id" required>
				<option value=""></option>
				@if ($agreement)
				@foreach ($vaccineSeries as $item)
				<option value="{{ $item->id }}" {{ ($agreement && $item->id == $agreement->vaccine_serie_id) ? 'selected' : '' }}>{{ $item->name }} до {{ Carbon\Carbon::parse($item->valid_to)->format('d/m/Y') }}</option>
				@endforeach
				@endif
			</select>
		</div>
		<div class="form-group col-md-4" id="own_vaccine_block" style="display: none">
			<label for="own_vaccine_info">Название, серия, срок</label>
			<input type="text" class="form-control" id="own_vaccine_info" name="own_vaccine_info">
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_spot">Место введения</label>
			<select id="vaccine_spot" class="form-control" name="vaccine_spot_id" required>
				<option value=""></option>
				@foreach ($vaccineSpots as $item)
				<option value="{{ $item->id }}" {{ ($agreement && $item->id == $agreement->vaccine_spot_id) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_level">Кратность</label>
			<select id="vaccine_level" class="form-control" name="vaccine_level_id" required>
				<option value=""></option>
				@foreach ($vaccineLevels as $item)
				<option value="{{ $item->id }}" {{ ($vaccination && ($item->id == $vaccination->vaccine_level_id + 1)) ? 'selected' : '' }} {{ ($vaccination && ($vaccination->vaccine_level->name == 'RV') && ($item->id == $vaccination->vaccine_level_id)) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-md-6"></div>
		<div class="form-group col-md-12">
			<label for="comment">Примечание</label>
			<input type="text" class="form-control" id="comment" name="comment">
		</div>
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 500px;"></div>

<script type="text/javascript" src="/js/vaccinations.js"></script>

@endsection