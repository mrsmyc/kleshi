@extends ('layouts.master', ['title' => 'Новая серия вакцины'])

@section ('content')

<br>
<h2>Новая серия вакцины "{{ $vaccine->name }}"</h2>

@include('layouts.errors')

<form action="/vaccine_series" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="col-md-12 section-title">
			<hr>
			<span>Общие данные</span>
		</div>
		<input type="hidden" name="vaccine_id" value="{{ $vaccine->id }}">
		<div class="form-group col-md-4">
			<label for="name">Номер серии</label>
			<input type="text" class="form-control" id="name" name="name" required>
		</div>
		<div class="form-group col-md-4">
			<label for="valid_to">Годна до</label>
			<input type="date" class="form-control" id="valid_to" name="valid_to" required>
		</div>
		<div class="form-group col-md-4">
			<label for="enabled">В наличии</label>
			<select id="enabled" class="form-control" name="enabled" required>
				<option value="1">Да</option>
				<option value="0">Нет</option>
			</select>
		</div>
		
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 500px;"></div>

@endsection