@extends ('layouts.master', ['title' => 'Импорт'])

@section ('content')

<br>
<h2>Импорт</h2>

<form action="/import" method="POST" enctype="multipart/form-data">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-4">
			<label for="import_file">Файл</label>
			<input type="file" class="form-control-file" id="import_file" name="import_file" required>
		</div>
		<div class="form-group col-md-4">
			<label for="import_type">Тип</label>
			<select id="import_type" class="form-control" name="import_type" required>
				<option value="admissions">Первичные приёмы</option>
				<option value="antibodies">Антитела</option>
				<option value="old_antibodies">Старые укусы</option>
				<option value="vaccinations">Вакцинации</option>
				<option value="update_pmz">Исправить ПМЖ</option>
				<option value="update_bite_spot">Исправить место укуса</option>
			</select>
		</div>
	</div>

	<button type="submit" class="btn btn-primary" disabled>Импортировать</button>
</form>

@endsection