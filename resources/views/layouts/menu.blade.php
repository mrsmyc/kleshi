<!--nav class="navbar sticky-top navbar-expand-lg navbar-light" style="background-color: #e3f2fd;"-->
<nav class="navbar sticky-top navbar-expand-lg navbar-light">
    <div class="container">
        <a class="navbar-brand" href="/">CRM</a>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/admissions">Укусы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/antibodies">Антитела</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/old_antibodies">Давние укусы</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/vaccinations">Вакцинации</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/vaccination_agreements">Договоры на вакцинацию</a>
                </li>
                <div class="dropdown open">
                    <button class="nav-link dropdown-toggle" type="button" id="dropdown-down-results" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; background:none;">
                    Результаты
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdown-down-results">
                        <a class="dropdown-item" href="/results/admissions">ПП результаты</a>
                        <a class="dropdown-item" href="/results/antibodies">АТ результаты</a>
                    </div>
                </div>
                @if (auth()->check())
                   @if (auth()->user()->hasRole('laboratory_worker'))
                <li class="nav-item">
                    <a class="nav-link" href="/export">Экспорт</a>
                </li>
                    @endif
                @endif
                <!--li class="nav-item">
                    <a class="nav-link" href="/administrating">Администрирование</a>
                </li-->
                @if (auth()->check())
                   @if (auth()->user()->hasRole('admin'))
                <div class="dropdown open">
                    <button class="nav-link dropdown-toggle" type="button" id="dropdown-down-admin" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="border:none; background:none;">
                    Администрирование
                    </button>
                    <div class="dropdown-menu" aria-labelledby="dropdown-down-admin">
                        <a class="dropdown-item" href="/insurance_companies">Страховые компании</a>
                        <a class="dropdown-item" href="/prices">Цены</a>
                        <!--a class="dropdown-item" href="/import">Импорт</a-->
                        <a class="dropdown-item" href="/export">Экспорт</a>
                        <a class="dropdown-item" href="/reports">Отчёты</a>
                        <a class="dropdown-item" href="/users">Пользователи</a>
                        <a class="dropdown-item" href="/vaccines">Вакцины</a>
                        <a class="dropdown-item" href="/sms_notifications">Уведомления</a>
                        <a class="dropdown-item" href="results/testReport">ТестЭкспортПоСтраховым</a>
                    </div>
                </div>
                    @endif
                @endif
                <!--li class="nav-item">
                    <a class="nav-link" href="/insurance_companies">Страховые компании</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/prices">Цены</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/reports">Отчёты</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/import">Импорт</a>
                </li-->
            </ul>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
    </div>
</nav>

<script type="text/javascript">
    // display checked menu element
    $(document).ready(function() {
        $('a[href="' + location.pathname + '"]').closest('li').addClass('active'); 
    });

    // activate color bg
    $(function () {
        $(document).scroll(function () {
            var $nav = $(".sticky-top");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
        });
    });
</script>