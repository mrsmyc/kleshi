<!DOCTYPE html>
<html>
<head>
	<meta name="theme-color" content="#ffffff">
	<title>Клещевой центр</title>
	<link rel="stylesheet" type="text/css" href="/css/print.css">
	<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
</head>
<body>
	@yield ('content')
</body>
</html>