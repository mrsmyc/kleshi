<!DOCTYPE html>
<html>
<head>
	<meta name="theme-color" content="#ffffff">
	<title>Клещевой центр</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.0/css/bootstrap.min.css" integrity="sha384-PDle/QlgIONtM1aqA2Qemk5gPOE7wFq8+Em+G/hmo5Iq0CCmYZLv3fVRDJ4MMwEA" crossorigin="anonymous">
	<link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
</head>
<body>

	@if ($flash = session('message'))

		<div id="flash-message" class="alert alert-success" role="alert">

		{{ $flash }}

		</div>

	@endif

	@yield ('content')
</body>
</html>