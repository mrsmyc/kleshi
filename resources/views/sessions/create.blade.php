@extends ('layouts.plain')

@section ('content')
<div class="container col-lg-4 col-md-8" style="margin-top: 15%;">
	<div class="card shadow-sm">
		<div class="card-header">Вход</div>
		<form class="card-body shadow-sm" method="post" action="/login">
			{{ csrf_field() }}
			<div class="form-group">
				<label>Логин</label>
				<input class="form-control" type="text" name="login">
			</div>
			<div class="form-group">
				<label>Пароль</label>
				<input class="form-control" type="password" name="password">
			</div>
			<div class="form-group">
				<button class="btn btn-lg btn-block btn-primary">Войти</button>
			</div>
			@include ('layouts.errors')
		</form>
	</div>
</div>
@endsection