@extends ('layouts.master', ['title' => 'Вакцины'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Вакцины</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccines/create">Добавить</a></div>
</div>
<br>

<form action="/vaccines/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-4">
			<label for="name">Название вакцины</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="Название">
		</div>

		<button type="submit" class="btn btn-primary mt-3">Найти</button>
	</div>
</form>

<table class="table table-striped">
	<thead>
		<tr>
			<th>Название вакцины</th>
			<th>Количество актуальных серий</th>
			<th>Статус</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($vaccines as $item)
		<tr>
			<td>{{ $item->name }}</td>
			<td>{{ $item->series->where('enabled')->count() }}</td>
			<td>{{ ($item->enabled) ? 'Используется' : 'Не используется'}}</td>
			<td>
				<a href="/vaccines/{{ $item->id }}/edit">Просмотр</a><br>
				<a href="/vaccines/{{ $item->id }}/edit#vaccine-series">Серии</a><br>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $vaccines->links() }}</div>

@endsection