@extends ('layouts.master', ['title' => 'Редактирование вакцины'])

@section ('content')

<br>
<h2>Вакцина "{{ $vaccine->name }}"</h2>
<hr>

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#vaccine-info">Вакцина</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#vaccine-series">Серии вакцины</a>
	</li>
</ul>
<br>

@include('layouts.errors')

<div class="tab-content">
	<div class="tab-pane active" id="vaccine-info">
		<form action="/vaccines/{{ $vaccine->id }}" method="POST">
			<div class="row">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group col-md-4">
					<label for="name">Название</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ $vaccine->name }}" required>
				</div>
				<div class="form-group col-md-4">
					<label for="price_id">Соответствие в прайсе</label>
					<select id="price_id" class="form-control" name="price_id">
						<option value=""></option>
						@foreach ($price as $element)
						<option value="{{ $element->id }}" {{ ($vaccine->price_id == $element->id) ? 'selected' : '' }}>{{ $element->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-4">
					<label for="enabled">Используется</label>
					<select id="enabled" class="form-control" name="enabled" required>
						<option value="1" {{ ($vaccine->enabled == '1') ? 'selected' : '' }}>Да</option>
						<option value="0" {{ ($vaccine->enabled == '0') ? 'selected' : '' }}>Нет</option>
					</select>
				</div>
				
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>
	</div>
	<div class="tab-pane" id="vaccine-series">
		<div class="row">
			<div class="col-md-10 section-title">
				<span>Серии вакцины "{{ $vaccine->name }}"</span>
			</div>
			<div class="col-md-2"><a class="btn btn-primary float-right" href="/vaccine_series/{{ $vaccine->id }}/create">Добавить</a></div>
		</div>
		<table class="table table-striped">
			<thead>
				<tr>
					<th>Серия</th>
					<th>В наличии</th>
					<th>Действия</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($vaccineSeries as $serie)
				<form>
				<tr>
					<td>{{ $serie->name }}</td>
					<td>{{ ($serie->enabled) ? 'Да' : 'Нет'}}</td>
					<td><a href="/vaccine_series/{{ $serie->id }}/edit">Просмотр</a></td>
				</tr>
				<form>
				@endforeach
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		var hash = window.location.hash;
		console.log(hash);
		hash && $('ul.nav a[href="' + hash + '"]').click();
	});
</script>

<div style="height: 500px;"></div>

@endsection