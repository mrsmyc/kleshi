@extends ('layouts.master', ['title' => 'Новая вакцина'])

@section ('content')

<br>
<h2>Новая вакцина</h2>

@include('layouts.errors')

<form action="/vaccines/" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="col-md-12 section-title">
			<hr>
			<span>Общие данные</span>
		</div>
		<div class="form-group col-md-4">
			<label for="name">Название</label>
			<input type="text" class="form-control" id="name" name="name" required>
		</div>
		<div class="form-group col-md-4">
			<label for="price_id">Соответствие в прайсе</label>
			<select id="price_id" class="form-control" name="price_id">
				<option value=""></option>
				@foreach ($price as $element)
				<option value="{{ $element->id }}" {{ ($vaccine->price_id == $element->id) ? 'selected' : '' }}>{{ $element->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="enabled">Используется</label>
			<select id="enabled" class="form-control" name="enabled" required>
				<option value="1">Да</option>
				<option value="0">Нет</option>
			</select>
		</div>
		
	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 500px;"></div>

@endsection