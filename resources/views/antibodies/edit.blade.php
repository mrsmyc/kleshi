@extends ('layouts.master', ['title' => 'Антитела'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-8">Антитела
		@if ($antibody->year_id)
			<b>№{{ $antibody->year_id }}</b> (№{{ $antibody->id }})
		@else
			№{{ $antibody->id }}
		@endif
	</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/admissions/{{ $antibody->admission->id }}/edit">Перв. приём</a></div>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/antibodies/{{ $antibody->id }}/print" target="_blank">Распечатать</a></div>
</div>
<br>

<ul class="nav nav-tabs">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#patient-card">Карточка пациента</a>
	</li>
	<li class="nav-item">
		<a class="nav-link" data-toggle="tab" href="#results">Результаты исследования</a>
	</li>
</ul>
<br>

<div class="tab-content">
	<div class="tab-pane active" id="patient-card">
		<form action="/antibodies/{{ $antibody->id}}" method="POST">
			<div class="row">
				{{ csrf_field() }}
				{{ method_field('PATCH') }}
				<div class="form-group col-md-1 extra-width">
					<label for="year_id">№-Г</label>
					<input type="text" class="form-control" id="year_id" value="{{ ($antibody->year_id) ? $antibody->year_id . '-Г' : '' }}" disabled>
				</div>
				<div class="form-group col-md-1 extra-width">
					<label for="id">№</label>
					<input type="number" class="form-control" id="id" value="{{ $antibody->id }}" disabled>
				</div>
				<div class="form-group col-md-2">
					<label for="date">Дата</label>
					<input type="date" class="form-control" id="date" value="{{ ($antibody->date) ? Carbon\Carbon::parse($antibody->date)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="date">
				</div>
				<div class="form-group col-md-2">
					<label for="admission">П/п №</label>
					<input type="number" class="form-control" id="admission_id" value="{{ $antibody->admission->id }}" disabled>
				</div>
				<div class="form-group col-md-2">
					<label for="bite_date">Дата укуса</label>
					<input type="date" class="form-control" id="bite_date" max="2100-12-31" min="2010-01-01" name="bite_date" value="{{ Carbon\Carbon::parse($antibody->admission->bite_date)->format('Y-m-d') }}" disabled>
				</div>
				<div class="form-group col-md-1">
					<label for="vaccine_level">Вакцинация</label>
					<select id="vaccine_level" class="form-control" name="vaccine_level_id" disabled>
						<option value=""></option>
						@foreach ($vaccineLevels as $item)
						<option value="{{ $item->id }}" {{ $item->id == $antibody->admission->vaccine_level_id ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="responsible">Ответственный*</label>
					<select id="responsible" class="form-control" name="responsible_id" required>
						@foreach ($managers as $item)
							<option value="{{ $item->id }}" {{ ( $antibody->responsible_id == $item->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				
				<div class="col-md-12 section-title">
					<hr>
					<span>Личные данные</span>
				</div>

				<div class="form-group col-md-6">
					<label for="name">ФИО</label>
					<input type="text" class="form-control" id="name" value="{{ $antibody->name }}" name="name" required>
				</div>
				<div class="form-group col-md-2">
					<label for="age">Возраст</label>
					<input type="number" min="0" class="form-control" value="{{ $antibody->age }}" id="age" required>
				</div>
				<div class="form-group col-md-4">
					<label for="phone">Телефон</label>
					<input type="text" class="form-control" id="phone" value="{{ $antibody->phone }}"  name="phone" required>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Данные страховки</span>
				</div>

				<div class="form-group col-md-3">
					<label for="insurance_company">Страховая компания</label>
					<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
						<option value=""></option>
						@foreach ($insuranceCompanies as $item)
						<option value="{{ $item->id }}" {{ ( $antibody->insurance_company_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group col-md-3">
					<label for="policy_num">Номер полиса</label>
					<input type="text" class="form-control" id="policy_num" value="{{ $antibody->policy_num }}" name="policy_num">
				</div>
				<div class="form-group col-md-2">
					<label for="policy_start">Срок действия (от)</label>
					<input type="date" class="form-control" id="policy_start" value="{{ ($antibody->policy_start) ? Carbon\Carbon::parse($antibody->policy_start)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_start">
				</div>
				<div class="form-group col-md-2">
					<label for="policy_end">Срок действия (до)</label>
					<input type="date" class="form-control" id="policy_end" value="{{ ($antibody->policy_end) ? Carbon\Carbon::parse($antibody->policy_end)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_end">
				</div>
				<div class="form-group col-md-2">
					<label for="policy_scan">Скан полиса</label>
					<input type="file" class="form-control-file" id="policy_scan" name="policy_scan">
				</div>

				<input type="hidden" name="asko_ic_name" value="">
				<input type="hidden" name="asko_immunocard" value="">
				<div class="form-group col-md-6 {{ ($antibody->insurance_company && in_array($antibody->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_ic_name_block">
					<label for="asko_ic_name">Страховая компания (уточнение)</label>
					<input type="text" class="form-control" id="asko_ic_name" name="asko_ic_name" data-block="insurance_data" value="{{ $antibody->asko_ic_name }}">
				</div>
				<div class="form-group col-md-6 {{ ($antibody->insurance_company && in_array($antibody->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_immunocard_block">
					<label for="immunocard">Номер иммунокарты (или ГП)</label>
					<input type="text" class="form-control" id="asko_immunocard" name="asko_immunocard" data-block="insurance_data" value="{{ $antibody->asko_immunocard }}">
				</div>

				<div class="form-group col-md-8">
					<label for="comment">Дополнительная информация</label>
					<input type="text" class="form-control" id="comment" value="{{ $antibody->comment }}" name="comment">
				</div>
				<div class="form-group col-md-4">
					<label for="infect_consult">Консультация инфекциониста</label>
					<select id="infect_consult" class="form-control" name="infect_consult" required>
						<option value=""></option>
						<option value="0" {{ ( $antibody->infect_consult == 0 ) ? 'selected' : ''}}>Нет</option>
						<option value="1" {{ ( $antibody->infect_consult == 1 ) ? 'selected' : ''}}>Да</option>
					</select>
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Виды исследований</span>
				</div>

				@foreach ($researches as $item)
				<div class="form-group col-md-2">
					<div class="form-check">
						<input class="form-check-input research {{ ($item->id == '1' || $item->id == '2') ? 'research-te-tb' : ''}}{{ ($item->id == '3' || $item->id == '4') ? 'research-hga-hme' : ''}}" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}" {{ (in_array($item->id, $checkedResearches)) ? 'checked' : ''}}>
						<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
					</div>
				</div>
				@endforeach
			</div>

			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>
	</div>

	<div class="tab-pane" id="results">
		<table class="table table-striped table-dark" id="scroll-to-result">
			<thead>
				<tr>
					<th scope="col" width="13%">#</th>
					<th scope="col" width="25%">ФИО</th>
					<th scope="col">Пол</th>
					<th scope="col">Возраст</th>
					<th scope="col">Вакц</th>
					<th scope="col">Дата укуса</th>
					<th scope="col">Исследования</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><b>{{ $antibody->year_id }}-Г</b> ({{ $antibody->id }})<br>
						п/п: {{ $antibody->admission->id }}</td>
					<td>{{ $antibody->name }}</td>
					<td>{{ mb_substr($antibody->admission->gender->name, 0, 1) }}</td>
					<td>{{ $antibody->age }}</td>
					<td>{{ ($antibody->admission->vaccine_level) ? mb_substr($antibody->admission->vaccine_level->name, 0, 6) : ''}}</td>
					<td>{{ ($antibody->admission->bite_date) ? Carbon\Carbon::parse($antibody->admission->bite_date)->format('d/m/Y') : '' }}</td>
					<td>@foreach ($researches as $item)
						@if (in_array($item->id, $checkedResearches)) 
						<b>{{ $item->name }}</b> |
						@endif
					@endforeach</td>
				</tr>
			</tbody>
		</table>
		<form action="/antibodies/{{ $antibody->id }}/update_result" method="POST">
			{{ csrf_field() }}
			{{ method_field('PATCH') }}
			<!--div class="row" id="scroll-to-result">
				<div class="form-group col-md-2">
					<label for="">Номер: </label>
					<b>{{ $antibody->id }}</b>
				</div>
				<div class="form-group col-md-4">
					<label for="">ФИО: </label>
					<b>{{ $antibody->name }}</b>
				</div>
				<div class="form-group col-md-6">
					<label for="">Запрошенные исследования: </label>
					@foreach ($researches as $item)
						@if (in_array($item->id, $checkedResearches)) 
						<b>{{ $item->name }}</b> |
						@endif
					@endforeach
				</div>
			</div-->
			<div class="row">
				<div class="col-md-12 section-title">
					<hr>
					<span>Данные Кровь</span>
				</div>

				<div class="form-group col-md-2">
					<label for="blood_pcr_vte">Кровь ПЦР ВКЭ</label>
					<select id="blood_pcr_vte" class="form-control {{ ($result->blood_pcr_vte == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_vte == '1') ? 'positive' : '' }}" name="blood_pcr_vte">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_vte == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_vte == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tb">Кровь ПЦР КБ</label>
					<select id="blood_pcr_tb" class="form-control {{ ($result->blood_pcr_tb == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_tb == '1') ? 'positive' : '' }}" name="blood_pcr_tb">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hme">Кровь ПЦР МЭЧ</label>
					<select id="blood_pcr_hme" class="form-control {{ ($result->blood_pcr_hme == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_hme == '1') ? 'positive' : '' }}" name="blood_pcr_hme">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hme == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hme == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hga">Кровь ПЦР ГАЧ</label>
					<select id="blood_pcr_hga" class="form-control {{ ($result->blood_pcr_hga == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_hga == '1') ? 'positive' : '' }}" name="blood_pcr_hga">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_hga == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_hga == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_hrf">Кровь ПЦР КВЛ</label>
					<select id="blood_pcr_hrf" class="form-control {{ ($result->blood_pcr_trf == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_trf == '1') ? 'positive' : '' }}" name="blood_pcr_trf">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_trf == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_trf == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="blood_pcr_tr">Кровь ПЦР КР</label>
					<select id="blood_pcr_tr" class="form-control {{ ($result->blood_pcr_tr == '0') ? 'negative' : '' }} {{ ($result->blood_pcr_tr == '1') ? 'positive' : '' }}" name="blood_pcr_tr">
						<option value=""></option>
						<option value="0" {{ ($result->blood_pcr_tr == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->blood_pcr_tr == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>

				<div class="col-md-12 section-title">
					<span>Данные Ig</span>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_m_te">Ig M КЭ</label>
					<select id="ig_m_te" class="form-control {{ ($result->ig_m_te == '0') ? 'negative' : '' }} {{ ($result->ig_m_te == '1') ? 'positive' : '' }}" name="ig_m_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_te">Ig G КЭ</label>
					<select id="ig_g_te" class="form-control {{ ($result->ig_g_te == '0') ? 'negative' : '' }} {{ ($result->ig_g_te == '1') ? 'positive' : '' }}" name="ig_g_te">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_te == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_te == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_te">Титр Ig G КЭ</label>
					<input type="text" class="form-control" id="titr_ig_g_te" value="{{ $result->titr_ig_g_te }}" name="titr_ig_g_te">
				</div>
				<div class="form-group col-md-2">
					<label for="ig_m_tb">Ig M КБ</label>
					<select id="ig_m_tb" class="form-control {{ ($result->ig_m_tb == '0') ? 'negative' : '' }} {{ ($result->ig_m_tb == '1') ? 'positive' : '' }}" name="ig_m_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_m_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_m_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="ig_g_tb">Ig G КБ</label>
					<select id="ig_g_tb" class="form-control {{ ($result->ig_g_tb == '0') ? 'negative' : '' }} {{ ($result->ig_g_tb == '1') ? 'positive' : '' }}" name="ig_g_tb">
						<option value=""></option>
						<option value="0" {{ ($result->ig_g_tb == '0') ? 'selected' : ''}}>Отрицательный</option>
						<option value="1" {{ ($result->ig_g_tb == '1') ? 'selected' : ''}}>Положительный</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					<label for="titr_ig_g_tb">Титр Ig G КБ</label>
					<input type="text" class="form-control" id="titr_ig_g_tb" value="{{ $result->titr_ig_g_tb }}" name="titr_ig_g_tb">
				</div>

				<div class="col-md-12 section-title">
					<hr>
					<span>Другие данные</span>
				</div>

				<div class="form-group col-md-12">
					<label for="comment_result">Комментарий</label>
					<textarea class="form-control" id="comment_result" name="comment" rows="2">{{ $result->comment }}</textarea>
				</div>

				<div class="form-group col-md-3">
					<label for="ready_to_notify">Результат готов к отправке (SMS)*</label>
					<select id="ready_to_notify" class="form-control" name="ready_to_notify" {{ ($result->sms_notification && (in_array($result->sms_notification->status, ['sent', 'failed']))) ? 'disabled' : ''}} required="">
						<option value=""></option>
						<option value="1" {{ ($result->ready_to_notify == '1') ? 'selected' : ''}}>Готов</option>
						<option value="0" {{ ($result->ready_to_notify == '0') ? 'selected' : ''}}>НЕ готов</option>
					</select>
				</div>

				<div class="form-group col-md-3">
					<label for="issue_date">Дата выдачи {{ (!$result->issue_date) ? '(не сохранено)' : '' }}</label>
					<input type="date" class="form-control" id="issue_date" value="{{ ($result->issue_date) ? Carbon\Carbon::parse($result->issue_date)->format('Y-m-d') : Carbon\Carbon::now()->format('Y-m-d') }}" max="2100-12-31" min="2010-01-01" name="issue_date" required>
				</div>

				@if ($result->sms_notification)
				<table class="table table-dark" width="100%">
					<tr>
						<th width="60%">Текст уведомления</th>
						<th width="25%">Статус</th>
						<th width="15%">Дата отправки</th>
					</tr>
					<tr>
						<td>{{ $result->sms_notification->text }}</td>
						<td>{{ $smsStatuses[$result->sms_notification->status] }}</td>
						<td>{{ ($result->sms_notification->status == 'sent') ? Carbon\Carbon::parse($result->sms_notification->updated_at)->format('d.m.Y H:i') : ''}}</td>
					</tr>
				</table>
				@endif
			</div>
			<br>
			<button type="submit" class="btn btn-primary">Сохранить</button>
		</form>

		<div style="height: 200px;"></div>
	</div>
</div>

<script type="text/javascript" src="/js/antibody.js"></script>

@endsection