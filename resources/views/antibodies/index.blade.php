@extends ('layouts.master', ['title' => 'Регистрация крови на антитела'])

@section ('content')

<br>
<div class="row">
	<h3 class="col-md-10">Антитела</h3>
	<div class="col-md-2"><a class="btn btn-primary float-right" href="/antibodies/create">Добавить</a></div>
</div>
<br>

<form action="/antibodies/search" method="GET">
	<div class="form-row align-items-center">
		<div class="form-group col-md-1">
			<label for="year_id">№-Г</label>
			<input type="number" class="form-control" id="year_id" name="year_id" value="{{ (isset($filter)) ? $filter['year_id'] : ''}}" placeholder="ID-Г">
		</div>
		
		<div class="form-group col-md-3">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" name="name" value="{{ (isset($filter)) ? $filter['name'] : ''}}" placeholder="ФИО">
		</div>
		<div class="form-group col-md-2">
			<label for="date_from">Дата (от)</label>
			<input type="date" class="form-control" id="date_from" name="date_from" value="{{ (isset($filter)) ? $filter['date_from'] : ''}}" placeholder="Дата">
		</div>
		<div class="form-group col-md-2">
			<label for="date_to">Дата (до)</label>
			<input type="date" class="form-control" id="date_to" name="date_to" value="{{ (isset($filter)) ? $filter['date_to'] : ''}}" placeholder="Дата">
		</div>
		<div class="form-group col-md-2">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}" {{ (isset($filter) && ($filter['insurance_company_id'] == $item->id )) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-1">
			<label for="id">№</label>
			<input type="text" class="form-control" id="id" name="id" value="{{ (isset($filter)) ? $filter['id'] : ''}}" placeholder="ID">
		</div>
		<button type="submit" class="btn btn-primary mt-2">Найти</button>
	</div>
</form>
<p>Всего: {{ $total }}</p>

<table class="table table-striped">
	<thead>
		<tr>
			<th>#</th>
			<th>ФИО</th>
			<th>Возраст</th>
			<th>Дата обращения</th>
			<th>Телефон</th>
			<th>Действия</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($antibodies as $ant)
		<tr>
			<th width="7%">
				@if ($ant->year_id) 
				{{ $ant->year_id }}-Г <br>
				@endif
				{{ $ant->id }}
			</th>
			<td>{{ $ant->name }}</td>
			<td>{{ $ant->age }}</td>
			<td>{{ Carbon\Carbon::parse($ant->date)->format('d/m/Y') }}</td>
			<td>{{ $ant->phone }}</td>
			<td>
				<a href="/antibodies/{{ $ant->id }}/edit">Просмотр</a><br>
				<a href="/antibodies/{{ $ant->id }}/edit#results">Результаты</a>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

<div class="pagination-center">{{ $antibodies->links() }}</div>

@endsection