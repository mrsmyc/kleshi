@extends ('layouts.master', ['title' => 'Результаты исследований'])

@section ('content')

<div class="no-print">
<br>
<div class="row">
	<h3 class="col-md-10">Результаты исследований на антитела</h3>
</div>
	<br>

	<form action="/results/antibodies/filter" method="GET">
		<div class="form-row align-items-center">
			<div class="form-group col-md-2">
				<label for="date">Дата выдачи</label>
				<input type="date" class="form-control" id="date" name="date" value="{{ (isset($filter)) ? $filter['date'] : ''}}" placeholder="Дата" required>
			</div>
			<button type="submit" class="btn btn-primary mt-3">Найти</button>
		</div>
	</form>
</div>

@if ($results->count() > 0)
<table class="table table-striped results-check" border="1" style="position: absolute; left: 2.5vw; width: 95vw;">
	<thead>
		<tr align="center">
			<th rowspan="2">#</th>
			<th rowspan="2">ФИО</th>
			<th rowspan="2">Дата обр.</th>
			<th colspan="6">Кровь</th>
			<th colspan="6">Ig</th>
		</tr>
		<tr align="center">
			<th>КЭ</th>
			<th>КБ</th>
			<th>МЭЧ</th>
			<th>ГАЧ</th>
			<th>КР</th>
			<th>КВЛ</th>

			<th>M КЭ</th>
			<th>G КЭ</th>
			<th>Титр G КЭ</th>
			<th>M КБ</th>
			<th>G КБ</th>
			<th>Титр G КБ</th>
		</tr>
	</thead>
	<tbody>
			@foreach ($results as $item)
			<tr align="center">
				<th>{{ $item->antibody->year_id }}-Г<br>
					{{ $item->antibody->id }}
				</th>
				<td><a href="/antibodies/{{ $item->antibody->id }}/edit#results" target="_blank">{{ $item->antibody->name }}</a></td>
				<td>{{ Carbon\Carbon::parse($item->antibody->date)->format('d/m') }}</td>

				<td>
					@if (!is_null($item->blood_pcr_vte))
						{{ $item->blood_pcr_vte == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->blood_pcr_tb))
						{{ $item->blood_pcr_tb == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->blood_pcr_hme))
						{{ $item->blood_pcr_hme == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->blood_pcr_hga))
						{{ $item->blood_pcr_hga == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->blood_pcr_tr))
						{{ $item->blood_pcr_tr == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->blood_pcr_trf))
						{{ $item->blood_pcr_trf == '1' ? 'пол' : 'отр' }}
					@endif
				</td>

				<td>
					@if (!is_null($item->ig_m_te))
						{{ $item->ig_m_te == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->ig_g_te))
						{{ $item->ig_g_te == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>{{ $item->titr_ig_g_te }}</td>
				<td>
					@if (!is_null($item->ig_m_tb))
						{{ $item->ig_m_tb == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>
					@if (!is_null($item->ig_g_tb))
						{{ $item->ig_g_tb == '1' ? 'пол' : 'отр' }}
					@endif
				</td>
				<td>{{ $item->titr_ig_g_tb }}</td>
			</tr>
			@endforeach
	</tbody>
</table>
@else
<div>За выбранный день нет результатов для отображения</div>
@endif

@endsection