@extends ('layouts.master', ['title' => 'Новый пациент'])

@section ('content')

<br>
<h2>Регистрация крови на антитела</h2>

@if (empty($admission->id))

<!-- Modal -->
<div class="modal fade" id="admission_id_request" tabindex="-1" role="dialog" data-backdrop="static">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<form class="modal-content" id="antibody_admission_form" action="/antibodies/create/" method="GET">
			<div class="modal-header">
				<h5 class="modal-title">Введите номер п/п пациента</h5>
				<button type="button" class="close" id="admission_entry_close" data-dismiss="modal" aria-label="Close">
		        	<span aria-hidden="true">&times;</span>
		        </button>
				<!--button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button-->
			</div>
			<div class="modal-body">
				<input class="form-control" style="width:100%" type="number" min="0" id="antibody_admission_id" name="antibody_admission_id" placeholder="Регистрационный номер" required>
			</div>
			<div class="modal-footer">
				<a class="btn btn-secondary" href="/admissions/create">Первичный приём</a>
				<button class="btn btn-primary">Создать</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$('#admission_id_request').modal('toggle')

	var form = $('#antibody_admission_form'),
		input = $('#antibody_admission_id');

	form.submit(function(event){
		event.preventDefault();
		location.href = form.attr('action') + input.val();
	});
</script>

@endif 

<form action="/antibodies/{{ $admission->id }}" method="POST">
	<div class="row">
		{{ csrf_field() }}
		<div class="form-group col-md-1">
			<label for="year_id">№-Г</label>
			<input type="text" class="form-control" id="year_id" value="" disabled>
		</div>
		<div class="form-group col-md-1">
			<label for="id">№</label>
			<input type="number" class="form-control" id="id" value="" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="date">Дата</label>
			<input type="date" class="form-control" id="date" value="{{ Carbon\Carbon::today()->format('Y-m-d') }}" max="2100-12-31" min="2010-01-01" name="date">
		</div>
		<div class="form-group col-md-2">
			<label for="admission_id">П/п №</label>
			<input type="number" class="form-control" id="admission_id" value="{{ $admission->id }}" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="bite_date">Дата укуса</label>
			<input type="date" class="form-control" id="bite_date" max="2100-12-31" min="2010-01-01" name="bite_date" value="{{ Carbon\Carbon::parse($admission->date)->format('Y-m-d') }}" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="vaccine_level">Вакцинация</label>
			<select id="vaccine_level" class="form-control" name="vaccine_level_id" disabled>
				<option value=""></option>
				@foreach ($vaccineLevels as $item)
				<option value="{{ $item->id }}" {{ $item->id == $admission->vaccine_level_id ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-2">
			<label for="responsible">Ответственный</label>
			<select id="responsible" class="form-control" name="responsible_id" required>
				@foreach ($managers as $item)
				<option value="{{ $item->id }}" {{ ( $item->id == auth()->user()->id ) ? 'selected' : '' }}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Личные данные</span>
		</div>

		<div class="form-group col-md-6">
			<label for="name">ФИО</label>
			<input type="text" class="form-control" id="name" value="{{ $admission->name }}" name="name" required>
		</div>
		<div class="form-group col-md-2">
			<label for="age">Возраст</label>
			<input type="number" min="0" class="form-control" value="{{ $admission->age }}" id="age" name="age" required>
		</div>
		<div class="form-group col-md-4">
			<label for="phone">Телефон</label>
			<input type="text" class="form-control" id="phone" value="{{ $admission->phone }}" name="phone" required>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Данные страховки</span>
		</div>

		<!-- старая страховка -->

		@if ($admission->policy_end < Carbon\Carbon::today() && $admission->insurance_company && $admission->insurance_company->new_policy)

		<div class="col-md-12">Предыдущая страховка</div>

		<div class="form-group col-md-3">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id" disabled>
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}" {{ ( $admission->insurance_company_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="policy_num">Номер полиса</label>
			<input type="text" class="form-control" id="policy_num" value="{{ $admission->policy_num }}" name="policy_num" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="policy_start">Срок действия (от)</label>
			<input type="date" class="form-control" id="policy_start" value="{{ ($admission->policy_start) ? Carbon\Carbon::parse($admission->policy_start)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_start" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="policy_end">Срок действия (до)</label>
			<input type="date" class="form-control" id="policy_end" value="{{ ($admission->policy_end) ? Carbon\Carbon::parse($admission->policy_end)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_end" disabled>
		</div>
		<div class="form-group col-md-2">
			<label for="policy_scan">Скан полиса</label>
			@if ($admission->policy_scan)
			<div class="policy-view">
				<a data-fancybox="gallery" href="{{ Storage::url($admission->policy_scan) }}"><i class="fas fa-search"></i> Посмотреть</a>
			</div>
			@endif
		</div>

		<div class="col-md-12">Новая страховка</div>

		<div class="form-group col-md-3">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}">{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="policy_num">Номер полиса</label>
			<input type="text" class="form-control" id="policy_num" value="" name="policy_num">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_start">Срок действия (от)</label>
			<input type="date" class="form-control" id="policy_start" value="" max="2100-12-31" min="2010-01-01" name="policy_start">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_end">Срок действия (до)</label>
			<input type="date" class="form-control" id="policy_end" value="" max="2100-12-31" min="2010-01-01" name="policy_end">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_scan">Скан полиса</label>
			<input type="file" class="form-control-file" id="policy_scan" name="policy_scan">
		</div>

		@else

		<div class="form-group col-md-3">
			<label for="insurance_company">Страховая компания</label>
			<select id="insurance_company" class="form-control selectpicker" data-live-search="true" data-none-selected-text name="insurance_company_id">
				<option value=""></option>
				@foreach ($insuranceCompanies as $item)
				<option value="{{ $item->id }}" {{ ( $admission->insurance_company_id == $item->id ) ? 'selected' : ''}}>{{ $item->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group col-md-3">
			<label for="policy_num">Номер полиса</label>
			<input type="text" class="form-control" id="policy_num" value="{{ $admission->policy_num }}" name="policy_num">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_start">Срок действия (от)</label>
			<input type="date" class="form-control" id="policy_start" value="{{ ($admission->policy_start) ? Carbon\Carbon::parse($admission->policy_start)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_start">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_end">Срок действия (до)</label>
			<input type="date" class="form-control" id="policy_end" value="{{ ($admission->policy_end) ? Carbon\Carbon::parse($admission->policy_end)->format('Y-m-d') : '' }}" max="2100-12-31" min="2010-01-01" name="policy_end">
		</div>
		<div class="form-group col-md-2">
			<label for="policy_scan">Скан полиса</label>
			<div class="policy-view">
				<a data-fancybox="gallery" href="{{ Storage::url($admission->policy_scan) }}"><i class="fas fa-search"></i> Посмотреть</a>
			</div>
			<!--input type="file" class="form-control-file" id="policy_scan" name="policy_scan"-->
		</div>

		<div class="form-group col-md-6 {{ ($admission->insurance_company && in_array($admission->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_ic_name_block">
			<label for="asko_ic_name">Страховая компания (уточнение)</label>
			<input type="text" class="form-control" id="asko_ic_name" name="asko_ic_name" value="{{ $admission->asko_ic_name }}"  data-block="insurance_data">
		</div>
		<div class="form-group col-md-6 {{ ($admission->insurance_company && in_array($admission->insurance_company->id, ['5', '6'])) ? '' : 'hidden' }}" id="asko_immunocard_block">
			<label for="immunocard">Номер иммунокарты (или ГП)</label>
			<input type="text" class="form-control" id="asko_immunocard" name="asko_immunocard" value="{{ $admission->asko_immunocard }}"  data-block="insurance_data">
		</div>

		@endif
		<div class="form-group col-md-8">
			<label for="comment">Дополнительная информация</label>
			<input type="text" class="form-control" id="comment" name="comment">
		</div>
		<div class="form-group col-md-4">
			<label for="infect_consult">Консультация инфекциониста</label>
			<select id="infect_consult" class="form-control" name="infect_consult" required>
				<option value="0">Нет</option>
				<option value="1">Да</option>
			</select>
		</div>

		<div class="col-md-12 section-title">
			<hr>
			<span>Виды исследований</span>
		</div>


		@foreach ($researches as $item)
		<div class="form-group col-md-2">
			<div class="form-check">
				<input class="form-check-input research {{ ($item->id == '1' || $item->id == '2') ? 'research-te-tb' : ''}}{{ ($item->id == '3' || $item->id == '4') ? 'research-hga-hme' : ''}}" type="checkbox" id="research_{{ $item->id }}" name="researches[]" value="{{ $item->id }}">
				<label class="form-check-label" for="research_{{ $item->id }}">{{ $item->name }}</label>
			</div>
		</div>
		@endforeach

	</div>

	<button type="submit" class="btn btn-primary">Сохранить</button>
</form>

<div style="height: 200px;"></div>

<script type="text/javascript" src="/js/antibody.js"></script>

@endsection