$("#report_type").change(function() {
	switch ($(this).val()) {
		case 'insurance_report':
			$('#insurance_block').show();
	        $('#insurance_company_id').attr('disabled', false);
	        $('#year-block').hide();
			$('#year').attr('disabled', true);
			$('#date_from').attr('disabled', false);
			$('#date_to').attr('disabled', false);
			break;
		case 'yearly_report':
			$('#insurance_block').hide();
        	$('#insurance_company_id').attr('disabled', true);
			$('.date-block').hide();
			$('#date_from').attr('disabled', true);
			$('#date_to').attr('disabled', true);
			$('#year-block').show();
			$('#year').attr('disabled', false);
			break;
		default:
			$('#insurance_block').hide();
        	$('#insurance_company_id').attr('disabled', true);
        	$('#year-block').hide();
			$('#year').attr('disabled', true);
			$('.date-block').show();
			$('#date_from').attr('disabled', false);
			$('#date_to').attr('disabled', false);
			break;
	}
});