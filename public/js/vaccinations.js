$(document).ready(function(){
    var hash = window.location.hash;
    console.log(hash);
    hash && $('ul.nav a[href="' + hash + '"]').click();
});

if ($("#vaccine")) {
	$("#vaccine").change(function() {
	    if ($(this).val()) {
	    	if ($(this).val() == 1) {
	    		$("#our_series_block").css('display', 'none');
	    		$("#vaccine_serie").val('');
	    		$("#vaccine_serie").attr('required', false);
	    		$("#own_vaccine_block").css('display', 'block');
	    	} else {
	    		$("#our_series_block").css('display', 'block');
	    		$("#vaccine_serie_id").attr('required', true);
	    		$("#own_vaccine_info").val('');
	    		$("#own_vaccine_block").css('display', 'none');
	    		var data = '_token=' + $("input[name=_token]").val() + '&vaccine_id=' + $("#vaccine").val();
	    		if ($("#vaccination_id").val()) {
	    			data += '&vaccination_id=' + $("#vaccination_id").val();
	    		}
		    	$.ajax
		        ({
		            type: "POST",
		            url: '/vaccines/get_series',
		            //dataType: 'json',
		            data: data,
		            success: function (series) {
		            	console.log(series);
		            	if (series) {
		            		//$('#vaccine_serie').attr('disabled', false);
		            		$('#vaccine_serie').attr('required', true);
		            		$('#vaccine_serie').empty();
		            		$('#vaccine_serie').append('<option value=""></option>');
		            		$.each(series, function(key, serie) {
		            			console.log(serie);
		            			var date = new Date(serie.valid_to);
		            			var formattedDate = date.getDate() + '.' + ( '0' + (date.getMonth() + 1) ).slice(-2) + '.' + date.getFullYear();
								$('#vaccine_serie').append('<option value="' + serie.id + '">' + serie.name + ' до ' + formattedDate + '</option>');
							});
		                }
		            }
		        });
		    }
	    } else {
	        $('#vaccine_serie').val('');
	    }
	});
}

/*
$("#birth_date").focusout(function() {
	birth = new Date($(this).val());
	today = new Date($("#date").val());
	age = (Math.floor((today - birth) / (31536000000)));
	$("#age").val(age);
});
*/

function calculateAge(birth) {
    birth = new Date(birth);
    birth.setDate(birth.getDate() - 1); 
    today = new Date($("#date").val());
    var diff_ms = today.getTime() - birth.getTime();
    var age_dt = new Date(diff_ms);
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

$("#birth_date").focusout(function() {
    $("#age").val(calculateAge($(this).val()));
});