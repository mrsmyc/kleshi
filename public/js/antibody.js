$("#admission_entry_close").click(function() {
	document.location.href = '/antibodies'
});

$(document).ready(function(){
	var hash = window.location.hash;
    console.log(hash);
    hash && $('ul.nav a[href="' + hash + '"]').click();
});

$(".research-te-tb").change(function() {
    if ($(this).is(':checked')) {
        $(".research-te-tb").prop("checked", true);
    } else {
        $(".research-te-tb").prop("checked", false);
    }
});

$(".research-hga-hme").change(function() {
    if ($(this).is(':checked')) {
        $(".research-hga-hme").prop("checked", true);
    } else {
        $(".research-hga-hme").prop("checked", false);
    }
});

function calculateAge(birth) {
    birth = new Date(birth);
    birth.setDate(birth.getDate() - 1); 
    today = new Date($("#date").val());
    var diff_ms = today.getTime() - birth.getTime();
    var age_dt = new Date(diff_ms);
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

if ($("#birth_date")) {
    $("#birth_date").focusout(function() {
        $("#age").val(calculateAge($(this).val()));
    });
}

$("#insurance_company").change(function() {
    if (($(this).val() == '5') || ($(this).val() == '6')) {
        $('#asko_ic_name_block').show();
        $('#asko_immunocard_block').show();
        $('#asko_ic_name').attr('disabled', false);
        $('#asko_immunocard').attr('disabled', false);
        $('#policy_num').attr('required', false);
        $('#policy_start').attr('required', false);
        $('#policy_end').attr('required', false);
    } else {
        $('#asko_ic_name_block').hide();
        $('#asko_immunocard_block').hide();
        $('#asko_ic_name').attr('disabled', true);
        $('#asko_immunocard').attr('disabled', true);
        $('#policy_num').attr('required', true);
        $('#policy_start').attr('required', true);
        $('#policy_end').attr('required', true);
    }
});