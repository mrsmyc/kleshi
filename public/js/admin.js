if ($("#show-password")) {
	$("#show-password").click(function() {
		if ($("#password").attr('type') == 'password') {
			$("#password").attr('type', 'text');
			$(this).find('img').attr('src', '/img/close-eyes.svg')
		} else {
			$("#password").attr('type', 'password');
			$(this).find('img').attr('src', '/img/eye-close-up.svg')
		}
		
	});
}