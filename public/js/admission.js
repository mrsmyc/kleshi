datedMaterials = ['1', '2', '3', '4'];

$("#insurance_company").change(function() {
    if ($(this).val()) {
    	$.ajax
        ({
            type: "POST",
            url: '/insurance_companies/check_guarantee_letter',
            //dataType: 'json',
            data: '_token=' + $("input[name=_token]").val() + '&insurance_company_id=' + $("#insurance_company").val(),
            success: function (result) {
            	if (result['guarantee_letter']) {
            		//alert('Компания требует гарантийное письмо');
            		$('#guarantee-letter-modal').modal({
    					keyboard: false
    				})
                    $('#guarantee-letter-reminder').show();
            	} else {
                    $('#guarantee-letter-reminder').hide();
                }
            }
        });

        $('[data-block="insurance_data"]').attr('disabled', false);

        if (($(this).val() == '5') || ($(this).val() == '6')) {
            $('#asko_ic_name_block').show();
            $('#asko_immunocard_block').show();
            $('#asko_ic_name').attr('disabled', false);
            $('#asko_immunocard').attr('disabled', false);
            $('#policy_num').attr('required', false);
            $('#policy_start').attr('required', false);
            $('#policy_end').attr('required', false);
        } else {
            $('#asko_ic_name_block').hide();
            $('#asko_immunocard_block').hide();
            $('#asko_ic_name').attr('disabled', true);
            $('#asko_immunocard').attr('disabled', true);
            $('#policy_num').attr('required', true);
            $('#policy_start').attr('required', true);
            $('#policy_end').attr('required', true);
        }
    } else {
        //$('[data-block="insurance_data"]').val('');
        $('#guarantee-letter-reminder').hide();
        $('[data-block="insurance_data"]').attr('disabled', true);
    }
});

function checkOnEditStart() {
    if (!$("#insurance_company").val()) {
        $('[data-block="insurance_data"]').attr('disabled', true);
    }

    if ($("#material").val() != 1) {
        $('#material_amount').attr('disabled', true);
    }

    if (!datedMaterials.includes($("#material").val()))  {
        $('#material_date').attr('disabled', true);
    }
}

function getTodayDate() {
    var now = new Date(),
        day = ("0" + now.getDate()).slice(-2),
        month = ("0" + (now.getMonth() + 1)).slice(-2),
        today = now.getFullYear() + "-" + (month) + "-" + (day);
    return today;
}

$("#material").change(function() {
    if ($(this).val() == '1') {
        $('#material_amount').attr('disabled', false);
        $('#material_amount').val('1');
    } else {
        $('#material_amount').val('');
        $('#material_amount').attr('disabled', true);
    }

    if (datedMaterials.includes($(this).val())) {
        $('#material_date').attr('disabled', false);
    } else {
        $('#material_date').val('');
        $('#material_date').attr('disabled', true);
    }
});

$("#policy-delete").click(function(){
    $(this).closest('.policy-view').remove();
    $("#policy-file").show();
});

function checkResearchTypes() {
    if ($('input[data-type="pcr"]:checked').length > 0) {
        $('input[data-type="pcr"]:not(:checked)').attr('disabled', true);
    }
    if ($('input[data-type="ifa"]:checked').length > 0) {
        $('input[data-type="ifa"]:not(:checked)').attr('disabled', true);
    }
}

$(document).ready(function(){
    var hash = window.location.hash;
    console.log(hash);
    hash && $('ul.nav a[href="' + hash + '"]').click();

    /*
    if (hash == '#results') {
        $('html, body').animate({
            scrollTop: ($('#scroll-to-result').offset().top)
        },500);
    }
    */

    checkOnEditStart();
    var submitted = false,
        forms = $('form');

    forms.each(function() {
        $(this).data('serialize', $(this).serialize());

        $(this).submit(function() {
            submitted = true;
        });
    });

    function ifChanged() {
        var changed = false;
        forms.each(function() {
            if ($(this).serialize() != $(this).data('serialize')) 
                changed = true;
        });

        return changed;
    }

    $(window).bind('beforeunload', function(e) {
        if (ifChanged() && !submitted) {
            return true;
        }
        
        e=null;
    });

    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    });

    //checkResearchTypes();
});

$('.research-type').change(function() {
    var type = $(this).data('type'),
        typeChecks = $('.research-type[data-type = ' + type + ']');
    if($(this).is(":checked")) {
        typeChecks.attr('disabled', true);
        $(this).attr('disabled', false);
    } else {
        typeChecks.attr('disabled', false);
    } 
});


/*
$('#admin_territory').change(function() {
    if ($(this).val()) {
        $('#locality_clarification').attr('disabled', false);
        $.ajax
        ({
            type: "POST",
            url: '/admissions/get_localities',
            //dataType: 'json',
            data: '_token=' + $("input[name=_token]").val() + '&admin_territory_id=' + $("#admin_territory").val(),
            success: function (localities) {
                if (localities.length) {
                    $('#locality').attr('disabled', false);
                    $('#locality').empty();
                    $('#locality').append('<option value=""></option>');
                    $.each(localities, function(key, locality) {
                        $('#locality').append('<option value="' + locality.id + '" data-clarification="' + locality.needs_clarification + '">' + locality.name + '</option>');
                    });
                } else {
                    $('#locality').attr('disabled', true);
                }
            }
        });
    } else {
        $('#locality').attr('disabled', true);
        $('#locality').val('');
        $('#locality_clarification').attr('disabled', true);
        $('#locality_clarification').val('');
    }

    $('.selectpicker').selectpicker('refresh');
});

$('#locality').change(function() {
    //if ($(this).val()) {
        if ($(this).find(':selected').data('clarification')) {
            $('#locality_clarification').attr('required', true);
        } else {
            $('#locality_clarification').attr('required', false);
        }
    //} else {
    //    $('#locality_clarification').attr('required', false);
    //}
});
*/


$("#infect_consult").change(function() {
    if ($(this).val() == '1') {
        $('#consult_date').attr('disabled', false);
        $('#consult_date').val(getTodayDate());
    } else {
        $('#consult_date').val('');
        $('#consult_date').attr('disabled', true);
    }
});

$("#ig_dose").change(function() {
    if ($(this).val()) {
        $('#ig_date').attr('disabled', false);
        $('#ig_date').val(getTodayDate());
    } else {
        $('#ig_date').val('');
        $('#ig_date').attr('disabled', true);
    }
});

function checkMeasures() {
    if ($("#bacterial_method").val() || $("#encephalitis_method").val()) {
        $('#measures_date').attr('disabled', false);
        $('#measures_date').val(getTodayDate());
    } else {
        $("#measures_date").val('');
        $("#measures_date").attr('disabled', true)
    }
}

$("#bacterial_method").change(function() {
    checkMeasures();
});
$("#encephalitis_method").change(function() {
    checkMeasures();
});

$(".research-te-tb").change(function() {
    if ($(this).is(':checked')) {
        $(".research-te-tb").prop("checked", true);
    } else {
        $(".research-te-tb").prop("checked", false);
    }
});

$(".research-hga-hme").change(function() {
    if ($(this).is(':checked')) {
        $(".research-hga-hme").prop("checked", true);
    } else {
        $(".research-hga-hme").prop("checked", false);
    }
});

function checkResearchNum() {
    if ($("#policy_research_num").val()) {
        researchNum = $(".research:checked").length;
        policyResearchNum = $("#policy_research_num").val();
        if (researchNum > policyResearchNum) {
            $("#research_num_reminder").css("display", "block");
        } else {
            $("#research_num_reminder").css("display", "none");
        }
    } else {
        $("#research_num_reminder").css("display", "none");
    }
}

$(".research").change(function() {
    checkResearchNum();
});

$("#policy_research_num").change(function() {
    checkResearchNum();
});

function calculateAge(birth) {
    birth = new Date(birth);
    birth.setDate(birth.getDate() - 1); 
    today = new Date($("#date").val());
    var diff_ms = today.getTime() - birth.getTime();
    var age_dt = new Date(diff_ms);
  
    return Math.abs(age_dt.getUTCFullYear() - 1970);
}

$("#birth_date").focusout(function() {
    $("#age").val(calculateAge($(this).val()));
});

$("#form").submit(function(event) {
    event.preventDefault();
    var phone = $("#phone").val().trim().replace(/\D+/g, ''),
        letters = $("#phone").val().trim().replace(/[^a-zA-Zа-яА-Я]+/g, ''),
        warningText = '';
        //cleanPhone = phone.replace(/\D+/g, '');
    if (letters.length != 0) {
        warningText = 'Пожалуйста, <b>удалите из строки номера все символы, кроме цифр и дефисов</b>. Иначе смс-сообщение отправлено не будет! При необходимости используйте поле «Примечание».<br><br>';
    }
    if (phone.charAt(0) == 8 && phone.length != 11) {
        warningText += 'Похоже, Вы допустили ошибку в номере телефона: ' + phone + ' (не 11 цифр). <b>Исправьте ошибку.</b>';
    }
    if (warningText.length > 0) {
        $('#phone-validation-modal').modal({
            keyboard: false
        });
        $('#phone-validation-modal .modal-body').html(warningText);
        $('#phone-validation-modal').show();
    } else {
        $("#form").off("submit");
        $("#form").submit();
    }
});