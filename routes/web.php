<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if(!Auth::check())
    	{ return Redirect::to('login'); }
    return view('welcome');
})->name('home');

Route::get('login', 'SessionController@create')->name('login');

Route::post('login', 'SessionController@store');

Route::get('logout', 'SessionController@destroy');


Route::get('admissions', 'AdmissionController@index');

Route::get('admissions/create/{admission?}', 'AdmissionController@create');

Route::post('admissions', 'AdmissionController@store');

Route::get('admissions/{admission}/edit', 'AdmissionController@edit');

Route::patch('admissions/{admission}', 'AdmissionController@update');

Route::delete('admissions/{admission}', 'AdmissionController@destroy');

Route::get('admissions/search', 'AdmissionController@search');

Route::patch('admissions/{admission}/update_result', 'AdmissionController@update_result');

Route::post('admissions/get_localities', 'AdmissionController@get_localities');

Route::get('admissions/{admission}/print', 'AdmissionController@print');

//Route::get('admissions/fix_ticks', 'AdmissionController@fix_ticks');

//Route::get('admissions/test', 'AdmissionController@test');


Route::get('results/admissions','ResearchResultController@admission_result');

Route::get('results/admissions/filter','ResearchResultController@admission_result_filter');

Route::get('results/antibodies','ResearchResultController@antibody_result');

Route::get('results/antibodies/filter','ResearchResultController@antibody_result_filter');


Route::get('antibodies', 'AntibodyController@index');

//Route::get('/antibodies/create/', 'AntibodyController@create');

Route::get('antibodies/create/{admission?}', 'AntibodyController@create');

Route::post('antibodies/{admission}', 'AntibodyController@store');

Route::get('antibodies/{antibody}/edit', 'AntibodyController@edit');

Route::patch('antibodies/{antibody}', 'AntibodyController@update');

Route::delete('antibodies/{antibody}', 'AntibodyController@destroy');

Route::get('antibodies/search', 'AntibodyController@search');

Route::patch('antibodies/{antibody}/update_result', 'AntibodyController@update_result');

Route::get('antibodies/fix_issue_date', 'AntibodyController@fix_issue_date');

Route::get('antibodies/{antibody}/print', 'AntibodyController@print');



Route::get('old_antibodies', 'OldAntibodyController@index');

Route::get('old_antibodies/create/', 'OldAntibodyController@create');

Route::post('old_antibodies', 'OldAntibodyController@store');

Route::get('old_antibodies/{antibody}/edit', 'OldAntibodyController@edit');

Route::patch('old_antibodies/{antibody}', 'OldAntibodyController@update');

Route::delete('old_antibodies/{antibody}', 'OldAntibodyController@destroy');

Route::get('old_antibodies/search', 'OldAntibodyController@search');

Route::patch('old_antibodies/{antibody}/update_result', 'OldAntibodyController@update_result');

Route::get('old_antibodies/{antibody}/print', 'OldAntibodyController@print');



Route::get('vaccinations', 'VaccinationController@index');

Route::get('vaccinations/create/{agreement?}/{vaccination?}', 'VaccinationController@create');

Route::get('vaccinations/continue/{vaccination}', 'VaccinationController@continue');

Route::post('vaccinations', 'VaccinationController@store');

Route::get('vaccinations/{vaccination}/edit', 'VaccinationController@edit');

Route::patch('vaccinations/{vaccination}', 'VaccinationController@update');

Route::delete('vaccinations/{vaccination}', 'VaccinationController@destroy');

Route::get('vaccinations/search', 'VaccinationController@search');

Route::get('vaccinations/{vaccination}/print', 'VaccinationController@print');

Route::post('vaccinations/get_vaccination_list', 'VaccinationController@get_vaccination_list');


Route::get('vaccination_agreements', 'VaccinationAgreementController@index');

Route::get('vaccination_agreements/create', 'VaccinationAgreementController@create');

Route::post('vaccination_agreements', 'VaccinationAgreementController@store');

Route::get('vaccination_agreements/{agreement}/edit', 'VaccinationAgreementController@edit');

Route::patch('vaccination_agreements/{agreement}', 'VaccinationAgreementController@update');

Route::delete('vaccination_agreements/{agreement}', 'VaccinationAgreementController@destroy');

Route::get('vaccination_agreements/search', 'VaccinationAgreementController@search');


Route::get('insurance_companies/', 'InsuranceCompanyController@index');

Route::get('insurance_companies/create', 'InsuranceCompanyController@create');

Route::post('insurance_companies', 'InsuranceCompanyController@store');

Route::get('insurance_companies/{company}/edit', 'InsuranceCompanyController@edit');

Route::patch('insurance_companies/{company}', 'InsuranceCompanyController@update');

Route::post('insurance_companies/check_guarantee_letter', 'InsuranceCompanyController@check_guarantee_letter');


Route::get('prices', 'PriceController@index');

Route::get('prices/create', 'PriceController@create');

Route::post('prices', 'PriceController@store');

Route::get('prices/{price}/edit', 'PriceController@edit');

Route::patch('prices/{price}', 'PriceController@update');


Route::get('reports', 'ReportController@index');

Route::post('reports/make_report', 'ReportController@make_report');

Route::get('reports/generate_vaccination_agreement_report/{agreement}', 'ReportController@generate_vaccination_agreement_report');


Route::get('import', 'ImportController@index');

Route::post('import', 'ImportController@store');

Route::get('import/unify', 'ImportController@unify');


Route::get('export', 'ExportController@index');

Route::post('export/export_data', 'ExportController@export_data');


Route::get('users', 'UserController@index');

Route::get('users/create', 'UserController@create');

Route::post('users', 'UserController@store');

Route::get('users/{user}/edit', 'UserController@edit');

Route::patch('users/{user}', 'UserController@update');

Route::delete('users/{user}', 'UserController@destroy');

Route::get('users/search', 'UserController@search');



Route::get('vaccines', 'VaccineController@index');

Route::get('vaccines/create', 'VaccineController@create');

Route::post('vaccines', 'VaccineController@store');

Route::get('vaccines/{vaccine}/edit', 'VaccineController@edit');

Route::patch('vaccines/{vaccine}', 'VaccineController@update');

Route::delete('vaccines/{vaccine}', 'VaccineController@destroy');

Route::post('vaccines/get_series', 'VaccineController@get_series');

Route::get('vaccines/search', 'VaccineController@search');



Route::get('vaccine_series/{vaccine}/create', 'VaccineSerieController@create');

Route::post('vaccine_series', 'VaccineSerieController@store');

Route::get('vaccine_series/{vaccineSerie}/edit', 'VaccineSerieController@edit');

Route::patch('vaccine_series/{vaccineSerie}', 'VaccineSerieController@update');


Route::get('sms_notifications', 'SmsNotificationController@index');

Route::get('sms_notifications/send', 'SmsNotificationController@send');


Route::get('generate-pdf','ReportController@generatePDF');




////////testReport
Route::get('results/testReport', function(){
    return view('test.report');
});

Route::post('results/testReport', 'ReportController@testReport');