<?php

use Illuminate\Database\Seeder;

class RoleUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_user')->insert([
            'role_id' => App\Role::find('1')->id,
            'user_id' => App\User::find('1')->id,
        ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('2')->id,
        //     'user_id' => App\User::find('2')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('3')->id,
        //     'user_id' => App\User::find('2')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('3')->id,
        //     'user_id' => App\User::find('3')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('3')->id,
        //     'user_id' => App\User::find('4')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('3')->id,
        //     'user_id' => App\User::find('5')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('3')->id,
        //     'user_id' => App\User::find('6')->id,
        // ]);

        // DB::table('role_user')->insert([
        //     'role_id' => App\Role::find('5')->id,
        //     'user_id' => App\User::find('7')->id,
        // ]);

    }
}
