<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name' => 'master', 'tag' => 'Master'],
            ['name' => 'admin', 'tag' => 'Администратор'],
            ['name' => 'manager', 'tag' => 'Менеджер'],
            ['name' => 'doctor', 'tag' => 'Врач'],
            ['name' => 'laboratory_worker', 'tag' => 'Работник лаборатории'],
    	];

    	foreach ($roles as $role) {
    		DB::table('roles')->insert([
                'name' => $role['name'],
	            'tag' => $role['tag'],
	        ]);
    	}
    }
}
