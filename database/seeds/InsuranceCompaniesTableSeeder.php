<?php

use Illuminate\Database\Seeder;

class InsuranceCompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Абсолют Страхование', 'full_name' => 'ООО "Абсолют Страхование"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'Альфа-Страхование', 'full_name' => 'АО "Альфа-Страхование"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Альянс Жизнь', 'full_name' => 'ООО "СК "Альянс Жизнь"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'Ангара', 'full_name' => 'ООО СК "АНГАРА"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'АСКО-МЕД', 'full_name' => 'ООО "Медицинский центр "АСКО-МЕД"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'АСКО-МЕД-ПЛЮС', 'full_name' => 'ООО "АСКО-МЕД-ПЛЮС"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Верна', 'full_name' => 'ООО "СО "ВЕРНА"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'ВостСибЖасо', 'full_name' => 'АО "ВостСибЖАСО"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'ВСК', 'full_name' => 'САО "ВСК"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'ВТБ-Медицина', 'full_name' => 'ООО "Страховая компания ВТБ-Медицина"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'ВТБ Страхование', 'full_name' => 'ООО "СК "ВТБ Страхование"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'Гелиос', 'full_name' => 'ООО "Страховая компания "Гелиос"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Ингосстрах', 'full_name' => 'СПАО "Ингосстрах"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'Инкор', 'full_name' => 'ООО "ИНКОР Страхование"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Капитал Лайф (РГС Жизнь)', 'full_name' => 'ООО "Капитал Лайф Страхование Жизни"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Колымская', 'full_name' => 'АО "СК "Колымская"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Либерти страхование', 'full_name' => 'АО "Либерти страхование"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'МАКС', 'full_name' => 'ЗАО "МАКС"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'МВД', 'full_name' => 'ФКУЗ "МСЧ МВД России по Иркутской области"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Медпрактика', 'full_name' => 'ООО "Медпрактика"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Метлайф', 'full_name' => 'АО "СК "Метлайф"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'МЛДЦ НТ', 'full_name' => 'ООО "МЛДЦ-НТ"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Надежда', 'full_name' => 'САО "Надежда"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'НАСКО', 'full_name' => 'АО "НАСКО"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Ресо Гарантия', 'full_name' => 'СПАО "РЕСО_Гарантия"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Росгосстрах', 'full_name' => 'ПАО "СК "Росгосстрах"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Ренессанс', 'full_name' => 'ООО "Группа Ренессанс Страхование"', 'guarantee_letter' => true, 'new_policy' => false],
            ['name' => 'РОСЭНЕРГО', 'full_name' => 'ООО "НЦГ – "РОСЭНЕРГО"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Санаторий Байкал', 'full_name' => 'Санаторий Байкал', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'СОГАЗ', 'full_name' => 'АО "СОГАЗ"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Согласие', 'full_name' => 'ООО "Страховая компания "Согласие"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Солидарность', 'full_name' => 'АО "Страховая компания "Солидарность"', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Удостоверение ВОВ', 'full_name' => 'Удостоверение ВОВ', 'guarantee_letter' => false, 'new_policy' => false],
            ['name' => 'Энергогарант', 'full_name' => 'ПАО "Страховая акционерная компания "ЭНЕРГОГАРАНТ"', 'guarantee_letter' => false, 'new_policy' => true],
            ['name' => 'Югория', 'full_name' => 'ОАО "Государственная страховая компания "Югория"', 'guarantee_letter' => false, 'new_policy' => true],
            ['name' => 'Другая', 'full_name' => '', 'guarantee_letter' => false, 'new_policy' => false],

            //['name' => 'Ангара', 'full_name' => ''],
            //['name' => 'Бин-страхование', 'full_name' => ''],
            //['name' => 'Солидарность', 'full_name' => ''],
            //['name' => 'Зетта', 'full_name' => ''],
            //['name' => 'Инвестстрах', 'full_name' => ''],
            //['name' => 'Компаньон', 'full_name' => ''],
            //['name' => 'МСК', 'full_name' => ''],
            //['name' => 'Селекта', 'full_name' => ''],
            //['name' => 'СО ЖАСО', 'full_name' => ''],
            //['name' => 'УралСиб', 'full_name' => ''],
            //['name' => 'ЦСО', 'full_name' => ''],
        ];

        foreach($items as $item) {
            DB::table('insurance_companies')->insert([
                'name' => $item['name'],
                'full_name' => $item['full_name'],
                'guarantee_letter' => $item['guarantee_letter'],
                'new_policy' => $item['new_policy'],
            ]);
        }
    }
}
