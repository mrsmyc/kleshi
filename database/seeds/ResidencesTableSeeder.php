<?php

use Illuminate\Database\Seeder;

class ResidencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'г. Иркутск', 'permanent_residence_id' => 1],
            ['name' => 'Иркутский р-н', 'permanent_residence_id' => 2],
            ['name' => 'г. Ангарск', 'permanent_residence_id' => 6],
            ['name' => 'Зиминский р-н, г. Зима, г.Саянск', 'permanent_residence_id' => 6],
            ['name' => 'Бодайбинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Жигаловский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Заларинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Балаганский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Качугский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Куйтунский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Казачинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Нижнеудинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Ольхонский р-н', 'permanent_residence_id' => 5],
            ['name' => 'Тайшет, Тайшетский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Тулун, Тулунский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Усолье, Усольский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Усть-Кутский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Усть-Удинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Черемхово, Черемховский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Шелеховский р-н', 'permanent_residence_id' => 3],
            ['name' => 'Слюдянский р-н', 'permanent_residence_id' => 4],
            ['name' => 'Чунский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Усть-Илимск, Усть-Илимский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Аларский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Осинский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Боханский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Нукутский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Эхирит-Булагатский р-н', 'permanent_residence_id' => 6],
            ['name' => 'Баяндаевский р-н', 'permanent_residence_id' => 6],
            ['name' => 'РФ', 'permanent_residence_id' => 7],
            ['name' => 'Другие страны', 'permanent_residence_id' => 8],
        ];

        foreach($items as $item) {
            DB::table('residences')->insert([
                'name' => $item['name'],
                'permanent_residence_id' => $item['permanent_residence_id'],
            ]);
        }
    }
}
