<?php

use Illuminate\Database\Seeder;

class AdmissionResultsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
            	'admission_id' => 1,

            ],
        ];

        foreach($items as $item) {
            DB::table('admission_results')->insert([
            	'admission_id' => $item['admission_id'],
            ]);
        }
    }
}
