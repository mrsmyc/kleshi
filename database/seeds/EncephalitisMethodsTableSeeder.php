<?php

use Illuminate\Database\Seeder;

class EncephalitisMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Анаферон'],
            ['name' => 'Эргоферон'],
            ['name' => 'Йодантипирин'],
            ['name' => 'Циклоферон'],
            ['name' => 'Рекомендовано'],
        ];

        foreach($items as $item) {
            DB::table('encephalitis_methods')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
