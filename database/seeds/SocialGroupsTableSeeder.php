<?php

use Illuminate\Database\Seeder;

class SocialGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Студенты'],
            ['name' => 'Дети'],
            ['name' => 'Служащие/рабочие'],
            ['name' => 'Прочее население'],
        ];

        foreach($items as $item) {
            DB::table('social_groups')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
