<?php

use Illuminate\Database\Seeder;

class ChildCareFacilitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Загородные оздоровительные'],
            ['name' => 'Оздоровительных дневного пребывания'],
            ['name' => 'Образовательные (дет. сады, школы и т.д.)'],
        ];

        foreach($items as $item) {
            DB::table('child_care_facilities')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
