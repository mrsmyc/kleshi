<?php

use Illuminate\Database\Seeder;

class ResearchTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'КЭ-КБ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на клещевой энцефалит, клещевой боррелиоз (ПЦР)', 'price' => '1100', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-КР', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 3 инфекции: клещевой энцефалит, клещевой боррелиоз и клещевой риккетсиоз (ПЦР)', 'price' => '1650', 'includes' => array('1'), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-КВЛ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 3 инфекции: клещевой энцефалит, клещевой боррелиоз и клещевую возвратную лихорадку (ПЦР)', 'price' => '1650', 'includes' => array('1'), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-МЭЧ-ГАЧ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 4 инфекции: клещевой энцефалит, клещевой боррелиоз, моноцитарный эрлихиоз человека, гранулоцитарный анаплазмоз человека (ПЦР)', 'price' => '2100', 'includes' => array('1'), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-МЭЧ-ГАЧ-КР', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 5 инфекций: клещевой энцефалит, клещевой боррелиоз, клещевой риккетсиоз, моноцитарный эрлихиоз человека, гранулоцитарный анаплазмоз человека (ПЦР)', 'price' => '3000', 'includes' => array('1', '2', '4'), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-МЭЧ-ГАЧ-КВЛ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 5 инфекций: клещевой энцефалит, клещевой боррелиоз, клещевую возвратную лихорадку, моноцитарный эрлихиоз человека, гранулоцитарный анаплазмоз человека (ПЦР)', 'price' => '3000', 'includes' => array('1', '3', '4'), 'type' => 'pcr', 'price_codes' => array('К035'), 'owner_type' => 'admission'],

            ['name' => 'КЭ-КБ-МЭЧ-ГАЧ-КВЛ-КР', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на 6 инфекций: клещевой энцефалит, клещевой боррелиоз, клещевую возвратную лихорадку, клещевой риккетсиоз, моноцитарный эрлихиоз человека, гранулоцитарный анаплазмоз человека (ПЦР)', 'price' => '3600', 'includes' => array('1', '2', '3', '4', '5'), 'type' => 'pcr', 'price_codes' => array('К042'), 'owner_type' => 'admission'],

            ['name' => 'КЭ (антиген)', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) на антиген вируса клещевого энцефалита (ИФА)', 'price' => '800', 'includes' => array(), 'type' => 'ag', 'price_codes' => array('К018'), 'owner_type' => 'admission'],

            ['name' => 'Ig M КЭ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса M к вирусу клещевого энцефалита (ИФА)', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К023'), 'owner_type' => 'admission'],

            ['name' => 'Ig G КЭ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса G к вирусу клещевого энцефалита (ИФА)', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К024'), 'owner_type' => 'admission'],

            ['name' => 'Ig M G КЭ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса M и класса G к вирусу клещевого энцефалита (ИФА)', 'price' => '1800', 'includes' => array('9', '10'), 'type' => 'ifa', 'price_codes' => array('К023', 'К024'), 'owner_type' => 'admission'],

            ['name' => 'Ig M КБ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса M к возбудителю клещевого боррелиоза (ИФА)', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К025'), 'owner_type' => 'admission'],

            ['name' => 'Ig G КБ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса G к возбудителю клещевого боррелиоза (ИФА)', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К026'), 'owner_type' => 'admission'],

            ['name' => 'Ig M G КБ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса M и класса G к возбудителю клещевого боррелиоза (ИФА)', 'price' => '1800', 'includes' => array('12', '13'), 'type' => 'ifa', 'price_codes' => array('К025', 'К026'), 'owner_type' => 'admission'],

            ['name' => 'Ig M G КЭ - M G КБ', 'description' => 'Исследование биоматериала (клещ/нимфа, кровь, сыворотка, ликвор и др.) для выявления иммуноглобулинов класса M и класса G к вирусу клещевого энцефалита (ИФА) и к возбудителю клещевого боррелиоза (ИФА)', 'price' => '3600', 'includes' => array('9', '10', '11', '12', '13', '14'), 'type' => 'ifa', 'price_codes' => array('К023', 'К024', 'К025', 'К026'), 'owner_type' => 'admission'],


            ['name' => 'Кровь ПЦР ВКЭ', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Кровь ПЦР КБ', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Кровь ПЦР МЭЧ', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Кровь ПЦР ГАЧ', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Кровь ПЦР КВЛ', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Кровь ПЦР КР', 'description' => '', 'price' => '0', 'includes' => array(), 'type' => 'pcr', 'price_codes' => array(''), 'owner_type' => 'antibody'],
            ['name' => 'Ig M КЭ', 'description' => '', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К023'), 'owner_type' => 'antibody'],
            ['name' => 'Ig G КЭ', 'description' => '', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К024'), 'owner_type' => 'antibody'],
            ['name' => 'Ig M КБ', 'description' => '', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К025'), 'owner_type' => 'antibody'],
            ['name' => 'Ig G КБ', 'description' => '', 'price' => '900', 'includes' => array(), 'type' => 'ifa', 'price_codes' => array('К026'), 'owner_type' => 'antibody'],

        ];

        foreach($items as $item) {
            DB::table('research_types')->insert([
                'name' => $item['name'],
                'description' => $item['description'],
                'price' => $item['price'],
                'includes' => json_encode($item['includes']),
                'price_codes' => json_encode($item['price_codes']),
                'type' => $item['type'],
                'owner_type' => $item['owner_type'],
            ]);
        }
    }
}
