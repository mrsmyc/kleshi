<?php

use Illuminate\Database\Seeder;

class RemovalInstitutionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Карла Маркса, 3'],
            ['name' => 'Травмпункт'],
            ['name' => 'Самостоятельно'],
            ['name' => 'Другие ЛПУ'],
        ];

        foreach($items as $item) {
            DB::table('removal_institutions')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
