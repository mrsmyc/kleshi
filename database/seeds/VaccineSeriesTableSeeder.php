<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class VaccineSeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Т21', 'valid_to' => Carbon::parse('01.07.2019'), 'vaccine_id' => 2, 'enabled' => false],
            ['name' => 'Т24', 'valid_to' => Carbon::parse('01.09.2019'), 'vaccine_id' => 2, 'enabled' => false],
            ['name' => 'Т41', 'valid_to' => Carbon::parse('01.09.2020'), 'vaccine_id' => 2, 'enabled' => true],
            ['name' => 'Т45', 'valid_to' => Carbon::parse('01.11.2020'), 'vaccine_id' => 2, 'enabled' => false],

			['name' => '119', 'valid_to' => Carbon::parse('01.03.2019'), 'vaccine_id' => 3, 'enabled' => false],
            ['name' => '144', 'valid_to' => Carbon::parse('01.11.2019'), 'vaccine_id' => 3, 'enabled' => false],
            ['name' => '186', 'valid_to' => Carbon::parse('01.10.2020'), 'vaccine_id' => 3, 'enabled' => false],
            ['name' => '189', 'valid_to' => Carbon::parse('01.10.2020'), 'vaccine_id' => 3, 'enabled' => false],
            ['name' => '196', 'valid_to' => Carbon::parse('01.12.2020'), 'vaccine_id' => 3, 'enabled' => true],

            ['name' => '144', 'valid_to' => Carbon::parse('01.11.2019'), 'vaccine_id' => 4, 'enabled' => false],
            ['name' => '189', 'valid_to' => Carbon::parse('01.10.2020'), 'vaccine_id' => 4, 'enabled' => false],
            ['name' => '197', 'valid_to' => Carbon::parse('01.12.2020'), 'vaccine_id' => 4, 'enabled' => true],
        ];

        foreach($items as $item) {
            DB::table('vaccine_series')->insert([
                'name' => $item['name'],
                'valid_to' => $item['valid_to'],
                'vaccine_id' => $item['vaccine_id'],
                'enabled' => $item['enabled'],
            ]);
        }
    }
}
