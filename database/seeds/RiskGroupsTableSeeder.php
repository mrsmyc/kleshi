<?php

use Illuminate\Database\Seeder;

class RiskGroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Электросети'],
            ['name' => 'Строители'],
            ['name' => 'Дорожные рабочие'],
            ['name' => 'Сельхоз. рабочие'],
            ['name' => 'Другие'],
        ];

        foreach($items as $item) {
            DB::table('risk_groups')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
