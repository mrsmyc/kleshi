<?php

use Illuminate\Database\Seeder;

class ProfContingentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'ЛПХ'],
            ['name' => 'ХЛХ'],
            ['name' => 'ЛХ'],
            ['name' => 'Лесники'],
            ['name' => 'Охотники'],
            ['name' => 'Геологи'],
            ['name' => 'Другие'],
        ];

        foreach($items as $item) {
            DB::table('prof_contingents')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
