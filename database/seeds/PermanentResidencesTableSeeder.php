<?php

use Illuminate\Database\Seeder;

class PermanentResidencesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Иркутск'],
            ['name' => 'Иркутский р-н'],
            ['name' => 'Шелеховский р-н'],
            ['name' => 'Слюдянский р-н'],
            ['name' => 'Ольхонский р-н'],
            ['name' => 'Другие территории области'],
            ['name' => 'Жители РФ'],
            ['name' => 'Иностранные граждане'],
        ];

        foreach($items as $item) {
            DB::table('permanent_residences')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
