<?php

use Illuminate\Database\Seeder;

class BiteSpotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Голова'],
            ['name' => 'Спина'],
            ['name' => 'Живот'],
            ['name' => 'Бок'],
            ['name' => 'Рука'],
            ['name' => 'Нога'],
            ['name' => 'Пах'],
            ['name' => 'Ухо'],
            ['name' => 'Ягодицы'],
            ['name' => 'Лицо'],
            ['name' => 'Подмышки'],
            ['name' => 'Грудь'],
            ['name' => 'Гениталии'],
            ['name' => 'Шея'],
            ['name' => 'Не кусал'],
            ['name' => 'Множественные укусы'],
        ];

        foreach($items as $item) {
            DB::table('bite_spots')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
