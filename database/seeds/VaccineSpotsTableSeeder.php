<?php

use Illuminate\Database\Seeder;

class VaccineSpotsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'В/м 0,5мл'],
            ['name' => 'В/м 0,25мл'],
        ];

        foreach($items as $item) {
            DB::table('vaccine_spots')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
