<?php

use Illuminate\Database\Seeder;

class GendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Мужской'],
            ['name' => 'Женский'],
        ];

        foreach($items as $item) {
            DB::table('genders')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
