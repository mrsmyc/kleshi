<?php

use Illuminate\Database\Seeder;

class BiteCircumstancesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Сбор дикоросов', 'type' => 'При выходе в лес'],
            ['name' => 'Заготовка дров', 'type' => 'При выходе в лес'],
            ['name' => 'Выпас личного скота', 'type' => 'При выходе в лес'],
            ['name' => 'На даче', 'type' => 'При выходе в лес'],
            ['name' => 'На кладбище', 'type' => 'При выходе в лес'],
            ['name' => 'Отдых на природе', 'type' => 'При выходе в лес'],
            ['name' => 'В ЛОУ', 'type' => 'При выходе в лес'],
            ['name' => 'При выполнении проф. обязанностей', 'type' => 'При выходе в лес'],
            ['name' => 'Другое', 'type' => 'При выходе в лес'],
            ['name' => 'Укус отрицает', 'type' => 'При выходе в лес'],
            ['name' => 'Занос скотом, собаками, дикоросами', 'type' => 'Лес не посещали'],
            ['name' => 'Присасывание в черте насел. пункта', 'type' => 'Лес не посещали'],
            ['name' => 'Источник заражения не установлен', 'type' => 'Лес не посещали'],
        ];

        foreach($items as $item) {
            DB::table('bite_circumstances')->insert([
                'name' => $item['name'],
                'type' => $item['type'],
            ]);
        }
    }
}
