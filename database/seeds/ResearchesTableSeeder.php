<?php

use Illuminate\Database\Seeder;

class ResearchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'КЭ', 'type' => 'pcr', 'price_id' => NULL],
            ['name' => 'КБ', 'type' => 'pcr', 'price_id' => NULL],
            ['name' => 'МЭЧ', 'type' => 'pcr', 'price_id' => NULL],
            ['name' => 'ГАЧ', 'type' => 'pcr', 'price_id' => NULL],
            ['name' => 'КВЛ', 'type' => 'pcr', 'price_id' => NULL],
            ['name' => 'КР', 'type' => 'pcr', 'price_id' => NULL],

            ['name' => 'КЭ (антиген)', 'type' => 'other', 'price_id' => '7'],

            ['name' => 'Ig M КЭ', 'type' => 'ifa', 'price_id' => '8'],
            ['name' => 'Ig M КБ', 'type' => 'ifa', 'price_id' => '9'],
            ['name' => 'Ig G КЭ', 'type' => 'ifa', 'price_id' => '10'],
            ['name' => 'Ig G КБ', 'type' => 'ifa', 'price_id' => '11'],
        ];

        foreach($items as $item) {
            DB::table('researches')->insert([
                'name' => $item['name'],
                'type' => $item['type'],
                'price_id' => $item['price_id'],
            ]);
        }
    }
}
