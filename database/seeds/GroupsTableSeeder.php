<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'ЛПХ', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'ХЛХ', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'ЛХ', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'Лесники', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'Охотники', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'Геологи', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'Другие', 'type' => 'prof_contingent', 'type_title' => 'Проф. контингент'],
            ['name' => 'Электросети', 'type' => 'risk_group', 'type_title' => 'Группа риска'],
            ['name' => 'Строители', 'type' => 'risk_group', 'type_title' => 'Группа риска'],
            ['name' => 'Дорожные рабочие', 'type' => 'risk_group', 'type_title' => 'Группа риска'],
            ['name' => 'Сельхоз. рабочие', 'type' => 'risk_group', 'type_title' => 'Группа риска'],
            ['name' => 'Другие', 'type' => 'risk_group', 'type_title' => 'Группы риска'],
            ['name' => 'Студенты', 'type' => 'social_group', 'type_title' => 'Социальная группа'],
            ['name' => 'Дети', 'type' => 'social_group', 'type_title' => 'Социальная группа'],
            ['name' => 'Служащие/рабочие', 'type' => 'social_group', 'type_title' => 'Социальная группа'],
            ['name' => 'Прочее население', 'type' => 'social_group', 'type_title' => 'Социальная группа'],
        ];

        foreach($items as $item) {
            DB::table('groups')->insert([
                'name' => $item['name'],
                'type' => $item['type'],
                'type_title' => $item['type_title'],
            ]);
        }
    }
}
