<?php

use Illuminate\Database\Seeder;

class LocalitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Академгородок', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'М-н Юбилейный', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'М-н Первомайский', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Лисиха', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Кузьмиха', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => '6 пос. ГЭС', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Марата', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Радищево', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Рабочее', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Солнечный', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Ново-Ленино', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Зелёный', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Парки Иркутска', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'О. Юность', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Кладбища', 'admin_territory_id' => '1', 'needs_clarification' => false],
            ['name' => 'Другие места в черте города', 'admin_territory_id' => '1', 'needs_clarification' => true],

            ['name' => 'Байкальский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'ЖД Иркутск-Слюдянка', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Александровский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Качугский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Московский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Голоустненский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Култукский тракт', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Заливы Иркутского водохранилища', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Побережье о. Байкал', 'admin_territory_id' => '2', 'needs_clarification' => false],
            ['name' => 'Другие', 'admin_territory_id' => '2', 'needs_clarification' => true],

            ['name' => 'Мельничная Падь', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Пивовариха', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Маркова', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Стеклянка', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Ст. Юннатов', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Плишкино', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Падь Топка', 'admin_territory_id' => '3', 'needs_clarification' => false],
            ['name' => 'Другие', 'admin_territory_id' => '3', 'needs_clarification' => true],

            ['name' => 'В черте населённых пунктов', 'admin_territory_id' => '4', 'needs_clarification' => true],
            ['name' => 'На территории района', 'admin_territory_id' => '4', 'needs_clarification' => false],
            ['name' => 'Побережье о. Байкал', 'admin_territory_id' => '4', 'needs_clarification' => false],

            ['name' => 'В черте населённых пунктов', 'admin_territory_id' => '5', 'needs_clarification' => true],
            ['name' => 'На территории района', 'admin_territory_id' => '5', 'needs_clarification' => false],

            ['name' => 'В черте населённых пунктов', 'admin_territory_id' => '6', 'needs_clarification' => true],
            ['name' => 'На территории района', 'admin_territory_id' => '6', 'needs_clarification' => false],

            /*
            ['name' => 'Александровский тракт'],
            ['name' => 'Ангарск'],
            ['name' => 'Ангасолка'],
            ['name' => 'Аршан'],
            ['name' => 'Бабушкин'],
            ['name' => 'Байкальск'],
            ['name' => 'Байкальский тракт'],
            ['name' => 'Балаганск'],
            ['name' => 'Баяндай'],
            ['name' => 'Большая Речка'],
            ['name' => 'Большие Коты'],
            ['name' => 'Большое Голоустное'],
            ['name' => 'Большой Луг'],
            ['name' => 'Бохан'],
            ['name' => 'Братск'],
            ['name' => 'Выдрино'],
            ['name' => 'Глубокая ж.д.с.'],
            ['name' => 'Голоустненский тракт'],
            ['name' => 'Голубый Ели ж.д.с.'],
            ['name' => 'Горохово'],
            ['name' => 'Горячие Ключи'],
            ['name' => 'Грудинино'],
            ['name' => 'Дачная ж.д.с.'],
            ['name' => 'Еланцы'],
            ['name' => 'Жигалово'],
            ['name' => 'Заливы Иркутского водохранилища'],
            ['name' => 'Зелёный Мыс'],
            ['name' => 'Капсал'],
            ['name' => 'Карлук'],
            ['name' => 'Качуг'],
            ['name' => 'Качугский тракт'],
            ['name' => 'Курма'],
            ['name' => 'Летняя ж.д.с.'],
            ['name' => 'Листвянка'],
            ['name' => 'Малое Море'],
            ['name' => 'Малое Голоустное'],
            ['name' => 'Мангутай'],
            ['name' => 'Марково'],
            ['name' => 'Мегет'],
            ['name' => 'Мельничная Падь'],
            ['name' => 'Молодёжный микрорайон'],
            ['name' => 'Московский тракт'],
            ['name' => 'Моты'],
            ['name' => 'МРС'],
            ['name' => 'Мурино'],
            ['name' => 'Никола'],
            ['name' => 'Никольск'],
            ['name' => 'Ново-Грудинино'],
            ['name' => 'Оёк'],
            ['name' => 'Олха'],
            ['name' => 'Ольхон'],
            ['name' => 'Орлёнок ж.д.с.'],
            ['name' => 'Оса'],
            ['name' => 'Пивовариха'],
            ['name' => 'Плишкино'],
            ['name' => 'Порт Байкал'],
            ['name' => 'Садовая ж.д.с.'],
            ['name' => 'Санаторий Байкал'],
            ['name' => 'Сарма'],
            ['name' => 'Слюдянка'],
            ['name' => 'Смоленщина'],
            ['name' => 'Смоленское кладбище'],
            ['name' => 'Стеклянка'],
            ['name' => 'Тальцы'],
            ['name' => 'Тёплые озёра'],
            ['name' => 'Тункинская Долина'],
            ['name' => 'Улан-Удэ'],
            ['name' => 'Урик'],
            ['name' => 'Усть-Илимск'],
            ['name' => 'Усть-Ордынский'],
            ['name' => 'Утулик'],
            ['name' => 'Хамар-Дабан'],
            ['name' => 'Хомутово'],
            ['name' => 'Худяково'],
            ['name' => 'Черемхово'],
            ['name' => 'Шаманка'],
            ['name' => 'Шелехов'],
            ['name' => 'Ягодная ж.д.с.'],
            ['name' => 'Другое'],*/
        ];

        foreach($items as $item) {
            DB::table('localities')->insert([
                'name' => $item['name'],
                'admin_territory_id' => $item['admin_territory_id'],
                'needs_clarification' => $item['needs_clarification'],
            ]);
        }
    }
}
