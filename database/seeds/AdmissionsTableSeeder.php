<?php

use Illuminate\Database\Seeder;

use Carbon\Carbon;

class AdmissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            [
            	'date' => Carbon::today()->format('Y-m-d H:i:s'),
            	'name' => 'Tick Bitten',
            	'gender_id' => '1',
            	'age' => '0',
            	'phone' => '89006546565',
            	'permanent_residence_id' => '1',
                'insurance_company_id' => '7',
                'policy_num' => 'ASD 10092',
                'policy_start' => '2018-02-10',
                'policy_end' => '2019-02-10',
            	'comment_insurance' => '2 месяца',
                'bite_date' => '2019-01-10',
                'removal_date' => '2018-01-10',
            	'removal_institution_id' => '2',
            	'bite_spot_id' => '4',
            	'bite_circumstance_id' => '1',
                'vaccine_level_id' => '1',
                'ig_date' => '2019-01-11',
            	'ig_dose' => '2',
            	'admin_territory_id' => '1',
            	'city_area_id' => '1',
            	'locality_id' => '1',
            	'encephalitis_method_id' => '1',
            	'bacterial_method_id' => '1',
            	'material_id' => '1',
            	'material_amount' => '2',
            	//'research_type_id' => '1',
                'research_types' => '["4", "8", "15"]',
            	'responsible_id' => '1',

            ],
        ];

        foreach($items as $item) {
            DB::table('admissions')->insert([
            	'date' => $item['date'],
                'name' => $item['name'],
                'gender_id' => $item['gender_id'],
                'age' => $item['age'],
                'phone' => $item['phone'],
                'permanent_residence_id' => $item['permanent_residence_id'],
                'insurance_company_id' => $item['insurance_company_id'],
                'policy_num' => $item['policy_num'],
                'policy_start' => $item['policy_start'],
                'policy_end' => $item['policy_end'],
                'comment_insurance' => $item['comment_insurance'],
                'bite_date' => $item['bite_date'],
                'removal_date' => $item['removal_date'],
                'removal_institution_id' => $item['removal_institution_id'],
                'bite_spot_id' => $item['bite_spot_id'],
                'bite_circumstance_id' => $item['bite_circumstance_id'],
                'ig_date' => $item['ig_date'],
                'ig_dose' => $item['ig_dose'],
                'vaccine_level_id' => $item['vaccine_level_id'],
                'admin_territory_id' => $item['admin_territory_id'],
                'city_area_id' => $item['city_area_id'],
                'locality_id' => $item['locality_id'],
                'encephalitis_method_id' => $item['encephalitis_method_id'],
                'bacterial_method_id' => $item['bacterial_method_id'],
                'material_id' => $item['material_id'],
                'material_amount' => $item['material_amount'],
                //'research_type_id' => $item['research_type_id'],
                'research_types' => $item['research_types'],
                'responsible_id' => $item['responsible_id'],

            ]);
        }
    }
}
