<?php

use Illuminate\Database\Seeder;

class PaymentTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Наличный'],
            ['name' => 'Безналичный'],
            ['name' => 'Гарантийное письмо'],
        ];

        foreach($items as $item) {
            DB::table('payment_types')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
