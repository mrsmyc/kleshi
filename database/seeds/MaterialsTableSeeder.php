<?php

use Illuminate\Database\Seeder;

class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Клещ', 'enabled' => true],
            ['name' => 'Кровь', 'enabled' => true],
            ['name' => 'Сыворотка', 'enabled' => true],
            ['name' => 'Ликвор', 'enabled' => true],
            ['name' => 'Без исследования', 'enabled' => true],
            ['name' => 'Нимфа', 'enabled' => false],
            ['name' => 'Отказались', 'enabled' => true],
        ];

        foreach($items as $item) {
            DB::table('materials')->insert([
                'name' => $item['name'],
                'enabled' => $item['enabled'],
            ]);
        }
    }
}
