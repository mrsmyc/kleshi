<?php

use Illuminate\Database\Seeder;

class VaccinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Принесли с собой'],
            ['name' => 'Энцевир'],
            ['name' => 'Клещ-Э-Вак Взрослый'],
            ['name' => 'Клещ-Э-Вак Детский'],
        ];

        foreach($items as $item) {
            DB::table('vaccines')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
