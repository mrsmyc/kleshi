<?php

use Illuminate\Database\Seeder;

class TickTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Таёжный (I.p.)'],
            ['name' => 'Степной (D.Sp.)'],
            ['name' => 'H. concinna (H.c.)'],
            ['name' => 'Вид не определен (ВНО)'],
            ['name' => 'Не клещ (НК)'],
        ];

        foreach($items as $item) {
            DB::table('tick_types')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
