<?php

use Illuminate\Database\Seeder;

class TickGendersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'F'],
            ['name' => 'M'],
            ['name' => 'N'],
            ['name' => 'L'],
        ];

        foreach($items as $item) {
            DB::table('tick_genders')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
