<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Developer',
            'login' => 'developer',
            'password' => bcrypt('devPASS'),
            /*
            hash password = $2y$10$RqUGpk2ZjaLz8gMjir8EYuq.8G4Bnxv3TIO6aEaCAtg8MSUWo7gGu    mJ7ihmZe6OMW9cK2MIPlIs1jowOSayLIc4U9Qi5UQRZ6JiwPPS4ldugfoJLX
            */
        ]);
    }

}
