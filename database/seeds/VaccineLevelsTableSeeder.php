<?php

use Illuminate\Database\Seeder;

class VaccineLevelsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'V1'],
            ['name' => 'V2'],
            ['name' => 'V3'],
            ['name' => 'RV'],
            ['name' => 'Не вакцинирован'],
        ];

        foreach($items as $item) {
            DB::table('vaccine_levels')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
