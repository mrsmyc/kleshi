<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
        	InsuranceCompaniesTableSeeder::class,
        	GendersTableSeeder::class,
        	PermanentResidencesTableSeeder::class,
            ResidencesTableSeeder::class,
        	RemovalInstitutionsTableSeeder::class,
        	BiteSpotsTableSeeder::class,
        	BiteCircumstancesTableSeeder::class,
        	VaccineLevelsTableSeeder::class,
        	AdminTerritoriesTableSeeder::class,
        	CityAreasTableSeeder::class,
        	EncephalitisMethodsTableSeeder::class,
        	BacterialMethodsTableSeeder::class,
        	MaterialsTableSeeder::class,
            PricesTableSeeder::class,
            ResearchesTableSeeder::class,
            ResearchTypesTableSeeder::class,
            LocalitiesTableSeeder::class,
            ChildCareFacilitiesTableSeeder::class,
            //AdmissionsTableSeeder::class,
            //AdmissionResultsTableSeeder::class,
            TickGendersTableSeeder::class,
            TickTypesTableSeeder::class,
            TickConditionsTableSeeder::class,
            PaymentTypesTableSeeder::class,
            VaccinesTableSeeder::class,
            VaccineSeriesTableSeeder::class,
            VaccineSpotsTableSeeder::class,
        	GroupsTableSeeder::class,
            ProfContingentsTableSeeder::class,
            RiskGroupsTableSeeder::class,
            SocialGroupsTableSeeder::class,
            RolesTableSeeder::class,
            RoleUserTableSeeder::class,
        ]);
    }
}
