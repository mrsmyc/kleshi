<?php

use Illuminate\Database\Seeder;

class BacterialMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Амоксициллин'],
            ['name' => 'Доксициклин'],
            ['name' => 'Рекомендовано'],
        ];

        foreach($items as $item) {
            DB::table('bacterial_methods')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
