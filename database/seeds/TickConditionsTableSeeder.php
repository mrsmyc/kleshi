<?php

use Illuminate\Database\Seeder;

class TickConditionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['name' => 'Живой'],
            ['name' => 'Погибший'],
            ['name' => 'Сухой'],
            ['name' => 'Фрагменты'],
            ['name' => 'Разорван'],
            ['name' => 'Не пригоден'],
            ['name' => 'Напитан'],
        ];

        foreach($items as $item) {
            DB::table('tick_conditions')->insert([
                'name' => $item['name'],
            ]);
        }
    }
}
