<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldAntibodyResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_antibody_results', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('antibody_id');
            $table->foreign('antibody_id')
                  ->references('id')->on('old_antibodies')
                  ->onDelete('restrict');

            $table->boolean('ig_m_te')->nullable();
            $table->boolean('ig_g_te')->nullable();
            $table->string('titr_ig_g_te')->nullable();
            $table->boolean('ig_m_tb')->nullable();
            $table->boolean('ig_g_tb')->nullable();
            $table->string('titr_ig_g_tb')->nullable();

            /*
            $table->boolean('ig_m_hme')->nullable();
            $table->boolean('ig_g_hme')->nullable();

            $table->boolean('ig_m_hga')->nullable();
            $table->boolean('ig_g_hga')->nullable();
            */

            $table->boolean('blood_pcr_vte')->nullable();
            $table->boolean('blood_pcr_tb')->nullable();
            $table->boolean('blood_pcr_hme')->nullable();
            $table->boolean('blood_pcr_hga')->nullable();
            $table->boolean('blood_pcr_trf')->nullable();
            $table->boolean('blood_pcr_tr')->nullable();

            $table->text('comment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_antibody_results');
    }
}
