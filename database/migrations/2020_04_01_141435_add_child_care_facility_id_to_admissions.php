<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChildCareFacilityIdToAdmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->unsignedInteger('child_care_facility_id')->nullable()->after('locality_clarification');
            $table->foreign('child_care_facility_id')
                  ->references('id')->on('child_care_facilities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->dropForeign('admissions_child_care_facility_id_foreign');
            $table->dropColumn('child_care_facility_id');
        });
    }
}
