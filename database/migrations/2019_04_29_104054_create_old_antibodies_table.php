<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldAntibodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_antibodies', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date');

            $table->string('name');

            $table->integer('age');

            $table->string('phone');

            $table->string('bite_year')->nullable();

            $table->text('anamnesis')->nullable();

            $table->json('research_types')->nullable();

            $table->unsignedInteger('responsible_id');
            $table->foreign('responsible_id')
                  ->references('id')->on('users');

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')
                  ->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_antibodies');
    }
}
