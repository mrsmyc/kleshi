<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOldAntibodyResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('old_antibody_research', function (Blueprint $table) {
            $table->unsignedInteger('old_antibody_id');
            $table->foreign('old_antibody_id')
                    ->references('id')->on('old_antibodies');
            $table->unsignedInteger('research_id');
            $table->foreign('research_id')
                    ->references('id')->on('researches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('old_antibody_research');
    }
}
