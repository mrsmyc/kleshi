<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_results', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('admission_id');
            $table->foreign('admission_id')
                  ->references('id')->on('admissions')
                  ->onDelete('restrict');

            $table->boolean('antigen_te')->nullable();
            $table->boolean('ig_m_te')->nullable();
            $table->boolean('ig_g_te')->nullable();
            $table->boolean('ig_m_tb')->nullable();
            $table->boolean('ig_g_tb')->nullable();
            $table->string('titr_ig_g_te')->nullable();
            $table->string('titr_ig_g_tb')->nullable();

            //пцр вкэ
            $table->boolean('tick_pcr_vte')->nullable();
            //пцр кб
            $table->boolean('tick_pcr_tb')->nullable();
            //мэч
            $table->boolean('tick_pcr_hme')->nullable();
            //гач
            $table->boolean('tick_pcr_hga')->nullable();
            //пцр кр
            $table->boolean('tick_pcr_tr')->nullable();
            //пцр квл
            $table->boolean('tick_pcr_trf')->nullable();

            //пцр вкэ
            $table->boolean('blood_pcr_vte')->nullable();
            //пцр кб
            $table->boolean('blood_pcr_tb')->nullable();
            //пцр мэч
            $table->boolean('blood_pcr_hme')->nullable();
            //пцр гач
            $table->boolean('blood_pcr_hga')->nullable();
            //кр
            $table->boolean('blood_pcr_tr')->nullable();
            //квл
            $table->boolean('blood_pcr_trf')->nullable();

            $table->date('issue_date')->nullable();

            $table->unsignedInteger('tick_type_id')->nullable();
            $table->foreign('tick_type_id')->references('id')->on('tick_types');

            $table->unsignedInteger('tick_gender_id')->nullable();
            $table->foreign('tick_gender_id')->references('id')->on('tick_genders');

            $table->unsignedInteger('tick_condition_id')->nullable();
            $table->foreign('tick_condition_id')->references('id')->on('tick_conditions');

            $table->text('comment')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_results');
    }
}
