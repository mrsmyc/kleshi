<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admissions', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date');
            $table->time('time');

            $table->string('name');

            $table->unsignedInteger('gender_id');
            $table->foreign('gender_id')->references('id')->on('genders');

            $table->integer('age');

            $table->string('phone')->nullable();

            $table->unsignedInteger('permanent_residence_id')->nullable();
            $table->foreign('permanent_residence_id')->references('id')->on('permanent_residences');

            $table->unsignedInteger('insurance_company_id')->nullable();
            $table->foreign('insurance_company_id')
                  ->references('id')->on('insurance_companies')
                  ->onDelete('restrict');

            $table->string('policy_num')->nullable();

            $table->integer('policy_research_num')->nullable();

            $table->date('policy_start')->nullable();

            $table->date('policy_end')->nullable();

            $table->string('policy_scan')->nullable();

            $table->string('comment_insurance')->nullable();

            $table->date('bite_date')->nullable();

            $table->date('removal_date')->nullable();

            $table->unsignedInteger('removal_institution_id')->nullable();
            $table->foreign('removal_institution_id')
                  ->references('id')->on('removal_institutions');

            $table->unsignedInteger('bite_spot_id')->nullable();
            $table->foreign('bite_spot_id')
                  ->references('id')->on('bite_spots');

            $table->unsignedInteger('bite_circumstance_id')->nullable();
            $table->foreign('bite_circumstance_id')
                  ->references('id')->on('bite_circumstances');

            $table->unsignedInteger('vaccine_level_id')->nullable();
            $table->foreign('vaccine_level_id')
                  ->references('id')->on('vaccine_levels');

            $table->date('ig_date')->nullable();

            $table->integer('ig_dose')->nullable();

            $table->unsignedInteger('city_area_id')->nullable();
            $table->foreign('city_area_id')
                  ->references('id')->on('city_areas');

            $table->unsignedInteger('admin_territory_id')->nullable();
            $table->foreign('admin_territory_id')
                  ->references('id')->on('admin_territories');

            $table->unsignedInteger('locality_id')->nullable();
            $table->foreign('locality_id')
                  ->references('id')->on('localities');

            $table->string('locality_clarification')->nullable();

            $table->unsignedInteger('encephalitis_method_id')->nullable();
            $table->foreign('encephalitis_method_id')
                  ->references('id')->on('encephalitis_methods');

            $table->unsignedInteger('bacterial_method_id')->nullable();
            $table->foreign('bacterial_method_id')
                  ->references('id')->on('bacterial_methods');

            $table->date('measures_date')->nullable();

            $table->unsignedInteger('material_id')->nullable();
            $table->foreign('material_id')
                  ->references('id')->on('materials');

            $table->integer('material_amount')->nullable();

            //$table->json('research_types')->nullable();

            $table->boolean('infect_consult')->default(false);

            $table->date('consult_date')->nullable();

            $table->text('comment')->nullable();
            $table->date('refund')->nullable();

            $table->unsignedInteger('responsible_id');
            $table->foreign('responsible_id')
                  ->references('id')->on('users');

            $table->boolean('needs_attention')->default(false);
            
            $table->boolean('to_skip')->default(false);

            $table->boolean('is_reported')->default(false);

            $table->date('reported_date')->nullable();

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')
                  ->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admissions');
    }
}
