<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTickResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tick_results', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('admission_id');
            $table->foreign('admission_id')
                  ->references('id')->on('admissions')
                  ->onDelete('restrict');

            //пцр вкэ
            $table->boolean('tick_pcr_vte')->nullable();
            //пцр кб
            $table->boolean('tick_pcr_tb')->nullable();
            //мэч
            $table->boolean('tick_pcr_hme')->nullable();
            //гач
            $table->boolean('tick_pcr_hga')->nullable();
            //пцр кр
            $table->boolean('tick_pcr_tr')->nullable();
            //пцр квл
            $table->boolean('tick_pcr_trf')->nullable();

            $table->unsignedInteger('tick_type_id')->nullable();
            $table->foreign('tick_type_id')->references('id')->on('tick_types');

            $table->unsignedInteger('tick_gender_id')->nullable();
            $table->foreign('tick_gender_id')->references('id')->on('tick_genders');

            $table->unsignedInteger('tick_condition_id')->nullable();
            $table->foreign('tick_condition_id')->references('id')->on('tick_conditions');

            $table->timestamps();

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tick_results');
    }
}
