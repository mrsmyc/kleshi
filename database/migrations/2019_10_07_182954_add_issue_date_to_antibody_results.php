<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIssueDateToAntibodyResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('antibody_results', function (Blueprint $table) {
            $table->date('issue_date')->nullable()->after('blood_pcr_tr');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antibody_results', function (Blueprint $table) {
            $table->dropColumn('issue_date');
        });
    }
}
