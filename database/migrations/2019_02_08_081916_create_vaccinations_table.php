<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaccinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccinations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');

            $table->unsignedInteger('payment_type_id')->nullable();
            $table->foreign('payment_type_id')
                  ->references('id')->on('payment_types');

            $table->unsignedInteger('vaccination_agreement_id')->nullable();
            $table->foreign('vaccination_agreement_id')
                  ->references('id')->on('vaccination_agreements')
                  ->onDelete('restrict');

            $table->string('phone')->nullable();
            $table->string('name');
            $table->integer('age');
            $table->string('address')->nullable();
            $table->boolean('organized')->default(true);
            $table->string('occupation_place')->nullable();

            $table->unsignedInteger('vaccine_id')->nullable();
            $table->foreign('vaccine_id')
                  ->references('id')->on('vaccines')
                  ->onDelete('restrict');

            $table->unsignedInteger('vaccine_serie_id')->nullable();
            $table->foreign('vaccine_serie_id')
                  ->references('id')->on('vaccine_series')
                  ->onDelete('restrict');

            $table->string('own_vaccine_info')->nullable();

            $table->unsignedInteger('vaccine_level_id')->nullable();
            $table->foreign('vaccine_level_id')
                  ->references('id')->on('vaccine_levels');

            $table->unsignedInteger('vaccine_spot_id')->nullable();
            $table->foreign('vaccine_spot_id')
                  ->references('id')->on('vaccine_spots');

            /*
            $table->unsignedInteger('prof_contingent_id')->nullable();
            $table->foreign('prof_contingent_id')
                  ->references('id')->on('prof_contingents');

            $table->unsignedInteger('risk_group_id')->nullable();
            $table->foreign('risk_group_id')
                  ->references('id')->on('risk_groups');

            $table->unsignedInteger('social_group_id')->nullable();
            $table->foreign('social_group_id')
                  ->references('id')->on('social_groups');
            */

            $table->unsignedInteger('group_id')->nullable();
            $table->foreign('group_id')
                  ->references('id')->on('groups');

            $table->text('comment')->nullable();

            $table->unsignedInteger('responsible_id');
            $table->foreign('responsible_id')
                  ->references('id')->on('users');

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')
                  ->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaccinations');
    }
}
