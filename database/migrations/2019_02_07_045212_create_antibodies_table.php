<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntibodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antibodies', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('admission_id')->nullable();
            $table->foreign('admission_id')
                  ->references('id')->on('admissions')
                  ->onDelete('restrict');

            $table->date('date');

            $table->string('name');

            $table->integer('age');

            $table->string('phone');

            $table->unsignedInteger('insurance_company_id')->nullable();
            $table->foreign('insurance_company_id')
                  ->references('id')->on('insurance_companies')
                  ->onDelete('restrict');

            $table->string('policy_num')->nullable();

            $table->date('policy_start')->nullable();

            $table->date('policy_end')->nullable();

            $table->string('policy_scan')->nullable();

            $table->text('comment')->nullable();

            $table->json('research_types')->nullable();

            $table->boolean('infect_consult')->default(false);

            $table->unsignedInteger('responsible_id');
            $table->foreign('responsible_id')
                  ->references('id')->on('users');

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')
                  ->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antibodies');
    }
}
