<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAskoFieldsToAntibodiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('antibodies', function (Blueprint $table) {
            $table->string('asko_ic_name')->nullable()->after('insurance_company_id');
            $table->string('asko_immunocard')->nullable()->after('asko_ic_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antibodies', function (Blueprint $table) {
            $table->dropColumn('asko_ic_name');
            $table->dropColumn('asko_immunocard');
        });
    }
}
