<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAntibodyResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antibody_research', function (Blueprint $table) {
            $table->unsignedInteger('antibody_id');
            $table->foreign('antibody_id')
                    ->references('id')->on('antibodies');
            $table->unsignedInteger('research_id');
            $table->foreign('research_id')
                    ->references('id')->on('researches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('antibody_research');
    }
}
