<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaccineSeriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccine_series', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('vaccine_id');
            $table->foreign('vaccine_id')
                  ->references('id')->on('vaccines');

            $table->string('name');
            $table->date('valid_to');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaccine_series');
    }
}
