<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReadyToNotifyToAntibodyResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('antibody_results', function (Blueprint $table) {
            $table->boolean('ready_to_notify')->nullable()->after('comment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antibody_results', function (Blueprint $table) {
            $table->dropColumn('ready_to_notify');
        });
    }
}
