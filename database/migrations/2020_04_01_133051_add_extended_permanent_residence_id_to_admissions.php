<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtendedPermanentResidenceIdToAdmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->unsignedInteger('extended_permanent_residence_id')->nullable()->after('permanent_residence_id');
            $table->foreign('extended_permanent_residence_id')
                  ->references('id')->on('residences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->dropForeign('admissions_extended_permanent_residence_id_foreign');
            $table->dropColumn('extended_permanent_residence_id');
        });
    }
}
