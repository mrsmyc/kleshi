<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVaccinationAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vaccination_agreements', function (Blueprint $table) {
            $table->increments('id');

            $table->date('date');

            $table->string('company_name');

            $table->integer('vaccine_amount');
            
            $table->unsignedInteger('payment_type_id')->nullable();
            $table->foreign('payment_type_id')
                  ->references('id')->on('payment_types');

            $table->unsignedInteger('vaccine_id')->nullable();
            $table->foreign('vaccine_id')
                  ->references('id')->on('vaccines');

            $table->unsignedInteger('vaccine_serie_id')->nullable();
            $table->foreign('vaccine_serie_id')
                  ->references('id')->on('vaccine_series');

            $table->unsignedInteger('vaccine_spot_id')->nullable();
            $table->foreign('vaccine_spot_id')
                  ->references('id')->on('vaccine_spots');

            /*
            $table->unsignedInteger('prof_contingent_id')->nullable();
            $table->foreign('prof_contingent_id')
                  ->references('id')->on('prof_contingents');
            */

            $table->unsignedInteger('group_id')->nullable();
            $table->foreign('group_id')
                  ->references('id')->on('groups');

            $table->string('comment')->nullable();

            $table->timestamps();

            $table->unsignedInteger('created_by')->nullable();
            $table->foreign('created_by')
                  ->references('id')->on('users');

            $table->unsignedInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')
                  ->references('id')->on('users');

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vaccination_agreements');
    }
}
