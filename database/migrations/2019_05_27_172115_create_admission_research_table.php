<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdmissionResearchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admission_research', function (Blueprint $table) {
            $table->unsignedInteger('admission_id');
            $table->foreign('admission_id')
                    ->references('id')->on('admissions');
            $table->unsignedInteger('research_id');
            $table->foreign('research_id')
                    ->references('id')->on('researches');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admission_research');
    }
}
