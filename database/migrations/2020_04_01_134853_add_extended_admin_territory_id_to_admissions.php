<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExtendedAdminTerritoryIdToAdmissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->unsignedInteger('extended_admin_territory_id')->nullable()->after('admin_territory_id');
            $table->foreign('extended_admin_territory_id')
                  ->references('id')->on('residences');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('admissions', function (Blueprint $table) {
            $table->dropForeign('admissions_extended_admin_territory_id_foreign');
            $table->dropColumn('extended_admin_territory_id');
        });
    }
}
