<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldAntibodyResult extends Model
{
    protected $guarded = ['id'];
	
    public function antibody()
    {
    	return $this->belongsTo(OldAntibody::class, 'antibody_id', 'id');
    }
}
