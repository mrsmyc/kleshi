<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Antibody extends Model
{
	//protected $guarded = ['id'];
	protected $guarded = [];

	public function admission()
	{
		return $this->belongsTo(Admission::class);
	}

	public function result()
	{
		return $this->hasOne(AntibodyResult::class);
	}

	public function sms_notifications()
    {
        return $this->hasMany(SmsNotification::class);
    }

    public function researches()
    {
        return $this->belongsToMany(Research::class);
    }

    public function responsible()
    {
    	return $this->belongsTo(User::class);
    }

    public function insurance_company()
    {
    	return $this->belongsTo(InsuranceCompany::class);
    }

    public function manageNotification()
    {
        if ($this->result->sms_notification && in_array($this->result->sms_notification->status, ['sent', 'failed'])) {
            return;
        }

        $phone = $this->phone;
        if ($phone) {
            $researches = ['antigen_te', 'ig_m_te', 'ig_g_te', 'ig_m_tb', 'ig_g_tb', 'blood_pcr_vte', 'blood_pcr_tb', 'blood_pcr_hme', 'blood_pcr_hga', 'blood_pcr_tr', 'blood_pcr_trf'];

            $text = "По проведённым анализам возбудители инфекций НЕ обнаружены.";
            $researchResult = false;

            foreach ($researches as $research) {
                if ($this->result->$research == 1) {
                    $text = "Анализы готовы. ОБНАРУЖЕНЫ возбудители. Нужна консультация врача.";
                    $researchResult = true;
                    break;
                }
            }

            $this->result->sms_notification()->updateOrCreate(['owner_id' => $this->result->id],
            [
                'text' => $text,
                'research_result' => $researchResult,
                'phone' => $phone,
                'status' => 'new',
                'type' => 'result',
            ]);
        }

        if ($this->result->ready_to_notify == 1) {
            $status = 'new';
        } else {
            $status = 'cancelled';
        }

        $this->result->sms_notification()->update([
            'status' => $status,
        ]);
            
        return;
    }

    public function setYearId()
    {
        $year = Carbon::parse($this->date)->format('Y');

        if ($prev = Antibody::whereYear('date', $year)->orderBy('year_id', 'DESC')->first()) {
            
            $yearId = $prev->year_id + 1;

        } else {

            $yearId = 1;

        }

        $this->update([

            'year_id' => $yearId,

        ]);
    }
}
