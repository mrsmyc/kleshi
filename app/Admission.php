<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;

class Admission extends Model
{
    //protected $guarded = ['id'];
    protected $guarded = [];

    protected $appends = ['full_location'];

    public function result()
    {
    	return $this->hasOne(AdmissionResult::class);
    }

    public function tick_results()
    {
        return $this->hasMany(TickResult::class);
    }

    public function gender()
    {
    	return $this->belongsTo(Gender::class);
    }

    public function permanent_residence()
    {
    	return $this->belongsTo(PermanentResidence::class);
    }

    public function extended_permanent_residence()
    {
        return $this->belongsTo(Residence::class);
    }

    public function insurance_company()
    {
    	return $this->belongsTo(InsuranceCompany::class);
    }

    public function removal_institution()
    {
    	return $this->belongsTo(RemovalInstitution::class);
    }

    public function bite_spot()
    {
    	return $this->belongsTo(BiteSpot::class);
    }

    public function bite_circumstance()
    {
    	return $this->belongsTo(BiteCircumstance::class);
    }

    public function vaccine_level()
    {
    	return $this->belongsTo(VaccineLevel::class);
    }

    public function admin_territory()
    {
    	return $this->belongsTo(AdminTerritory::class);
    }

    public function extended_admin_territory()
    {
        return $this->belongsTo(Residence::class);
    }

    public function city_area()
    {
    	return $this->belongsTo(CityArea::class);
    }

    public function locality()
    {
    	return $this->belongsTo(Locality::class);
    }

    public function child_care_facility()
    {
        return $this->belongsTo(ChildCareFacility::class);
    }

    public function encephalitis_method()
    {
    	return $this->belongsTo(EncephalitisMethod::class);
    }

    public function bacterial_method()
    {
    	return $this->belongsTo(BacterialMethod::class);
    }

    public function material()
    {
    	return $this->belongsTo(Material::class);
    }

    public function responsible()
    {
        return $this->belongsTo(User::class);
    }

    /*
    public function research_type()
    {
    	return $this->belongsTo(ResearchType::class);
    }
    */

    public function manager()
    {
    	return $this->belongsTo(User::class);
    }

    public function antibody()
    {
        return $this->hasOne(Antibody::class);
    }

    public function sms_notifications()
    {
        return $this->hasMany(SmsNotification::class);
    }

    public function managePolicy()
    {
        if (!request()->has('uploaded_policy_scan')) {

            $image = NULL;

            if ($this->policy_scan) {

                Storage::delete($this->policy_scan);

            }

            if (request()->hasFile('policy_scan')) {

                $file = request()->file('policy_scan');

                $image = $file->store('public/policy_scans');

            }

            $this->update([

                'policy_scan' => $image,

            ]);

        }

        return;
    }

    public function researches()
    {
        return $this->belongsToMany(Research::class);
    }

    public function getFullLocationAttribute()
    {
        $location = [];

        if ($this->admin_territory) {
            $location[] = $this->admin_territory->name;
        }

        if ($this->location) {
            $location[] = $this->location->name;
        }

        if ($this->location_clarification) {
            $location[] = $this->location_clarification;
        }

        return implode(' ,', $location);
    }

    public function manageNotification()
    {
        if ($this->result->sms_notification && in_array($this->result->sms_notification->status, ['sent', 'failed'])) {
            return;
        }

        $phone = $this->phone;
        if ($phone) {
            $researches = ['antigen_te', 'ig_m_te', 'ig_g_te', 'ig_m_tb', 'ig_g_tb', 'blood_pcr_vte', 'blood_pcr_tb', 'blood_pcr_hme', 'blood_pcr_hga', 'blood_pcr_tr', 'blood_pcr_trf'];

            $tickResearches = ['tick_pcr_vte', 'tick_pcr_tb', 'tick_pcr_hme', 'tick_pcr_hga', 'tick_pcr_tr', 'tick_pcr_trf'];

            $text = "По проведённым анализам №" . $this->year_id . " возбудители инфекций НЕ обнаружены.";
            $researchResult = false;

            foreach ($researches as $research) {
                if ($this->result->$research == 1) {
                    $text = "Анализ №" . $this->year_id . " готов. ОБНАРУЖЕНЫ возбудители. Нужна консультация врача.";
                    $researchResult = true;
                    break;
                }
            }

            foreach ($tickResearches as $research) {
                if ($this->tick_results()->where($research, 1)->count() > 0) {
                    $text = "Анализ №" . $this->year_id . " готов. ОБНАРУЖЕНЫ возбудители. Нужна консультация врача.";
                    $researchResult = true;
                    break;
                }
            }

            $this->result->sms_notification()->updateOrCreate(['owner_id' => $this->result->id],
            [
                'text' => $text,
                'research_result' => $researchResult,
                'phone' => $phone,
                'status' => 'new',
                'type' => 'result',
            ]);
        }

        if ($this->result->ready_to_notify == 1) {
            $status = 'new';
        } else {
            $status = 'cancelled';
        }

        $this->result->sms_notification()->update([
            'status' => $status,
        ]);
            
        return;
    }

    public function setYearId()
    {
        $year = Carbon::parse($this->date)->format('Y');

        if ($prev = Admission::whereYear('date', $year)->orderBy('year_id', 'DESC')->first()) {
            
            $yearId = $prev->year_id + 1;

        } else {

            $yearId = 1;

        }

        $this->update([

            'year_id' => $yearId,

        ]);
    }
}
