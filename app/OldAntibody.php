<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OldAntibody extends Model
{
    //protected $guarded = ['id'];
    protected $guarded = [];

	public function result()
	{
		return $this->hasOne(OldAntibodyResult::class, 'antibody_id', 'id');
	}

	public function researches()
    {
        return $this->belongsToMany(Research::class);
    }

    public function responsible()
    {
    	return $this->belongsTo(User::class);
    }
}
