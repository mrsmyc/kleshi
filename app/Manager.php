<?php

namespace App;

//use App\Scopes\ManagerUserScope;

class Manager extends User {

    /**
     * The "booting" method of the model.
     *
     * @return void
     */

    protected $table = 'users';

    protected static function boot()
    {
        parent::boot();

        //static::addGlobalScope(new ManagerUserScope);
        
        static::addGlobalScope(function ($query) {
            $query->whereHas('roles', function($roleQuery) {
                $roleQuery->where('roles.name', '=','manager');
            });
        });
    }

    /**
    *  Relationship: Orders (Has many)
    */

    public function roles() {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id');
    }

    public function admissions()
    {
        return $this->hasMany(Admission::class, 'responsible_id');
    }
    
}