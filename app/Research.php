<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    public function admissions()
    {
        return $this->belongsToMany(Admission::class);
    }

    public function antibodies()
    {
        return $this->belongsToMany(Antibody::class);
    }

    public function old_antibodies()
    {
        return $this->belongsToMany(OldAntibody::class);
    }

    public function price()
    {
    	return $this->belongsTo(Price::class);
    }
}
