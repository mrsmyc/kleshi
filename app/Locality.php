<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locality extends Model
{
    public function admin_territory()
    {
    	return $this->belongsTo(AdminTerritory::class);
    }
}
