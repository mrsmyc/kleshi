<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminTerritory extends Model
    public function localities()
    {
    	return $this->hasMany(Locality::class);
    }
}
