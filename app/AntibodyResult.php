<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AntibodyResult extends Model
{
	protected $guarded = ['id'];
	
    public function antibody()
    {
    	return $this->belongsTo(Antibody::class);
    }

    public function sms_notification()
    {
    	return $this->morphOne(SmsNotification::class, 'owner');
    }

    public function manageNotification()
    {
    	$phone = $this->antibody->phone;
	    if ($phone) {
	    	$researches = ['ig_m_te', 'ig_g_te', 'ig_m_tb', 'ig_g_tb', 'blood_pcr_vte', 'blood_pcr_tb', 'blood_pcr_hme', 'blood_pcr_hga', 'blood_pcr_trf', 'blood_pcr_tr'];

	    	$text = "По проведённым анализам возбудители НЕ обнаружены";
	    	$researchResult = false;

	    	foreach ($researches as $research) {
	    		if ($this->$research == 1) {
	    			$text = "Анализы готовы. ОБНАРУЖЕНЫ возбудители. Нужна консультация врача";
	    			$researchResult = true;
	    			break;
	    		}
	    	}

	    	$this->sms_notification()->updateOrCreate(['owner_id' => $this->id],
	    	[
	    		'text' => $text,
	    		'research_result' => $researchResult,
	    		'phone' => $phone,
	    		'status' => 'new',
	    		'type' => 'result',
	    	]);
	    }

	    return;
    }
}
