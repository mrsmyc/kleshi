<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VaccinationAgreement extends Model
{
	protected $guarded = ['id'];

	protected $appends = ['is_finished'];

    public function vaccinations()
    {
    	return $this->hasMany(Vaccination::class);
    }

    public function vaccine()
    {
    	return $this->belongsTo(Vaccine::class);
    }

    public function getIsFinishedAttribute()
    {
        if ($this->vaccinations->count() < $this->vaccine_amount)
        	return false;

        return true;
    }
}
