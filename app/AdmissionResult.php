<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdmissionResult extends Model
{
	protected $guarded = ['id'];
	
    public function admission()
    {
    	return $this->belongsTo(Admission::class);
    }

    public function sms_notification()
    {
    	return $this->morphOne(SmsNotification::class, 'owner');
    }

    public function tick_type()
    {
    	return $this->belongsTo(TickType::class);
    }

    public function tick_gender()
    {
    	return $this->belongsTo(TickGender::class);
    }

    public function tick_condition()
    {
    	return $this->belongsTo(TickCondition::class);
    }
}
