<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsNotification extends Model
{
    protected $guarded = ['id'];

    /*public function recipient() {

        return $this->morphTo();

    }*/

    public function owner() {

        return $this->morphTo();

    }

    public function send()
    {
    	$smsService = resolve('App\SMSru');

    	$data = new \stdClass();

		$data->to = $this->phone;

		$data->text = $this->text; // Текст сообщения

		$data->from = 'StopKlesh';

		//$data->test = 1;
		
		$sms = $smsService->send_one($data);

		if ($sms->status_code == '100') {

			$this->status = 'sent';

			$this->smsru_id = $sms->sms_id;

			$this->save();

			$data = [

				'done' => true,

				'message' => $sms->status,

			];

		} else {

			$data = [

				'done' => false,

				'message' => $sms->status,

			];
		}

		return $data;
    }
}
