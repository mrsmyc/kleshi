<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Carbon\Carbon;

use App\Admission;

use App\Antibody;

use App\AdminTerritory;

class ExportController extends Controller
{
    public function index()
    {
        return view('export.index');

        //$adminTerritories = AdminTerritory::all();

        //return view('export.index', compact('adminTerritories'));
    }

    public function export_data()
    {
        ini_set('memory_limit','512M');

        ini_set('max_execution_time','300');

        $dateFrom = request()->date_from;

        $dateTo = request()->date_to;

        $type = request()->export_type;

        $func = 'export_' . $type . '_data';

        $fileName = $this->$func($dateFrom, $dateTo);

        return response()->download($fileName)->deleteFileAfterSend();
    }

    public function gg()
    {
        /*
        $a = Admission::where('material_id', '1')
        ->whereHas('result', function($query) {
            $query->whereNotNull('blood_pcr_vte')
            ->orWhereNotNull('blood_pcr_tb')
            ->orWhereNotNull('blood_pcr_hme')
            ->orWhereNotNull('blood_pcr_hga')
            ->orWhereNotNull('blood_pcr_tr')
            ->orWhereNotNull('blood_pcr_trf');
        })
        ->pluck('id');
        dd($a);

        $a = Admission::where('material_id', '1')
        ->whereHas('result', function($query) {
            $query->whereNull('tick_pcr_vte')
            ->WhereNull('tick_pcr_tb')
            ->WhereNull('tick_pcr_hme')
            ->WhereNull('tick_pcr_hga')
            ->WhereNull('tick_pcr_tr')
            ->WhereNull('tick_pcr_trf');
        })
        ->pluck('id');
        foreach($a as $b) {
            echo $b . "</br>";
        }
        dd($a);


        $a = Admission::where('material_id', '1')
        ->whereHas('result', function($query) {
            $query->whereNotNull('antigen_te');
        })
        ->pluck('id');
        foreach($a as $b) {
            echo $b . "</br>";
        }
        dd();

        */
    }

    private function export_admission_data($dateFrom, $dateTo)
    {
        //$adminTerritoryId = request()->admin_territory_id;
        
        $spreadsheet = new Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setTitle('Выгрузка данных');

        $columnNames = [
        	[
        		'name' => 'ID',
        		'function' => 'getYearId',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Дата и время',
        		'function' => 'getDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'ФИО',
        		'function' => 'getName',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Пол',
        		'function' => 'getGender',
        		'type' => 'general',
        	],
            [
                'name' => 'Возраст',
                'function' => 'getAge',
                'type' => 'general',
            ],
        	[
        		'name' => 'ПМЖ',
        		'function' => 'getPermanentResidence',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Сведения о вакцинации',
        		'function' => 'getVaccineLevel',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Страховая компания',
        		'function' => 'getInsuranceCompany',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Номер полиса',
        		'function' => 'getPolicyNum',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Срок действия',
        		'function' => 'getPolicyValidity',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Дата укуса',
        		'function' => 'getBiteDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Дата удаления',
        		'function' => 'getRemovalDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Учреждение, удалившее клеща',
        		'function' => 'getRemovalInstitution',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Место укуса',
        		'function' => 'getBiteSpot',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Обстоятельства',
        		'function' => 'getBiteCircumstance',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Админ. территория',
        		'function' => 'getAdminTerritory',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Местность',
        		'function' => 'getLocality',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Уточнение местности',
        		'function' => 'getLocalityClarification',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Консультация инфекциониста (дата)',
        		'function' => 'getConsultDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Ig (доз)',
        		'function' => 'getIgDose',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Ig (дата)',
        		'function' => 'getIgDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Проф. КЭ',
        		'function' => 'getEncephalitisMethod',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Проф. БИ',
        		'function' => 'getBacterialMethod',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Дата применения мер',
        		'function' => 'getMeasuresDate',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Материал',
        		'function' => 'getMaterial',
        		'type' => 'general',
        	],
        	[
        		'name' => 'Кол-во',
        		'function' => 'getMaterialAmount',
        		'type' => 'general',
        	],
            [
                'name' => 'Примечание',
                'function' => 'getComment',
                'type' => 'general',
            ],
        	[
        		'name' => 'Клещ КЭ',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_vte',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Клещ КБ',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_tb',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Клещ МЭЧ',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_hme',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Клещ ГАЧ',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_hga',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Клещ КР',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_tr',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Клещ КВЛ',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_pcr_trf',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Вид клеща',
        		'function' => 'getTickType',
        		'param_name' => 'tick_type_id',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Пол клеща',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_gender_id',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Состояние клеща',
        		'function' => 'getTickInfo',
        		'param_name' => 'tick_condition_id',
        		'type' => 'tick_result',
        	],
        	[
        		'name' => 'Антиген КЭ',
        		'param_name' => 'antigen_te',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь КЭ',
        		'param_name' => 'blood_pcr_vte',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь КБ',
        		'param_name' => 'blood_pcr_tb',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь МЭЧ',
        		'param_name' => 'blood_pcr_hme',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь ГАЧ',
        		'param_name' => 'blood_pcr_hga',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь КР',
        		'param_name' => 'blood_pcr_tr',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Кровь КВЛ',
        		'param_name' => 'blood_pcr_trf',
        		'type' => 'blood_result',
        	],

        	[
        		'name' => 'Ig M КЭ',
        		'function' => 'getBlood',
        		'param_name' => 'ig_m_te',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Ig G КЭ',
        		'function' => 'getBlood',
        		'param_name' => 'ig_g_te',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Титр Ig G КЭ',
        		'param_name' => 'titr_ig_g_te',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Ig M КБ',
        		'param_name' => 'ig_m_tb',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Ig G КБ',
        		'param_name' => 'ig_g_tb',
        		'type' => 'blood_result',
        	],
        	[
        		'name' => 'Титр Ig G КБ',
        		'param_name' => 'titr_ig_g_tb',
        		'type' => 'blood_result',
        	],
            [
                'name' => 'Примечание (лаб.)',
                'function' => 'getResultComment',
                'type' => 'general',
            ],
        ];

        foreach ($columnNames as $key => $value) {

        	$column = $this->getColumnFromNum($key + 1);

        	$cell = $column . '1';

        	$worksheet->setCellValue($cell, $value['name']);

        	$worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('32CD32');

        	$worksheet->getColumnDimension($column)->setAutoSize(true);

        }

        $worksheet->setAutoFilter('A1:' . $cell);

		$row = 1;

		$height = 0;

        /*
        $admissions = Admission::whereDate('date', '>=', $dateFrom)
                        ->whereDate('date', '<=', $dateTo)
                        ->when($adminTerritoryId, function ($query) use ($adminTerritoryId){
                            return $query->where('admin_territory_id', $adminTerritoryId);
                        });
        */

        $admissions = Admission::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);

		foreach ($admissions->cursor() as $admission) {
 			
 			$row = $row + $height + 1;

 			if ($admission->material_id == 1) {

        		$height = $admission->material_amount - 1;

        	} else {

        		$height = 0;

        	}
        	
        	foreach ($columnNames as $key => $value) {

        		$subRow = $row;

        		$column = $this->getColumnFromNum($key + 1);

        		switch ($value['type']) {
        			case 'general':
        				$func = $value['function'];

        				if ($height > 0) {

		        			$worksheet->mergeCells($column . $row . ':' . $column . ($row + $height));

		        		}

		        		$worksheet->setCellValue($column . $row, $this->$func($admission));

        				break;
        			case 'tick_result':
        				foreach ($admission->tick_results as $tickResult) {

        					$param = $value['param_name'];

        					$worksheet->setCellValue($column . $subRow, $this->getTickResult($tickResult, $param));

        					$subRow++;
        				}
        				break;
        			case 'blood_result':
        				if ($height > 0) {

		        			$worksheet->mergeCells($column . $row . ':' . $column . ($row + $height));

		        		}
        				$worksheet->setCellValue($column . $row, $this->getBloodResult($admission, $value['param_name']));
        				break;
        		}

	        }
		}

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Выгрузка данных (ПП).xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    private function export_antibody_data($dateFrom, $dateTo)
    {
        $spreadsheet = new Spreadsheet();

        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->setTitle('Выгрузка данных');
        
        $columnNames = [
            [
                'name' => 'ID',
                'function' => 'getId',
                'type' => 'general',
            ],
            [
                'name' => 'ID п/п',
                'function' => 'getRegId',
                'type' => 'general',
            ],
            [
                'name' => 'Дата',
                'function' => 'getDate',
                'type' => 'general',
            ],
            [
                'name' => 'ФИО',
                'function' => 'getName',
                'type' => 'general',
            ],
            [
                'name' => 'Сведения о вакцинации',
                'function' => 'getAntibodyVaccineLevel',
                'type' => 'general',
            ],
            [
                'name' => 'Страховая компания',
                'function' => 'getInsuranceCompany',
                'type' => 'general',
            ],
            [
                'name' => 'Номер полиса',
                'function' => 'getPolicyNum',
                'type' => 'general',
            ],
            [
                'name' => 'Срок действия',
                'function' => 'getPolicyValidity',
                'type' => 'general',
            ],
            [
                'name' => 'Дата укуса',
                'function' => 'getAntibodyBiteDate',
                'type' => 'general',
            ],
            [
                'name' => 'Консультация инфекциониста (дата)',
                'function' => 'getConsultDate',
                'type' => 'general',
            ],
            [
                'name' => 'Примечание',
                'function' => 'getComment',
                'type' => 'general',
            ],
            [
                'name' => 'Кровь КЭ',
                'param_name' => 'blood_pcr_vte',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Кровь КБ',
                'param_name' => 'blood_pcr_tb',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Кровь МЭЧ',
                'param_name' => 'blood_pcr_hme',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Кровь ГАЧ',
                'param_name' => 'blood_pcr_hga',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Кровь КР',
                'param_name' => 'blood_pcr_tr',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Кровь КВЛ',
                'param_name' => 'blood_pcr_trf',
                'type' => 'blood_result',
            ],

            [
                'name' => 'Ig M КЭ',
                'function' => 'getBlood',
                'param_name' => 'ig_m_te',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Ig G КЭ',
                'function' => 'getBlood',
                'param_name' => 'ig_g_te',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Титр Ig G КЭ',
                'param_name' => 'titr_ig_g_te',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Ig M КБ',
                'param_name' => 'ig_m_tb',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Ig G КБ',
                'param_name' => 'ig_g_tb',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Титр Ig G КБ',
                'param_name' => 'titr_ig_g_tb',
                'type' => 'blood_result',
            ],
            [
                'name' => 'Примечание (лаб.)',
                'function' => 'getResultComment',
                'type' => 'general',
            ],
        ];

        foreach ($columnNames as $key => $value) {

            $column = $this->getColumnFromNum($key + 1);

            $cell = $column . '1';

            $worksheet->setCellValue($cell, $value['name']);

            $worksheet->getStyle($cell)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('32CD32');

            $worksheet->getColumnDimension($column)->setAutoSize(true);

        }

        $worksheet->setAutoFilter('A1:' . $cell);

        $row = 1;

        foreach (Antibody::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo)->cursor() as $antibody) {
            
            $row++;
            
            foreach ($columnNames as $key => $value) {

                $subRow = $row;

                $column = $this->getColumnFromNum($key + 1);

                switch ($value['type']) {
                    case 'general':
                        $func = $value['function'];

                        $worksheet->setCellValue($column . $row, $this->$func($antibody));

                        break;
                    case 'blood_result':

                        $worksheet->setCellValue($column . $row, $this->getBloodResult($antibody, $value['param_name']));

                        break;
                }

            }
        }

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Выгрузка данных (Антитела).xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    private function getId($item)
	{
		return $item->id;
	}

    private function getYearId($item)
    {
        return $item->year_id;
    }

    private function getRegId($item)
    {
        return $item->admission->id;
    }

	private function getDate($item)
	{
		//return Carbon::parse($admission->date)->format('d/m/Y') . ' ' . Carbon::parse($admission->time)->format('H:i');
        return Carbon::parse($item->date)->format('d/m/Y');
	}

	private function getName($item)
	{
		return $item->name;
	}

    private function getAge($item)
    {
        return $item->age;
    }

	private function getGender($item)
	{
		return $item->gender->name;
	}

	private function getPermanentResidence($item)
	{
		//return $item->permanent_residence->name;
        if ($item->extended_permanent_residence)
            return $item->extended_permanent_residence->name;
	}

	private function getVaccineLevel($admission)
	{
		if ($admission->vaccine_level)
			return $admission->vaccine_level->name;
	}

    private function getAntibodyVaccineLevel($antibody)
    {
        if ($antibody->admission->vaccine_level)
            return $antibody->admission->vaccine_level->name;
    }

	private function getInsuranceCompany($item)
	{
		if ($item->insurance_company)
			return $item->insurance_company->name;
	}

	private function getPolicyNum($item)
	{
		return $item->policy_num;
	}

	private function getPolicyValidity($item)
	{
		if ($item->insurance_company)
			return Carbon::parse($item->policy_start)->format('d/m/Y') . ' – ' . Carbon::parse($item->policy_end)->format('d/m/Y');
	}

	private function getBiteDate($admission)
	{
        if ($admission->bite_date)
		  return Carbon::parse($admission->bite_date)->format('d/m/Y');
	}

    private function getAntibodyBiteDate($antibody)
    {
        if ($antibody->admission->bite_date)
            return Carbon::parse($antibody->admission->bite_date)->format('d/m/Y');
    }

	private function getRemovalDate($admission)
	{
		if ($admission->removal_date)
			return Carbon::parse($admission->removal_date)->format('d/m/Y');
	}

	private function getRemovalInstitution($admission)
	{
		if ($admission->removal_institution)
			return $admission->removal_institution->name;
	}

	private function getBiteSpot($admission)
	{
		if ($admission->bite_spot)
			return $admission->bite_spot->name;
	}

	private function getBiteCircumstance($admission)
	{
		if ($admission->bite_circumstance)
			return $admission->bite_circumstance->name;
	}

	private function getAdminTerritory($admission)
	{
		/*
        if ($admission->admin_territory) 
			return $admission->admin_territory->name;
        */
        if ($admission->extended_admin_territory) 
            return $admission->extended_admin_territory->name;
	}

	private function getLocality($admission)
	{
		if ($admission->locality) 
			return $admission->locality->name;
	}

	private function getLocalityClarification($admission)
	{
		return $admission->locality_clarification;
	}

	private function getConsultDate($item)
	{
		if ($item->consult_date)
			return Carbon::parse($item->consult_date)->format('d/m/Y');
	}

	private function getIgDose($admission)
	{
		return $admission->ig_dose;
	}

	private function getIgDate($admission)
	{
		if ($admission->ig_date)
			return Carbon::parse($admission->ig_date)->format('d/m/Y');
	}

	private function getEncephalitisMethod($admission)
	{
		if ($admission->encephalitis_method) 
			return $admission->encephalitis_method->name;
	}

	private function getBacterialMethod($admission)
	{
		if ($admission->bacterial_method) 
			return $admission->bacterial_method->name;
	}

	private function getMeasuresDate($admission)
	{
		if ($admission->measures_date)
			return Carbon::parse($admission->measures_date)->format('d/m/Y');
	}

	private function getMaterial($admission)
	{
		if ($admission->material) 
			return $admission->material->name;
	}

	private function getMaterialAmount($admission)
	{
		return $admission->material_amount;
	}

	private function getTickResult($tickResult, $param)
	{
		if (strpos($param, '_id')) {
			$param = substr($param, 0, -3);

				if ($tickResult->$param)
					return $tickResult->$param->name;

		} else {
			switch ($tickResult->$param) {
				case '0':
					return 'отриц';
				case '1':
					return 'полож';
			}
		}
	}

	private function getBloodResult($item, $param)
	{
		switch ($item->result->$param) {
			case '0':
				return 'отриц';
			case '1':
				return 'полож';
			default:
				return $item->result->$param;
		}
	}

    private function getComment($item)
    {
        return $item->comment;
    }

    private function getResultComment($item)
    {
        return $item->result->comment;
    }


    private function getColumnFromNum($num)
    {
	    $numeric = ($num - 1) % 26;
	    $letter = chr(65 + $numeric);
	    $num2 = intval(($num - 1) / 26);
	    if ($num2 > 0) {
	        return $this->getColumnFromNum($num2) . $letter;
	    } else {
	        return $letter;
	    }
    }
}
