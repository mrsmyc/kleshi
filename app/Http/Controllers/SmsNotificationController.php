<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SmsNotification;

class SmsNotificationController extends Controller
{
    public function index()
    {
    	$notificationsCount = SmsNotification::where('status', 'new')->count();

    	$smsService = resolve('App\SMSru');

    	$balance = $smsService->getBalance()->balance;

    	return view('sms_notifications.index', compact('balance', 'notificationsCount'));
    }

    /*
    public function send()
    {
    	$notifications = SmsNotification::where('status', 'new')->get();

    	foreach ($notifications as $notification) {
    		$notification->send();
    	}

    	return redirect('sms_notifications');
    }
    */

    public function send()
    {
    	$notifications = SmsNotification::where('status', 'new')->get();

    	$notificationCount = $notifications->count();

    	$smsArray = [];
    	foreach ($notifications as $notification) {
    		$smsArray[$notification->phone] = $notification->text;
    	}

    	$smsService = resolve('App\SMSru');

    	$data = new \stdClass();

        $data->from = 'StopKlesh';

        //$data->test = 1;

        foreach (array_chunk($smsArray, 100, true) as $smsArrayChunked) {

            $data->multi = $smsArrayChunked;
    	
    		$request = $smsService->send($data);

    		if ($request->status == "OK") { // Запрос выполнен успешно
    		    foreach ($request->sms as $phone => $sms) { // Перебираем массив отправленных сообщений

    		    	$thisNotification = $notifications->where('phone', $phone)->first();
    		    	
    		        if ($sms->status == "OK") {
    		        	$thisNotification->status = 'sent';

    		            $thisNotification->smsru_id = $sms->sms_id;
    		        } else {

    		        	$thisNotification->status = 'failed';
    		        }

    		        $thisNotification->save();

    		    }

    		}
        }

		return redirect('sms_notifications');

    }

    
}
