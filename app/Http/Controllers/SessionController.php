<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'destroy']);
    }

    //
    public function create()
    {
    	//auth()->login();

        return view('sessions.create');

    }

    public function store()
    {
        if (!auth()->attempt(request(['login', 'password']))) {
            return back()->withErrors(['Неправильный логин и/или пароль']);
        }

        return redirect()->intended();
    }

    public function destroy()
    {
    	auth()->logout();

    	return redirect()->home();
    }
}
