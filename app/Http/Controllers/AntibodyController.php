<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admission;

use App\Antibody;

use App\InsuranceCompany;

use App\VaccineLevel;

use App\User;

use App\Research;

use Carbon\Carbon;

use App\AntibodyResult;

class AntibodyController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $antibodiesQuery = Antibody::orderBy('id', 'desc');

        $total = $antibodiesQuery->count();

        $antibodies = $antibodiesQuery->paginate(20);

        $insuranceCompanies = InsuranceCompany::all();

        return view('antibodies.index', compact('antibodies', 'insuranceCompanies', 'total'));
    }

    public function create(Admission $admission)
    {
        $insuranceCompanies = InsuranceCompany::all();

    	$vaccineLevels = VaccineLevel::all();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        //$researchTypes = ResearchType::where('owner_type', 'antibody')->get();

        $researches = Research::where('name', '<>', 'КЭ (антиген)')->get();

    	return view('antibodies.create', compact('admission', 'vaccineLevels', 'insuranceCompanies', 'managers', 'researches'));
    }

    public function store(Admission $admission, Request $request)
    {
        $antibody = $admission->antibody()->create($request->except('researches'));

        $antibody->setYearId();

        $antibody->researches()->sync($request->researches);

        $antibody->result()->create();

        return redirect('antibodies/' . $antibody->id . '/edit');
    }

    public function edit(Antibody $antibody)
    {
        $result = $antibody->result;

        //$insuranceCompanies = InsuranceCompany::all();

        $insuranceCompanies = InsuranceCompany::where('enabled', true)->orWhere('id', $antibody->insurance_company_id)->orderBy('name')->get();

        $vaccineLevels = VaccineLevel::all();

        $admission = $antibody->admission;

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        //$researchTypes = ResearchType::where('owner_type', 'antibody')->get();

        $researches = Research::where('name', '<>', 'КЭ (антиген)')->get();

        $checkedResearches = $antibody->researches->pluck('id')->toArray();

        $smsStatuses = [

            'new' => 'Ожидает отправки',

            'cancelled' => 'Отправка отменена',

            'sent' => 'Отправлено',

            'failed' => 'Отправка не удалась',
        ];

        return view('antibodies.edit', compact('antibody', 'vaccineLevels', 'insuranceCompanies', 'result', 'managers', 'researches', 'checkedResearches', 'smsStatuses'));
    }

    public function update(Antibody $antibody, Request $request)
    {
        $antibody->update($request->except('researches'));

        $antibody->researches()->sync($request->researches);

        return redirect('antibodies');
    }

    public function update_result(Antibody $antibody, Request $request)
    {
        $result = $antibody->result;

        $result->update($request->all());

        $result->manageNotification();

        return redirect('antibodies/' . $antibody->id . '/edit#results');
    }

    public function search(Request $request)
    {
        $insuranceCompanies = InsuranceCompany::all();

        $id = $request->input('id');

        $yearId = $request->input('year_id');

        $name = $request->input('name');

        $dateFrom = $request->input('date_from');

        $dateTo = $request->input('date_to');

        $insuranceCompanyId = $request->input('insurance_company_id');

        $filter = [

            'id' => $id,

            'year_id' => $yearId,

            'name' => $name,

            'date_from' => $dateFrom,

            'date_to' => $dateTo,

            'insurance_company_id' => $insuranceCompanyId,
        ];

        $antibodiesQuery = Antibody::when($id, function ($query) use ($id) {
                        return $query->where('id', $id);
                    })
                    ->when($yearId, function ($query) use ($yearId) {
                        return $query->where('year_id', $yearId)->whereYear('date', now()->year);
                    })
                    ->when($name, function ($query) use ($name){
                        return $query->orWhere('name', 'like', '%' . $name . '%');
                    })
                    ->when($insuranceCompanyId, function ($query) use ($insuranceCompanyId){
                        return $query->where('insurance_company_id', $insuranceCompanyId);
                    })
                    ->where(function($query) use ($dateFrom, $dateTo) {
                        $query->when($dateFrom, function ($q) use ($dateFrom){
                            return $q->whereDate('date', '>=', $dateFrom);
                        })
                        ->when($dateTo, function ($q) use ($dateTo){
                            return $q->whereDate('date', '<=', $dateTo);
                        });
                    })
                    ->orderBy('id', 'desc');;

        $total = $antibodiesQuery->count();

        $antibodies = $antibodiesQuery->paginate('20');

        $antibodies->appends($request->except('page'));

        return view('antibodies.index', compact('antibodies', 'filter', 'insuranceCompanies', 'total'));
    }

    public function fix_issue_date()
    {
        $antibodies = AntibodyResult::whereNull('issue_date')->get();

        foreach ($antibodies as $antibody) {
            
            $date = Carbon::parse($antibody->updated_at)->format('Y-m-d');

            $antibody->issue_date = $date;

            $antibody->save();
            
        }
    }

    public function print(Antibody $antibody)
    {
        return view('antibodies.print_forms.print', compact('antibody'));
    }

    /*
    public function fix_year_id()
    {
        $antibodies = Antibody::all();

        foreach ($antibodies as $antibody) {
            if (!$antibody->year_id) {
                $antibody->setYearId();
            }
        }
    }
    */
}
