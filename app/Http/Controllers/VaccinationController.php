<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PaymentType;

use App\Vaccine;

use App\VaccinationAgreement;

use App\VaccineLevel;

use App\VaccineSpot;

use App\VaccineSerie;

use App\ProfContingent;

use App\RiskGroup;

use App\SocialGroup;

use App\Vaccination;

use App\Group;

use App\User;

class VaccinationController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $vaccinations = Vaccination::orderBy('id', 'desc')->paginate(20);

        $vaccines = Vaccine::all();

        $vaccineLevels = VaccineLevel::where('name', '<>', 'Не вакцинирован')->get();

        $total = Vaccination::count();

        return view('vaccinations.index', compact('vaccinations', 'vaccines', 'vaccineLevels', 'total'));
    }

    public function create(VaccinationAgreement $agreement = null, Vaccination $vaccination = null)
    {
    	$paymentTypes = PaymentType::all();

        $vaccines = Vaccine::all();

        if ($agreement) {
    	   
           $vaccineSeries = $agreement->vaccine->series->where('enabled', 1);

        }

    	//кроме не вакцинирован
    	$vaccineLevels = VaccineLevel::where('name', '<>', 'Не вакцинирован')->get();

        $vaccineSpots = VaccineSpot::all();

    	/*
        $profContingents = ProfContingent::all();

    	$riskGroups = RiskGroup::all();

    	$socialGroups = SocialGroup::all();
        */

        $profContingents = Group::where('type', 'prof_contingent')->get();

        $riskGroups = Group::where('type', 'risk_group')->get();

        $socialGroups = Group::where('type', 'social_group')->get();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

    	return view('vaccinations.create', compact('agreement', 'vaccination', 'paymentTypes', 'vaccines',  'vaccineLevels', 'vaccineSpots', 'profContingents', 'riskGroups', 'socialGroups', 'managers'));
    }

    public function continue(Vaccination $vaccination)
    {
        $paymentTypes = PaymentType::all();

        $vaccines = Vaccine::all();

        //кроме не вакцинирован
        $vaccineLevels = VaccineLevel::where('name', '<>', 'Не вакцинирован')->get();

        $vaccineSpots = VaccineSpot::all();

        $vaccineSeries = $vaccination->vaccine->series->where('enabled', 1);

        /*
        $profContingents = ProfContingent::all();

        $riskGroups = RiskGroup::all();

        $socialGroups = SocialGroup::all();
        */

        $profContingents = Group::where('type', 'prof_contingent')->get();

        $riskGroups = Group::where('type', 'risk_group')->get();

        $socialGroups = Group::where('type', 'social_group')->get();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        return view('vaccinations.continue', compact('vaccination', 'paymentTypes', 'vaccines', 'vaccineSeries', 'vaccineLevels', 'vaccineSpots', 'profContingents', 'riskGroups', 'socialGroups', 'managers'));
    }

    public function store(Request $request)
    {
        $vaccination = Vaccination::create($request->all());

        if (is_null($request->vaccination_agreement_id)) {

            return redirect('vaccinations');
            
        } else {

            return redirect('vaccination_agreements/' . $vaccination->vaccination_agreement->id . '/edit');
            
        }

    }

    public function edit(Vaccination $vaccination)
    {
        $paymentTypes = PaymentType::all();

        $vaccines = Vaccine::all();

        $vaccineSeries = VaccineSerie::where(function($query) use ($vaccination) {
            $query->where('vaccine_id', $vaccination->vaccine_id)
            ->where('enabled', 1);
        })->orWhere('id', $vaccination->vaccine_serie_id)->get();

        //кроме не вакцинирован
        $vaccineLevels = VaccineLevel::where('name', '<>', 'Не вакцинирован')->get();

        $vaccineSpots = VaccineSpot::all();

        /*
        $profContingents = ProfContingent::all();

        $riskGroups = RiskGroup::all();

        $socialGroups = SocialGroup::all();
        */

        $profContingents = Group::where('type', 'prof_contingent')->get();

        $riskGroups = Group::where('type', 'risk_group')->get();

        $socialGroups = Group::where('type', 'social_group')->get();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        return view('vaccinations.edit', compact('vaccination', 'paymentTypes', 'vaccines', 'vaccineSeries', 'vaccineLevels', 'vaccineSpots', 'profContingents', 'riskGroups', 'socialGroups', 'managers'));
    }

    public function update(Vaccination $vaccination, Request $request)
    {
        $vaccination->update($request->all());

        return redirect()->back();
    }

    public function search(Request $request)
    {
        $vaccineLevels = VaccineLevel::where('name', '<>', 'Не вакцинирован')->get();

        $id = $request->input('id');

        $name = $request->input('name');

        $dateFrom = $request->input('date_from');

        $dateTo = $request->input('date_to');

        $vaccineId = $request->input('vaccine_id');

        $vaccineLevelId = $request->input('vaccine_level_id');

        $filter = [

            'id' => $id,

            'name' => $name,

            'date_from' => $dateFrom,

            'date_to' => $dateTo,

            'vaccine_id' => $vaccineId,

            'vaccine_level_id' => $vaccineLevelId,
        ];

        $vaccinationsQuery = Vaccination::when($id, function ($query) use ($id) {
                        return $query->where('id', $id);
                    })
                    ->when($name, function ($query) use ($name){
                        return $query->where('name', 'like', '%' . $name . '%');
                    })
                    ->when($vaccineId, function ($query) use ($vaccineId){
                        return $query->where('vaccine_id', $vaccineId);
                    })
                    ->when($vaccineLevelId, function ($query) use ($vaccineLevelId){
                        return $query->where('vaccine_level_id', $vaccineLevelId);
                    })
                    ->where(function($query) use ($dateFrom, $dateTo) {
                        $query->when($dateFrom, function ($q) use ($dateFrom){
                            return $q->whereDate('date', '>=', $dateFrom);
                        })
                        ->when($dateTo, function ($q) use ($dateTo){
                            return $q->whereDate('date', '<=', $dateTo);
                        });
                    });

        $vaccines = Vaccine::all();

        $total = $vaccinationsQuery->count();

        $vaccinations = $vaccinationsQuery->orderBy('id', 'desc')->paginate('20');

        $vaccinations->appends($request->except('page'));

        return view('vaccinations.index', compact('vaccinations', 'filter', 'vaccines', 'vaccineLevels', 'total'));
    }

    public function print(Vaccination $vaccination)
    {
        return view('vaccinations.print_forms.print', compact('vaccination'));
    }

    public function get_vaccination_list()
    {
        $req = request()->all();

        $name = $req['name'];

        $vaccinationsQuery = Vaccination::when($name, function ($query) use ($name){

            return $query->where('name', 'like', '%' . $name . '%');

        });

        $vaccinations = $vaccinationsQuery->orderBy('id', 'desc')->take('10')->get();

        $data['names'] = $vaccinations;

        return $data;
    }
}
