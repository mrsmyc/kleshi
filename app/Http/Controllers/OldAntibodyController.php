<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\OldAntibody;

use App\User;

use App\Research;

class OldAntibodyController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $antibodies = OldAntibody::orderBy('id', 'desc')->paginate(20);

        return view('old_antibodies.index', compact('antibodies'));
    }

    public function create()
    {
        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        $researches = Research::where('name', '<>', 'КЭ (антиген)')->get();

    	return view('old_antibodies.create', compact('managers', 'researches'));
    }

    public function store(Request $request)
    {
        $antibody = OldAntibody::create($request->except('researches'));

        $antibody->researches()->sync($request->researches);

        $antibody->result()->create();

        return redirect('old_antibodies');
    }

    public function edit(OldAntibody $antibody)
    {
        $result = $antibody->result;

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        $researches = Research::where('name', '<>', 'КЭ (антиген)')->get();

        $checkedResearches = $antibody->researches->pluck('id')->toArray();

        return view('old_antibodies.edit', compact('antibody', 'result', 'managers', 'researches', 'checkedResearches'));
    }

    public function update(OldAntibody $antibody, Request $request)
    {
    	$antibody->update($request->except('researches'));

        $antibody->researches()->sync($request->researches);

        return redirect('old_antibodies');
    }

    public function update_result(OldAntibody $antibody, Request $request)
    {
        $result = $antibody->result;

        $result->update($request->all());

        return redirect()->back();
    }

    public function search(Request $request)
    {
        $id = $request->input('id');

        $name = $request->input('name');

        $date = $request->input('date');

        $filter = [

            'id' => $id,

            'name' => $name,

            'date' => $date,
        ];

        $antibodies = OldAntibody::when($id, function ($query) use ($id) {
                        return $query->where('id', $id);
                    })
                    ->when($name, function ($query) use ($name){
                        return $query->orWhere('name', 'like', '%' . $name . '%');
                    })
                    ->when($date, function ($query) use ($date){
                        return $query->orWhereDate('date', $date);
                    })
                    ->paginate('20');

        return view('old_antibodies.index', compact('antibodies', 'filter'));
    }

    public function print(OldAntibody $antibody)
    {
        return view('old_antibodies.print_forms.print', compact('antibody'));
    }
}
