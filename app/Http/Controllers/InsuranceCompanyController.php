<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\InsuranceCompany;

class InsuranceCompanyController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$companies = InsuranceCompany::where('name', '<>', 'Другая')->orderBy('name')->paginate(20);

    	return view('insurance_companies.index', compact('companies'));
    }

    public function create()
    {
    	return view('insurance_companies.create');
    }

    public function store(Request $request)
    {
        $company = InsuranceCompany::create($request->all());

        return redirect('insurance_companies/' . $company->id . '/edit');
    }

    public function edit(InsuranceCompany $company)
    {
    	return view('insurance_companies.edit', compact('company'));
    }

    public function update(InsuranceCompany $company, Request $request)
    {
    	$company->update($request->all());

        return redirect()->back();
    }

    public function check_guarantee_letter()
    {
        $req = request()->all();

        $company = InsuranceCompany::find($req['insurance_company_id']);

        $data['guarantee_letter'] = $company->guarantee_letter;

        return $data;
    }
}
