<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Vaccine;

use App\VaccineSerie;

use App\Price;

class VaccineController extends Controller
{
    public function index()
    {
    	$vaccines = Vaccine::paginate(20);

    	return view('vaccines.index', compact('vaccines'));
    }

    public function create()
    {
        $price = Price::all();

    	return view('vaccines.create', compact('price'));
    }

    public function store(Request $request)
    {
    	$vaccine = Vaccine::create($request->all());

    	return redirect('vaccines/' . $vaccine->id . '/edit');
    }

    public function edit(Vaccine $vaccine)
    {
        $price = Price::all();

    	$vaccineSeries = $vaccine->series;

    	return view('vaccines.edit', compact('vaccine', 'vaccineSeries', 'price'));
    }

    public function update(Vaccine $vaccine, Request $request)
    {
    	$vaccine->update($request->all());

    	return redirect('vaccines/' . $vaccine->id . '/edit');
    }

    public function get_series(Request $request)
    {
        $vaccineId = $request->vaccine_id;

        $vaccinationId = $request->vaccination_id;

        if ($vaccinationId) {
            $vaccination = Vaccination::find($vaccinationId);

            $currentVaccineSerie = $vaccination->vaccine_serie_id;
        } else {
            $currentVaccineSerie = NULL;
        }

        if ($currentVaccineSerie) {
            $vaccineSeries = VaccineSerie::where('vaccine_id', $vaccineId)
            ->where(function($query) use ($currentVaccineSerie) {
                $query->where('enabled', 1)->orWhere('id', $currentVaccineSerie);
            })->get();
        } else {
            $vaccineSeries = VaccineSerie::where('vaccine_id', $vaccineId)->where('enabled', 1)->get();
        }
       
        return $vaccineSeries;
    }

    public function search(Request $request)
    {
        $name = $request->input('name');

        $filter = [

            'name' => $name,

        ];

        $vaccines = Vaccine::when($name, function ($query) use ($name){
                        return $query->orWhere('name', 'like', '%' . $name . '%');
                    })
                    ->paginate('20');

        $vaccines->appends($request->except('page'));
        
        return view('vaccines.index', compact('vaccines', 'filter'));
    }

}
