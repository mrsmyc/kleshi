<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;

use App\Role;

class UserController extends Controller
{
	public function __construct() 
    {
        $this->middleware('auth');
    }

    public function index()
    {
    	$users = User::where('login', '<>', 'zlautumn')->get();

    	return view('users.index', compact('users'));
    }

    public function create()
    {
    	$roles = Role::where('name', '<>', 'master')->get();

    	return view('users.create', compact('roles'));
    }

    public function store(Request $request)
    {
        $attributeNames = array(

           'name' => 'ФИО',

           'login' => 'Логин',

           'password' => 'Пароль',

        );

        $this->validate(request(), [

            'name' => 'required',

            'login' => 'required|unique:users',

            'password' => 'min:6',

        ], [], $attributeNames);

    	$data = $request->except('roles');

    	$data['password'] = bcrypt($data['password']);

    	$user = User::create($data);

        $roles = $request->roles;

    	$user->manageRoles($roles);

    	return redirect('users');
    }

    public function edit(User $user)
    {
    	$roles = Role::where('name', '<>', 'master')->get();

    	return view('users.edit', compact('user', 'roles'));
    }

    public function update(User $user, Request $request)
    {
    	$user->update($request->except('roles', 'password'));

    	if ($request->password) {
    		
    		$user->update([

    			'password' => $request->password,

			]);
    	}

    	$user->manageRoles($request->roles);

    	return redirect('users'); 
    }

}
