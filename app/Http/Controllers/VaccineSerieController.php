<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Vaccine;

use App\VaccineSerie;

class VaccineSerieController extends Controller
{
    public function create(Vaccine $vaccine)
    {
    	return view('vaccine_series.create', compact('vaccine'));
    }

    public function store(Request $request)
    {
    	$vaccineSerie = VaccineSerie::create($request->all());

    	return redirect('vaccine_series/' . $vaccineSerie->id . '/edit');
    }

    public function edit(VaccineSerie $vaccineSerie)
    {
    	return view('vaccine_series.edit', compact('vaccine', 'vaccineSerie'));
    }

    public function update(VaccineSerie $vaccineSerie, Request $request)
    {
    	$vaccineSerie->update($request->all());

    	return redirect('vaccine_series/' . $vaccineSerie->id . '/edit');
    }
}
