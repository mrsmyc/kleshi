<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Admission;

use App\Gender;

use App\PermanentResidence;

use App\Residence;

use App\InsuranceCompany;

use App\RemovalInstitution;

use App\BiteSpot;

use App\BiteCircumstance;

use App\VaccineLevel;

use App\AdminTerritory;

use App\CityArea;

use App\Locality;

use App\ChildCareFacility;

use App\EncephalitisMethod;

use App\BacterialMethod;

use App\Material;

use App\Research;

use App\ResearchType;

use App\TickType;

use App\TickGender;

use App\TickCondition;

use App\User;

use Carbon\Carbon;

class AdmissionController extends Controller
{
    private $researchNum;

    public function __construct() 
    {
        $this->middleware('auth');

        $this->researchNum = [
            'Без исследования', 
            '1 исследование', 
            '2 исследования',
            '3 исследования',
            '4 исследования',
            '5 исследований',
            '6 исследований',
        ];
    }
    
    public function index()
    {
        $encephalitisMethods = EncephalitisMethod::all();

        $bacterialMethods = BacterialMethod::all();

        $adminTerritories = Residence::all();

        $permanentResidences = Residence::all();

        $materials = Material::where('enabled', '1')->get();

        $genders = Gender::all();

        //$insuranceCompanies = InsuranceCompany::where('enabled', true)->get();
        $insuranceCompanies = InsuranceCompany::all();

    	$admissionsQuery = Admission::orderBy('id', 'desc');

        $total = $admissionsQuery->count();

        $admissions = $admissionsQuery->paginate(20);

    	return view('admissions.index', compact('admissions', 'insuranceCompanies', 'bacterialMethods', 'encephalitisMethods', 'adminTerritories', 'permanentResidences', 'genders', 'materials', 'total'));
    }

    public function create(Admission $admission = NULL)
    {
    	$genders = Gender::all();

    	//$residences = PermanentResidence::all();

        $residences = Residence::all();

        //$extendedResidences = ExtendedPermanentResidence::all();

        $insuranceCompanies = InsuranceCompany::where('enabled', true)->orderBy('name')->get();

    	$removalInstitutions = RemovalInstitution::all();

    	$biteSpots = BiteSpot::orderBy('name')->get();

    	$biteCircumstances = BiteCircumstance::all();

    	$vaccineLevels = VaccineLevel::all();

    	$adminTerritories = AdminTerritory::all();

    	$cityAreas = CityArea::all();

    	$localities = Locality::all();

        $childCareFacilities = ChildCareFacility::all();

    	$encephalitisMethods = EncephalitisMethod::all();

    	$bacterialMethods = BacterialMethod::all();

    	$materials = Material::where('enabled', '1')->get();

        //$researchTypes = ResearchType::where('owner_type', 'admission')->get();

        $researches = Research::all();

        $tickTypes = TickType::all();

        $tickGenders = TickGender::all();

    	$tickConditions= TickCondition::all();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        $researchNum = $this->researchNum;

        $isPolicyValid = false;

        if ($admission) {
            $isPolicyValid = ($admission->policy_end >= Carbon::today());
        }

    	return view('admissions.create', compact('admission', 'isPolicyValid', 'genders', 'residences', 'insuranceCompanies', 'researchNum', 'removalInstitutions', 'biteSpots', 'biteCircumstances', 'vaccineLevels', 'adminTerritories', 'cityAreas', 'localities', 'childCareFacilities', 'encephalitisMethods', 'bacterialMethods', 'materials', 'researches', 'tickTypes', 'tickGenders', 'tickConditions', 'managers'));
    }

    public function store(Request $request)
    {
        /*

        $data = $request->all();

        
        if ($request->research_types) {

            $types = $request->research_types;

        } else {

            $types = [];

        }

        $data['research_types'] = json_encode($types);
        */

    	$admission = Admission::create($request->except('researches'));

        $admission->setYearId();

        $admission->researches()->sync($request->researches);

        $admission->result()->create();

        if ($admission->material_id == '1') {

            for ($i = 0; $i < $admission->material_amount; $i++) {
                
                $admission->tick_results()->create();

            }
        }

        $admission->managePolicy();

        $request->session()->flash('status', 'Task was successful!');

        return redirect('admissions');
    }

    public function edit(Admission $admission)
    {
        $result = $admission->result;

        $tickResults = $admission->tick_results;

    	$genders = Gender::all();

        $residences = Residence::all();

        //$extendedResidences = ExtendedPermanentResidence::all();

        $insuranceCompanies = InsuranceCompany::where('enabled', true)->orWhere('id', $admission->insurance_company_id)->orderBy('name')->get();

        $removalInstitutions = RemovalInstitution::all();

        $biteSpots = BiteSpot::orderBy('name')->get();

        $biteCircumstances = BiteCircumstance::all();

        $vaccineLevels = VaccineLevel::all();

        $adminTerritories = AdminTerritory::all();

        //$cityAreas = CityArea::all();

        //$localities = Locality::all();
        /*
        if ($admission->admin_territory) {

            $localities = $admission->admin_territory->localities;

        } else {

            $localities = [];

        }
        */

        $childCareFacilities = ChildCareFacility::all();

        $encephalitisMethods = EncephalitisMethod::all();

        $bacterialMethods = BacterialMethod::all();

        $materials = Material::where('enabled', '1')->get();

        $researchTypes = ResearchType::where('owner_type', 'admission')->get();

        $researches = Research::all();

        $checkedResearches = $admission->researches->pluck('id')->toArray();

        $tickTypes = TickType::all();

        $tickGenders = TickGender::all();

        $tickConditions= TickCondition::all();

        $managers = User::whereHas('roles', function($roleQuery) {
            $roleQuery->where('roles.name', '=','manager');
        })->get();

        $previous = Admission::where('id', '<', $admission->id)->max('id');

        $next = Admission::where('id', '>', $admission->id)->min('id');

        $smsStatuses = [

            'new' => 'Ожидает отправки',

            'cancelled' => 'Отправка отменена',

            'sent' => 'Отправлено',

            'failed' => 'Отправка не удалась',
        ];

        if (($admission->material_id == '1') && ($admission->tick_results()->count() == 0)) {

            for ($i = 0; $i < $admission->material_amount; $i++) {
                
                $admission->tick_results()->create();

            }
        }

        $researchNum = $this->researchNum;

        return view('admissions.edit', compact('admission', 'genders', 'residences', 'insuranceCompanies', 'researchNum', 'removalInstitutions', 'biteSpots', 'biteCircumstances', 'vaccineLevels', 'adminTerritories', 'childCareFacilities', 'encephalitisMethods', 'bacterialMethods', 'materials', 'researches', 'checkedResearches', 'result', 'tickResults', 'tickTypes', 'tickGenders', 'tickConditions', 'managers', 'smsStatuses', 'previous', 'next'));
    }

    public function update(Admission $admission, Request $request)
    {
        /*$data = $request->except('uploaded_policy_scan', 'policy_scan');

        if ($request->research_types) {

            $types = $request->research_types;

        } else {

            $types = [];

        }

        $data['research_types'] = json_encode($types);*/

        $admission->update($request->except('uploaded_policy_scan', 'policy_scan', 'researches'));

        $admission->researches()->sync($request->researches);

        $admission->managePolicy();

    	$request->session()->flash('status', 'Task was successful!');

        return redirect('admissions');
    }

    public function delete()
    {
    	
    }

    public function search(Request $request)
    {
        $genders = Gender::all();

        $materials = Material::where('enabled', '1')->get();

        $encephalitisMethods = EncephalitisMethod::all();

        $bacterialMethods = BacterialMethod::all();

        $adminTerritories = Residence::all();

        $permanentResidences = Residence::all();

        $insuranceCompanies = InsuranceCompany::where('enabled', true)->get();

    	$id = $request->input('id');

        $yearId = $request->input('year_id');

        $name = $request->input('name');

        $dateFrom = $request->input('date_from');

        $dateTo = $request->input('date_to');

        $insuranceCompanyId = $request->input('insurance_company_id');

        $genderId = $request->input('gender_id');

        $materialId = $request->input('material_id');

        //$encephalitisMethodId = $request->input('encephalitis_method_id');

        //$bacterialMethodId = $request->input('bacterial_method_id');

        $needsAttention = $request->input('needs_attention');

        $encephalitisMethodIds = $request->input('encephalitis_method_ids');

        $bacterialMethodIds = $request->input('bacterial_method_ids');

        $adminTerritoryIds = $request->input('admin_territory_ids');

        $permanentResidenceIds = $request->input('permanent_residence_ids');

        $iged = $request->input('iged');

        $filter = [
            'id' => $id,

            'year_id' => $yearId,

            'name' => $name,

            'date_from' => $dateFrom,

            'date_to' => $dateTo,

            'insurance_company_id' => $insuranceCompanyId,

            'gender_id' => $genderId,

            'material_id' => $materialId,

            //'encephalitis_method_id' => $encephalitisMethodId,

            //'bacterial_method_id' => $bacterialMethodId,

            'encephalitis_method_ids' => $encephalitisMethodIds,

            'bacterial_method_ids' => $bacterialMethodIds,

            'admin_territory_ids' => $adminTerritoryIds,

            'permanent_residence_ids' => $permanentResidenceIds,

            'needs_attention' => $needsAttention,

            'iged' => $iged,
        ];

        /*
        $admissions = Admission::where('id', $id)
                        ->orWhere('name', 'like', '%' . $name . '%')
                        ->orWhereDate('date', $date)
                        ->paginate('20');
        */

        $admissionsQuery = Admission::when($id, function ($query) use ($id) {
                        return $query->where('id', $id);
                    })
                    ->when($yearId, function ($query) use ($yearId) {
                        return $query->where('year_id', $yearId)->whereYear('date', now()->year);
                    })
                    ->when($name, function ($query) use ($name){
                        return $query->where('name', 'like', '%' . $name . '%');
                    })
                    ->where(function($query) use ($dateFrom, $dateTo) {
                        $query->when($dateFrom, function ($q) use ($dateFrom){
                            return $q->whereDate('date', '>=', $dateFrom);
                        })
                        ->when($dateTo, function ($q) use ($dateTo){
                            return $q->whereDate('date', '<=', $dateTo);
                        });
                    })
                    ->when($insuranceCompanyId, function ($query) use ($insuranceCompanyId){
                        return $query->where('insurance_company_id', $insuranceCompanyId);
                    })
                    ->when($genderId, function ($query) use ($genderId){
                        return $query->where('gender_id', $genderId);
                    })
                    ->when($materialId, function ($query) use ($materialId){
                        return $query->where('material_id', $materialId);
                    })
                    /*->when($encephalitisMethodId, function ($query) use ($encephalitisMethodId){
                        return $query->where('encephalitis_method_id', $encephalitisMethodId);
                    })*/
                    /*->when($bacterialMethodId, function ($query) use ($bacterialMethodId){
                        return $query->where('bacterial_method_id', $bacterialMethodId);
                    })*/
                    ->when($encephalitisMethodIds, function ($query) use ($encephalitisMethodIds){
                        return $query->whereIn('encephalitis_method_id', $encephalitisMethodIds);
                    })
                    ->when($bacterialMethodIds, function ($query) use ($bacterialMethodIds){
                        return $query->whereIn('bacterial_method_id', $bacterialMethodIds);
                    })
                    ->when($adminTerritoryIds, function ($query) use ($adminTerritoryIds){
                        return $query->whereIn('extended_admin_territory_id', $adminTerritoryIds);
                    })
                    ->when($permanentResidenceIds, function ($query) use ($permanentResidenceIds){
                        return $query->whereIn('extended_permanent_residence_id', $permanentResidenceIds);
                    })
                    ->when($needsAttention, function ($query) use ($needsAttention){
                        return $query->where('needs_attention', $needsAttention);
                    })
                    ->when($iged, function ($query) use ($iged){
                        return $query->where('ig_dose', '>', 0);
                    })
                    ->orderBy('id', 'desc');

        $total = $admissionsQuery->count();

        $admissions = $admissionsQuery->paginate('20');

        $admissions->appends($request->except('page'));

        return view('admissions.index', compact('admissions', 'filter', 'insuranceCompanies', 'bacterialMethods', 'encephalitisMethods', 'adminTerritories', 'permanentResidences', 'genders', 'materials', 'total'));
    }

    public function update_result(Admission $admission, Request $request)
    {
        $result = $admission->result;

        $tickParams = ['tick_pcr_vte', 'tick_pcr_tb', 'tick_pcr_hme', 'tick_pcr_hga', 'tick_pcr_tr', 'tick_pcr_trf', 'tick_type_id', 'tick_gender_id', 'tick_condition_id'];

        $result->update($request->except($tickParams));

        if ($admission->material_id == '1') {

            $tickData = $request->only($tickParams);

            $tickResults = $admission->tick_results;

            //for ($i = 0; $i < sizeof($request->tick_pcr_vte); $i++) {
            foreach ($tickResults as $key=>$tickResult){

                foreach ($tickParams as $param) {
                    
                    $tickResult->$param = $tickData[$param][$key];

                }

                $tickResult->save();
            }
        }


        $admission->manageNotification();

        return redirect('admissions/' . $admission->id . '/edit#results');
    }

    public function get_localities()
    {
        $req = request()->all();

        $adminTerritory = AdminTerritory::find($req['admin_territory_id']);

        $localities = $adminTerritory->localities->toArray();

        return $localities;
    }

    public function fix_ticks()
    {
        $tickParams = ['tick_pcr_vte', 'tick_pcr_tb', 'tick_pcr_hme', 'tick_pcr_hga', 'tick_pcr_tr', 'tick_pcr_trf', 'tick_type_id', 'tick_gender_id', 'tick_condition_id'];

        $admissions = Admission::where('material_id', '1')->get();

        foreach($admissions as $admission) {

            if ($admission->tick_results()->count() == 0) {

                $amount = $admission->material_amount;

                for ($i = 0; $i < $amount; $i++) {

                    $admission->tick_results()->create();

                }

                $tickResult = $admission->tick_results()->first();

                foreach ($tickParams as $param) {
                        
                    $tickResult->$param = $admission->result->$param;

                }

                $tickResult->save();

            }
        }

        return redirect('admissions');
    }

    public function print(Admission $admission)
    {
        $research = [];

        $requestedResearch = $admission->researches->where('type', 'pcr');
        if ($requestedResearch->count() > 0) {
            $researches['pcr'] = $requestedResearch;
        }

        $requestedResearch = $admission->researches->whereIn('type', array('ifa', 'other'));
        if ($requestedResearch->count() > 0) {
            $researches['ifa'] = $requestedResearch;
        }

        return view('admissions.print_forms.print', compact('admission', 'researches'));
    }

    /* проверка на наличие некорректных телефонов */
    /*
    public function test()
    {
        $admissions = Admission::whereDate('date', '>=', '2020-07-01')->get();

        foreach ($admissions as $key => $admission) {
            $phone = $admission->phone;
            $cleanPhone = preg_replace('/[^0-9]/', '', trim($phone));
            //$cleanPhone = preg_replace('/[^a-zA-Zа-яА-Я]/', '', trim($phone));

            if (strlen($cleanPhone) != 11) {
            //if (strlen($cleanPhone) > 0) {
                echo $admission->id . '<br>';
            }
        }

        echo 'finished';
    }
    */

    /*
    public function fix_year_id()
    {
        $admissions = Admission::all();

        foreach ($admissions as $admission) {
            if (!$admission->year_id) {
                $admission->setYearId();
            }
        }
    }
    */
}
