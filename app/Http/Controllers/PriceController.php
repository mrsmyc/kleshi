<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Price;

class PriceController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$prices = Price::all();

    	return view('prices.index', compact('prices'));
    }

    public function create()
    {
    	return view('prices.create');
    }

    public function store(Request $request)
    {
        $price = Price::create($request->all());

        //return redirect('prices/' . $price->id . '/edit');
        return redirect('prices');
    }

    public function edit(Price $price)
    {
    	return view('prices.edit', compact('price'));
    }

    public function update(Price $price, Request $request)
    {
    	$price->update($request->all());

    	return redirect('prices');
        //return redirect()->back();
    }
}
