<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use App\Admission;

use App\Antibody;

use App\OldAntibody;

use App\Gender;

use App\PermanentResidence;

use App\InsuranceCompany;

use App\RemovalInstitution;

use App\BiteSpot;

use App\BiteCircumstance;

use App\VaccineLevel;

use App\Group;

use App\AdminTerritory;

use App\CityArea;

use App\Locality;

use App\EncephalitisMethod;

use App\BacterialMethod;

use App\Material;

use App\Research;

use App\PaymentType;

use App\Vaccination;

use App\VaccineSpot;

use App\Vaccine;

use App\VaccineSerie;

use App\TickType;

use App\TickGender;

use App\TickCondition;

use App\User;

use Carbon\Carbon;

class ImportController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }

    public function index()
    {	
    	return view('import.index');
    }

    public function unify()
    {
    	$admissions = Admission::all();

    	foreach ($admissions as $admission) {
    		if ($admission->insurance_company_id) {

	    		$comment = $admission->comment_insurance;

	    		//if (preg_match_all('/\d{2}[\.,]\d{2}[\.,]\d{4}/', $comment, $matches)) {
	    		//if (preg_match_all('/\d{1,2}[\.,]\d{2}[\.,]\d{2,4}/', $comment, $matches)) {
	    		if (preg_match_all('/\d{1,2}[\.,\/]\d{2}[\.,\/]\d{2,4}/', $comment, $matches)) {

	    			if (isset($matches['0']['0']) && isset($matches['0']['1'])) {

			    		$dateFrom = str_replace('/', '.', $matches['0']['0']);
			    		$dateFrom = Carbon::parse(str_replace(',', '.', $dateFrom))->format('Y-m-d');
			    		$dateTo = str_replace('/', '.', $matches['0']['1']);
			    		$dateTo = Carbon::parse(str_replace(',', '.', $dateTo))->format('Y-m-d');

			    		$admission->update([
			    			'policy_start' => $dateFrom,
			    			'policy_end' => $dateTo,
			    		]);
			    	}
			    }
	    	}
    	}
    	echo 'ready';
    }

    public function store(Request $request) {
    	$type = $request->input('import_type');
    	$function = 'import_' . $type;

    	$this->$function();

    	return redirect($type);
    }

    public function import_antibodies()
    {
    	if (request()->hasFile('import_file')) {

                $importedFile = request()->file('import_file')->store('public/import');

                //$image = $file;

            } else {

                dd ("нет файла");

            }

        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$data = [];

			$data['id'] = $worksheet->getCell('X' . $i)->getValue();

			$date = $worksheet->getCell('A' . $i)->getValue();
			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);

			$data['date'] = $date->format('Y-m-d');
			//$data['date'] = Carbon::now();
			$data['name'] = $worksheet->getCell('B' . $i)->getValue();

			$age = $worksheet->getCell('C' . $i)->getValue();
			if ($age) {
				$data['age'] = $age;
			} else {
				$data['age'] = 0;
			}

			$insuranceCompanyName = $worksheet->getCell('D' . $i)->getValue();

			if ($insuranceCompanyName) {
				$insuranceCompany = InsuranceCompany::where('name', $insuranceCompanyName)->first();
				if (!$insuranceCompany) {
					$insuranceCompany = InsuranceCompany::where('name', 'Другая')->first();
				}
				$data['insurance_company_id'] = $insuranceCompany->id;
			}

			$data['policy_num'] = $worksheet->getCell('E' . $i)->getValue();

			$data['phone'] = $worksheet->getCell('F' . $i)->getValue();

			$biteDate = $worksheet->getCell('G' . $i)->getValue();
			$biteDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($biteDate);
			$biteData['bite_date'] = $biteDate->format('Y-m-d');

			$data['admission_id'] = $worksheet->getCell('H' . $i)->getValue();

			$data['comment'] = $worksheet->getCell('I' . $i)->getValue();

			$data['responsible_id'] = '2';

			$antibody = Antibody::create($data);

			$result = $antibody->result()->create();

		}

		return;
    }

    public function import_admissions()
    {
    	if (request()->hasFile('import_file')) {

                $importedFile = request()->file('import_file')->store('public/import');

                //$image = $file;

            } else {

                dd ("нет файла");

            }

        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		//$value = $spreadsheet->getActiveSheet()->getCell('G3')->getValue();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$data = [];

			$data['id'] = $worksheet->getCell('A' . $i)->getValue();

			$date = $worksheet->getCell('B' . $i)->getValue();
			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);

			$data['date'] = $date->format('Y-m-d');
			$data['time'] = $date->format('H:i:s');
			//$data['date'] = Carbon::now();
			$data['name'] = $worksheet->getCell('C' . $i)->getValue();

			$age = $worksheet->getCell('D' . $i)->getValue();
			if ($age) {
				$data['age'] = $age;
			} else {
				$data['age'] = 0;
			}

			$gender = $worksheet->getCell('E' . $i)->getValue();
			if ($gender == 'Ж') {
				$genderId = '2';
			} else {
				$genderId = '1';
			}
			$data['gender_id'] = $genderId;
			$data['phone'] = $worksheet->getCell('F' . $i)->getValue();

			$insuranceCompanyName = $worksheet->getCell('G' . $i)->getValue();

			$data['comment_insurance'] = $worksheet->getCell('I' . $i)->getValue();

			if ($insuranceCompanyName) {
				$insuranceCompany = InsuranceCompany::where('name', $insuranceCompanyName)->first();
				if (!$insuranceCompany) {
					$insuranceCompany = InsuranceCompany::where('name', 'Другая')->first();
					$data['comment_insurance'] .= ' ' . $insuranceCompanyName;
				}
				$data['insurance_company_id'] = $insuranceCompany->id;
			}

			$data['policy_num'] = $worksheet->getCell('H' . $i)->getValue();

			$biteDate = $worksheet->getCell('J' . $i)->getValue();
			if ($biteDate) {
				$biteDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($biteDate);
				$data['bite_date'] = $biteDate->format('Y-m-d');
			}

			$removalDate = $worksheet->getCell('K' . $i)->getValue();
			if ($removalDate) {
				$removalDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($removalDate);
				$data['removal_date'] = $removalDate->format('Y-m-d');
			}

			$removalInstitutionName = $worksheet->getCell('L' . $i)->getValue();
			$removalInstitution = RemovalInstitution::where('name', $removalInstitutionName)->first();
			if ($removalInstitution) {
				$data['removal_institution_id'] = $removalInstitution->id;
			}

			$biteSpotName = $worksheet->getCell('P' . $i)->getValue();
			$biteSpot = BiteSpot::where('name', $biteSpotName)->first();
			if ($biteSpot) {
				$data['bite_spot_id'] = $biteSpot->id;
			}

			$biteCircumstanceName = $worksheet->getCell('AL' . $i)->getValue();
			$biteCircumstance = BiteCircumstance::where('name', $biteCircumstanceName)->first();
			if ($biteCircumstance) {
				$data['bite_circumstance_id'] = $biteCircumstance->id;
			}

			$biteSpotName = $worksheet->getCell('P' . $i)->getValue();
			$biteSpot = BiteSpot::where('name', $biteSpotName)->first();
			if ($biteSpot) {
				$data['bite_spot_id'] = $biteSpot->id;
			}

			$vaccineLevelName = $worksheet->getCell('Q' . $i)->getValue();
			$vaccineLevel = VaccineLevel::where('name', $vaccineLevelName)->first();
			if ($vaccineLevel) {
				$data['vaccine_level_id'] = $vaccineLevel->id;
			}

			$igDate = $worksheet->getCell('R' . $i)->getValue();
			if ($igDate) {
				$igDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($igDate);
				$data['ig_date'] = $igDate->format('Y-m-d');
			}

			$igDose = $worksheet->getCell('S' . $i)->getValue();
			if ($igDose) {
				$data['ig_dose'] = $igDose;
			}

			$encephalitisMethodName = $worksheet->getCell('T' . $i)->getValue();
			$encephalitisMethod = EncephalitisMethod::where('name', $encephalitisMethodName)->first();
			if ($encephalitisMethod) {
				$data['encephalitis_method_id'] = $encephalitisMethod->id;
			}

			$bacterialMethodName = $worksheet->getCell('U' . $i)->getValue();
			$bacterialMethod = BacterialMethod::where('name', $bacterialMethodName)->first();
			if ($bacterialMethod) {
				$data['bacterial_method_id'] = $bacterialMethod->id;
			}

			if ($encephalitisMethod || $bacterialMethod) {
				$data['measures_date'] = $date->format('Y-m-d');
			}

			$materialData = explode(' ', $worksheet->getCell('V' . $i)->getValue());
			$materialName = $materialData[0];
			$material = Material::where('name', $materialName)->first();
			if ($material) {
				$data['material_id'] = $material->id;

				if ($material->id == 1) {

					if (array_key_exists(1, $materialData)) {
						$data['material_amount'] = $materialData[1];
					} else {
						$data['material_amount'] = 1;
					}
				}
			}

			/*
			$researches = explode('+', $worksheet->getCell('W' . $i)->getValue());
			$arr = [];
			foreach ($researches as $research) {
				$researchType = ResearchType::where('name', $research)->first();
				if ($researchType) {
					$arr[] = $researchType->id;
				}
			}
			
			$data['research_types'] = json_encode($arr);
			*/

			$data['comment'] = $worksheet->getCell('X' . $i)->getValue();

			$refund = $worksheet->getCell('AK' . $i)->getValue();
			if ($refund) {
				$refund = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($refund);
				$data['refund'] = $refund->format('Y-m-d');
			}

			$permanentResidenceName = $worksheet->getCell('AX' . $i)->getValue();
			$permanentResidence = PermanentResidence::where('name', $permanentResidenceName)->first();
			if ($permanentResidence) {
				$data['permanent_residence_id'] = $permanentResidence->id;
			}

			$data['responsible_id'] = '2';

			$admission = Admission::create($data);

			$researchTypes = explode('+', $worksheet->getCell('W' . $i)->getValue());
			$arr = [];
			foreach ($researchTypes as $researchType) {
				$researches = explode('-', $researchType);
				foreach ($researches as $researchName) {
					$research = Research::where('name', $researchName)->first();
					if ($research) {
						$arr[] = $research->id;
					}
				}
			}
			
			/*$data['research_types'] = json_encode($arr);*/

			$admission->researches()->sync($arr);

			$result = $admission->result()->create();

			$data = [];

			$antigen_te = $worksheet->getCell('Y' . $i)->getValue();
			if ($antigen_te) {
				if ($antigen_te == 'ОТР') {
					$data['antigen_te'] = 0;
				} else {
					$data['antigen_te'] = 1;
				}
			}

			$ig_m_te = $worksheet->getCell('Z' . $i)->getValue();
			if ($ig_m_te) {
				if ($ig_m_te == 'ОТР') {
					$data['ig_m_te'] = 0;
				} else {
					$data['ig_m_te'] = 1;
				}
			}

			$ig_g_te = $worksheet->getCell('AA' . $i)->getValue();
			if ($ig_g_te) {
				if ($ig_g_te == 'ОТР') {
					$data['ig_g_te'] = 0;
				} else {
					$data['ig_g_te'] = 1;
				}
			}

			$titr_ig_g_te = $worksheet->getCell('AB' . $i)->getValue();
			if ($titr_ig_g_te) {
				$data['titr_ig_g_te'] = $titr_ig_g_te;
			}

			$ig_m_tb = $worksheet->getCell('AC' . $i)->getValue();
			if ($ig_m_tb) {
				if ($ig_m_tb == 'ОТР') {
					$data['ig_m_tb'] = 0;
				} else {
					$data['ig_m_tb'] = 1;
				}
			}

			$ig_g_tb = $worksheet->getCell('AD' . $i)->getValue();
			if ($ig_g_tb) {
				if ($ig_g_tb == 'ОТР') {
					$data['ig_g_tb'] = 0;
				} else {
					$data['ig_g_tb'] = 1;
				}
			}

			$titr_ig_g_te = $worksheet->getCell('AE' . $i)->getValue();
			if ($titr_ig_g_te) {
				$data['titr_ig_g_te'] = $titr_ig_g_te;
			}

			$comment = $worksheet->getCell('AF' . $i)->getValue();
			if ($comment) {
				$data['comment'] = $comment;
			}

			$issueDate = $worksheet->getCell('AG' . $i)->getValue();
			if ($issueDate) {
				$issueDate = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($issueDate);
				$data['issue_date'] = $issueDate->format('Y-m-d');
			}

			$tickTypeName = $worksheet->getCell('AH' . $i)->getValue();
			if ($tickTypeName) {
				$tickType = TickType::where('name', 'like', '%' . $tickTypeName . '%')->first();
				if ($tickType) {
					$data['tick_type_id'] = $tickType->id;
				}
			}

			//клещ

			$tick_pcr_vte = $worksheet->getCell('AP' . $i)->getValue();
			if ($tick_pcr_vte) {
				if (strpos($tick_pcr_vte, 'полож') !== false) {
					$data['tick_pcr_vte'] = 1;
				} else {
					$data['tick_pcr_vte'] = 0;
				}
			}

			$tick_pcr_tb = $worksheet->getCell('AQ' . $i)->getValue();
			if ($tick_pcr_tb) {
				if (strpos($tick_pcr_tb, 'полож') !== false) {
					$data['tick_pcr_tb'] = 1;
				} else {
					$data['tick_pcr_tb'] = 0;
				}
			}

			$tick_pcr_hme = $worksheet->getCell('AR' . $i)->getValue();
			if ($tick_pcr_hme) {
				if (strpos($tick_pcr_hme, 'полож') !== false) {
					$data['tick_pcr_hme'] = 1;
				} else {
					$data['tick_pcr_hme'] = 0;
				}
			}

			$tick_pcr_hga = $worksheet->getCell('AS' . $i)->getValue();
			if ($tick_pcr_hga) {
				if (strpos($tick_pcr_hga, 'полож') !== false) {
					$data['tick_pcr_hga'] = 1;
				} else {
					$data['tick_pcr_hga'] = 0;
				}
			}

			$tick_pcr_tr = $worksheet->getCell('AY' . $i)->getValue();
			if ($tick_pcr_tr) {
				if (strpos($tick_pcr_tr, 'полож') !== false) {
					$data['tick_pcr_tr'] = 1;
				} else {
					$data['tick_pcr_tr'] = 0;
				}
			}

			$tick_pcr_trf = $worksheet->getCell('AZ' . $i)->getValue();
			if ($tick_pcr_trf) {
				if (strpos($tick_pcr_trf, 'полож') !== false) {
					$data['tick_pcr_trf'] = 1;
				} else {
					$data['tick_pcr_trf'] = 0;
				}
			}

			// кровь

			$blood_pcr_vte = $worksheet->getCell('AT' . $i)->getValue();
			if ($blood_pcr_vte) {
				if (strpos($blood_pcr_vte, 'полож') !== false) {
					$data['blood_pcr_vte'] = 1;
				} else {
					$data['blood_pcr_vte'] = 0;
				}
			}

			$blood_pcr_tb = $worksheet->getCell('AU' . $i)->getValue();
			if ($blood_pcr_tb) {
				if (strpos($blood_pcr_tb, 'полож') !== false) {
					$data['blood_pcr_tb'] = 1;
				} else {
					$data['blood_pcr_tb'] = 0;
				}
			}

			$blood_pcr_hme = $worksheet->getCell('AV' . $i)->getValue();
			if ($blood_pcr_hme) {
				if (strpos($blood_pcr_hme, 'полож') !== false) {
					$data['blood_pcr_hme'] = 1;
				} else {
					$data['blood_pcr_hme'] = 0;
				}
			}

			$blood_pcr_hga = $worksheet->getCell('AW' . $i)->getValue();
			if ($blood_pcr_hga) {
				if (strpos($blood_pcr_hga, 'полож') !== false) {
					$data['blood_pcr_hga'] = 1;
				} else {
					$data['blood_pcr_hga'] = 0;
				}
			}

			$blood_pcr_tr = $worksheet->getCell('BA' . $i)->getValue();
			if ($blood_pcr_tr) {
				if (strpos($blood_pcr_tr, 'полож') !== false) {
					$data['blood_pcr_tr'] = 1;
				} else {
					$data['blood_pcr_tr'] = 0;
				}
			}

			$blood_pcr_trf = $worksheet->getCell('BB' . $i)->getValue();
			if ($blood_pcr_trf) {
				if (strpos($blood_pcr_trf, 'полож') !== false) {
					$data['blood_pcr_trf'] = 1;
				} else {
					$data['blood_pcr_trf'] = 0;
				}
			}

			$result->update($data);
		}

		Admission::whereNotNull('insurance_company_id')
		//->whereJsonContains('research_types', 4)
		//->orWhereJsonContains('research_types', 5)
		//->orWhereJsonContains('research_types', 6)
		->update([
			'policy_research_num' => 4,
		]);

		/*
		Admission::whereNotNull('insurance_company_id')->whereJsonContains('research_types', 7)->update([
			'policy_research_num' => 7,
		]);
		*/
		Admission::whereNotNull('insurance_company_id')->has('researches', '>=', '6')
		->update([
			'policy_research_num' => 6,
		]);


		return;
    }

    public function import_vaccinations()
    {
    	if (request()->hasFile('import_file')) {

            $importedFile = request()->file('import_file')->store('public/import');

            //$image = $file;

        } else {

            dd ("нет файла");

        }


        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$data = [];

			$data['id'] = $worksheet->getCell('A' . $i)->getValue();

			$date = $worksheet->getCell('B' . $i)->getValue();

			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);

			$data['date'] = $date->format('Y-m-d');

			$data['name'] = $worksheet->getCell('D' . $i)->getValue();

			$paymentTypeName = $worksheet->getCell('C' . $i)->getValue();

			$paymentType = PaymentType::where('name', $paymentTypeName)->first();

			$data['payment_type_id'] = $paymentType->id;

			$age = $worksheet->getCell('E' . $i)->getValue();
			if ($age) {
				$data['age'] = $age;
			} else {
				$data['age'] = 0;
			}

			$data['address'] = $worksheet->getCell('F' . $i)->getValue();

			$data['occupation_place'] = $worksheet->getCell('G' . $i)->getValue();

			$vaccineLevelName = $worksheet->getCell('J' . $i)->getValue();
			$vaccineLevel = VaccineLevel::where('name', $vaccineLevelName)->first();
			$data['vaccine_level_id'] = $vaccineLevel->id;

			$vaccineSpotName = $worksheet->getCell('K' . $i)->getValue() . 'мл';
			$vaccineSpot = VaccineSpot::where('name', $vaccineSpotName)->first();
			$data['vaccine_spot_id'] = $vaccineSpot->id;

			$groupName = $worksheet->getCell('L' . $i)->getValue();
			if (!$groupName) {
				$groupName = $worksheet->getCell('M' . $i)->getValue();
			}
			if (!$groupName) {
				$groupName = $worksheet->getCell('N' . $i)->getValue();
			}
			$group = Group::where('name', $groupName)->first();

			$data['group_id'] = $group->id;

			$data['phone'] = $worksheet->getCell('P' . $i)->getValue();

			$vaccineSerieName = $worksheet->getCell('I' . $i);

			$vaccineSerie = explode(' до ', $vaccineSerieName);

			$vaccineDate = explode('.', $vaccineSerie[1]);

			$vaccineDate[1] = '20' . $vaccineDate[1];

			array_unshift($vaccineDate, '01');

			$vaccineSerie[1] = implode('.', $vaccineDate);

			$vaccine = VaccineSerie::where('name', $vaccineSerie[0])->whereDate('valid_to', Carbon::parse($vaccineSerie[1])->format('Y-m-d'))->first();

			if ($vaccine) {
				$data['vaccine_id'] = $vaccine->vaccine->id;

				$data['vaccine_serie_id'] = $vaccine->id;
			} else {
				$vaccineName = $worksheet->getCell('H' . $i);

				$data['vaccine_id'] = 1;

				$data['own_vaccine_info'] = $vaccineName . ', ' . $vaccineSerieName;
			}

			$data['responsible_id'] = '2';

			$vaccination = Vaccination::create($data);

		}

		return;
    }

    public function import_old_antibodies()
    {
    	if (request()->hasFile('import_file')) {

                $importedFile = request()->file('import_file')->store('public/import');

            } else {

                dd ("нет файла");

            }

        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$data = [];

			$data['id'] = $worksheet->getCell('A' . $i)->getValue();

			$date = $worksheet->getCell('B' . $i)->getValue();
			$date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($date);

			$data['date'] = $date->format('Y-m-d');
			//$data['date'] = Carbon::now();
			$data['name'] = $worksheet->getCell('C' . $i)->getValue();

			$age = $worksheet->getCell('D' . $i)->getValue();
			if ($age) {
				$data['age'] = $age;
			} else {
				$data['age'] = 0;
			}

			$data['phone'] = $worksheet->getCell('E' . $i)->getValue();

			$data['bite_year'] = $worksheet->getCell('F' . $i)->getValue();

			$data['anamnesis'] = $worksheet->getCell('G' . $i)->getValue();

			$data['responsible_id'] = '2';

			$antibody = OldAntibody::create($data);

			$result = $antibody->result()->create();

		}

		return;
    }

    public function import_update_pmz()
    {
    	if (request()->hasFile('import_file')) {

                $importedFile = request()->file('import_file')->store('public/import');

                //$image = $file;

            } else {

                dd ("нет файла");

            }

        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		//$value = $spreadsheet->getActiveSheet()->getCell('G3')->getValue();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$id = $worksheet->getCell('A' . $i)->getValue();

			$permanentResidenceName = $worksheet->getCell('AX' . $i)->getValue();
			$permanentResidence = PermanentResidence::where('name', $permanentResidenceName)->first();
			if ($permanentResidence) {
				$permanentResidenceId = $permanentResidence->id;
			}

			$admission = Admission::find($id);

			$admission->update([

				'permanent_residence_id' => $permanentResidenceId,

			]);

		}

		return;
    }

    public function import_update_bite_spot()
    {
    	if (request()->hasFile('import_file')) {

                $importedFile = request()->file('import_file')->store('public/import');

                //$image = $file;

            } else {

                dd ("нет файла");

            }

        $file = ltrim(Storage::url($importedFile), '/');

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

		$worksheet = $spreadsheet->getActiveSheet();

		//$value = $spreadsheet->getActiveSheet()->getCell('G3')->getValue();

		$data = array();

		$highestRow = $worksheet->getHighestRow();

		for ($i = 2; $i <= $highestRow; $i++) {

			$id = $worksheet->getCell('A' . $i)->getValue();
			$biteSpotName = $worksheet->getCell('P' . $i)->getValue();
			$biteSpot = BiteSpot::where('name', $biteSpotName)->first();
			if ($biteSpot) {
				$biteSpotId = $biteSpot->id;
			}

			$admission = Admission::find($id);

			$admission->update([

				'bite_spot_id' => $biteSpotId,

			]);

		}

		return;
    }
}
