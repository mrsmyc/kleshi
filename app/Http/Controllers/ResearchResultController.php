<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AdmissionResult;

use App\AntibodyResult;

use Carbon\Carbon;

class ResearchResultController extends Controller
{
	public function __construct() 
    {
        $this->middleware('auth');
    }

    public function admission_result()
    {
        $date = Carbon::today()->format('Y-m-d');

        $filter = [
            'date' => $date,
        ];

    	$results = AdmissionResult::with('admission')->whereDate('issue_date', $date)->get();

    	$total = $results->count();

    	return view('admissions.results.index', compact('results', 'filter'));
    }

    public function admission_result_filter(Request $request)
    {
    	$date = $request->input('date');

        $filter = [
            'date' => $date,
        ];

        $results = AdmissionResult::with('admission')
					->when($date, function ($query) use ($date) {
						$query->whereDate('issue_date', '=', $date);
					})
					->orderBy('id', 'asc')
					->get();

        $total = $results->count();

        return view('admissions.results.index', compact('results', 'filter'));
    }

    public function antibody_result()
    {
        $date = Carbon::today()->format('Y-m-d');

        $filter = [
            'date' => $date,
        ];

    	$results = AntibodyResult::with('antibody')
                    ->whereDate('issue_date', $date)
                    ->orderBy('id', 'asc')
                    ->get();

    	$total = $results->count();

    	return view('antibodies.results.index', compact('results', 'filter'));
    }

    public function antibody_result_filter(Request $request)
    {
    	$date = $request->input('date');

        $filter = [
            'date' => $date,
        ];

        $results = AntibodyResult::with('antibody')
					->when($date, function ($query) use ($date) {
						$query->whereDate('issue_date', '=', $date);
					})
					->orderBy('id', 'desc')
					->get();

        $total = $results->count();

        return view('antibodies.results.index', compact('results', 'filter'));
    }
}
