<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use DB;

use PDF;

use Carbon\Carbon;

use App\Admission;

use App\AdmissionResult;

use App\TickResult;

use App\AdminTerritory;

use App\Vaccine;

use App\Vaccination;

use App\VaccinationAgreement;

use App\Group;

use App\BiteSpot;

use App\BiteCircumstance;

use App\ChildCareFacility;

use App\TickType;

use App\PermanentResidence;

use App\Residence;

use App\InsuranceCompany;

use App\Antibody;

use App\Price;

use App\Research;

use App\Locality;

use App\Gender;

use App\TickGender;

use App\TickCondition;

use App\RemovalInstitution;

use App\VaccineLevel;

use App\EncephalitisMethod;

use App\BacterialMethod;

use App\Material;

use App\User;

class ReportController extends Controller
{
	private $months, $borders, $centerAlignment, $rightAlignment, $headerStyleArray, $cellStyleArray, $totalStyleArray;

	public function __construct() 
    {
        $this->middleware('auth');

        $this->alphabet = range('A', 'Z');

        $this->months = array('Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь');

        $this->borders = [
	        'top' => [
	            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
	        ],
	        'right' => [
	            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
	        ],
	        'bottom' => [
	            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
	        ],
	        'left' => [
	            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
	        ],
	    ];

		$this->centerAlignment = [
	        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
	        'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
	    ];

	    $this->leftAlignment = [
	        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
	    ];

	    $this->rightAlignment = [
	        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT,
	    ];

	    $this->headerStyleArray = [
		    'alignment' => $this->centerAlignment,
		    'borders' => $this->borders,
		    'fill' => [
		        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
		        'color' => ['argb' => 'FFCCCCCC'],
		    ],
		];

		$this->cellStyleArray = [
		    'alignment' => $this->leftAlignment,
		    'borders' => $this->borders,
		];

		$this->totalStyleArray = [
		    'font' => [
		        'bold' => true,
		    ],
		    'alignment' => $this->rightAlignment,
		];

		$this->smallerText = [
		    'font' => [
		        'size' => 9,
		    ],
		];
    }

	public function index()
	{
		request()->user()->authorizeRoles(['admin']);

		$insuranceCompanies = InsuranceCompany::where('enabled', '1')->get();

    	return view('reports.index', compact('insuranceCompanies'));
	}

	public function make_report() 
	{
    	$type = request()->input('report_type');

    	$fileName = $this->$type();

    	return response()->download($fileName)->deleteFileAfterSend();
    }

    private function all_reports()
    {
    	$insuranceCompanies = InsuranceCompany::where('enabled', '1')->where('name', '<>', 'Другая')->get();

    	$publicDir = public_path();

    	$zipFileName = 'Реестры.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

		$files = [];

    	foreach ($insuranceCompanies as $company) {

    		$fileName = $this->insurance_report($company->id);

    		$files[] = $fileName;

    		$zipFile->addFile($fileName);

    	}

    	$zipFile->close();

    	/*header('Content-Type: application/zip');
		
		header('Content-disposition: attachment; filename='.$zipFileName);
		
		header('Content-Length: ' . filesize($pathToZip));
		
		readfile($pathToZip);

		unlink($zipFileName);

		*/

		foreach ($files as $file) {

    		unlink($file);
    	}
    	return $zipFileName;
    }

    private function insurance_report($insuranceCompanyId = NULL)
    {
    	if (is_null($insuranceCompanyId)) {
			$insuranceCompanyId = request()->insurance_company_id;
		}

    	/*
    	if ($insuranceCompanyId != 2 && $insuranceCompanyId != 9 && $insuranceCompanyId != 13 && $insuranceCompanyId != 25 && $insuranceCompanyId != 30) {

    		return $this->insurance_report_typical($insuranceCompanyId);

    	}
    	*/

    	/*
    	$specialIC = [2, 9, 13, 25, 30];

    	if (!in_array($insuranceCompanyId, $specialIC)) {

    		return $this->insurance_report_typical($insuranceCompanyId);

    	}
    	*/

    	switch ($insuranceCompanyId) {
    		case 2:
    		case 13:
    			return $this->insurance_report_ingos($insuranceCompanyId);
    		case 5:
    		case 6:
    			return $this->insurance_report_asko($insuranceCompanyId);
    		case 9:
    			return $this->insurance_report_vsk($insuranceCompanyId);
    		case 25:
    			return $this->insurance_report_reso($insuranceCompanyId);
    		case 30:
    			return $this->insurance_report_sogaz($insuranceCompanyId);
    		default:
    			return $this->insurance_report_typical($insuranceCompanyId);
    	}
    }

    private function insurance_report_typical($insuranceCompanyId) 
    {
    	//$insuranceCompanyId = request()->insurance_company_id;

    	$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$worksheet->getDefaultColumnDimension()->setWidth(10);

		$worksheet->getColumnDimension('A')->setWidth(5);
		$worksheet->getColumnDimension('B')->setWidth(12);
		$worksheet->getColumnDimension('C')->setWidth(30);
		$worksheet->getColumnDimension('D')->setWidth(30);
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:V1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:V2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:V3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->mergeCells('A4:V4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:B7');    
		$worksheet->setCellValue('B6', 'Дата обращения');

		$worksheet->mergeCells('C6:C7');        
		$worksheet->setCellValue('C6', 'Ф.И.О. пациента');

		$worksheet->mergeCells('D6:D7');       
		$worksheet->setCellValue('D6', '№ полиса');

        $prices = DB::select('select name, group_concat(code) as code, column_name from prices where column_name is not null and column_name <> "" group by name, column_name');
		foreach ($prices as $price) {
			$worksheet->setCellValue($price->column_name . '6', $price->name);
			$worksheet->setCellValue($price->column_name . '7', $price->code);
		}

		$worksheet->mergeCells('V6:V7');
		$worksheet->setCellValue('V6', 'Итого');

		foreach (range('A', 'V') as $letter) {
			$worksheet->getStyle($letter . '6')->getAlignment()->setWrapText(true);

			$worksheet->getStyle($letter . '6:' . $letter . '7')->applyFromArray($this->headerStyleArray);
		}

		foreach (range('E', 'U') as $letter) {
			$worksheet->getStyle($letter . '7')->applyFromArray($this->headerStyleArray);
		}

		$items = [];

		$publicDir = public_path();

		// первичные приемы, которые попадают в период либо по дате приема, либо по дате иг, либо по дате мер, либо по дате консультации инфекциониста, либо по дате возврата
		$admissions = Admission::where('insurance_company_id', $insuranceCompanyId)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();
						// dd($admissions);

		if ($admissions->count() > 0) {

			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			// разбор массива первичных приемов
			foreach ($admissions as $admission) {
				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];
				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}
				$currentItem['B'] = Carbon::parse($admission->date)->format('d/m/Y');
				$currentItem['C'] = $admission->name;
				$currentItem['D'] = $admission->policy_num;

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {
					//первичный приём
					$price = Price::where('code', 'К014')->first();
					$currentItem['E'] = $price->price;

					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						$price = Price::where('code', 'К015')->first();
						$currentItem['F'] = $price->price;
					}
					//забор крови
					if ($admission->material_id == '2') {
						$price = Price::where('code', 'А2331196')->first();
						$currentItem[$price->column_name] = $price->price;
					}
				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							
							$currentItem[$research->price->column_name] = $research->price->price;
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if ($admission->material_id == '1') {

							$currentItem[$price->column_name] = $price->price * $admission->material_amount;

						} else {

							$currentItem[$price->column_name] = $price->price;

						}

					}

				}

				// если дата постановки иммуноглобулина или дата возврата попадает в обозначенный период, фиксируем количество доз и постановку иммунглобулина
				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						$price = Price::where('code', 'К019')->first();
						$currentItem[$price->column_name] = $admission->ig_dose * $price->price;
						$price = Price::where('code', 'П002')->first();
						$currentItem[$price->column_name] = $price->price;
					}

				}

				// если дата применения мер или дата возврата попадает в обозначенный период, фиксируем препараты и консультацию специалиста
				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						$price = Price::where('code', 'К021')->first();
						$currentItem[$price->column_name] = $price->price;
					}

					// профилактика КЭ
					// if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
					// 	$price = Price::where('code', 'К020')->first();
					// 	$currentItem[$price->column_name] = $price->price;
					// }


				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						$price = Price::where('code', 'К022')->first();
						$currentItem[$price->column_name] = $price->price;
					}

				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {
					//консультация инфекциониста
					if ($admission->infect_consult) {
						$price = Price::where('code', 'В036')->first();
						$currentItem[$price->column_name] = $price->price;
					}

				}

				$items[] = $currentItem;

				unset($price);
			}

			$regZipFile->close();
		}


		// приемы по антителам, которые по дате попадают в обозначенный период
		$antibodies = Antibody::where('insurance_company_id', $insuranceCompanyId)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];
			$currentItem['B'] = Carbon::parse($antibody->date)->format('d/m/Y');
			$currentItem['C'] = $antibody->name;
			$currentItem['D'] = $antibody->policy_num;

			$price = Price::where('code', 'А2331196')->first();
			$currentItem[$price->column_name] = $price->price;

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$price = Price::where('code', 'В036')->first();
				$currentItem[$price->column_name] = $price->price;
			}

			// ПЦР и другие исследования

			// количество ПЦР исследований
			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				/*
				if ($pcrNum == 1) {

					// если всего один ПЦР, то делаем 2, т.к. в прайсе нет ценника на 1.
					$pcrNum = 2;

				}
				*/

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$currentItem[$price->column_name] = $price->price;
			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {
				
				$currentItem[$research->price->column_name] = $research->price->price;

			}

			$items[] = $currentItem;
		}

		$rowStart = 8;
		$row = $rowStart;

		usort($items, array($this, 'sortByDate'));

		foreach ($items as $key => $item) {
			$row = $rowStart + $key;
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':V' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 
			foreach ($item as $key => $value) {
				$worksheet->setCellValue($key . $row, $value);
			}
			$worksheet->setCellValue('V' . $row, '=SUM(E' . $row . ':U' . $row . ')');
		}
		$rowEnd = $row;
		$row++;

		foreach (range('E', 'V') as $letter) {
			$worksheet->setCellValue($letter . $row, '=SUM(' . $letter . $rowStart . ':' . $letter . $rowEnd . ')');
		}

		foreach (range('A', 'V') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		foreach (range('E', 'V') as $letter) {
			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);
		}

		$totalCell = 'V' . $row;

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);

		$row++;
		$worksheet->setCellValue('A' . $row, 'Итого предоставлено медицинских услуг');
		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Зав. центром');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Петрова И.В.');

		$writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y'). '.xlsx';

        if (count($items) == 0) {

        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;

        } 

        $writer->save($fileName);

    	$zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
    }

    private function insurance_report_asko($insuranceCompanyId) 
    {
    	$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$worksheet->getDefaultColumnDimension()->setWidth(10);

		$worksheet->getColumnDimension('A')->setWidth(5);
		$worksheet->getColumnDimension('B')->setWidth(12);
		$worksheet->getColumnDimension('C')->setWidth(30);
		$worksheet->getColumnDimension('D')->setWidth(12);
		$worksheet->getColumnDimension('E')->setWidth(12);
		$worksheet->getColumnDimension('F')->setWidth(20);
		$worksheet->getColumnDimension('G')->setWidth(20);
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:V1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:V2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение: ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:V3');
        $worksheet->setCellValue('A3', 'г. Иркутск, ул. Карла Маркса, 3 телефон регистратуры 33-34-45, E-mail: karla_marksa-3@mail.ru');

        $worksheet->mergeCells('A4:V4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:B7');    
		$worksheet->setCellValue('B6', 'Дата обращения');

		$worksheet->mergeCells('C6:C7');        
		$worksheet->setCellValue('C6', 'Ф.И.О. пациента');

		$worksheet->mergeCells('D6:D7');        
		$worksheet->setCellValue('D6', 'Дата рождения');

		$worksheet->mergeCells('E6:E7');        
		$worksheet->setCellValue('E6', 'Страховая компания');

		$worksheet->mergeCells('F6:F7');       
		$worksheet->setCellValue('F6', '№ полиса');

		$worksheet->mergeCells('G6:G7');       
		$worksheet->setCellValue('G6', 'Номер иммунокарты');

        $prices = DB::select('select name, group_concat(code) as code, column_name from prices where column_name is not null and column_name <> "" group by name, column_name');
		foreach ($prices as $price) {
			$worksheet->setCellValue($this->offsetColumn($price->column_name, 3) . '6', $price->name);
			$worksheet->setCellValue($this->offsetColumn($price->column_name, 3) . '7', $price->code);
		}

		$worksheet->mergeCells('V6:V7');
		$worksheet->setCellValue('V6', 'Итого');

		foreach (range('A', 'Y') as $letter) {
			$worksheet->getStyle($letter . '6')->getAlignment()->setWrapText(true);

			$worksheet->getStyle($letter . '6:' . $letter . '7')->applyFromArray($this->headerStyleArray);
		}

		foreach (range('H', 'U') as $letter) {
			$worksheet->getStyle($letter . '7')->applyFromArray($this->headerStyleArray);
		}

		$items = [];

		$publicDir = public_path();

		// первичные приемы, которые попадают в период либо по дате приема, либо по дате иг, либо по дате мер, либо по дате консультации инфекциониста, либо по дате возврата
		$askoCompanies = [5, 6];
		$admissions = Admission::whereIn('insurance_company_id', $askoCompanies)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

		if ($admissions->count() > 0) {

			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			// разбор массива первичных приемов
			foreach ($admissions as $admission) {
				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];
				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}
				$currentItem['B'] = Carbon::parse($admission->date)->format('d/m/Y');
				$currentItem['C'] = $admission->name;
				$currentItem['D'] = Carbon::parse($admission->birth_date)->format('d/m/Y');
				$currentItem['E'] = $admission->asko_ic_name;
				$currentItem['F'] = $admission->policy_num;
				$currentItem['G'] = $admission->asko_immunocard;

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {
					//первичный приём
					$price = Price::where('code', 'К014')->first();
					$currentItem['H'] = $price->price;

					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						$price = Price::where('code', 'К015')->first();
						$currentItem['I'] = $price->price;
					}
				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					//забор крови
					if ($admission->material_id == '2') {
						$price = Price::where('code', 'А2331196')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
					}

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							
							$currentItem[$this->offsetColumn($research->price->column_name, 3)] = $research->price->price;
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if ($admission->material_id == '1') {

							$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price * $admission->material_amount;

						} else {

							$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;

						}

					}

				}

				// если дата постановки иммуноглобулина или дата возврата попадает в обозначенный период, фиксируем количество доз и постановку иммунглобулина
				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						$price = Price::where('code', 'К019')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $admission->ig_dose * $price->price;
						$price = Price::where('code', 'П002')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
					}

				}

				// если дата применения мер или дата возврата попадает в обозначенный период, фиксируем препараты и консультацию специалиста
				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						$price = Price::where('code', 'К021')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
					}

					// профилактика КЭ
					// if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
					// 	$price = Price::where('code', 'К020')->first();
					// 	$currentItem[$price->column_name] = $price->price;
					// }


				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						$price = Price::where('code', 'К022')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
					}

				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста
					if ($admission->infect_consult) {
						$price = Price::where('code', 'В036')->first();
						$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
					}

				}

				$items[] = $currentItem;

				unset($price);
			}

			$regZipFile->close();
		}


		// приемы по антителам, которые по дате попадают в обозначенный период
		$antibodies = Antibody::whereIn('insurance_company_id', $askoCompanies)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];
			$currentItem['B'] = Carbon::parse($antibody->date)->format('d/m/Y');
			$currentItem['C'] = $antibody->name;
			$currentItem['D'] = Carbon::parse($antibody->admission->birth_date)->format('d/m/Y');
			$currentItem['E'] = $antibody->admission->asko_ic_name;
			$currentItem['F'] = $antibody->policy_num;
			$currentItem['G'] = $antibody->admission->asko_immunocard;
			
			$price = Price::where('code', 'А2331196')->first();
			$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$price = Price::where('code', 'В036')->first();
				$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
			}

			// ПЦР и другие исследования

			// количество ПЦР исследований
			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				/*
				if ($pcrNum == 1) {

					// если всего один ПЦР, то делаем 2, т.к. в прайсе нет ценника на 1.
					$pcrNum = 2;

				}
				*/

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$currentItem[$this->offsetColumn($price->column_name, 3)] = $price->price;
			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {
				
				$currentItem[$this->offsetColumn($research->price->column_name, 3)] = $research->price->price;

			}

			$items[] = $currentItem;
		}

		$rowStart = 8;
		$row = $rowStart;

		usort($items, array($this, 'sortByDate'));

		foreach ($items as $key => $item) {
			$row = $rowStart + $key;
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':Y' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 

			foreach ($item as $key => $value) {
				$worksheet->setCellValue($key . $row, $value);
			}
			$worksheet->setCellValue('Y' . $row, '=SUM(H' . $row . ':X' . $row . ')');
		}
		$rowEnd = $row;
		$row++;

		foreach (range('H', 'Y') as $letter) {
			$worksheet->setCellValue($letter . $row, '=SUM(' . $letter . $rowStart . ':' . $letter . $rowEnd . ')');
		}

		foreach (range('A', 'Y') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		foreach (range('H', 'Y') as $letter) {
			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);
		}

		$totalCell = 'Y' . $row;

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);

		$row++;
		$worksheet->setCellValue('A' . $row, 'Итого предоставлено медицинских услуг');
		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->mergeCells('A' . $row . ':E' . $row);
		$worksheet->setCellValue('A' . $row, 'Зав. центром');

		$worksheet->mergeCells('F' . $row . ':K' . $row);
		$worksheet->setCellValue('F' . $row, 'Петрова И.В.');

		$writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y'). '.xlsx';

        if (count($items) == 0) {

        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;

        } 

        $writer->save($fileName);

    	$zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
    }

	private function insurance_report_sogaz($insuranceCompanyId)
	{
		$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$spreadsheet->getDefaultStyle()->getFont()->setSize(10);

		$worksheet->getDefaultColumnDimension()->setWidth(6);

		$worksheet->getColumnDimension('B')->setWidth(18);
		$worksheet->getColumnDimension('C')->setWidth(18);
		$worksheet->getColumnDimension('D')->setWidth(12);
		$worksheet->getColumnDimension('F')->setWidth(50);
        
		$spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(30);

        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:J1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:J2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:J3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->mergeCells('A4:J4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:C6');
        $worksheet->setCellValue('B6', 'Сведения о застрахованном');

        $worksheet->mergeCells('D6:D7');
        $worksheet->setCellValue('D6', 'Дата обращения');

        $worksheet->mergeCells('E6:E7');
        $worksheet->setCellValue('E6', 'Код по МКБ');

        $worksheet->mergeCells('F6:F7');
        $worksheet->setCellValue('F6', 'Наименование услуги');

        $worksheet->mergeCells('G6:G7');
        $worksheet->setCellValue('G6', 'Код услуги');
        $worksheet->setCellValue('H6', 'Цена');

        $worksheet->mergeCells('I6:I7');
        $worksheet->setCellValue('I6', 'Кол-во');

        $worksheet->setCellValue('J6', 'Цена');

        //$worksheet->setCellValue('A7', 'п/п');
        $worksheet->setCellValue('B7', 'Ф.И.О.');
        $worksheet->setCellValue('C7', '№ полиса ДМС');
        $worksheet->setCellValue('H7', '(руб)');
        $worksheet->setCellValue('J7', '(руб)');

        foreach (range('A', 'J') as $letter) {
        	for ($i = 6; $i <= 7; $i++) {
        		$worksheet->getStyle($letter . $i)->getAlignment()->setWrapText(true);

				$worksheet->getStyle($letter . $i)->applyFromArray($this->headerStyleArray);
        	} 
        }

        $items = [];

        $publicDir = public_path();

    	$admissions = Admission::where('insurance_company_id', $insuranceCompanyId)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

		if ($admissions->count() > 0) {

			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			foreach ($admissions as $admission) {

				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];

				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}

				$currentItem['main']['B'] = $admission->name;
				$currentItem['main']['C'] = $admission->policy_num;
				$currentItem['main']['D'] = Carbon::parse($admission->date)->format('d/m/Y');

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {

					//первичный приём
					$serviceKey = 0;
					$price = Price::where('code', 'К014')->first();
					$currentItem['services'][$serviceKey]['E'] = 'W57';
					$currentItem['services'][$serviceKey]['F'] = $price->full_name;
					$currentItem['services'][$serviceKey]['G'] = $price->code; 
					$currentItem['services'][$serviceKey]['H'] = $price->price; 
					$currentItem['services'][$serviceKey]['I'] = '1'; 

					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К015')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}

				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							if (isset($serviceKey)) {
								$serviceKey++;
							} else {
								$serviceKey = 0;
							}

							$price = $research->price;

							$currentItem['services'][$serviceKey]['E'] = 'W57';
							$currentItem['services'][$serviceKey]['F'] = $price->full_name;
							$currentItem['services'][$serviceKey]['G'] = $price->code; 
							$currentItem['services'][$serviceKey]['H'] = $price->price; 
							$currentItem['services'][$serviceKey]['I'] = '1'; 
							
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price;
						
						if ($admission->material_id == '1') {
							$currentItem['services'][$serviceKey]['I'] = $admission->material_amount;
						} else {
							$currentItem['services'][$serviceKey]['I'] = '1';
						}

					}

					//забор крови
					if ($admission->material_id == '2') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'А2331196')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}

				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К019')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = $admission->ig_dose;

						$serviceKey++;
						$price = Price::where('code', 'П002')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}
				}

				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К021')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}

					//профилактика КЭ
					if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К020')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}


				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К022')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}

				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста
					if ($admission->infect_consult) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'В036')->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}

				}

				$items[] = $currentItem;
			}

			$regZipFile->close();

		}

		$antibodies = Antibody::where('insurance_company_id', $insuranceCompanyId)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];

			$currentItem['main']['B'] = $antibody->name;
			$currentItem['main']['C'] = $antibody->policy_num;
			$currentItem['main']['D'] = Carbon::parse($antibody->date)->format('d/m/Y');

			$currentItem['services'] = [];

			$serviceKey = 0;

			$price = Price::where('code', 'А2331196')->first();
			$currentItem['services'][$serviceKey]['E'] = 'W57';
			$currentItem['services'][$serviceKey]['F'] = $price->full_name;
			$currentItem['services'][$serviceKey]['G'] = $price->code; 
			$currentItem['services'][$serviceKey]['H'] = $price->price; 
			$currentItem['services'][$serviceKey]['I'] = '1';

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$serviceKey++;
				$price = Price::where('code', 'В036')->first();
				$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['F'] = $price->full_name;
				$currentItem['services'][$serviceKey]['G'] = $price->code; 
				$currentItem['services'][$serviceKey]['H'] = $price->price; 
				$currentItem['services'][$serviceKey]['I'] = '1'; 
			}

			//исследования
			/*
			$researchTypes = json_decode($antibody->research_types);
			if (!empty($researchTypes)) {

				foreach ($researchTypes as $typeId) {
					$type = ResearchType::find($typeId);
					$codes = json_decode($type->price_codes);

					foreach ($codes as $code) {
						$serviceKey++;
						$price = Price::where('code', $code)->first();
						$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['F'] = $price->full_name;
						$currentItem['services'][$serviceKey]['G'] = $price->code; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['I'] = '1'; 
					}
				}
			}*/

			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$serviceKey++;
				
				$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['F'] = $price->full_name;
				$currentItem['services'][$serviceKey]['G'] = $price->code; 
				$currentItem['services'][$serviceKey]['H'] = $price->price;
			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {
				$serviceKey++;

				$price = $research->price;

				$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['F'] = $price->full_name;
				$currentItem['services'][$serviceKey]['G'] = $price->code; 
				$currentItem['services'][$serviceKey]['H'] = $price->price; 
				$currentItem['services'][$serviceKey]['I'] = '1'; 
				
			}

			$items[] = $currentItem;
		}

		usort($items, array($this, 'sortByDateSogaz'));

		$rowStart = $row = $rowEnd = 8;
		//$row = $rowStart;

		foreach ($items as $key=>$item) {
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':J' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 


			foreach ($item['main'] as $column => $value) {
				$worksheet->setCellValue($column . $row, $value);
			}

			foreach ($item['services'] as $rowOffset => $service) {
				foreach ($service as $column => $value) {
					$worksheet->setCellValue($column . $row, $value);
				}
				$worksheet->setCellValue('J' . $row, '=H' . $row . '*I' . $row);
				$rowEnd = $row;
				$row++;
			}
		}

		$worksheet->setCellValue('I' . $row, '=SUM(I' . $rowStart . ':I' . $rowEnd . ')');
		$worksheet->setCellValue('J' . $row, '=SUM(J' . $rowStart . ':J' . $rowEnd . ')');
		$totalCell = 'J' . $row;

		foreach (range('A', 'J') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		for ($i = $rowStart; $i <= $rowEnd; $i++) {

    		$worksheet->getStyle('B' . $i)->getAlignment()->setWrapText(true);

    		$worksheet->getStyle('F' . $i)->getAlignment()->setWrapText(true);

    	}

		foreach (range('I', 'J') as $letter) {

			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);

		}

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);

		$row++;
		$worksheet->setCellValue('A' . $row, 'Итого предоставлено медицинских услуг');
		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');
		$worksheet->setCellValue('H' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');
		$worksheet->setCellValue('H' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Зав. центром');
		$worksheet->setCellValue('H' . $row, 'Петрова И.В.');


        $writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . $dateFrom . ' - ' . $dateTo . '.xlsx';

        if (count($items) == 0) {

        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;
        }

        $writer->save($fileName);

        $zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
	}

	private function insurance_report_vsk($insuranceCompanyId)
	{
		$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$worksheet->getDefaultColumnDimension()->setWidth(10);

		$worksheet->getColumnDimension('A')->setWidth(5);
		$worksheet->getColumnDimension('B')->setWidth(30);
		$worksheet->getColumnDimension('C')->setWidth(30);
		$worksheet->getColumnDimension('D')->setWidth(12);
		$worksheet->getColumnDimension('F')->setWidth(80);
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:I1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:I2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:I3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->mergeCells('A4:I4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:C6');
        $worksheet->setCellValue('B6', 'Сведения о застрахованном');

        $worksheet->mergeCells('D6:D7');
        $worksheet->setCellValue('D6', 'Дата обращения');
        //$worksheet->setCellValue('E6', 'Код по МКБ');

        $worksheet->mergeCells('E6:E7');
        $worksheet->setCellValue('E6', 'Код услуги');

        $worksheet->mergeCells('F6:F7');
        $worksheet->setCellValue('F6', 'Наименование услуги');
        $worksheet->setCellValue('G6', 'Цена');
        $worksheet->mergeCells('H6:H7');
        $worksheet->setCellValue('H6', 'Кол-во');
        $worksheet->setCellValue('I6', 'Цена');

        //$worksheet->setCellValue('A7', 'п/п');
        $worksheet->setCellValue('B7', 'Ф.И.О.');
        $worksheet->setCellValue('C7', '№ полиса ДМС');
        $worksheet->setCellValue('G7', '(руб)');
        $worksheet->setCellValue('I7', '(руб)');

        foreach (range('A', 'I') as $letter) {
        	for ($i = 6; $i <= 7; $i++) {
        		$worksheet->getStyle($letter . $i)->getAlignment()->setWrapText(true);

				$worksheet->getStyle($letter . $i)->applyFromArray($this->headerStyleArray);
        	} 
        }

        $items = [];

        $publicDir = public_path();

    	$admissions = Admission::where('insurance_company_id', $insuranceCompanyId)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

		if ($admissions->count() > 0) {

			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			foreach ($admissions as $admission) {

				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];

				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}

				$currentItem['main']['B'] = $admission->name;
				$currentItem['main']['C'] = str_replace(' ', '', $admission->policy_num);
				$currentItem['main']['D'] = Carbon::parse($admission->date)->format('d.m.Y');

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {

					//первичный приём
					$serviceKey = 0;
					$price = Price::where('code', 'К014')->first();
					//$currentItem['services'][$serviceKey]['E'] = 'W57';
					$currentItem['services'][$serviceKey]['E'] = $price->code;
					$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
					$currentItem['services'][$serviceKey]['G'] = $price->price; 
					$currentItem['services'][$serviceKey]['H'] = '1'; 

					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К015')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							if (isset($serviceKey)) {
								$serviceKey++;
							} else {
								$serviceKey = 0;
							}

							$price = $research->price;

							//$currentItem['services'][$serviceKey]['E'] = 'W57';
							$currentItem['services'][$serviceKey]['E'] = $price->code;
							$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
							$currentItem['services'][$serviceKey]['G'] = $price->price; 
							$currentItem['services'][$serviceKey]['H'] = '1'; 
							
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price;
						
						if ($admission->material_id == '1') {
							$currentItem['services'][$serviceKey]['H'] = $admission->material_amount;
						} else {
							$currentItem['services'][$serviceKey]['H'] = '1';
						}

					}

					//забор крови
					if ($admission->material_id == '2') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'А2331196')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К019')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = $admission->ig_dose;

						$serviceKey++;
						$price = Price::where('code', 'П002')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}
				}

				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К021')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

					//профилактика КЭ
					if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К020')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}


				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К022')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста
					if ($admission->infect_consult) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'В036')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				$items[] = $currentItem;
			}

			$regZipFile->close();

		}

		$antibodies = Antibody::where('insurance_company_id', $insuranceCompanyId)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];

			$currentItem['main']['B'] = $antibody->name;
			$currentItem['main']['C'] = $antibody->policy_num;
			$currentItem['main']['D'] = Carbon::parse($antibody->date)->format('d.m.Y');

			$currentItem['services'] = [];

			$serviceKey = 0;

			$price = Price::where('code', 'А2331196')->first();
			//$currentItem['services'][$serviceKey]['E'] = 'W57';
			$currentItem['services'][$serviceKey]['E'] = $price->code;
			$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
			$currentItem['services'][$serviceKey]['G'] = $price->price; 
			$currentItem['services'][$serviceKey]['H'] = '1';

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$serviceKey++;
				$price = Price::where('code', 'В036')->first();
				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price; 
				$currentItem['services'][$serviceKey]['H'] = '1'; 
			}

			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$serviceKey++;
				
				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price;
			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {
				$serviceKey++;

				$price = $research->price;

				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price; 
				$currentItem['services'][$serviceKey]['H'] = '1'; 
				
			}

			$items[] = $currentItem;
		}

		usort($items, array($this, 'sortByDateVsk'));

		$rowStart = $row = $rowEnd = 8;
		//$row = $rowStart;

		foreach ($items as $key => $item) {
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':I' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 

			$rowCurrentItem = $row;

			foreach ($item['main'] as $column => $value) {
				$worksheet->setCellValue($column . $row, $value);
			}

			foreach ($item['services'] as $rowOffset => $service) {
				foreach ($service as $column => $value) {
					$worksheet->setCellValue($column . $row, $value);
				}
				$worksheet->setCellValue('I' . $row, '=G' . $row . '*H' . $row);

				if ($row != $rowCurrentItem) {
					$worksheet->setCellValue('B' . $row, '=B' . $rowCurrentItem);
					$worksheet->setCellValue('C' . $row, '=C' . $rowCurrentItem);
					$worksheet->setCellValue('D' . $row, '=D' . $rowCurrentItem);
				}

				$rowEnd = $row;
				$row++;
			}
		}
		$worksheet->setCellValue('H' . $row, '=SUM(H' . $rowStart . ':H' . $rowEnd . ')');
		$worksheet->setCellValue('I' . $row, '=SUM(I' . $rowStart . ':I' . $rowEnd . ')');
		$totalCell = 'I' . $row;

		foreach (range('A', 'I') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		for ($i = $rowStart; $i <= $rowEnd; $i++) {

    		$worksheet->getStyle('F' . $i)->getAlignment()->setWrapText(true);

    	}

		foreach (range('H', 'I') as $letter) {

			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);

		}

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);

		$row++;
		$worksheet->setCellValue('A' . $row, 'Итого предоставлено медицинских услуг');
		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');
		$worksheet->setCellValue('F' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');
		$worksheet->setCellValue('F' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Зав. центром');
		$worksheet->setCellValue('F' . $row, 'Петрова И.В.');


        $writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . $dateFrom . ' - ' . $dateTo . '.xlsx';

        if (count($items) == 0) {

        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;
        }

        $writer->save($fileName);

        $zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
	}

	private function insurance_report_ingos($insuranceCompanyId)
	{
		$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$worksheet->getDefaultColumnDimension()->setWidth(7);

		$worksheet->getColumnDimension('A')->setWidth(5);
		$worksheet->getColumnDimension('B')->setWidth(15);
		$worksheet->getColumnDimension('C')->setWidth(15);
		$worksheet->getColumnDimension('D')->setWidth(12);
		$worksheet->getColumnDimension('F')->setWidth(35);
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:I1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:I2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:I3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->mergeCells('A4:I4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:C6');
        $worksheet->setCellValue('B6', 'Сведения о застрахованном');

        $worksheet->mergeCells('D6:D7');
        $worksheet->setCellValue('D6', 'Дата обращения');
        //$worksheet->setCellValue('E6', 'Код по МКБ');

        $worksheet->mergeCells('E6:E7');
        $worksheet->setCellValue('E6', 'Код услуги');

        $worksheet->mergeCells('F6:F7');
        $worksheet->setCellValue('F6', 'Наименование услуги');
        $worksheet->setCellValue('G6', 'Цена');
        $worksheet->mergeCells('H6:H7');
        $worksheet->setCellValue('H6', 'Кол-во');
        $worksheet->setCellValue('I6', 'Цена');

        //$worksheet->setCellValue('A7', 'п/п');
        $worksheet->setCellValue('B7', 'Ф.И.О.');
        $worksheet->setCellValue('C7', '№ полиса ДМС');
        $worksheet->setCellValue('G7', '(руб)');
        $worksheet->setCellValue('I7', '(руб)');

        foreach (range('A', 'I') as $letter) {
        	for ($i = 6; $i <= 7; $i++) {
        		$worksheet->getStyle($letter . $i)->getAlignment()->setWrapText(true);

				$worksheet->getStyle($letter . $i)->applyFromArray($this->headerStyleArray);
        	} 
        }

        $items = [];

        $publicDir = public_path();

    	$admissions = Admission::where('insurance_company_id', $insuranceCompanyId)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

		if ($admissions->count() > 0) {

			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			foreach ($admissions as $admission) {

				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];

				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}

				$currentItem['main']['B'] = $admission->name;
				$currentItem['main']['C'] = str_replace(' ', '', $admission->policy_num);
				$currentItem['main']['D'] = Carbon::parse($admission->date)->format('d.m.Y');

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {

					//первичный приём
					$serviceKey = 0;
					$price = Price::where('code', 'К014')->first();
					//$currentItem['services'][$serviceKey]['E'] = 'W57';
					$currentItem['services'][$serviceKey]['E'] = $price->code;
					$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
					$currentItem['services'][$serviceKey]['G'] = $price->price; 
					$currentItem['services'][$serviceKey]['H'] = '1'; 

					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К015')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							if (isset($serviceKey)) {
								$serviceKey++;
							} else {
								$serviceKey = 0;
							}

							$price = $research->price;

							//$currentItem['services'][$serviceKey]['E'] = 'W57';
							$currentItem['services'][$serviceKey]['E'] = $price->code;
							$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
							$currentItem['services'][$serviceKey]['G'] = $price->price; 
							$currentItem['services'][$serviceKey]['H'] = '1'; 
							
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price;
						
						if ($admission->material_id == '1') {
							$currentItem['services'][$serviceKey]['H'] = $admission->material_amount;
						} else {
							$currentItem['services'][$serviceKey]['H'] = '1';
						}

					}

					//забор крови
					if ($admission->material_id == '2') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'А2331196')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К019')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = $admission->ig_dose;

						$serviceKey++;
						$price = Price::where('code', 'П002')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}
				}

				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К021')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

					//профилактика КЭ
					if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К020')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}
				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К022')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}
				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста
					if ($admission->infect_consult) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'В036')->first();
						//$currentItem['services'][$serviceKey]['E'] = 'W57';
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
						$currentItem['services'][$serviceKey]['G'] = $price->price; 
						$currentItem['services'][$serviceKey]['H'] = '1'; 
					}

				}

				$items[] = $currentItem;
			}

			$regZipFile->close();

		}

		$antibodies = Antibody::where('insurance_company_id', $insuranceCompanyId)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];

			$currentItem['main']['B'] = $antibody->name;
			$currentItem['main']['C'] = $antibody->policy_num;
			$currentItem['main']['D'] = Carbon::parse($antibody->date)->format('d.m.Y');

			$currentItem['services'] = [];

			$serviceKey = 0;

			$price = Price::where('code', 'А2331196')->first();
			//$currentItem['services'][$serviceKey]['E'] = 'W57';
			$currentItem['services'][$serviceKey]['E'] = $price->code;
			$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
			$currentItem['services'][$serviceKey]['G'] = $price->price; 
			$currentItem['services'][$serviceKey]['H'] = '1';

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$serviceKey++;
				$price = Price::where('code', 'В036')->first();
				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price; 
				$currentItem['services'][$serviceKey]['H'] = '1'; 
			}

			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$serviceKey++;
				
				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price;
			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {
				$serviceKey++;

				$price = $research->price;

				//$currentItem['services'][$serviceKey]['E'] = 'W57';
				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->full_name; 
				$currentItem['services'][$serviceKey]['G'] = $price->price; 
				$currentItem['services'][$serviceKey]['H'] = '1'; 
				
			}

			$items[] = $currentItem;
		}

		usort($items, array($this, 'sortByDateIngos'));

		$rowStart = $row = $rowEnd = 8;
		//$row = $rowStart;

		foreach ($items as $key => $item) {
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':I' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 

			$rowCurrentItem = $row;

			foreach ($item['main'] as $column => $value) {
				$worksheet->setCellValue($column . $row, $value);
			}

			foreach ($item['services'] as $rowOffset => $service) {
				foreach ($service as $column => $value) {
					$worksheet->setCellValue($column . $row, $value);
				}
				$worksheet->setCellValue('I' . $row, '=G' . $row . '*H' . $row);

				/*
				if ($row != $rowCurrentItem) {
					$worksheet->setCellValue('B' . $row, '=B' . $rowCurrentItem);
					$worksheet->setCellValue('C' . $row, '=C' . $rowCurrentItem);
					$worksheet->setCellValue('D' . $row, '=D' . $rowCurrentItem);
				}
				*/

				$rowEnd = $row;
				$row++;
			}
		}
		$worksheet->setCellValue('H' . $row, '=SUM(H' . $rowStart . ':H' . $rowEnd . ')');
		$worksheet->setCellValue('I' . $row, '=SUM(I' . $rowStart . ':I' . $rowEnd . ')');
		$totalCell = 'I' . $row;

		foreach (range('A', 'I') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		for ($i = $rowStart; $i <= $rowEnd; $i++) {

    		$worksheet->getStyle('E' . $i)->applyFromArray($this->smallerText);
    		$worksheet->getStyle('F' . $i)->applyFromArray($this->smallerText);
    		$worksheet->getStyle('B' . $i)->getAlignment()->setWrapText(true);
    		$worksheet->getStyle('C' . $i)->getAlignment()->setWrapText(true);
    		$worksheet->getStyle('F' . $i)->getAlignment()->setWrapText(true);

    	}

		foreach (range('H', 'I') as $letter) {

			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);

		}

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);

		$row++;
		$worksheet->setCellValue('A' . $row, 'Итого предоставлено медицинских услуг');
		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');
		$worksheet->setCellValue('F' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');
		$worksheet->setCellValue('F' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Зав. центром');
		$worksheet->setCellValue('F' . $row, 'Петрова И.В.');


        $writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . $dateFrom . ' - ' . $dateTo . '.xlsx';

        if (count($items) == 0) {

        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;
        }

        $writer->save($fileName);

        $zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
	}

	private function insurance_report_reso($insuranceCompanyId)
	{
		$insuranceCompany = InsuranceCompany::find($insuranceCompanyId);

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();

		$worksheet->getDefaultColumnDimension()->setWidth(10);

		$worksheet->getColumnDimension('A')->setWidth(5);
		$worksheet->getColumnDimension('B')->setWidth(12);
		$worksheet->getColumnDimension('C')->setWidth(20);
		$worksheet->getColumnDimension('D')->setWidth(30);
		$worksheet->getColumnDimension('F')->setWidth(30);
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($dateTo)->month - 1]);

        $worksheet->mergeCells('A1:N1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $insuranceCompany->full_name);

        $worksheet->mergeCells('A2:N2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:N3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->mergeCells('A4:N4');
        $worksheet->setCellValue('A4', 'Вид страхования: добровольное медицинское страхование');

        $worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A6', '№ п/п');

        $worksheet->mergeCells('B6:B7');
        $worksheet->setCellValue('B6', 'Дата');

        $worksheet->mergeCells('C6:C7');
        $worksheet->setCellValue('C6', '№ полиса');

        $worksheet->mergeCells('D6:D7');
        $worksheet->setCellValue('D6', 'ФИО');

        $worksheet->mergeCells('E6:E7');
        $worksheet->setCellValue('E6', 'Код услуги');

        $worksheet->mergeCells('F6:F7');
        $worksheet->setCellValue('F6', 'Наименование услуги');

        $worksheet->mergeCells('G6:G7');
        $worksheet->setCellValue('G6', 'Количество');

        $worksheet->mergeCells('H6:H7');
        $worksheet->setCellValue('H6', 'Цена');

        $worksheet->mergeCells('I6:I7');
        $worksheet->setCellValue('I6', 'Врач');

        $worksheet->mergeCells('J6:J7');
		$worksheet->setCellValue('J6', 'Диагноз');

		$worksheet->mergeCells('K6:K7');
		$worksheet->setCellValue('K6', 'Код ЛПУ (если отличается от кода в счете)');

		$worksheet->mergeCells('L6:L7');
		$worksheet->setCellValue('L6', 'Дата окончания услуги');

		$worksheet->mergeCells('M6:M7');
        $worksheet->setCellValue('M6', 'Примечание');

        $worksheet->mergeCells('N6:N7');
        $worksheet->setCellValue('N6', 'Итого');

        $spreadsheet->getActiveSheet()->getRowDimension('6')->setRowHeight(30);

        foreach (range('A', 'N') as $letter) {
        	for ($i = 6; $i <= 7; $i++) {
        		$worksheet->getStyle($letter . $i)->getAlignment()->setWrapText(true);

				$worksheet->getStyle($letter . $i)->applyFromArray($this->headerStyleArray);
        	} 
        }

        $items = [];

        $publicDir = public_path();

    	$admissions = Admission::where('insurance_company_id', $insuranceCompanyId)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

		if ($admissions->count() > 0) {
			// регистрационные карты

	    	$regZipFileName = 'Регистрационные_карты.zip';

	    	$pathToZip = $publicDir . '/' . $regZipFileName;

			$regZipFile = new \ZipArchive();

			$regZipFile->open($regZipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

			$files = [];

			// разбор массива первичных приемов
			foreach ($admissions as $admission) {
				/* 
				добавление в архив регистрационной карты приёма 
				*/
				$fileName = $this->generate_pdf($admission);

	    		$files[] = $fileName;

	    		$regZipFile->addFile($fileName);

	    		/* 
	    		END 
	    		добавление в архив регистрационной карты приёма 
	    		*/

				$currentItem = [];

				if ($admission->needs_attention) {
					$currentItem['attention'] = 1;
				}

				$currentItem['main']['B'] = Carbon::parse($admission->date)->format('d/m/Y');
				$currentItem['main']['C'] = $admission->policy_num;
				$currentItem['main']['D'] = $admission->name;

				$currentItem['services'] = [];

				// если дата обращения или дата возврата попадает в обозначенный период, фиксируем первичное обращение и при необходимости удаление клеща.
				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {

					//первичный приём
					$serviceKey = 0;
					$price = Price::where('code', 'К014')->first();
					$currentItem['services'][$serviceKey]['E'] = $price->code;
					$currentItem['services'][$serviceKey]['F'] = $price->name;
					$currentItem['services'][$serviceKey]['G'] = '1';
					$currentItem['services'][$serviceKey]['H'] = $price->price;
					$currentItem['services'][$serviceKey]['J'] = 'W57';
					$currentItem['services'][$serviceKey]['L'] = Carbon::parse($admission->date)->format('d/m/Y');


					//удаление клеща
					if ($admission->removal_institution_id == '1') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К015')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}
				}

				// если дата результатов или дата возврата попадает в обозначенный период, фиксируем исследования
				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {

					// ПЦР и другие исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {

							if (isset($serviceKey)) {
								$serviceKey++;
							} else {
								$serviceKey = 0;
							}

							$price = $research->price;

							$currentItem['services'][$serviceKey]['E'] = $price->code; 
							$currentItem['services'][$serviceKey]['F'] = $price->name;
							$currentItem['services'][$serviceKey]['G'] = '1'; 
							$currentItem['services'][$serviceKey]['H'] = $price->price; 
							$currentItem['services'][$serviceKey]['J'] = 'W57';
							
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$currentItem['services'][$serviceKey]['E'] = $price->code;
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['H'] = $price->price;
						$currentItem['services'][$serviceKey]['J'] = 'W57';
						
						if ($admission->material_id == '1') {
							$currentItem['services'][$serviceKey]['G'] = $admission->material_amount;
						} else {
							$currentItem['services'][$serviceKey]['G'] = '1';
						}

					}

					//забор крови
					if ($admission->material_id == '2') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'А2331196')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}
				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig по весу человека
					if ($admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К019')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = $admission->ig_dose; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';

						$serviceKey++;
						$price = Price::where('code', 'П002')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}

				}

				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ
					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К021')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}

					//профилактика КЭ
					if ($admission->encephalitis_method_id && $admission->encephalitis_method_id != '4') {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К020')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}
				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация специалиста по факту укуса
					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'К022')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}

				}

				// если дата консультации инфекциониста или дата возврата попадает в обозначенный период, фиксируем консультацию
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста
					if ($admission->infect_consult) {
						if (isset($serviceKey)) {
							$serviceKey++;
						} else {
							$serviceKey = 0;
						}
						$price = Price::where('code', 'В036')->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}

				}

				$items[] = $currentItem;
			}

			$regZipFile->close();
		}

		$antibodies = Antibody::where('insurance_company_id', $insuranceCompanyId)
					  ->whereDate('date', '>=', $dateFrom)
					  ->whereDate('date', '<=', $dateTo)->get();

		foreach ($antibodies as $antibody) {
			$currentItem = [];

			$currentItem['main']['B'] = Carbon::parse($antibody->date)->format('d/m/Y');
			$currentItem['main']['C'] = $antibody->policy_num;
			$currentItem['main']['D'] = $antibody->name;

			$currentItem['services'] = [];

			$serviceKey = 0;

			$price = Price::where('code', 'А2331196')->first();
			$currentItem['services'][$serviceKey]['E'] = $price->code; 
			$currentItem['services'][$serviceKey]['F'] = $price->name;
			$currentItem['services'][$serviceKey]['G'] = '1'; 
			$currentItem['services'][$serviceKey]['H'] = $price->price; 
			$currentItem['services'][$serviceKey]['J'] = 'W57';
			$currentItem['services'][$serviceKey]['L'] = Carbon::parse($antibody->date)->format('d/m/Y');

			//исследования
			/*$researchTypes = json_decode($antibody->research_types);
			if (!empty($researchTypes)) {

				foreach ($researchTypes as $typeId) {
					$type = ResearchType::find($typeId);
					$codes = json_decode($type->price_codes);

					foreach ($codes as $code) {
						$serviceKey++;
						$price = Price::where('code', $code)->first();
						$currentItem['services'][$serviceKey]['E'] = $price->code; 
						$currentItem['services'][$serviceKey]['F'] = $price->name;
						$currentItem['services'][$serviceKey]['G'] = '1'; 
						$currentItem['services'][$serviceKey]['H'] = $price->price; 
						$currentItem['services'][$serviceKey]['J'] = 'W57';
					}
				}
			}*/

			$pcrNum = $antibody->researches->where('type', 'pcr')->count();

			if ($pcrNum > 0) {

				$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

				$serviceKey++;

				$currentItem['services'][$serviceKey]['E'] = $price->code;
				$currentItem['services'][$serviceKey]['F'] = $price->name;
				$currentItem['services'][$serviceKey]['H'] = $price->price;
				$currentItem['services'][$serviceKey]['J'] = 'W57';

			}

			$restRes = $antibody->researches->where('type', '<>', 'pcr');

			foreach ($restRes as $research) {

				$serviceKey++;

				$price = $research->price;

				$currentItem['services'][$serviceKey]['E'] = $price->code; 
				$currentItem['services'][$serviceKey]['F'] = $price->name;
				$currentItem['services'][$serviceKey]['G'] = '1'; 
				$currentItem['services'][$serviceKey]['H'] = $price->price; 
				$currentItem['services'][$serviceKey]['J'] = 'W57';
				
			}

			//консультация инфекциониста
			if ($antibody->infect_consult) {
				$serviceKey++;
				$price = Price::where('code', 'В036')->first();
				$currentItem['services'][$serviceKey]['E'] = $price->code; 
				$currentItem['services'][$serviceKey]['F'] = $price->name;
				$currentItem['services'][$serviceKey]['G'] = '1'; 
				$currentItem['services'][$serviceKey]['H'] = $price->price; 
				$currentItem['services'][$serviceKey]['J'] = 'W57';
			}

			$items[] = $currentItem;
		}

		usort($items, array($this, 'sortByDateReso'));

		$rowStart = $row = $rowEnd = 8;

		foreach ($items as $key=>$item) {
			$worksheet->setCellValue('A' . $row, $key + 1);

			if (isset($item['attention'])) {
    			$worksheet->getStyle('A' . $row . ':N' . $row)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffff00');

    			unset($item['attention']);
			} 

			foreach ($item['main'] as $column => $value) {
				$worksheet->setCellValue($column . $row, $value);
			}

			foreach ($item['services'] as $service) {
				foreach ($service as $column => $value) {
					$worksheet->setCellValue($column . $row, $value);
				}
				$worksheet->setCellValue('N' . $row, '=G' . $row . '*H' . $row);
				$rowEnd = $row;
				$row++;
			}
		}
		$worksheet->setCellValue('G' . $row, '=SUM(G' . $rowStart . ':G' . $rowEnd . ')');
		$worksheet->setCellValue('N' . $row, '=SUM(N' . $rowStart . ':N' . $rowEnd . ')');
		$totalCell = 'N' . $row;

		foreach (range('A', 'N') as $letter) {
			for ($i = $rowStart; $i <= $rowEnd; $i++) {

				$worksheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}
		}

		foreach (range('G', 'N') as $letter) {

			$worksheet->getStyle($letter . $row)->applyFromArray($this->cellStyleArray);

		}

		$worksheet->getStyle($totalCell)->applyFromArray($this->totalStyleArray);


		$row++;
		$worksheet->setCellValue('A' . $row, '=CONCATENATE("Итого предоставлено медицинских услуг по диагностике и профилактике клещевых инфекций для СПАО Ресо Гарантия на общую сумму ", ' . $totalCell . ', " рублей, 00 копеек")');

		$row += 2;
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');
		$worksheet->setCellValue('I' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');
		$worksheet->setCellValue('I' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Зав. центром');
		$worksheet->setCellValue('I' . $row, 'Петрова И.В.');


        $writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр ' . $insuranceCompany->name . ' ' . $dateFrom . ' - ' . $dateTo . '.xlsx';

        $writer->save($fileName);

        if (count($items) == 0) {
        	$fileName = 'ПУСТОЙ ' . $fileName;

        	$writer->save($fileName);

        	return $fileName;
        }

        $writer->save($fileName);

    	$zipFileName = 'Реестр и рег.карты ' . $insuranceCompany->name . ' ' . Carbon::parse($dateFrom)->format('d.m.Y') . ' - ' . Carbon::parse($dateTo)->format('d.m.Y') . '.zip';

    	$pathToZip = $publicDir . '/' . $zipFileName;

		$zipFile = new \ZipArchive();

		$zipFile->open($zipFileName, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

    	$zipFile->addFile($fileName);

    	$files[] = $fileName;

    	if (isset($regZipFileName)) {

	    	$zipFile->addFile($regZipFileName);

	    	$files[] = $regZipFileName;

    	}

    	$zipFile->close();

    	foreach ($files as $file) {

    		unlink($file);
    	}

        return $zipFileName;
	}

	private function insurance_report_kolymskaya($insuranceCompanyId)
	{
	}

	private function sortByDate($a, $b) 
	{
	    if (Carbon::createFromFormat('d/m/Y', $a['B']) == Carbon::createFromFormat('d/m/Y', $b['B'])) {
	        return 0;
	    }
	    return (Carbon::createFromFormat('d/m/Y', $a['B']) < Carbon::createFromFormat('d/m/Y', $b['B'])) ? -1 : 1;
	}

	private function sortByDateVsk($a, $b) 
	{
	    if (Carbon::createFromFormat('d.m.Y', $a['main']['D']) == Carbon::createFromFormat('d.m.Y', $b['main']['D'])) {
	        return 0;
	    }
	    return (Carbon::createFromFormat('d.m.Y', $a['main']['D']) < Carbon::createFromFormat('d.m.Y', $b['main']['D'])) ? -1 : 1;
	}

	private function sortByDateIngos($a, $b) 
	{
	    if (Carbon::createFromFormat('d.m.Y', $a['main']['D']) == Carbon::createFromFormat('d.m.Y', $b['main']['D'])) {
	        return 0;
	    }
	    return (Carbon::createFromFormat('d.m.Y', $a['main']['D']) < Carbon::createFromFormat('d.m.Y', $b['main']['D'])) ? -1 : 1;
	}

	private function sortByDateSogaz($a, $b) 
	{
	    if (Carbon::createFromFormat('d/m/Y', $a['main']['D']) == Carbon::createFromFormat('d/m/Y', $b['main']['D'])) {
	        return 0;
	    }
	    return (Carbon::createFromFormat('d/m/Y', $a['main']['D']) < Carbon::createFromFormat('d/m/Y', $b['main']['D'])) ? -1 : 1;
	}

	private function sortByDateReso($a, $b)
	{
	    if (Carbon::createFromFormat('d/m/Y', $a['main']['B']) == Carbon::createFromFormat('d/m/Y', $b['main']['B'])) {
	        return 0;
	    }
	    return (Carbon::createFromFormat('d/m/Y', $a['main']['B']) < Carbon::createFromFormat('d/m/Y', $b['main']['B'])) ? -1 : 1;
	}

    private function yearly_report() 
    {
    	//$this->abc = range('A', 'Z');
    	
    	$year = request()->input('year');

    	$spreadsheet = new Spreadsheet();

    	for ($i = 1; $i <= 8; $i++) {

    		$functionName = 'ses_report_page_' . $i;

    		$spreadsheet = $this->$functionName($spreadsheet, $year);

    	}

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Отчёт в СЭС.xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    // done
    private function ses_report_page_1($spreadsheet, $year)
    {
    	/* СТРАНИЦА 1 : Вакцинации */
        /* Виды вакцин */
        $worksheet = $spreadsheet->getActiveSheet();
        $worksheet->setTitle('Вакцинации');

        $worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица №1. Виды применяемых вакцин клещевого энцефалита.');

        $worksheet->mergeCells('A3:A4');

        $worksheet->mergeCells('B3:C3');
        $worksheet->setCellValue('B3', 'вакцинировано');

        $worksheet->mergeCells('D3:E3');
        $worksheet->setCellValue('D3', 'ревакцинировано');

        $worksheet->mergeCells('F3:F4');
        $worksheet->setCellValue('F3', 'Израсходовано доз вакцины КЭ');
        $worksheet->getStyle('F3')->getAlignment()->setWrapText(true);

        $worksheet->setCellValue('B4', 'всего');
        $worksheet->setCellValue('C4', 'в т.ч. детей до 14 лет');
        $worksheet->setCellValue('D4', 'всего');
        $worksheet->setCellValue('E4', 'в т.ч. детей до 14 лет');

        $vaccines = Vaccine::all();
        $vaccinationQuery = Vaccination::whereYear('date', $year);

        $row = 5;

        foreach ($vaccines as $vaccine) {
        	$worksheet->setCellValue('A' . $row, $vaccine->name);

        	$query = (clone $vaccinationQuery)->where('vaccine_id', $vaccine->id);

        	$all = (clone $query)->count();

        	$vacQuery = (clone $query)->where('vaccine_level_id', '1');
        	$revacQuery = (clone $query)->where('vaccine_level_id', '=', '4');

        	$vacAll = (clone $vacQuery)->count();
        	$vacKids = (clone $vacQuery)->where('age', '<=', '14')->count();

        	$revacAll = (clone $revacQuery)->count();
        	$revacKids = (clone $revacQuery)->where('age', '<=', '14')->count();

        	$worksheet->setCellValue('B' . $row, $vacAll);
        	$worksheet->setCellValue('C' . $row, $vacKids);
        	$worksheet->setCellValue('D' . $row, $revacAll);
        	$worksheet->setCellValue('E' . $row, $revacKids);
        	$worksheet->setCellValue('F' . $row, $all);

        	$row++;
        }

        $worksheet->setCellValue('A' . $row, 'Всего');
        
        $all = (clone $vaccinationQuery)->count();

    	$vaccinatedQuery = (clone $vaccinationQuery)->where('vaccine_level_id', '1');
    	$revaccinatedQuery = (clone $vaccinationQuery)->where('vaccine_level_id', '=', '4');

    	$vaccinatedAll = (clone $vaccinatedQuery)->count();
    	$vaccinatedKids = (clone $vaccinatedQuery)->where('age', '<=', '14')->count();

    	$revaccinatedAll = (clone $revaccinatedQuery)->count();
    	$revaccinatedKids = (clone $revaccinatedQuery)->where('age', '<=', '14')->count();

    	$worksheet->setCellValue('B' . $row, $vaccinatedAll);
    	$worksheet->setCellValue('C' . $row, $vaccinatedKids);
    	$worksheet->setCellValue('D' . $row, $revaccinatedAll);
    	$worksheet->setCellValue('E' . $row, $revaccinatedKids);
    	$worksheet->setCellValue('F' . $row, $all);

    	/* END Виды вакцин */

    	/* Иммунизация по контингентам */

    	$row += 2;

    	$worksheet->mergeCells('A' . $row . ':F' . $row);
        $worksheet->setCellValue('A' . $row, 'Таблица № 2. Иммунизация по контингентам.');

        $row += 2;

        $worksheet->mergeCells('A' . $row . ':B' . $row);
        $worksheet->setCellValue('A' . $row, 'Привитые контингенты');
		$worksheet->setCellValue('C' . $row, 'Вакцинировано');
        $worksheet->setCellValue('D' . $row, 'Ревакцинировано');

        $row++;

        $groups = [
			[
				'type' => 'prof_contingent',
				'title' => 'Проф. контингенты',
			],
			[
				'type' => 'risk_group',
				'title' => 'Группы риска',
			],
			[
				'type' => 'social_group',
				'title' => 'Социальные группы',
			],
		];

		$vaccinatedTotal = 0;
		$revaccinatedTotal = 0;
        foreach ($groups as $group) {
        	$type = $group['type'];
        	$title = $group['title'];
        	$worksheet->setCellValue('A' . $row, $title . ', всего');
        
	        $groupItemIds = Group::select('id')->where('type', $type)->get()->toArray();
	        $vaccinated = (clone $vaccinationQuery)->whereIn('group_id', $groupItemIds)->count();

	        $groupItems = Group::where('type', $type)->get();

	        $totalRow = $row;
	        $groupTotal = 0;
	        $row++;
	        
	        foreach ($groupItems as $item) {
	        	$worksheet->setCellValue('A' . $row, $item->name);
	        	$query = (clone $vaccinationQuery)->where('group_id', $item->id);
	        	$vaccinated = (clone $query)->where('vaccine_level_id', '1')->count();
	        	$revaccinated = (clone $query)->where('vaccine_level_id', '!=', '1')->count();
	        	$all = (clone $query)->count();

	        	$worksheet->setCellValue('B' . $row, $all);
	        	$worksheet->setCellValue('C' . $row, $vaccinated);
	        	$worksheet->setCellValue('D' . $row, $revaccinated);

	        	$row++;

	        	$groupTotal += $all;
	        	$vaccinatedTotal += $vaccinated;
				$revaccinatedTotal += $revaccinated;
	        }
	        $worksheet->setCellValue('B' . $totalRow, $groupTotal);
        }
        $worksheet->setCellValue('A' . $row, 'Всего');
        $worksheet->setCellValue('B' . $row, $vaccinatedTotal + $revaccinatedTotal);
        $worksheet->setCellValue('C' . $row, $vaccinatedTotal);
        $worksheet->setCellValue('D' . $row, $revaccinatedTotal);
        
        /* END Иммунизация по контингентам */

        /* Иммунизация по возрастным группам */

        $row += 2;

    	$worksheet->mergeCells('A' . $row . ':F' . $row);
        $worksheet->setCellValue('A' . $row, 'Таблица № 3. Иммунизация по возрастным группам.');

        $row += 2;

        $worksheet->setCellValue('A' . $row, 'Привито');
        $worksheet->mergeCells('B' . $row . ':C' . $row);
		$worksheet->setCellValue('B' . $row, 'Вакцинировано');
        $worksheet->mergeCells('D' . $row . ':E' . $row);
		$worksheet->setCellValue('D' . $row, 'Ревакцинировано');

		$periods = [
			['from' => '0', 'symbol_from' => '>=', 'to' => '3', 'symbol_to' => '<', 'title' => 'Дети до 3 лет'],
			['from' => '3', 'symbol_from' => '>=', 'to' => '6', 'symbol_to' => '<=', 'title' => 'Дети от 3 до 6 лет'],
			['from' => '7', 'symbol_from' => '>=', 'to' => '14', 'symbol_to' => '<', 'title' => 'Дети от 7 до 14 лет'],
			['from' => '14', 'symbol_from' => '>=', 'to' => '18', 'symbol_to' => '<', 'title' => 'Дети от 14 до 18 лет'],
			//['from' => '18', 'symbol_from' => '>=', 'to' => '180', 'symbol_to' => '<=', 'title' => 'Дети от 14 до 18 лет'],
		];

		$kidsTotal = 0;

		foreach ($periods as $period) {
			$row++;
			$worksheet->setCellValue('A' . $row, $period['title']);
			$ageGroupQuery = (clone $vaccinationQuery)
							->where('age', $period['symbol_from'], $period['from'])
							->where('age', $period['symbol_to'], $period['to']);

			$ageGroupVacQuery = (clone $ageGroupQuery)->where('vaccine_level_id', 1);
			$ageGroupRevacQuery = (clone $ageGroupQuery)->where('vaccine_level_id', '!=', 1);
			$worksheet->mergeCells('B' . $row . ':C' . $row);
			$worksheet->setCellValue('B' . $row, $ageGroupVacQuery->count());
			$worksheet->mergeCells('D' . $row . ':E' . $row);
			$worksheet->setCellValue('D' . $row, $ageGroupRevacQuery->count());

			$row++;
			$worksheet->setCellValue('A' . $row, 'из них организованные');
			$ageGroupVacOrg = (clone $ageGroupVacQuery)->where('organized', 1)->count();
			$ageGroupRevacOrg = (clone $ageGroupRevacQuery)->where('organized', 1)->count();
			$worksheet->mergeCells('B' . $row . ':C' . $row);
			$worksheet->setCellValue('B' . $row, $ageGroupVacOrg);
			$worksheet->mergeCells('D' . $row . ':E' . $row);
			$worksheet->setCellValue('D' . $row, $ageGroupRevacOrg);

			$kidsTotal += $ageGroupQuery->count();
		}

		$row++;
		$worksheet->setCellValue('A' . $row, 'Прочие');
		$ageGroupQuery = (clone $vaccinationQuery)
						->where('age', '>=', '18');

		$ageGroupVacQuery = (clone $ageGroupQuery)->where('vaccine_level_id', 1);
		$ageGroupRevacQuery = (clone $ageGroupQuery)->where('vaccine_level_id', '!=', 1);
		$worksheet->mergeCells('B' . $row . ':C' . $row);
		$worksheet->setCellValue('B' . $row, $ageGroupVacQuery->count());
		$worksheet->mergeCells('D' . $row . ':E' . $row);
		$worksheet->setCellValue('D' . $row, $ageGroupRevacQuery->count());

		$row++;
		$worksheet->setCellValue('A' . $row, 'Всего');
		$worksheet->mergeCells('B' . $row . ':C' . $row);
		$worksheet->setCellValue('B' . $row, $vaccinatedTotal);
		$worksheet->mergeCells('D' . $row . ':E' . $row);
		$worksheet->setCellValue('D' . $row, $revaccinatedTotal);

		$row++;
		$worksheet->mergeCells('B' . $row . ':E' . $row);
		$worksheet->setCellValue('B' . $row, $vaccinatedTotal + $revaccinatedTotal);

		return $spreadsheet;

        /* END Иммунизация по возрастным группам */

        /* END СТРАНИЦА 1 : Вакцинации */
    }

    // done
    public function ses_report_page_2($spreadsheet, $year)
    {
    	/* СТРАНИЦА 2 : Сроки обращения */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Сроки обращения');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица №2. Сроки обращения населения по поводу присасывания клещей.');

        $admissionQuery = Admission::whereYear('date', $year)->whereNotNull('bite_date');
        
        /*
        $taigaTickQuery = (clone $admissionQuery)->whereHas('result', function($query) {
        	$query->where('tick_type_id', 1);
		*/
        $taigaTickQuery = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_type_id', 1);
        });
        if ($taigaTickQuery->count() > 0) {

	        $taigaTickFirst = (clone $taigaTickQuery)->orderBy('bite_date', 'ASC')->first()->bite_date;
	        $taigaTickLast = (clone $taigaTickQuery)->orderBy('bite_date', 'DESC')->first()->bite_date;

	        $worksheet->mergeCells('A3:D3');
	        $worksheet->setCellValue('A3', 'Первый укус таёжного клеща - ' . Carbon::Parse($taigaTickFirst)->format('d.m.Y'));
	        $worksheet->mergeCells('E3:H3');
	        $worksheet->setCellValue('E3', 'Последний укус таёжного клеща - ' . Carbon::Parse($taigaTickLast)->format('d.m.Y'));

        }

        /*
        $steppeTickQuery = (clone $admissionQuery)->whereHas('result', function($query) {
        	$query->where('tick_type_id', 2);
        });
        */
        $steppeTickQuery = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_type_id', 2);
        });
        if ($steppeTickQuery->count() > 0) {
        
	        $steppeTickFirst = (clone $steppeTickQuery)->orderBy('bite_date', 'ASC')->first()->bite_date;
	        $steppeTickLast = (clone $steppeTickQuery)->orderBy('bite_date', 'DESC')->first()->bite_date;

	        $worksheet->mergeCells('A4:D4');
	        $worksheet->setCellValue('A4', 'Первый укус степного клеща - ' . Carbon::Parse($steppeTickFirst)->format('d.m.Y'));
	        $worksheet->mergeCells('E4:H4');
	        $worksheet->setCellValue('E4', 'Последний укус степного клеща - ' . Carbon::Parse($steppeTickLast)->format('d.m.Y'));

	    }

	    //$months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
	    //$weeks = [range(1,7), range(8,14), range(15,21), range(22,31)];
	    $weeks = [[1, 7], [8, 14], [15, 21], [22, 31]];

	   	$admissionFirstMonth = intval(Carbon::Parse((clone $admissionQuery)->orderBy('date', 'ASC')->first()->date)->format('m'));
	   	$admissionLastMonth = intval(Carbon::Parse((clone $admissionQuery)->orderBy('date', 'DESC')->first()->date)->format('m'));

	   	$worksheet->mergeCells('A6:A7');
        $worksheet->setCellValue('A8', 'Взрослые');
        $worksheet->setCellValue('A9', 'Дети');
        $worksheet->setCellValue('A10', 'Всего');

        $column = 2;

	   	for ($i = $admissionFirstMonth; $i <= $admissionLastMonth; $i++) {
	   		$columnStart = $column;
	   		$admissionMonthQuery = (clone $admissionQuery)->whereMonth('date', $i);

	   		foreach ($weeks as $key=>$week) {
	   			$columnLetter = $this->getColumnFromNum($column);
	   			$admissionWeekQuery = (clone $admissionMonthQuery)->whereDay('date', '>=', $week[0])->whereDay('date', '<=', $week[1]);

	   			$worksheet->setCellValue($columnLetter . '7', $key+1 . 'нед');

	   			$admissionWeekKids = (clone $admissionWeekQuery)->where('age', '<', '18')->count();
	   			$admissionWeekAdults = (clone $admissionWeekQuery)->where('age', '>=', '18')->count();
	   			$admissionWeekAll = $admissionWeekQuery->count();


	   			$worksheet->setCellValue($columnLetter . '8', $admissionWeekAdults);
		        $worksheet->setCellValue($columnLetter . '9', $admissionWeekKids);
		        $worksheet->setCellValue($columnLetter . '10', $admissionWeekAll);

	   			$columnEnd = $column;
	   			$column++;
	   		}

	   		$total = $admissionMonthQuery->count();
	   		$worksheet->mergeCells($this->getColumnFromNum($columnStart) . '6:' . $this->getColumnFromNum($columnEnd) . '6');
        	$worksheet->setCellValue($this->getColumnFromNum($columnStart) . '6', $this->months[$i - 1]);
        	$worksheet->mergeCells($this->getColumnFromNum($columnStart) . '11:' . $this->getColumnFromNum($columnEnd) . '11');
        	$worksheet->setCellValue($this->getColumnFromNum($columnStart) . '11', $total);
	   	}

	   	$columnLetter = $this->getColumnFromNum($column);
	   	$worksheet->setCellValue($columnLetter . '6', 'Всего');
	   	$worksheet->setCellValue($columnLetter . '8', (clone $admissionQuery)->where('age', '>=', 18)->count());
	   	$worksheet->setCellValue($columnLetter . '9', (clone $admissionQuery)->where('age', '<', 18)->count());
	   	$worksheet->setCellValue($columnLetter . '10', $admissionQuery->count());

    	return $spreadsheet;
    }

    // done
    public function ses_report_page_3($spreadsheet, $year)
    {
    	/* СТРАНИЦА 3 : Обращаемость */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Обращаемость');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица №3.  Обращаемость населения с укусами клещей, проведение экспресс-исследования клещей и экстренной профилактики.');

        $worksheet->mergeCells('A3:A5');
        $worksheet->setCellValue('A3', 'Территория');

        $worksheet->mergeCells('B3:C4');
        $worksheet->setCellValue('B3', 'Всего обратилось с укусами клещей');
        $worksheet->setCellValue('B5', 'Всего');
        $worksheet->setCellValue('C5', 'Дети');

        $worksheet->mergeCells('D3:E4');
        $worksheet->setCellValue('D3', 'Из них привито');
        $worksheet->setCellValue('D5', 'Всего');
        $worksheet->setCellValue('E5', 'Дети');

        $worksheet->mergeCells('F3:F5');
        $worksheet->setCellValue('F3', 'Проведено экспресс-исследований клещей');

        $worksheet->mergeCells('G3:H4');
        $worksheet->setCellValue('G3', 'Из них положительные');
        $worksheet->setCellValue('G5', 'На КЭ');
        $worksheet->setCellValue('H5', 'На КБ');

        $worksheet->mergeCells('I3:I5');
        $worksheet->setCellValue('I3', 'Проведено экспресс-исследований клещей метод ПЦР');

        $worksheet->mergeCells('J3:M4');
        $worksheet->setCellValue('J3', 'Из них положительные');
        $worksheet->setCellValue('J5', 'На КЭ');
        $worksheet->setCellValue('K5', 'На КБ');
        $worksheet->setCellValue('L5', 'МЭЧ');
        $worksheet->setCellValue('M5', 'ГАЧ');

        $worksheet->mergeCells('N3:N5');
        $worksheet->setCellValue('N3', 'Исследовано сывороток крови на АГ ВКЭ');

        $worksheet->mergeCells('O3:O5');
        $worksheet->setCellValue('O3', 'Из них положительные');

        $worksheet->mergeCells('P3:P5');
        $worksheet->setCellValue('P3', 'Исследовано крови методом ПЦР');

        $worksheet->mergeCells('Q3:T4');
        $worksheet->setCellValue('Q3', 'Из них положительные');
        $worksheet->setCellValue('Q5', 'На КЭ');
        $worksheet->setCellValue('R5', 'На КБ');
        $worksheet->setCellValue('S5', 'МЭЧ');
        $worksheet->setCellValue('T5', 'ГАЧ');

        $worksheet->mergeCells('U3:X3');
        $worksheet->setCellValue('U3', 'Исследовано сывороток крови на АТ');
        $worksheet->mergeCells('U4:V4');
        $worksheet->setCellValue('U4', 'На КЭ');
        $worksheet->mergeCells('W4:X4');
        $worksheet->setCellValue('W4', 'На КБ');
        $worksheet->setCellValue('U5', 'M');
        $worksheet->setCellValue('V5', 'G');
        $worksheet->setCellValue('W5', 'M');
        $worksheet->setCellValue('X5', 'G');

        $worksheet->mergeCells('Y3:AB3');
        $worksheet->setCellValue('Y3', 'Из них положительные');
        $worksheet->mergeCells('Y4:Z4');
        $worksheet->setCellValue('Y4', 'На КЭ');
        $worksheet->mergeCells('AA4:AB4');
        $worksheet->setCellValue('AA4', 'На КБ');
        $worksheet->setCellValue('Y5', 'M');
        $worksheet->setCellValue('Z5', 'G');
        $worksheet->setCellValue('AA5', 'M');
        $worksheet->setCellValue('AB5', 'G');

        $worksheet->mergeCells('AC3:AF3');
        $worksheet->setCellValue('AC3', 'Проведена экстренная профилактика');
        $worksheet->mergeCells('AC4:AD4');
        $worksheet->setCellValue('AC4', 'Иммуноглобулин');
        $worksheet->setCellValue('AE4', 'Циклоферон');
        $worksheet->setCellValue('AF4', 'Антибиотиками');
        $worksheet->setCellValue('AC5', 'Всего');
        $worksheet->setCellValue('AD5', 'Дети');
        $worksheet->setCellValue('AE5', 'Всего');
        $worksheet->setCellValue('AF5', 'Всего');

        $row = 6;
        $residences = PermanentResidence::all();
        $admissionQuery = Admission::whereYear('date', $year);
        $absAdmitted = 0;
        $absAdmittedKids = 0;
        $absVaccinated = 0;
        $absVaccinatedKids = 0;
        $absTick = 0;
        $absTickVTE = 0;
        $absTickTB = 0;

        $absTickPCR = 0;
        $absTickPCRVTE = 0;
        $absTickPCRTB = 0;
        $absTickPCRHME = 0;
        $absTickPCRHGA = 0;

        $absAntigenTE = 0;
        $absAntigenTEPos = 0;

        $absBloodPCR = 0;
        $absBloodPCRVTE = 0;
        $absBloodPCRTB = 0;
        $absBloodPCRHME = 0;
        $absBloodPCRHGA = 0;

        $absMTE = 0;
        $absGTE = 0;
        $absMTB = 0;
        $absGTB = 0;

        $absMTEPos = 0;
        $absGTEPos = 0;
        $absMTBPos = 0;
        $absGTBPos = 0;

        $absIged = 0;
        $absIgedKids = 0;

        $absProfBac = 0;
        $absProfTE = 0;

        foreach ($residences as $residence) {
			$worksheet->setCellValue('A' . $row, $residence->name);
			$resQuery = (clone $admissionQuery)->where('permanent_residence_id', $residence->id);

			$admitted = (clone $resQuery)->count();
			$absAdmitted += $admitted;
			$worksheet->setCellValue('B' . $row, $admitted);

			$admittedKids = (clone $resQuery)->where('age', '<', '18')->count();
			$absAdmittedKids += $admittedKids;
			$worksheet->setCellValue('C' . $row, $admittedKids);

			$vacQuery = (clone $resQuery)->whereIn('vaccine_level_id', range(1,4));
			$vaccinated = $vacQuery->count();
			$worksheet->setCellValue('D' . $row, $vaccinated);
			$absVaccinated += $vaccinated;

			$vaccinatedKids = $vacQuery->where('age', '<', '18')->count();
			$worksheet->setCellValue('E' . $row, $vaccinatedKids);
			$absVaccinatedKids += $vaccinatedKids;

			$tickMaterialQuery = (clone $resQuery)->where('material_id', 1);
			$tickMaterial = $tickMaterialQuery->sum('material_amount');
			$worksheet->setCellValue('F' . $row, $tickMaterial);
			$absTick += $tickMaterial;


			$tickVTEQuery = TickResult::where('tick_pcr_vte', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickVTE = $tickVTEQuery->count();
			$worksheet->setCellValue('G' . $row, $tickVTE);
			$absTickVTE += $tickVTE;


			$tickTBQuery = TickResult::where('tick_pcr_tb', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickTB = $tickTBQuery->count();
			$worksheet->setCellValue('H' . $row, $tickTB);
			$absTickTB += $tickTB;

			/* ПЦР клещ */
			
			$tickPCRQuery = (clone $resQuery)->where('material_id', 1);
			$tickPCRResearched = $tickPCRQuery->sum('material_amount');
			$worksheet->setCellValue('I' . $row, $tickPCRResearched);
			$absTickPCR += $tickPCRResearched;

			//беру именно тех, у кого как материал был указан клещ, и у них смотрю результаты клеща

			// изменила $tickPCRQuery на $resQuery, так как результат по клещу у тех, у кого был сдан клещ

			$tickPCRVTEQuery = TickResult::where('tick_pcr_vte', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickPCRVTE = $tickPCRVTEQuery->count();
			$worksheet->setCellValue('J' . $row, $tickPCRVTE);
			$absTickPCRVTE += $tickPCRVTE;


			$tickPCRTBQuery = TickResult::where('tick_pcr_tb', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickPCRTB = $tickPCRTBQuery->count();
			$worksheet->setCellValue('K' . $row, $tickPCRTB);
			$absTickPCRTB += $tickPCRTB;


			$tickPCRHMEQuery = TickResult::where('tick_pcr_hme', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickPCRHME = $tickPCRHMEQuery->count();
			$worksheet->setCellValue('L' . $row, $tickPCRHME);
			$absTickPCRHME += $tickPCRHME;


			$tickPCRHGAQuery = TickResult::where('tick_pcr_hga', true)
							->whereHas('admission', function($query) use ($residence) {
								$query->where('permanent_residence_id', $residence->id);
							});
			$tickPCRHGA = $tickPCRHGAQuery->count();
			$worksheet->setCellValue('M' . $row, $tickPCRHGA);
			$absTickPCRHGA += $tickPCRHGA;

			/* END ПЦР клещ */

			/* Антиген КЭ */
			$antigenTE = (clone $resQuery)->whereHas('result', function ($query) {
				$query->whereNotNull('antigen_te');
			})->count();
			$worksheet->setCellValue('N' . $row, $antigenTE);
			$absAntigenTE += $antigenTE;

			$antigenTEPos = (clone $resQuery)->whereHas('result', function ($query) {
				$query->where('antigen_te', 1);
			})->count();
			$worksheet->setCellValue('O' . $row, $antigenTEPos);
			$absAntigenTEPos += $antigenTEPos;

			/* END Антиген КЭ */

			/* ПЦР кровь */

			$bloodPCRQuery = (clone $resQuery)->where('material_id', '2');
			$bloodPCRResearched = $bloodPCRQuery->count();
			$worksheet->setCellValue('P' . $row, $bloodPCRResearched);
			$absBloodPCR += $bloodPCRResearched;

			//беру именно тех, у кого как материал был указан кровь, и у них смотрю результаты крови

			$bloodPCRVTE = (clone $bloodPCRQuery)->whereHas('result', function ($query) {
				$query->where('blood_pcr_vte', 1);
			})->count();
			$worksheet->setCellValue('Q' . $row, $bloodPCRVTE);
			$absBloodPCRVTE += $bloodPCRVTE;

			$bloodPCRTB = (clone $bloodPCRQuery)->whereHas('result', function ($query) {
				$query->where('blood_pcr_tb', 1);
			})->count();
			$worksheet->setCellValue('R' . $row, $bloodPCRTB);
			$absBloodPCRTB += $bloodPCRTB;

			$bloodPCRHME = (clone $bloodPCRQuery)->whereHas('result', function ($query) {
				$query->where('blood_pcr_hme', 1);
			})->count();
			$worksheet->setCellValue('S' . $row, $bloodPCRHME);
			$absBloodPCRHME += $bloodPCRHME;

			$bloodPCRHGA = (clone $bloodPCRQuery)->whereHas('result', function ($query) {
				$query->where('blood_pcr_hga', 1);
			})->count();
			$worksheet->setCellValue('T' . $row, $bloodPCRHGA);
			$absBloodPCRHGA += $bloodPCRHGA;

			/* END ПЦР кровь */

			/* сыворотка */
			$IgMTE = (clone $resQuery)->whereHas('result', function ($query) {
				$query->whereNotNull('ig_m_te');
			})->count();
			$worksheet->setCellValue('U' . $row, $IgMTE);
			$absMTE += $IgMTE;

			$IgGTE = (clone $resQuery)->whereHas('result', function ($query) {
				$query->whereNotNull('ig_g_te');
			})->count();
			$worksheet->setCellValue('V' . $row, $IgGTE);
			$absGTE += $IgGTE;

			$IgMTB = (clone $resQuery)->whereHas('result', function ($query) {
				$query->whereNotNull('ig_m_tb');
			})->count();
			$worksheet->setCellValue('W' . $row, $IgMTB);
			$absMTB += $IgMTB;

			$IgGTB = (clone $resQuery)->whereHas('result', function ($query) {
				$query->whereNotNull('ig_g_tb');
			})->count();
			$worksheet->setCellValue('X' . $row, $IgGTB);
			$absGTB += $IgGTB;
			///

			$IgMTEPos = (clone $resQuery)->whereHas('result', function ($query) {
				$query->where('ig_m_te', 1);
			})->count();
			$worksheet->setCellValue('Y' . $row, $IgMTEPos);
			$absMTEPos += $IgMTEPos;

			$IgGTEPos = (clone $resQuery)->whereHas('result', function ($query) {
				$query->where('ig_g_te', 1);
			})->count();
			$worksheet->setCellValue('Z' . $row, $IgGTEPos);
			$absGTEPos += $IgGTEPos;

			$IgMTBPos = (clone $resQuery)->whereHas('result', function ($query) {
				$query->where('ig_m_tb', 1);
			})->count();
			$worksheet->setCellValue('AA' . $row, $IgMTBPos);
			$absMTBPos += $IgMTBPos;

			$IgGTBPos = (clone $resQuery)->whereHas('result', function ($query) {
				$query->where('ig_g_tb', 1);
			})->count();
			$worksheet->setCellValue('AB' . $row, $IgGTBPos);
			$absGTBPos += $IgGTBPos;

        	/* END сыворотка */ 

			/* Иммуноглобулин */

			$igedQuery = (clone $resQuery)->whereNotNull('ig_date');
			$iged = $igedQuery->count();
			$worksheet->setCellValue ('AC' . $row, $iged);
			$absIged += $iged;
			$igedKids = $igedQuery->where('age', '<', '18')->count();
			$worksheet->setCellValue('AD' . $row, $igedKids);
			$absIgedKids += $igedKids;

			/* END Иммуноглобулин */

			/* Профилактика */

			/*
			$profTE = (clone $resQuery)->whereNotNull('encephalitis_method_id')->where('encephalitis_method_id', '<>', 'Рекомендовано')->count();
			*/
			$profTE = (clone $resQuery)->whereNotNull('encephalitis_method_id')
						->whereHas('encephalitis_method', function($query) {
							$query->where('name', '<>', 'Рекомендовано');
						})->count();
			$worksheet->setCellValue('AE' . $row, $profTE);
			$absProfTE += $profTE;

			/*
			$profBac = (clone $resQuery)->whereNotNull('bacterial_method_id')->where('bacterial_method_id', '<>', 'Рекомендовано')->count();
			*/
			$profBac = (clone $resQuery)->whereNotNull('bacterial_method_id')
						->whereHas('bacterial_method', function($query) {
							$query->where('name', '<>', 'Рекомендовано');
						})->count();
			$worksheet->setCellValue('AF' . $row, $profBac);
			$absProfBac += $profBac;

			/* END Профилактика */

			$row++;
        }

        $worksheet->setCellValue('A' . $row, 'Всего');
        $worksheet->setCellValue('B' . $row, $absAdmitted);
        $worksheet->setCellValue('C' . $row, $absAdmittedKids);
        $worksheet->setCellValue('D' . $row, $absVaccinated);
        $worksheet->setCellValue('E' . $row, $absVaccinatedKids);

        $worksheet->setCellValue('F' . $row, $absTick);
        $worksheet->setCellValue('G' . $row, $absTickVTE);
        $worksheet->setCellValue('H' . $row, $absTickTB);

        $worksheet->setCellValue('I' . $row, $absTickPCR);
        $worksheet->setCellValue('J' . $row, $absTickPCRVTE);
        $worksheet->setCellValue('K' . $row, $absTickPCRTB);
        $worksheet->setCellValue('L' . $row, $absTickPCRHME);
        $worksheet->setCellValue('M' . $row, $absTickPCRHGA);

        $worksheet->setCellValue('N' . $row, $absAntigenTE);
        $worksheet->setCellValue('O' . $row, $absAntigenTEPos);

        $worksheet->setCellValue('P' . $row, $absBloodPCR);
        $worksheet->setCellValue('Q' . $row, $absBloodPCRVTE);
        $worksheet->setCellValue('R' . $row, $absBloodPCRTB);
        $worksheet->setCellValue('S' . $row, $absBloodPCRHME);
        $worksheet->setCellValue('T' . $row, $absBloodPCRHGA);

        $worksheet->setCellValue('U' . $row, $absMTE);
        $worksheet->setCellValue('V' . $row, $absGTE);
        $worksheet->setCellValue('W' . $row, $absMTB);
        $worksheet->setCellValue('X' . $row, $absGTB);

        $worksheet->setCellValue('Y' . $row, $absMTEPos);
        $worksheet->setCellValue('Z' . $row, $absGTEPos);
        $worksheet->setCellValue('AA' . $row, $absMTBPos);
        $worksheet->setCellValue('AB' . $row, $absGTBPos);

        $worksheet->setCellValue('AC' . $row, $absIged);
        $worksheet->setCellValue('AD' . $row, $absIgedKids);

        $worksheet->setCellValue('AE' . $row, $absProfTE);
        $worksheet->setCellValue('AF' . $row, $absProfBac);



    	return $spreadsheet;
    }

    // done
    public function ses_report_page_4($spreadsheet, $year)
    {
    	/* СТРАНИЦА 4 : Исследования клещей */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Исследования клещей');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица  № 1. Исследования клещей.');
		$admissionQuery = Admission::where(function($query) use ($year) {
        					$query->where(function($q) use ($year) {
        						$q->whereYear('date', $year);
        					})
        					->orWhereHas('result', function($q) use ($year) {
        						$q->whereYear('issue_date', $year);
        					});
        				});

		$period = 'за ' . $year . ' год';

        $worksheet->mergeCells('A2:G2');
        $worksheet->setCellValue('A2', 'Сводка  по клещам в Управление Роспотребнадзора по Иркутской области  в эпидемиологический отдел ФБУЗ  "Центр гигиены и эпидемиологии в Иркутской области" от ФГБНУ "НЦ ПЗСРЧ" Центр Диагностики и профилактики клещевых инфекций , К.Маркса,3');
        $worksheet->mergeCells('A3:G3');
        $worksheet->setCellValue('A3', $period);

        $worksheet->mergeCells('A4:A5');
        $worksheet->setCellValue('A4', '№п/п');

        $worksheet->mergeCells('B4:D5');
        $worksheet->setCellValue('B4', 'Наименование');

        $worksheet->mergeCells('E4:E5');
        $worksheet->setCellValue('E4', 'Всего');

       	$worksheet->mergeCells('F4:G4');
        $worksheet->setCellValue('F4', 'Методика');

        $worksheet->setCellValue('F5', 'Методом ПЦР');
        $worksheet->setCellValue('G5', 'Методом ИФА');

        $worksheet->mergeCells('A6:G6');
        $worksheet->setCellValue('A6',  'исследовано клещей');
        
        // всего исследовано
        $worksheet->mergeCells('B7:D7');
        $worksheet->setCellValue('B7', 'в том числе снятых с людей');
        $total = (clone $admissionQuery)->where('material_id', 1)->sum('material_amount');
        $worksheet->setCellValue('E7', $total);
        $worksheet->setCellValue('F7', $total);

        // вкэ
        $worksheet->setCellValue('A8', '1');
        $worksheet->mergeCells('B8:D8');
        $worksheet->setCellValue('B8', 'положительных на КЭ');

        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_vte', 1);
        })->count();
        */

        $total = TickResult::where('tick_pcr_vte', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
		$worksheet->setCellValue('E8', $total);
        $worksheet->setCellValue('F8', $total);

        // кб
        $worksheet->setCellValue('A9', '2');
        $worksheet->mergeCells('B9:D9');
        $worksheet->setCellValue('B9', 'положительных на КБ');
        
        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_tb', 1);
        })->count();
        */
        $total = TickResult::where('tick_pcr_tb', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
		$worksheet->setCellValue('E9', $total);
        $worksheet->setCellValue('F9', $total);

        // мэч
        $worksheet->setCellValue('A10', '3');
        $worksheet->mergeCells('B10:D10');
        $worksheet->setCellValue('B10', 'положительных на МЭЧ');
        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_hme', 1);
        })->count();*/
        $total = TickResult::where('tick_pcr_hme', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
        $worksheet->setCellValue('E10', $total);
        $worksheet->setCellValue('F10', $total);

        // гач
        $worksheet->setCellValue('A11', '4');
        $worksheet->mergeCells('B11:D11');
        $worksheet->setCellValue('B11', 'положительных на ГАЧ');
        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_hga', 1);
        })->count();
        */
        $total = TickResult::where('tick_pcr_hga', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
        $worksheet->setCellValue('E11', $total);
        $worksheet->setCellValue('F11', $total);

        // кр
        $worksheet->setCellValue('A12', '5');
        $worksheet->mergeCells('B12:D12');
        $worksheet->setCellValue('B12', 'положительных на КР');
        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_tr', 1);
        })->count();
        */
        $total = TickResult::where('tick_pcr_tr', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
        $worksheet->setCellValue('E12', $total);
        $worksheet->setCellValue('F12', $total);

        // квл
        $worksheet->setCellValue('A13', '6');
        $worksheet->mergeCells('B13:D13');
        $worksheet->setCellValue('B13', 'положительных на КВЛ');
        /*
        $total = (clone $admissionQuery)->whereHas('tick_results', function($query) {
        	$query->where('tick_pcr_trf', 1);
        })->count();
        */
        $total = TickResult::where('tick_pcr_trf', true)
						->whereHas('admission', function($query) use ($year) {
							$query->whereYear('date', $year);
						})->count();
        $worksheet->setCellValue('E13', $total);
        $worksheet->setCellValue('F13', $total);

    	return $spreadsheet;
    } 

    // done
    public function ses_report_page_5($spreadsheet, $year)
    {
    	/* СТРАНИЦА 5 : Обстоятельства заражения */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Обстоятельства заражения');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица  № 1. Обстоятельства заражения.');

        $worksheet->mergeCells('A3:A4');
        $worksheet->setCellValue('A3', 'Всего случае присасывания');

        $admissionQuery = Admission::whereYear('date', $year);
        $total = $admissionQuery->count();
        $worksheet->setCellValue('A5', $total);

        $circumTypes = DB::table('bite_circumstances')
        				->groupBy('type')
        				->pluck('type')
        				->toArray();

       	$columnStart = $column = 2;
        foreach ($circumTypes as $type) {
        	$circumsByType = BiteCircumstance::where('type', $type)->get();
        	foreach ($circumsByType as $circum) {
        		$columnLetter = $this->getColumnFromNum($column);
        		$worksheet->setCellValue($columnLetter . '4', $circum->name);
        		$circumNum = (clone $admissionQuery)->where('bite_circumstance_id', $circum->id)->count();
        		$worksheet->setCellValue($columnLetter . '5', $circumNum);
        		$column++;
        	}

        	$columnStartLetter = $this->getColumnFromNum($columnStart);
        	$worksheet->mergeCells($columnStartLetter . '3:' . $columnLetter . '3');
        	$worksheet->setCellValue($columnStartLetter . '3', $type);
        	$columnStart += $circumsByType->count();
        }


    	$row = 1;

    	/* END СТРАНИЦА 5 : Обстоятельства заражения */

    	return $spreadsheet;
    }

    public function ses_report_page_6($spreadsheet, $year)
    {
    	/* СТРАНИЦА 6 : Места присасывания */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Места присасывания');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица  № 1. Распределение случаев обращения с укусами клещей по месту присасывания.');

        $admissionQuery = Admission::whereYear('date', $year);
        $total = (clone $admissionQuery)->count();

        $worksheet->setCellValue('A3', 'Всего обратившихся');
        $worksheet->setCellValue('A4', $total);

        $spots = BiteSpot::all();

        foreach($spots as $key => $spot) {
        	$column = $this->getColumnFromNum($key+2);
        	$worksheet->setCellValue($column . '3', $spot->name);
        	$spotTotal = (clone $admissionQuery)->where('bite_spot_id', $spot->id)->count();
        	$worksheet->setCellValue($column . '4', $spotTotal);
        }

    	return $spreadsheet;
    } 

    public function ses_report_page_7($spreadsheet, $year)
    {
    	/* СТРАНИЦА 7 : Профилактика */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Профилактика');
    	$worksheet->mergeCells('A1:F1');
        $worksheet->setCellValue('A1', 'Таблица  № 4. Проведение экстренной профилактики.');

        $worksheet->mergeCells('A3:A4');

        $worksheet->mergeCells('B3:B4');
        $worksheet->setCellValue('B3', 'Всего получили');

        $worksheet->mergeCells('C3:C4');
        $worksheet->setCellValue('C3', 'В т.ч. дети до 14 лет');

        $worksheet->mergeCells('D3:E3');
        $worksheet->setCellValue('D3', 'В т.ч.');

        $worksheet->setCellValue('D4', 'Застрахованных');
        $worksheet->setCellValue('E4', 'Незастрахованных');

        $worksheet->mergeCells('F3:J3');
        $worksheet->setCellValue('F3', 'Сроки введения иммуноглобулина');

        $worksheet->setCellValue('F4', 'На 1-е сут');
        $worksheet->setCellValue('G4', 'На 2-е сут');
        $worksheet->setCellValue('H4', 'На 3-е сут');
        $worksheet->setCellValue('I4', 'На 4-е сут**');
        $worksheet->setCellValue('J4', 'Более 4-х сут');

        $worksheet->mergeCells('K3:L3');
        $worksheet->setCellValue('K3', 'Из них заболели*');
        $worksheet->setCellValue('K4', 'Взрослые');
        $worksheet->setCellValue('L4', 'Дети');

        $worksheet->mergeCells('N3:O3');
        $worksheet->setCellValue('N3', 'Схема введения у заболевших*');
        $worksheet->setCellValue('N4', 'Срок');
        $worksheet->setCellValue('O4', 'Доза');

        $worksheet->setCellValue('A5', 'Противоклещевой иммуноглобулин титр 1:320');
        $worksheet->setCellValue('A6', 'Циклоферон');
        $worksheet->setCellValue('A7', 'Антибиотики');

        $admissionQuery = Admission::whereYear('date', $year);

        // иммуноглобулин

        //$igedQuery = (clone $admissionQuery)->where('ig_dose', '<>', '');
        $igedQuery = (clone $admissionQuery)->whereNotNull('ig_date');
        $igedTotal = $igedQuery->count();
        $igedKids = (clone $igedQuery)->where('age', '<', 14)->count();
        $igedIns = (clone $igedQuery)->where('insurance_company_id', '<>', '')->count();
        $igedNotIns = (clone $igedQuery)->whereNull('insurance_company_id')->orWhere('insurance_company_id', '=', '')->count();
        $iged = $igedQuery->get();

        $igedFirst = $igedSecond = $igedThird = $igedForth = $igedMore = 0;

        foreach ($iged as $item) {
        	$biteDate = Carbon::parse($item->bite_date);
        	$igDate = Carbon::parse($item->ig_date);

        	$dateDiff = $igDate->diffInDays($biteDate);

        	switch ($dateDiff) {
        		case 0:
        		case 1:
        			$igedFirst++;
        			break;
        		case 2:
        			$igedSecond++;
        			break;
        		case 3:
        			$igedThird++;
        			break;
        		case 4:
        			$igedForth++;
        			break;
        		default:
        			$igedMore++;
        			break;
        	}

        	$worksheet->setCellValue('B5', $igedTotal);
        	$worksheet->setCellValue('C5', $igedKids);
        	$worksheet->setCellValue('D5', $igedIns);
        	$worksheet->setCellValue('E5', $igedNotIns);
        	$worksheet->setCellValue('F5', $igedFirst);
        	$worksheet->setCellValue('G5', $igedSecond);
        	$worksheet->setCellValue('H5', $igedThird);
        	$worksheet->setCellValue('I5', $igedForth);
        	$worksheet->setCellValue('J5', $igedMore);

        	// циклоферон

        	/*
        	$cicloferonQuery = (clone $admissionQuery)
        					->whereHas('encephalitis_method', function($query) {
        						$query->where('name', 'Циклоферон');
        					});
        	$cicloferonTotal = $cicloferonQuery->count();
        	$cicloferonKids = (clone $cicloferonQuery)->where('age', '<', 14)->count();
        	$cicloferonIns = (clone $cicloferonQuery)->where('insurance_company_id', '<>', '')->count();
        	$cicloferonNotIns = (clone $cicloferonQuery)->whereNull('insurance_company_id')->orWhere('insurance_company_id', '=', '')->count();

        	$worksheet->setCellValue('B6', $cicloferonTotal);
        	$worksheet->setCellValue('C6', $cicloferonKids);
        	$worksheet->setCellValue('D6', $cicloferonIns);
        	$worksheet->setCellValue('E6', $cicloferonNotIns);
        	*/

        	// профилактика энцефалита

        	$encephalitisQuery = (clone $admissionQuery)
        					->whereNotNull('encephalitis_method_id')->where('encephalitis_method_id', '<>', '')
        					->whereHas('encephalitis_method', function($query) {
        						$query->where('name', '<>', 'Рекомендовано');
        					});
        	$encephalitisTotal = $encephalitisQuery->count();
        	$encephalitisKids = (clone $encephalitisQuery)->where('age', '<', 14)->count();
        	$encephalitisIns = (clone $encephalitisQuery)->where('insurance_company_id', '<>', '')->count();
        	$encephalitisNotIns = (clone $encephalitisQuery)->whereNull('insurance_company_id')->orWhere('insurance_company_id', '=', '')->count();

        	$worksheet->setCellValue('B6', $encephalitisTotal);
        	$worksheet->setCellValue('C6', $encephalitisKids);
        	$worksheet->setCellValue('D6', $encephalitisIns);
        	$worksheet->setCellValue('E6', $encephalitisNotIns);

        	// антибиотики

        	$bacMethodsQuery = (clone $admissionQuery)
        					->whereNotNull('bacterial_method_id')->where('bacterial_method_id', '<>', '')
        					->whereHas('bacterial_method', function($query) {
        						$query->where('name', '<>', 'Рекомендовано');
        					});
        	$bacMethodsTotal = $bacMethodsQuery->count();
        	$bacMethodsKids = (clone $bacMethodsQuery)->where('age', '<', 14)->count();
        	$bacMethodsIns = (clone $bacMethodsQuery)->where('insurance_company_id', '<>', '')->count();
        	$bacMethodsNotIns = (clone $bacMethodsQuery)->whereNull('insurance_company_id')->orWhere('insurance_company_id', '=', '')->count();

        	$worksheet->setCellValue('B7', $bacMethodsTotal);
        	$worksheet->setCellValue('C7', $bacMethodsKids);
        	$worksheet->setCellValue('D7', $bacMethodsIns);
        	$worksheet->setCellValue('E7', $bacMethodsNotIns);

        }


    	return $spreadsheet;
    }

    public function ses_report_page_8($spreadsheet, $year)
    {
    	/* СТРАНИЦА 8 : Местность */

        $worksheet = $spreadsheet->createSheet();
    	$worksheet->setTitle('Местность');
    	$worksheet->mergeCells('A1:D1');
        $worksheet->setCellValue('A1', 'Приложение 4');

        $worksheet->mergeCells('A2:D2');
        $worksheet->setCellValue('A2', 'Местность, где произошло присасывание клещей у обратившихся в Центр диагностики и профилактики.');

        $worksheet->setCellValue('A4', '№');
        $worksheet->setCellValue('B4', 'Местность');
        $worksheet->setCellValue('C4', '');
        $worksheet->setCellValue('D4', 'Количество случаев присасывания');

        $admissionQuery = Admission::whereYear('date', $year);

        $adminTerritories = AdminTerritory::all();

        $rowStart = $row = 5;

        foreach ($adminTerritories as $key => $territory) {
        	$worksheet->setCellValue('A' . $row, $key + 1);
        	$worksheet->setCellValue('B' . $row, $territory->name);

        	$admissionsTotal = (clone $admissionQuery)->where('admin_territory_id', $territory->id)->count();
        	$worksheet->setCellValue('C' . $row, $admissionsTotal);
        	$row++;

        	$localities = $territory->localities;

        	foreach ($localities as $locality) {
        		$admissionLocalQuery = (clone $admissionQuery)->where('locality_id', $locality->id);

        		if ($locality->needs_clarification) {

        			$clarification = $admissionLocalQuery->pluck('locality_clarification')->toArray();

        			$name = $locality->name . ' (' . implode ($clarification, ', ') . ')';

        		} else {

        			$name = $locality->name;

        		}

        		$worksheet->setCellValue('B' . $row, $name);

        		$admissionLocalTotal = $admissionLocalQuery->count();
	        	
	        	$worksheet->setCellValue('D' . $row, $admissionLocalTotal);
	        	$row++;
        	}
        }

    	return $spreadsheet;
    }

    private function getColumnFromNum($num)
    {
	    $numeric = ($num - 1) % 26;
	    $letter = chr(65 + $numeric);
	    $num2 = intval(($num - 1) / 26);
	    if ($num2 > 0) {
	        return $this->getColumnFromNum($num2) . $letter;
	    } else {
	        return $letter;
	    }
    }

    private function getNumFromColumn($column)
    {
    	//works for two letters only
    	$letters = str_split($column);

    	$nums = array();

    	foreach ($letters as $letter) {
    		$nums[] = array_search($letter, $this->alphabet) + 1;
    	}

    	$num = 0;
    	$numsTopIndex = count($nums) - 1;

    	for ($i = 0; $i < $numsTopIndex; $i++) {
    		$num += $nums[$i] * 26;
    	}

    	$num += $nums[$numsTopIndex];

    	return $num;
    }

    private function offsetColumn($currentColumn, $offset)
	{
		return $this->getColumnFromNum($this->getNumFromColumn($currentColumn) + $offset);
	}

    // отчёт для Быковой 2019
    private function weekly_report_2019()
    {
    	$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
    	$worksheet->setTitle('c ' . $dateFrom . ' по ' . $dateTo);

        $worksheet->mergeCells('A1:A3');
        $worksheet->setCellValue('A1', 'местность, где произошло присасывание');

        $worksheet->mergeCells('B1:C2');
        $worksheet->setCellValue('B1', 'обратилось с укусами');
        $worksheet->setCellValue('B3', 'всего');
        $worksheet->setCellValue('C3', 'дети');

        $worksheet->mergeCells('D1:E2');
        $worksheet->setCellValue('D1', 'вакцинированные');
        $worksheet->setCellValue('D3', 'всего');
        $worksheet->setCellValue('E3', 'дети');

        $worksheet->mergeCells('F1:F3');
        $worksheet->setCellValue('F1', 'от экспресс-диагностики отказались');

        $worksheet->mergeCells('G1:N1');
        $worksheet->setCellValue('G1', 'исследование клеща методом ПЦР');
        $worksheet->mergeCells('G2:G3');
        $worksheet->setCellValue('G2', 'всего');
        $worksheet->mergeCells('H2:H3');
        $worksheet->setCellValue('H2', 'дети');
        $worksheet->mergeCells('I2:N2');
        $worksheet->setCellValue('I2', 'из них положительные');

        $worksheet->setCellValue('I3', 'КЭ');
        $worksheet->setCellValue('J3', 'КБ');
        $worksheet->setCellValue('K3', 'МЭЧ');
        $worksheet->setCellValue('L3', 'ГАЧ');
        $worksheet->setCellValue('M3', 'КР');
        $worksheet->setCellValue('N3', 'КВЛ');

        $worksheet->mergeCells('O1:Q1');
        $worksheet->setCellValue('O1', 'исследование крови ИФА на антиген к ВКЭ');
        $worksheet->mergeCells('O2:O3');
        $worksheet->setCellValue('O2', 'всего');
        $worksheet->mergeCells('P2:P3');
        $worksheet->setCellValue('P2', 'дети');
        $worksheet->setCellValue('Q2', 'положит');
        $worksheet->setCellValue('Q3', 'КЭ');

        $worksheet->mergeCells('R1:Y1');
        $worksheet->setCellValue('R1', 'исследование крови методом ПЦР');
        $worksheet->mergeCells('R2:R3');
        $worksheet->setCellValue('R2', 'всего');
        $worksheet->mergeCells('S2:S3');
        $worksheet->setCellValue('S2', 'дети');
        $worksheet->mergeCells('T2:Y2');
        $worksheet->setCellValue('T2', 'из них положительные');

        $worksheet->setCellValue('T3', 'КЭ');
        $worksheet->setCellValue('U3', 'КБ');
        $worksheet->setCellValue('V3', 'МЭЧ');
        $worksheet->setCellValue('W3', 'ГАЧ');
        $worksheet->setCellValue('X3', 'КР');
        $worksheet->setCellValue('Y3', 'КВЛ');

        $worksheet->mergeCells('Z1:AG1');
        $worksheet->setCellValue('Z1', 'исследование крови на антитела');
        $worksheet->mergeCells('Z2:AA2');
        $worksheet->setCellValue('Z2', 'КЭ');
        $worksheet->mergeCells('AB2:AC2');
        $worksheet->setCellValue('AB2', 'КБ');
        $worksheet->setCellValue('Z3', 'M');
        $worksheet->setCellValue('AA3', 'G');
        $worksheet->setCellValue('AB3', 'M');
        $worksheet->setCellValue('AC3', 'G');

        $worksheet->mergeCells('AD2:AG2');
        $worksheet->setCellValue('AD2', 'положительные');
        $worksheet->setCellValue('AD3', 'КЭ M');
        $worksheet->setCellValue('AE3', 'КЭ G');
        $worksheet->setCellValue('AF3', 'КБ M');
        $worksheet->setCellValue('AG3', 'КБ G');

        $worksheet->mergeCells('AH1:AM1');
        $worksheet->setCellValue('AH1', 'проведена экстренная профилактика');
        $worksheet->mergeCells('AH2:AI2');
        $worksheet->setCellValue('AH2', 'иммуноглобулин');
        $worksheet->setCellValue('AH3', 'всего');
        $worksheet->setCellValue('AI3', 'дети');
        $worksheet->mergeCells('AJ2:AK2');
        $worksheet->setCellValue('AJ2', 'антибиотики');
        $worksheet->setCellValue('AJ3', 'всего');
        $worksheet->setCellValue('AK3', 'дети');
        $worksheet->mergeCells('AL2:AM2');
        $worksheet->setCellValue('AL2', 'йодантипирин/ циклоферон');
        $worksheet->setCellValue('AL3', 'всего');
        $worksheet->setCellValue('AM3', 'дети');

        $worksheet->mergeCells('AN1:AP1');
        $worksheet->setCellValue('AN1', 'число укусов, зарегистрированных на территории');

        $worksheet->mergeCells('AN2:AN3');
        $worksheet->setCellValue('AN2', 'населён.пунктов (каких)');

        $worksheet->mergeCells('AO2:AP2');
        $worksheet->setCellValue('AO2', 'ДОУ,в т.ч.');
        $worksheet->setCellValue('AO3', 'с дневным пребыванием');
        $worksheet->setCellValue('AP3', 'загородные');

        /*$admissionQuery = Admission::where(function($query) use ($dateFrom, $dateTo) {
        					$query->where(function($q) use ($dateFrom, $dateTo) {
        						$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
        					})
        					->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
        						$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
        					});
        				});
        				*/

        $admissionQuery = Admission::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);

        $territories = DB::select('select weekly_type, group_concat(id) as ids from admin_territories where weekly_type is not null and weekly_type <> "" group by weekly_type');
        
        $rowStart = $row = 4;
        $items = [];
        foreach ($territories as $territory) {
        	$idArray = explode(',', $territory->ids);

        	$currentItem = [];

        	$currentItem['A'] = $territory->weekly_type;

        	foreach ($idArray as $id) {
        		
        		//$worksheet->setCellValue('A' . $row, $territory->weekly_type);
		       	$territoryAdmissionQuery = (clone $admissionQuery)->where('admin_territory_id', $id);

		       	// всего по территории
		       	$territoryAdmissionTotal = $territoryAdmissionQuery->count();
		       	if (isset($currentItem['B'])) {
		       		$currentItem['B'] += $territoryAdmissionTotal;
		       	} else {
		       		$currentItem['B'] = $territoryAdmissionTotal;
		       	}
		       	
		       	$territoryAdmissionKidsQuery = (clone $territoryAdmissionQuery)->where('age', '<', '18')->get();
		       	// детей по территории
		       	$territoryAdmissionKids = $territoryAdmissionKidsQuery->count();
		       	if (isset($currentItem['C'])) {
		       		$currentItem['C'] += $territoryAdmissionKids;
		       	} else {
		       		$currentItem['C'] = $territoryAdmissionKids;
		       	}

		       	// места, где укусили детей

		       	$territoryAdmissionVacQuery = (clone $territoryAdmissionQuery)->whereNotIn('vaccine_level_id', ['', '5']);
		       	// вакцинированных всего
		       	$territoryAdmissionVac = $territoryAdmissionVacQuery->count();
		       	if (isset($currentItem['D'])) {
		       		$currentItem['D'] += $territoryAdmissionVac;
		       	} else {
		       		$currentItem['D'] = $territoryAdmissionVac;
		       	}
		       	
		       	// вакцинированных детей
		       	$territoryAdmissionVacKids = $territoryAdmissionVacQuery->where('age', '<', '18')->count();
		       	if (isset($currentItem['E'])) {
					$currentItem['E'] += $territoryAdmissionVacKids;
		       	} else {
		       		$currentItem['E'] = $territoryAdmissionVacKids;

		       	}

				// отказались
				$territoryAdmissionDenied = (clone $territoryAdmissionQuery)->whereHas('material', function($query) { 
					$query->where('name', 'Отказались');
				})->count();
				if (isset($currentItem['F'])) {
		       		$currentItem['F'] += $territoryAdmissionDenied;
		       	} else {
		       		$currentItem['F'] = $territoryAdmissionDenied;
		       	}
		       	
		       	/*
		       	$territoryAdmissionPCRQuery = (clone $territoryAdmissionQuery)->where('material_id', '1')->whereHas('researches', function($query) {
		       		$query->where('type', 'pcr');
		       	});
		       	*/

		       	/* даёт результат не такой, как в отчёте в РПН,
		       	т.к.  брали тот, где есть результаты по ПЦР клеща
		       	а нимфу не учитывали*/
		       	$territoryAdmissionPCRQuery = (clone $territoryAdmissionQuery)->whereHas('tick_results', function($query) {
		       		$query->whereNotNull('tick_pcr_vte')
	       				->orWhereNotNull('tick_pcr_tb')
	       				->orWhereNotNull('tick_pcr_hga')
	       				->orWhereNotNull('tick_pcr_hme')
	       				->orWhereNotNull('tick_pcr_tr')
	       				->orWhereNotNull('tick_pcr_trf');
		       	});

		       	//$territoryAdmissionPCRQuery = (clone $territoryAdmissionQuery)->whereIn('material_id', ['1', '6']);

		       	// исследование клеща методом ПЦР всего
		       	$territoryAdmissionPCRTotal = $territoryAdmissionPCRQuery->count();
		       	if (isset($currentItem['G'])) {
		       		$currentItem['G'] += $territoryAdmissionPCRTotal;
		       	} else {
		       		$currentItem['G'] = $territoryAdmissionPCRTotal;
		       	}

		       	// исследование клеща методом ПЦР дети
		       	$territoryAdmissionPCRKids = (clone $territoryAdmissionPCRQuery)->where('age', '<', '18')->count();
		       	if (isset($currentItem['H'])) {
		       		$currentItem['H'] += $territoryAdmissionPCRKids;
		       	} else {
		       		$currentItem['H'] = $territoryAdmissionPCRKids;
		       	}

		       	// положительно на КЭ
		       	$territoryAdmissionPCRTE = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_vte', 1);
		       	})->count();
		       	if (isset($currentItem['I'])) {
		       		$currentItem['I'] += $territoryAdmissionPCRTE;
		       	} else {
		       		$currentItem['I'] = $territoryAdmissionPCRTE;
		       	}

		       	// положительно на КБ
		       	$territoryAdmissionPCRTB = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_tb', 1);
		       	})->count();
		       	if (isset($currentItem['J'])) {
		       		$currentItem['J'] += $territoryAdmissionPCRTB;
		       	} else {
		       		$currentItem['J'] = $territoryAdmissionPCRTB;
		       	}

		       	// положительно на МЭЧ
		       	$territoryAdmissionPCRHME = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_hme', 1);
		       	})->count();
		       	if (isset($currentItem['K'])) {
		       		$currentItem['K'] += $territoryAdmissionPCRHME;
		       	} else {
		       		$currentItem['K'] = $territoryAdmissionPCRHME;
		       	}

		       	// положительно на МЭЧ
		       	$territoryAdmissionPCRHGA = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_hga', 1);
		       	})->count();
		       	if (isset($currentItem['L'])) {
		       		$currentItem['L'] += $territoryAdmissionPCRHGA;
		       	} else {
		       		$currentItem['L'] = $territoryAdmissionPCRHGA;
		       	}

		       	// положительно на КЛ
		       	$territoryAdmissionPCRTR = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_tr', 1);
		       	})->count();
		       	if (isset($currentItem['M'])) {
		       		$currentItem['M'] += $territoryAdmissionPCRTR;
		       	} else {
		       		$currentItem['M'] = $territoryAdmissionPCRTR;
		       	}

		       	// положительно на КВЛ
		       	$territoryAdmissionPCRTRF = (clone $territoryAdmissionPCRQuery)->whereHas('tick_results', function($query) {
		       		$query->where('tick_pcr_trf', 1);
		       	})->count();
		       	if (isset($currentItem['N'])) {
		       		$currentItem['N'] += $territoryAdmissionPCRTRF;
		       	} else {
		       		$currentItem['N'] = $territoryAdmissionPCRTRF;
		       	}


		       	$territoryAdmissionAgQuery = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('antigen_te');
		       	});

				$territoryAdmissionAgTotal = $territoryAdmissionAgQuery->count();
		       	if (isset($currentItem['O'])) {
		       		$currentItem['O'] += $territoryAdmissionAgTotal;
		       	} else {
		       		$currentItem['O'] = $territoryAdmissionAgTotal;
		       	}

		       	$territoryAdmissionAgKids = (clone $territoryAdmissionAgQuery)->where('age', '<', 18)->count();
		       	if (isset($currentItem['P'])) {
		       		$currentItem['P'] += $territoryAdmissionAgKids;
		       	} else {
		       		$currentItem['P'] = $territoryAdmissionAgKids;
		       	}

		       	$territoryAdmissionAgPos = (clone $territoryAdmissionAgQuery)->whereHas('result', function($query) {
		       		$query->where('antigen_te', 1);
		       	})->count();
		       	if (isset($currentItem['Q'])) {
		       		$currentItem['Q'] += $territoryAdmissionAgPos;
		       	} else {
		       		$currentItem['Q'] = $territoryAdmissionAgPos;
		       	}
		       	


		       	$territoryAdmissionPCRQuery = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('blood_pcr_vte')
	       				->orWhereNotNull('blood_pcr_tb')
	       				->orWhereNotNull('blood_pcr_hga')
	       				->orWhereNotNull('blood_pcr_hme')
	       				->orWhereNotNull('blood_pcr_tr')
	       				->orWhereNotNull('blood_pcr_trf');
		       	});
		       	// исследование клеща методом ПЦР всего
		       	$territoryAdmissionPCRTotal = $territoryAdmissionPCRQuery->count();
		       	if (isset($currentItem['R'])) {
		       		$currentItem['R'] += $territoryAdmissionPCRTotal;
		       	} else {
		       		$currentItem['R'] = $territoryAdmissionPCRTotal;
		       	}

		       	// исследование клеща методом ПЦР дети
		       	$territoryAdmissionPCRKids = (clone $territoryAdmissionPCRQuery)->where('age', '<', '18')->count();
		       	if (isset($currentItem['S'])) {
		       		$currentItem['S'] += $territoryAdmissionPCRKids;
		       	} else {
		       		$currentItem['S'] = $territoryAdmissionPCRKids;
		       	}

		       	// положительно на КЭ
		       	$territoryAdmissionPCRTE = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_vte', 1);
		       	})->count();
		       	if (isset($currentItem['T'])) {
		       		$currentItem['T'] += $territoryAdmissionPCRTE;
		       	} else {
		       		$currentItem['T'] = $territoryAdmissionPCRTE;
		       	}

		       	// положительно на КБ
		       	$territoryAdmissionPCRTB = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_tb', 1);
		       	})->count();
		       	if (isset($currentItem['U'])) {
		       		$currentItem['U'] += $territoryAdmissionPCRTB;
		       	} else {
		       		$currentItem['U'] = $territoryAdmissionPCRTB;
		       	}

		       	// положительно на МЭЧ
		       	$territoryAdmissionPCRHME = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_hme', 1);
		       	})->count();
		       	if (isset($currentItem['V'])) {
		       		$currentItem['V'] += $territoryAdmissionPCRHME;
		       	} else {
		       		$currentItem['V'] = $territoryAdmissionPCRHME;
		       	}

		       	// положительно на МЭЧ
		       	$territoryAdmissionPCRHGA = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_hga', 1);
		       	})->count();
		       	if (isset($currentItem['W'])) {
		       		$currentItem['W'] += $territoryAdmissionPCRHGA;
		       	} else {
		       		$currentItem['W'] = $territoryAdmissionPCRHGA;
		       	}

		       	// положительно на КЛ
		       	$territoryAdmissionPCRTR = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_tr', 1);
		       	})->count();
		       	if (isset($currentItem['X'])) {
		       		$currentItem['X'] += $territoryAdmissionPCRTR;
		       	} else {
		       		$currentItem['X'] = $territoryAdmissionPCRTR;
		       	}

		       	// положительно на КВЛ
		       	$territoryAdmissionPCRTRF = (clone $territoryAdmissionPCRQuery)->whereHas('result', function($query) {
		       		$query->where('blood_pcr_trf', 1);
		       	})->count();
		       	if (isset($currentItem['Y'])) {
		       		$currentItem['Y'] += $territoryAdmissionPCRTRF;
		       	} else {
		       		$currentItem['Y'] = $territoryAdmissionPCRTRF;
		       	}

		       	// исследования на антитела
		       	
		       	$territoryAdmissionIGQuery = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('ig_m_te')
	       				->orWhereNotNull('ig_m_tb')
	       				->orWhereNotNull('ig_g_te')
	       				->orWhereNotNull('ig_g_tb');
		       	});

		       	// Ig M КЭ

		       	$territoryAdmissionIGMTETotal = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('ig_m_te');
		       	})->count();

		       	if (isset($currentItem['Z'])) {
		       		$currentItem['Z'] += $territoryAdmissionIGMTETotal;
		       	} else {
		       		$currentItem['Z'] = $territoryAdmissionIGMTETotal;
		       	}

		       	$territoryAdmissionIGMTEPos = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->where('ig_m_te', 1);
		       	})->count();

		       	if (isset($currentItem['AD'])) {
		       		$currentItem['AD'] += $territoryAdmissionIGMTEPos;
		       	} else {
		       		$currentItem['AD'] = $territoryAdmissionIGMTEPos;
		       	}

		       	// Ig G КЭ

		       	$territoryAdmissionIGGTETotal = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('ig_g_te');
		       	})->count();

		       	if (isset($currentItem['AA'])) {
		       		$currentItem['AA'] += $territoryAdmissionIGGTETotal;
		       	} else {
		       		$currentItem['AA'] = $territoryAdmissionIGGTETotal;
		       	}

		       	$territoryAdmissionIGGTEPos = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->where('ig_g_te', 1);
		       	})->count();

		       	if (isset($currentItem['AE'])) {
		       		$currentItem['AE'] += $territoryAdmissionIGGTEPos;
		       	} else {
		       		$currentItem['AE'] = $territoryAdmissionIGGTEPos;
		       	}

		       	// Ig M КБ

		       	$territoryAdmissionIGMTBTotal = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('ig_m_tb');
		       	})->count();

		       	if (isset($currentItem['AB'])) {
		       		$currentItem['AB'] += $territoryAdmissionIGMTBTotal;
		       	} else {
		       		$currentItem['AB'] = $territoryAdmissionIGMTBTotal;
		       	}

		       	$territoryAdmissionIGMTBPos = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->where('ig_m_tb', 1);
		       	})->count();

		       	if (isset($currentItem['AF'])) {
		       		$currentItem['AF'] += $territoryAdmissionIGMTBPos;
		       	} else {
		       		$currentItem['AF'] = $territoryAdmissionIGMTBPos;
		       	}

		       	// Ig G КБ

		       	$territoryAdmissionIGGTBTotal = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->whereNotNull('ig_g_tb');
		       	})->count();

		       	if (isset($currentItem['AC'])) {
		       		$currentItem['AC'] += $territoryAdmissionIGGTBTotal;
		       	} else {
		       		$currentItem['AC'] = $territoryAdmissionIGGTBTotal;
		       	}

		       	$territoryAdmissionIGGTBPos = (clone $territoryAdmissionQuery)->whereHas('result', function($query) {
		       		$query->where('ig_g_tb', 1);
		       	})->count();

		       	if (isset($currentItem['AG'])) {
		       		$currentItem['AG'] += $territoryAdmissionIGGTBPos;
		       	} else {
		       		$currentItem['AG'] = $territoryAdmissionIGGTBPos;
		       	}

		       	// проведена экстренная профилактика

		       	$territoryAdmissionIgedQuery = (clone $territoryAdmissionQuery)->where('ig_dose', '>', 0);
		       	$territoryAdmissionIgedTotal = $territoryAdmissionIgedQuery->count();
		       	$territoryAdmissionIgedKids = (clone $territoryAdmissionIgedQuery)->where('age', '>', '18')->count();

		       	if (isset($currentItem['AH'])) {
		       		$currentItem['AH'] += $territoryAdmissionIgedTotal;
		       	} else {
		       		$currentItem['AH'] = $territoryAdmissionIgedTotal;
		       	}

		       	if (isset($currentItem['AI'])) {
		       		$currentItem['AI'] += $territoryAdmissionIgedKids;
		       	} else {
		       		$currentItem['AI'] = $territoryAdmissionIgedKids;
		       	}

		       	// антибиотики

		       	$territoryAdmissionBacQuery = (clone $territoryAdmissionQuery)->whereHas('bacterial_method', function($query) {
		       		$query->where('name', '<>', 'Рекомендовано');
		       	});
		       	$territoryAdmissionBacTotal = $territoryAdmissionBacQuery->count();
		       	$territoryAdmissionBacKids = (clone $territoryAdmissionBacQuery)->where('age', '>', '18')->count();

		       	if (isset($currentItem['AJ'])) {
		       		$currentItem['AJ'] += $territoryAdmissionBacTotal;
		       	} else {
		       		$currentItem['AJ'] = $territoryAdmissionBacTotal;
		       	}

		       	if (isset($currentItem['AK'])) {
		       		$currentItem['AK'] += $territoryAdmissionBacKids;
		       	} else {
		       		$currentItem['AK'] = $territoryAdmissionBacKids;
		       	}

		       	// энцефалитные методы

		       	$territoryAdmissionEncQuery = (clone $territoryAdmissionQuery)->whereHas('encephalitis_method', function($query) {
		       		$query->where('name', '<>', 'Рекомендовано');
		       	});
		       	$territoryAdmissionEncTotal = $territoryAdmissionEncQuery->count();
		       	$territoryAdmissionEncKids = (clone $territoryAdmissionEncQuery)->where('age', '>', '18')->count();

		       	if (isset($currentItem['AL'])) {
		       		$currentItem['AL'] += $territoryAdmissionEncTotal;
		       	} else {
		       		$currentItem['AL'] = $territoryAdmissionEncTotal;
		       	}

		       	if (isset($currentItem['AM'])) {
		       		$currentItem['AM'] += $territoryAdmissionEncKids;
		       	} else {
		       		$currentItem['AM'] = $territoryAdmissionEncKids;
		       	}

        	}

        	$kidsTotal = (clone $admissionQuery)->where('age', '<', '18')->where('admin_territory_id', 1)->count();

        	$sql = 'select count(*) as total, locality_id from admissions where date between "' . $dateFrom . '" and "' . $dateTo . '" and age < 18 and admin_territory_id = 1 group by locality_id';

        	$kidsLocalities = DB::select($sql);

        	$kidsText = 'Укусы в черте г. Иркутска: дети всего - ' . $kidsTotal . '; ';

        	foreach ($kidsLocalities as $kidLocality) {
        		$locality = Locality::find($kidLocality->locality_id);
        		$kidsText .= $locality->name . ' - ' . $kidLocality->total . ', ';
        	}

        	$worksheet->mergeCells('AQ4:AT6');
        	$worksheet->setCellValue('AQ4', $kidsText);

        	$items[$row] = $currentItem;

        	$row++;

        }

        $total = [];
        for ($i = 2; $i < 40; $i++) {
        	$total[$this->getColumnFromNum($i)] = 0;
        }

        foreach ($items as $row => $item) {

			foreach ($item as $column => $value) {
				$worksheet->setCellValue($column . $row, $value);
				if ($column != 'A') {
					$total[$column] += $value;
				}
			}
		}
		$rowEnd = $row;
		$row++;

        foreach ($total as $column => $value) {
			$worksheet->setCellValue($column . $row, $value);
		}
		$worksheet->setCellValue('A' . $row, 'Всего');
		$row++;

		$worksheet->setCellValue('A' . $row, 'ПМЖ');
		$row++;

		$residences = PermanentResidence::all();

		foreach ($residences as $residence) {
			$worksheet->setCellValue('A' . $row, $residence->name);

			$resAdmissionsTotal = (clone $admissionQuery)->where('permanent_residence_id', $residence->id)->count();
			$worksheet->setCellValue('B' . $row, $resAdmissionsTotal);
			
			$row++;
		}
        
        $writer = new Xlsx($spreadsheet);

        $fileName = 'Отчёт для Быковой Натальи.xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    // отчёт для Быковой 2020
    private function weekly_report()
    {
    	$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
    	$worksheet->setTitle('c ' . $dateFrom . ' по ' . $dateTo);

    	$worksheet->mergeCells('A1:A4');
        $worksheet->setCellValue('A1', 'ПМЖ');

        $worksheet->mergeCells('B1:B4');
        $worksheet->setCellValue('B1', 'от экспресс-диагностики отказались');

        $worksheet->mergeCells('C1:P1');
        $worksheet->setCellValue('C1', 'исследовано клещей в ПЦР');
        $worksheet->mergeCells('C2:C4');
        $worksheet->setCellValue('C2', 'всего');
        $worksheet->mergeCells('D2:D4');
        $worksheet->setCellValue('D2', 'дети');
        $worksheet->mergeCells('E2:P2');
        $worksheet->setCellValue('E2', 'в т. ч. с положительным результатом');
        $worksheet->mergeCells('E3:F3');
        $worksheet->setCellValue('E3', 'КЭ');
        $worksheet->setCellValue('E4', 'всего');
        $worksheet->setCellValue('F4', 'дети');

        $worksheet->mergeCells('G3:H3');
        $worksheet->setCellValue('G3', 'КБ');
        $worksheet->setCellValue('G4', 'всего');
        $worksheet->setCellValue('H4', 'дети');

        $worksheet->mergeCells('I3:J3');
        $worksheet->setCellValue('I3', 'МЭЧ');
        $worksheet->setCellValue('I4', 'всего');
        $worksheet->setCellValue('J4', 'дети');

        $worksheet->mergeCells('K3:L3');
        $worksheet->setCellValue('K3', 'ГАЧ');
        $worksheet->setCellValue('K4', 'всего');
        $worksheet->setCellValue('L4', 'дети');

        $worksheet->mergeCells('M3:N3');
        $worksheet->setCellValue('M3', 'КР');
        $worksheet->setCellValue('M4', 'всего');
        $worksheet->setCellValue('N4', 'дети');

        $worksheet->mergeCells('O3:P3');
        $worksheet->setCellValue('O3', 'КВЛ');
        $worksheet->setCellValue('O4', 'всего');
        $worksheet->setCellValue('P4', 'дети');



        $worksheet->mergeCells('Q1:AD1');
        $worksheet->setCellValue('Q1', 'исследована кровь в ПЦР');
        $worksheet->mergeCells('Q2:Q4');
        $worksheet->setCellValue('Q2', 'всего');
        $worksheet->mergeCells('R2:R4');
        $worksheet->setCellValue('R2', 'дети');
        $worksheet->mergeCells('S2:AD2');
        $worksheet->setCellValue('S2', 'в т. ч. с положительным результатом');
        $worksheet->mergeCells('S3:T3');
        $worksheet->setCellValue('S3', 'КЭ');
        $worksheet->setCellValue('S4', 'всего');
        $worksheet->setCellValue('T4', 'дети');

        $worksheet->mergeCells('U3:V3');
        $worksheet->setCellValue('U3', 'КБ');
        $worksheet->setCellValue('U4', 'всего');
        $worksheet->setCellValue('V4', 'дети');

        $worksheet->mergeCells('W3:X3');
        $worksheet->setCellValue('W3', 'МЭЧ');
        $worksheet->setCellValue('W4', 'всего');
        $worksheet->setCellValue('X4', 'дети');

        $worksheet->mergeCells('Y3:Z3');
        $worksheet->setCellValue('Y3', 'ГАЧ');
        $worksheet->setCellValue('Y4', 'всего');
        $worksheet->setCellValue('Z4', 'дети');

        $worksheet->mergeCells('AA3:AB3');
        $worksheet->setCellValue('AA3', 'КР');
        $worksheet->setCellValue('AA4', 'всего');
        $worksheet->setCellValue('AB4', 'дети');

        $worksheet->mergeCells('AC3:AD3');
        $worksheet->setCellValue('AC3', 'КВЛ');
        $worksheet->setCellValue('AC4', 'всего');
        $worksheet->setCellValue('AD4', 'дети');



        $worksheet->mergeCells('AE1:AH1');
        $worksheet->setCellValue('AE1', 'исследовано сывороток на а/г');
        $worksheet->mergeCells('AE2:AE4');
        $worksheet->setCellValue('AE2', 'всего');
        $worksheet->mergeCells('AF2:AF4');
        $worksheet->setCellValue('AF2', 'дети');
        $worksheet->mergeCells('AG2:AH2');
        $worksheet->setCellValue('AG2', 'в т. ч. с полож.');
        $worksheet->mergeCells('AG3:AG4');
        $worksheet->setCellValue('AG3', 'всего');
        $worksheet->mergeCells('AH3:AH4');
        $worksheet->setCellValue('AH3', 'дети');

        $worksheet->mergeCells('AI1:AL1');
        $worksheet->setCellValue('AI1', 'проведена экстренная профилактика');
        $worksheet->mergeCells('AI2:AJ2');
        $worksheet->setCellValue('AI2', 'иммуноглобулин');
        $worksheet->mergeCells('AI3:AI4');
        $worksheet->setCellValue('AI3', 'всего');
        $worksheet->mergeCells('AJ3:AJ4');
        $worksheet->setCellValue('AJ3', 'дети');
        $worksheet->mergeCells('AK2:AK4');
        $worksheet->setCellValue('AK2', 'иодантипирин');
        $worksheet->mergeCells('AL2:AL4');
        $worksheet->setCellValue('AL2', 'антибиотики');

        $admissionQuery = Admission::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);

        $residences = Residence::all();

        $rowStart = $row = 5;
	    $items = [];
	    foreach ($residences as $residence) {
	    	$currentItem = [];

	    	$currentItem['A'] = $residence->name;
    		//$worksheet->setCellValue('A' . $row, $territory->weekly_type);
	       	$residenceAdmissionQuery = (clone $admissionQuery)->where('extended_permanent_residence_id', $residence->id);

	       	// отказались
			$residenceAdmissionDenied = (clone $residenceAdmissionQuery)->whereHas('material', function($query) { 
				$query->where('name', 'Отказались');
			})->count();

	       	$currentItem['B'] = $residenceAdmissionDenied;

	       	$residenceAdmissionPCRQuery = (clone $residenceAdmissionQuery)->whereHas('tick_results', function($query) {
	       		$query->whereNotNull('tick_pcr_vte')
       				->orWhereNotNull('tick_pcr_tb')
       				->orWhereNotNull('tick_pcr_hme')
       				->orWhereNotNull('tick_pcr_hga')
       				->orWhereNotNull('tick_pcr_tr')
       				->orWhereNotNull('tick_pcr_trf');
	       	});

	       	// исследование клеща методом ПЦР всего
	       	$residenceAdmissionPCRTotalQuery = (clone $residenceAdmissionQuery)->where('material_id', 1);
	       	// исследование клеща методом ПЦР дети
	       	$residenceAdmissionPCRKids = (clone $residenceAdmissionPCRTotalQuery)->where('age', '<', '18')->count();
	       	$currentItem['C'] = $residenceAdmissionPCRTotalQuery->count();
	       	$currentItem['D'] = $residenceAdmissionPCRKids;

	       	// положительно на КЭ
	       	$residenceAdmissionPCRTE = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_vte', 1);
	       	});
			$residenceAdmissionPCRTEKids = (clone $residenceAdmissionPCRTE)->where('age', '<', '18')->count();
			$currentItem['E'] = $residenceAdmissionPCRTE->count();
			$currentItem['F'] = $residenceAdmissionPCRTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionPCRTB = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_tb', 1);
	       	});
	       	$residenceAdmissionPCRTBKids = (clone $residenceAdmissionPCRTB)->where('age', '<', '18')->count();
			$currentItem['G'] = $residenceAdmissionPCRTB->count();
			$currentItem['H'] = $residenceAdmissionPCRTBKids;

	       	// положительно на МЭЧ
	       	$residenceAdmissionPCRHME = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_hme', 1);
	       	});
	       	$residenceAdmissionPCRHMEKids = (clone $residenceAdmissionPCRHME)->where('age', '<', '18')->count();
			$currentItem['I'] = $residenceAdmissionPCRHME->count();
			$currentItem['J'] = $residenceAdmissionPCRHMEKids;

	       	// положительно на ГАЧ
	       	$residenceAdmissionPCRHGA = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_hga', 1);
	       	});
	       	$residenceAdmissionPCRHGAKids = (clone $residenceAdmissionPCRHGA)->where('age', '<', '18')->count();
			$currentItem['K'] = $residenceAdmissionPCRHGA->count();
			$currentItem['L'] = $residenceAdmissionPCRHGAKids;

	       	// положительно на КЛ
	       	$residenceAdmissionPCRTR = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_tr', 1);
	       	});
	       	$residenceAdmissionPCRTRKids = (clone $residenceAdmissionPCRTR)->where('age', '<', '18')->count();
			$currentItem['M'] = $residenceAdmissionPCRTR->count();
			$currentItem['N'] = $residenceAdmissionPCRTRKids;

	       	// положительно на КВЛ
	       	$residenceAdmissionPCRTRF = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_trf', 1);
	       	});
	       	$residenceAdmissionPCRTRFKids = (clone $residenceAdmissionPCRTRF)->where('age', '<', '18')->count();
			$currentItem['O'] = $residenceAdmissionPCRTRF->count();
			$currentItem['P'] = $residenceAdmissionPCRTRFKids;



			//
			$residenceAdmissionPCRQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('blood_pcr_vte')
       				->orWhereNotNull('blood_pcr_tb')
       				->orWhereNotNull('blood_pcr_hme')
       				->orWhereNotNull('blood_pcr_hga')
       				->orWhereNotNull('blood_pcr_tr')
       				->orWhereNotNull('blood_pcr_trf');
	       	});

	       	// исследование клеща методом ПЦР всего
	       	$residenceAdmissionPCRTotal = $residenceAdmissionPCRQuery->count();
	       	// исследование клеща методом ПЦР дети
	       	$residenceAdmissionPCRKids = (clone $residenceAdmissionPCRQuery)->where('age', '<', '18')->count();
	       	$currentItem['Q'] = $residenceAdmissionPCRTotal;
	       	$currentItem['R'] = $residenceAdmissionPCRKids;

	       	// положительно на КЭ
	       	$residenceAdmissionPCRTE = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_vte', 1);
	       	});
			$residenceAdmissionPCRTEKids = (clone $residenceAdmissionPCRTE)->where('age', '<', '18')->count();
			$currentItem['S'] = $residenceAdmissionPCRTE->count();
			$currentItem['T'] = $residenceAdmissionPCRTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionPCRTB = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_tb', 1);
	       	});
	       	$residenceAdmissionPCRTBKids = (clone $residenceAdmissionPCRTB)->where('age', '<', '18')->count();
			$currentItem['U'] = $residenceAdmissionPCRTB->count();
			$currentItem['V'] = $residenceAdmissionPCRTBKids;

	       	// положительно на МЭЧ
	       	$residenceAdmissionPCRHME = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_hme', 1);
	       	});
	       	$residenceAdmissionPCRHMEKids = (clone $residenceAdmissionPCRHME)->where('age', '<', '18')->count();
			$currentItem['W'] = $residenceAdmissionPCRHME->count();
			$currentItem['X'] = $residenceAdmissionPCRHMEKids;

	       	// положительно на ГАЧ
	       	$residenceAdmissionPCRHGA = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_hga', 1);
	       	});
	       	$residenceAdmissionPCRHGAKids = (clone $residenceAdmissionPCRHGA)->where('age', '<', '18')->count();
			$currentItem['Y'] = $residenceAdmissionPCRHGA->count();
			$currentItem['Z'] = $residenceAdmissionPCRHGAKids;

	       	// положительно на КЛ
	       	$residenceAdmissionPCRTR = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_tr', 1);
	       	});
	       	$residenceAdmissionPCRTRKids = (clone $residenceAdmissionPCRTR)->where('age', '<', '18')->count();
			$currentItem['AA'] = $residenceAdmissionPCRTR->count();
			$currentItem['AB'] = $residenceAdmissionPCRTRKids;

	       	// положительно на КВЛ
	       	$residenceAdmissionPCRTRF = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_trf', 1);
	       	});
	       	$residenceAdmissionPCRTRFKids = (clone $residenceAdmissionPCRTRF)->where('age', '<', '18')->count();
			$currentItem['AC'] = $residenceAdmissionPCRTRF->count();
			$currentItem['AD'] = $residenceAdmissionPCRTRFKids;


			// AG		
	       	$residenceAdmissionAgQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('antigen_te');
	       	});

			$residenceAdmissionAgTotal = $residenceAdmissionAgQuery->count();
	       	$residenceAdmissionAgKids = $residenceAdmissionAgQuery->where('age', '<', '18')->count();
	       	$currentItem['AE'] = $residenceAdmissionAgTotal;
	       	$currentItem['AF'] = $residenceAdmissionAgKids;

	       	$residenceAdmissionAgPos = (clone $residenceAdmissionAgQuery)->whereHas('result', function($query) {
	       		$query->where('antigen_te', 1);
	       	});
	       	$residenceAdmissionAgPosKids = (clone $residenceAdmissionAgPos)->where('age', '<', '18')->count();
			$currentItem['AG'] = $residenceAdmissionAgPos->count();
			$currentItem['AH'] = $residenceAdmissionAgPosKids;
	       	
			/*
			// кровь на антитела
	       	$residenceAdmissionIgQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('ig_m_te')
       				->orWhereNotNull('ig_g_te')
       				->orWhereNotNull('ig_m_tb')
       				->orWhereNotNull('ig_g_tb');
	       	});
	       	// кровь на антитела всего
	       	$residenceAdmissionIgTotal = $residenceAdmissionIgQuery->count();
			$residenceAdmissionIgKids = (clone $residenceAdmissionIgQuery)->where('age', '<', '18')->count();
	       	$currentItem['P'] = $residenceAdmissionIgTotal . ' / ' . $residenceAdmissionIgKids;

	       	// положительно на КЭ
	       	$residenceAdmissionIgTE = (clone $residenceAdmissionIgQuery)->whereHas('result', function($query) {
	       		$query->where('ig_m_te', 1)
	       			->orWhere('ig_g_te', 1);
	       	});
	       	$residenceAdmissionIgTEKids = (clone $residenceAdmissionIgTE)->where('age', '<', '18')->count();
	       	$currentItem['Q'] = $residenceAdmissionIgTE->count() . ' / ' . $residenceAdmissionIgTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionIgTB = (clone $residenceAdmissionIgQuery)->whereHas('result', function($query) {
	       		$query->where('ig_m_tb', 1)
	       			->orWhere('ig_g_tb', 1);
	       	});
	       	$residenceAdmissionIgTBKids = (clone $residenceAdmissionIgTB)->where('age', '<', '18')->count();
	       	$currentItem['R'] = $residenceAdmissionIgTB->count() . ' / ' . $residenceAdmissionIgTBKids;
	       */

	       	// проведена экстренная профилактика

	       	$residenceAdmissionIgedQuery = (clone $residenceAdmissionQuery)->where('ig_dose', '>', 0);
	       	$residenceAdmissionIgedTotal = $residenceAdmissionIgedQuery->count();
	       	$residenceAdmissionIgedKids = (clone $residenceAdmissionIgedQuery)->where('age', '>', '18')->count();

	       	$currentItem['AI'] = $residenceAdmissionIgedTotal;
	       	$currentItem['AJ'] = $residenceAdmissionIgedKids;

	       	// энцефалитные методы

	       	$residenceAdmissionEncQuery = (clone $residenceAdmissionQuery)->whereHas('encephalitis_method', function($query) {
	       		$query->where('name', '<>', 'Рекомендовано');
	       	});
	       	$residenceAdmissionEncTotal = $residenceAdmissionEncQuery->count();
	       	
	       	$currentItem['AK'] = $residenceAdmissionEncTotal;

	       	// антибиотики

	       	$residenceAdmissionBacQuery = (clone $residenceAdmissionQuery)->whereHas('bacterial_method', function($query) {
	       		$query->where('name', '<>', 'Рекомендовано');
	       	});
	       	$residenceAdmissionBacTotal = $residenceAdmissionBacQuery->count();

	       	$currentItem['AL'] = $residenceAdmissionBacTotal;

	    	$items[$row] = $currentItem;

	    	$row++;

	    }

	    $total = [];
	    /*for ($i = 2; $i < 40; $i++) {
	    	$total[$this->getColumnFromNum($i)] = 0;
	    }*/

	    foreach ($items as $row => $item) {

			foreach ($item as $column => $value) {
				if ($value != '0') {
					$worksheet->setCellValue($column . $row, $value);
				}
				if ($column != 'A') {
					if (isset($total[$column]))
						$total[$column] += $value;
					else
						$total[$column] = $value;
				}
			}
		}
		$rowEnd = $row;
		$row++;

	    foreach ($total as $column => $value) {
			$worksheet->setCellValue($column . $row, $value);
		}
		$worksheet->setCellValue('A' . $row, 'Всего');
		$row++;

		$writer = new Xlsx($spreadsheet);

	    $fileName = 'Отчёт для Быковой Натальи.xlsx';

	    $writer->save($fileName);

	    return $fileName;
    }

    // дополнение к отчёту Быковой
    private function weekly_report_additional()
    {
    	$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$nextDay = Carbon::parse($dateTo)->addDay(1)->format('Y-m-d');

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
    	$worksheet->setTitle('c ' . $dateFrom . ' по ' . $dateTo);

    	$worksheet->mergeCells('A1:A4');
        $worksheet->setCellValue('A1', 'ПМЖ');

        $worksheet->mergeCells('B1:B4');
        $worksheet->setCellValue('B1', 'от экспресс-диагностики отказались');

        $worksheet->mergeCells('C1:P1');
        $worksheet->setCellValue('C1', 'исследовано клещей в ПЦР');
        $worksheet->mergeCells('C2:C4');
        $worksheet->setCellValue('C2', 'всего');
        $worksheet->mergeCells('D2:D4');
        $worksheet->setCellValue('D2', 'дети');
        $worksheet->mergeCells('E2:P2');
        $worksheet->setCellValue('E2', 'в т. ч. с положительным результатом');
        $worksheet->mergeCells('E3:F3');
        $worksheet->setCellValue('E3', 'КЭ');
        $worksheet->setCellValue('E4', 'всего');
        $worksheet->setCellValue('F4', 'дети');

        $worksheet->mergeCells('G3:H3');
        $worksheet->setCellValue('G3', 'КБ');
        $worksheet->setCellValue('G4', 'всего');
        $worksheet->setCellValue('H4', 'дети');

        $worksheet->mergeCells('I3:J3');
        $worksheet->setCellValue('I3', 'МЭЧ');
        $worksheet->setCellValue('I4', 'всего');
        $worksheet->setCellValue('J4', 'дети');

        $worksheet->mergeCells('K3:L3');
        $worksheet->setCellValue('K3', 'ГАЧ');
        $worksheet->setCellValue('K4', 'всего');
        $worksheet->setCellValue('L4', 'дети');

        $worksheet->mergeCells('M3:N3');
        $worksheet->setCellValue('M3', 'КР');
        $worksheet->setCellValue('M4', 'всего');
        $worksheet->setCellValue('N4', 'дети');

        $worksheet->mergeCells('O3:P3');
        $worksheet->setCellValue('O3', 'КВЛ');
        $worksheet->setCellValue('O4', 'всего');
        $worksheet->setCellValue('P4', 'дети');



        $worksheet->mergeCells('Q1:AD1');
        $worksheet->setCellValue('Q1', 'исследована кровь в ПЦР');
        $worksheet->mergeCells('Q2:Q4');
        $worksheet->setCellValue('Q2', 'всего');
        $worksheet->mergeCells('R2:R4');
        $worksheet->setCellValue('R2', 'дети');
        $worksheet->mergeCells('S2:AD2');
        $worksheet->setCellValue('S2', 'в т. ч. с положительным результатом');
        $worksheet->mergeCells('S3:T3');
        $worksheet->setCellValue('S3', 'КЭ');
        $worksheet->setCellValue('S4', 'всего');
        $worksheet->setCellValue('T4', 'дети');

        $worksheet->mergeCells('U3:V3');
        $worksheet->setCellValue('U3', 'КБ');
        $worksheet->setCellValue('U4', 'всего');
        $worksheet->setCellValue('V4', 'дети');

        $worksheet->mergeCells('W3:X3');
        $worksheet->setCellValue('W3', 'МЭЧ');
        $worksheet->setCellValue('W4', 'всего');
        $worksheet->setCellValue('X4', 'дети');

        $worksheet->mergeCells('Y3:Z3');
        $worksheet->setCellValue('Y3', 'ГАЧ');
        $worksheet->setCellValue('Y4', 'всего');
        $worksheet->setCellValue('Z4', 'дети');

        $worksheet->mergeCells('AA3:AB3');
        $worksheet->setCellValue('AA3', 'КР');
        $worksheet->setCellValue('AA4', 'всего');
        $worksheet->setCellValue('AB4', 'дети');

        $worksheet->mergeCells('AC3:AD3');
        $worksheet->setCellValue('AC3', 'КВЛ');
        $worksheet->setCellValue('AC4', 'всего');
        $worksheet->setCellValue('AD4', 'дети');

        $worksheet->mergeCells('AE1:AH1');
        $worksheet->setCellValue('AE1', 'исследовано сывороток на а/г');
        $worksheet->mergeCells('AE2:AE4');
        $worksheet->setCellValue('AE2', 'всего');
        $worksheet->mergeCells('AF2:AF4');
        $worksheet->setCellValue('AF2', 'дети');
        $worksheet->mergeCells('AG2:AH2');
        $worksheet->setCellValue('AG2', 'в т. ч. с полож.');
        $worksheet->mergeCells('AG3:AG4');
        $worksheet->setCellValue('AG3', 'всего');
        $worksheet->mergeCells('AH3:AH4');
        $worksheet->setCellValue('AH3', 'дети');

        $worksheet->mergeCells('AI1:AL1');
        $worksheet->setCellValue('AI1', 'проведена экстренная профилактика');
        $worksheet->mergeCells('AI2:AJ2');
        $worksheet->setCellValue('AI2', 'иммуноглобулин');
        $worksheet->mergeCells('AI3:AI4');
        $worksheet->setCellValue('AI3', 'всего');
        $worksheet->mergeCells('AJ3:AJ4');
        $worksheet->setCellValue('AJ3', 'дети');
        $worksheet->mergeCells('AK2:AK4');
        $worksheet->setCellValue('AK2', 'иодантипирин');
        $worksheet->mergeCells('AL2:AL4');
        $worksheet->setCellValue('AL2', 'антибиотики');

        $admissionQuery = Admission::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo)->whereHas('result', function($query) use ($nextDay) { 
				$query->whereDate('issue_date', $nextDay);
			});

        $residences = Residence::all();

        $rowStart = $row = 5;
	    $items = [];
	    foreach ($residences as $residence) {
	    	$currentItem = [];

	    	$currentItem['A'] = $residence->name;
    		//$worksheet->setCellValue('A' . $row, $territory->weekly_type);
	       	$residenceAdmissionQuery = (clone $admissionQuery)->where('extended_permanent_residence_id', $residence->id);

	       	// отказались
			$residenceAdmissionDenied = (clone $residenceAdmissionQuery)->whereHas('material', function($query) { 
				$query->where('name', 'Отказались');
			})->count();

	       	//$currentItem['B'] = $residenceAdmissionDenied;

	       	$residenceAdmissionPCRQuery = (clone $residenceAdmissionQuery)->whereHas('tick_results', function($query) {
	       		$query->whereNotNull('tick_pcr_vte')
       				->orWhereNotNull('tick_pcr_tb')
       				->orWhereNotNull('tick_pcr_hme')
       				->orWhereNotNull('tick_pcr_hga')
       				->orWhereNotNull('tick_pcr_tr')
       				->orWhereNotNull('tick_pcr_trf');
	       	});

	       	// исследование клеща методом ПЦР всего
	       	$residenceAdmissionPCRTotalQuery = (clone $residenceAdmissionQuery)->where('material_id', 1);
	       	// исследование клеща методом ПЦР дети
	       	$residenceAdmissionPCRKids = (clone $residenceAdmissionPCRTotalQuery)->where('age', '<', '18')->count();
	       	//$currentItem['C'] = $residenceAdmissionPCRTotalQuery->count();
	       	//$currentItem['D'] = $residenceAdmissionPCRKids;

	       	// положительно на КЭ
	       	$residenceAdmissionPCRTE = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_vte', 1);
	       	});
			$residenceAdmissionPCRTEKids = (clone $residenceAdmissionPCRTE)->where('age', '<', '18')->count();
			$currentItem['E'] = $residenceAdmissionPCRTE->count();
			$currentItem['F'] = $residenceAdmissionPCRTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionPCRTB = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_tb', 1);
	       	});
	       	$residenceAdmissionPCRTBKids = (clone $residenceAdmissionPCRTB)->where('age', '<', '18')->count();
			$currentItem['G'] = $residenceAdmissionPCRTB->count();
			$currentItem['H'] = $residenceAdmissionPCRTBKids;

	       	// положительно на МЭЧ
	       	$residenceAdmissionPCRHME = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_hme', 1);
	       	});
	       	$residenceAdmissionPCRHMEKids = (clone $residenceAdmissionPCRHME)->where('age', '<', '18')->count();
			$currentItem['I'] = $residenceAdmissionPCRHME->count();
			$currentItem['J'] = $residenceAdmissionPCRHMEKids;

	       	// положительно на ГАЧ
	       	$residenceAdmissionPCRHGA = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_hga', 1);
	       	});
	       	$residenceAdmissionPCRHGAKids = (clone $residenceAdmissionPCRHGA)->where('age', '<', '18')->count();
			$currentItem['K'] = $residenceAdmissionPCRHGA->count();
			$currentItem['L'] = $residenceAdmissionPCRHGAKids;

	       	// положительно на КЛ
	       	$residenceAdmissionPCRTR = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_tr', 1);
	       	});
	       	$residenceAdmissionPCRTRKids = (clone $residenceAdmissionPCRTR)->where('age', '<', '18')->count();
			$currentItem['M'] = $residenceAdmissionPCRTR->count();
			$currentItem['N'] = $residenceAdmissionPCRTRKids;

	       	// положительно на КВЛ
	       	$residenceAdmissionPCRTRF = (clone $residenceAdmissionPCRQuery)->whereHas('tick_results', function($query) {
	       		$query->where('tick_pcr_trf', 1);
	       	});
	       	$residenceAdmissionPCRTRFKids = (clone $residenceAdmissionPCRTRF)->where('age', '<', '18')->count();
			$currentItem['O'] = $residenceAdmissionPCRTRF->count();
			$currentItem['P'] = $residenceAdmissionPCRTRFKids;



			//
			$residenceAdmissionPCRQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('blood_pcr_vte')
       				->orWhereNotNull('blood_pcr_tb')
       				->orWhereNotNull('blood_pcr_hme')
       				->orWhereNotNull('blood_pcr_hga')
       				->orWhereNotNull('blood_pcr_tr')
       				->orWhereNotNull('blood_pcr_trf');
	       	});

	       	// исследование клеща методом ПЦР всего
	       	$residenceAdmissionPCRTotal = $residenceAdmissionPCRQuery->count();
	       	// исследование клеща методом ПЦР дети
	       	$residenceAdmissionPCRKids = (clone $residenceAdmissionPCRQuery)->where('age', '<', '18')->count();
	       	//$currentItem['Q'] = $residenceAdmissionPCRTotal;
	       	//$currentItem['R'] = $residenceAdmissionPCRKids;

	       	// положительно на КЭ
	       	$residenceAdmissionPCRTE = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_vte', 1);
	       	});
			$residenceAdmissionPCRTEKids = (clone $residenceAdmissionPCRTE)->where('age', '<', '18')->count();
			$currentItem['S'] = $residenceAdmissionPCRTE->count();
			$currentItem['T'] = $residenceAdmissionPCRTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionPCRTB = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_tb', 1);
	       	});
	       	$residenceAdmissionPCRTBKids = (clone $residenceAdmissionPCRTB)->where('age', '<', '18')->count();
			$currentItem['U'] = $residenceAdmissionPCRTB->count();
			$currentItem['V'] = $residenceAdmissionPCRTBKids;

	       	// положительно на МЭЧ
	       	$residenceAdmissionPCRHME = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_hme', 1);
	       	});
	       	$residenceAdmissionPCRHMEKids = (clone $residenceAdmissionPCRHME)->where('age', '<', '18')->count();
			$currentItem['W'] = $residenceAdmissionPCRHME->count();
			$currentItem['X'] = $residenceAdmissionPCRHMEKids;

	       	// положительно на ГАЧ
	       	$residenceAdmissionPCRHGA = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_hga', 1);
	       	});
	       	$residenceAdmissionPCRHGAKids = (clone $residenceAdmissionPCRHGA)->where('age', '<', '18')->count();
			$currentItem['Y'] = $residenceAdmissionPCRHGA->count();
			$currentItem['Z'] = $residenceAdmissionPCRHGAKids;

	       	// положительно на КЛ
	       	$residenceAdmissionPCRTR = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_tr', 1);
	       	});
	       	$residenceAdmissionPCRTRKids = (clone $residenceAdmissionPCRTR)->where('age', '<', '18')->count();
			$currentItem['AA'] = $residenceAdmissionPCRTR->count();
			$currentItem['AB'] = $residenceAdmissionPCRTRKids;

	       	// положительно на КВЛ
	       	$residenceAdmissionPCRTRF = (clone $residenceAdmissionPCRQuery)->whereHas('result', function($query) {
	       		$query->where('blood_pcr_trf', 1);
	       	});
	       	$residenceAdmissionPCRTRFKids = (clone $residenceAdmissionPCRTRF)->where('age', '<', '18')->count();
			$currentItem['AC'] = $residenceAdmissionPCRTRF->count();
			$currentItem['AD'] = $residenceAdmissionPCRTRFKids;


			// AG		
	       	$residenceAdmissionAgQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('antigen_te');
	       	});

			$residenceAdmissionAgTotal = $residenceAdmissionAgQuery->count();
	       	$residenceAdmissionAgKids = $residenceAdmissionAgQuery->where('age', '<', '18')->count();
	       	//$currentItem['AE'] = $residenceAdmissionAgTotal;
	       	//$currentItem['AF'] = $residenceAdmissionAgKids;

	       	$residenceAdmissionAgPos = (clone $residenceAdmissionAgQuery)->whereHas('result', function($query) {
	       		$query->where('antigen_te', 1);
	       	});
	       	$residenceAdmissionAgPosKids = (clone $residenceAdmissionAgPos)->where('age', '<', '18')->count();
			$currentItem['AG'] = $residenceAdmissionAgPos->count();
			$currentItem['AH'] = $residenceAdmissionAgPosKids;
	       	
			/*
			// кровь на антитела
	       	$residenceAdmissionIgQuery = (clone $residenceAdmissionQuery)->whereHas('result', function($query) {
	       		$query->whereNotNull('ig_m_te')
       				->orWhereNotNull('ig_g_te')
       				->orWhereNotNull('ig_m_tb')
       				->orWhereNotNull('ig_g_tb');
	       	});
	       	// кровь на антитела всего
	       	$residenceAdmissionIgTotal = $residenceAdmissionIgQuery->count();
			$residenceAdmissionIgKids = (clone $residenceAdmissionIgQuery)->where('age', '<', '18')->count();
	       	$currentItem['P'] = $residenceAdmissionIgTotal . ' / ' . $residenceAdmissionIgKids;

	       	// положительно на КЭ
	       	$residenceAdmissionIgTE = (clone $residenceAdmissionIgQuery)->whereHas('result', function($query) {
	       		$query->where('ig_m_te', 1)
	       			->orWhere('ig_g_te', 1);
	       	});
	       	$residenceAdmissionIgTEKids = (clone $residenceAdmissionIgTE)->where('age', '<', '18')->count();
	       	$currentItem['Q'] = $residenceAdmissionIgTE->count() . ' / ' . $residenceAdmissionIgTEKids;

	       	// положительно на КБ
	       	$residenceAdmissionIgTB = (clone $residenceAdmissionIgQuery)->whereHas('result', function($query) {
	       		$query->where('ig_m_tb', 1)
	       			->orWhere('ig_g_tb', 1);
	       	});
	       	$residenceAdmissionIgTBKids = (clone $residenceAdmissionIgTB)->where('age', '<', '18')->count();
	       	$currentItem['R'] = $residenceAdmissionIgTB->count() . ' / ' . $residenceAdmissionIgTBKids;
	       */

	       	// проведена экстренная профилактика

	       	$residenceAdmissionIgedQuery = (clone $residenceAdmissionQuery)->where('ig_dose', '>', 0);
	       	$residenceAdmissionIgedTotal = $residenceAdmissionIgedQuery->count();
	       	$residenceAdmissionIgedKids = (clone $residenceAdmissionIgedQuery)->where('age', '>', '18')->count();

	       	$currentItem['AI'] = $residenceAdmissionIgedTotal;
	       	$currentItem['AJ'] = $residenceAdmissionIgedKids;

	       	// энцефалитные методы

	       	$residenceAdmissionEncQuery = (clone $residenceAdmissionQuery)->whereHas('encephalitis_method', function($query) {
	       		$query->where('name', '<>', 'Рекомендовано');
	       	});
	       	$residenceAdmissionEncTotal = $residenceAdmissionEncQuery->count();
	       	
	       	$currentItem['AK'] = $residenceAdmissionEncTotal;

	       	// антибиотики

	       	$residenceAdmissionBacQuery = (clone $residenceAdmissionQuery)->whereHas('bacterial_method', function($query) {
	       		$query->where('name', '<>', 'Рекомендовано');
	       	});
	       	$residenceAdmissionBacTotal = $residenceAdmissionBacQuery->count();

	       	$currentItem['AL'] = $residenceAdmissionBacTotal;

	    	$items[$row] = $currentItem;

	    	$row++;

	    }

	    $total = [];
	    /*for ($i = 2; $i < 40; $i++) {
	    	$total[$this->getColumnFromNum($i)] = 0;
	    }*/

	    foreach ($items as $row => $item) {

			foreach ($item as $column => $value) {
				if ($value != '0') {
					$worksheet->setCellValue($column . $row, $value);
				}
				if ($column != 'A') {
					if (isset($total[$column]))
						$total[$column] += $value;
					else
						$total[$column] = $value;
				}
			}
		}
		$rowEnd = $row;
		$row++;

	    foreach ($total as $column => $value) {
	    	if ($value != '0') {
				$worksheet->setCellValue($column . $row, $value);
			}
		}
		$worksheet->setCellValue('A' . $row, 'Всего');
		$row++;

		$writer = new Xlsx($spreadsheet);

	    $fileName = 'Дополнение к отчёту для Быковой Натальи.xlsx';

	    $writer->save($fileName);

	    return $fileName;
    }

    // отчёт для Чумаченко 2019
    private function weekly_report_rpn_2019()
    {
    	$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$absTotal = TickResult::whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
		$period = 'c ' . $dateFrom . ' по ' . $dateTo;
    	$worksheet->setTitle($period);

        $worksheet->mergeCells('A1:G1');
        $worksheet->setCellValue('A1', 'Сводка  по клещам в Управление Роспотребнадзора по Иркутской области  в эпидемиологический отдел ФБУЗ  "Центр гигиены и эпидемиологии в Иркутской области" от ФГБНУ "НЦ ПЗСРЧ" Центр Диагностики и профилактики клещевых инфекций , К.Маркса,3');
        $worksheet->mergeCells('A2:G2');
        $worksheet->setCellValue('A2', $period);

        $worksheet->mergeCells('A3:A4');
        $worksheet->setCellValue('A3', '№п/п');

        $worksheet->mergeCells('B3:D4');
        $worksheet->setCellValue('B3', 'Наименование');

        $worksheet->mergeCells('E3:E4');
        $worksheet->setCellValue('E3', 'Всего');

       	$worksheet->mergeCells('F3:G3');
        $worksheet->setCellValue('F3', 'Методика');

        $worksheet->setCellValue('F4', 'Методом ПЦР');
        $worksheet->setCellValue('G4', 'Методом ИФА');

        $worksheet->mergeCells('A5:G5');
        $worksheet->setCellValue('A5',  'исследовано клещей');

        // вкэ
        $worksheet->setCellValue('A6', '1');
        $worksheet->mergeCells('B6:D6');
        $worksheet->setCellValue('B6', 'в том числе снятых с людей');
        $worksheet->setCellValue('E6', $absTotal);
        $worksheet->setCellValue('F6', $absTotal);

        // вкэ
        $worksheet->setCellValue('A7', '2');
        $worksheet->mergeCells('B7:D7');
        $worksheet->setCellValue('B7', 'положительных на клещевой вирусный энцефалит');

        $total = TickResult::where('tick_pcr_vte', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E7', $total);
        $worksheet->setCellValue('F7', $total);

        // кб
        $worksheet->setCellValue('A8', '3');
        $worksheet->mergeCells('B8:D8');
        $worksheet->setCellValue('B8', 'положительных на иксодовый клещевой боррелиоз');

        $total = TickResult::where('tick_pcr_tb', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E8', $total);
        $worksheet->setCellValue('F8', $total);

        // мэч
        $worksheet->setCellValue('A9', '4');
        $worksheet->mergeCells('B9:D9');
        $worksheet->setCellValue('B9', 'положительных на моницитарный эрлихиоз');
       
        $total = TickResult::where('tick_pcr_hme', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E9', $total);
        $worksheet->setCellValue('F9', $total);

        // гач
        $worksheet->setCellValue('A10', '5');
        $worksheet->mergeCells('B10:D10');
        $worksheet->setCellValue('B10', 'положительных на гранулоцитарный анаплазмоз человека');
        
        $total = TickResult::where('tick_pcr_hga', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E10', $total);
        $worksheet->setCellValue('F10', $total);

        // кр
        $worksheet->setCellValue('A11', '6');
        $worksheet->mergeCells('B11:D11');
        $worksheet->setCellValue('B11', 'положительных на клещевой риккетсиоз');
        
        $total = TickResult::where('tick_pcr_tr', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E11', $total);
        $worksheet->setCellValue('F11', $total);

        // квл
        $worksheet->setCellValue('A12', '7');
        $worksheet->mergeCells('B12:D12');
        $worksheet->setCellValue('B12', 'положительных на клещевую возвратную лихорадку');
        
        $total = TickResult::where('tick_pcr_trf', true)
					->whereHas('admission', function ($query) use ($dateFrom, $dateTo) {
						$query->whereHas('result', function ($q) use ($dateFrom, $dateTo) {
							$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
						});
					})->count();
        $worksheet->setCellValue('E12', $total);
        $worksheet->setCellValue('F12', $total);

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Отчёт в Роспотребнадзор.xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    // отчёт для Чумаченко 2020
    private function weekly_report_rpn()
    {
    	$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
		$period = 'c ' . $dateFrom . ' по ' . $dateTo;
    	$worksheet->setTitle($period);

        $worksheet->mergeCells('A1:A2');
        $worksheet->setCellValue('A1', 'ПМЖ');

        $worksheet->mergeCells('B1:C1');
        $worksheet->setCellValue('B1', 'Обратилось');
        $worksheet->setCellValue('B2', 'Всего');
        $worksheet->setCellValue('C2', 'Дети');


        $worksheet->mergeCells('D1:E1');
        $worksheet->setCellValue('D1', 'Из них привиты');
        $worksheet->setCellValue('D2', 'Всего');
        $worksheet->setCellValue('E2', 'Дети');


        $worksheet->mergeCells('F1:F2');
        $worksheet->setCellValue('F1', 'Всего случаев присасывания на данной территории');

        $worksheet->mergeCells('G1:G2');
        $worksheet->setCellValue('G1', 'Укусы в населённых пунктах');


        $worksheet->mergeCells('H1:J1');
        $worksheet->setCellValue('H1', 'Укусы на территории детских учреждений');
        $worksheet->setCellValue('H2', 'Загородные оздоровительные');
        $worksheet->setCellValue('I2', 'Оздоровительные дневного пребывания');
        $worksheet->setCellValue('J2', 'Образовательных (дет. сады, школы и т.д.)');

        $query = Admission::whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);

        $residences = Residence::all();

        $childCare = ChildCareFacility::all();

        $rowStart = $row = 3;
	    $items = [];

	    $lettersChildCare = ['H', 'I', 'J'];

        foreach ($residences as $residence) {
        	$admissionQuery = (clone $query)->where('extended_permanent_residence_id', $residence->id);
        	$currentItem = [];
        	$currentItem['A'] = $residence->name;

        	$currentItem['B'] = $admissionQuery->count();
        	$currentItem['C'] = (clone $admissionQuery)->where('age', '<', '18')->count();

        	$vaccinatedQuery = (clone $admissionQuery)->whereIn('vaccine_level_id', range(1,4));
        	$vaccinatedKids = (clone $vaccinatedQuery)->where('age', '<', '18')->count();

        	$currentItem['D'] = $vaccinatedQuery->count();;
        	$currentItem['E'] = $vaccinatedKids;

        	$bittenHereQuery = (clone $query)->where('extended_admin_territory_id', $residence->id);
        	$locations = implode (array_unique((clone $bittenHereQuery)->where('locality_clarification', '<>', '')->pluck('locality_clarification')->toArray()), ', ') ;

        	$currentItem['F'] = $bittenHereQuery->count();;
        	$currentItem['G'] = $locations;

        	foreach ($childCare as $key => $item) {
        		$currentItem[$lettersChildCare[$key]] = (clone $bittenHereQuery)->where('child_care_facility_id', $item->id)->count();
        	}

        	$items[$row] = $currentItem;

        	$row++;
        }

        $total = [];
	    /*for ($i = 2; $i < 40; $i++) {
	    	$total[$this->getColumnFromNum($i)] = 0;
	    }*/

	    foreach ($items as $row => $item) {

			foreach ($item as $column => $value) {
				if ($value != '0') {
					$worksheet->setCellValue($column . $row, $value);
				}
				if (($column != 'A') && ($column != 'G')) {
					if (isset($total[$column]))
						$total[$column] += $value;
					else
						$total[$column] = $value;
				}
			}
		}
		$rowEnd = $row;
		$row++;

	    foreach ($total as $column => $value) {
			$worksheet->setCellValue($column . $row, $value);
		}
		$worksheet->setCellValue('A' . $row, 'Всего');
		$row++;

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Отчёт в Роспотребнадзор.xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    private function generate_pdf($admission)
    {
        //PDF::setOptions(['dpi' => 150, 'defaultFont' => 'Montserrat']);

        $pdf = PDF::loadView('reports.reg_card', compact('admission'));

        $fileName = $admission->id . ' ' . $admission->name . '.pdf';

        $pdf->save($fileName);

        return $fileName;

        //return response()->download($fileName)->deleteFileAfterSend();
    }

    private function vac_report_rpn()
    {
		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
        
        //автоматически прописывать название месяца
        $worksheet->setTitle('с ' . $dateFrom . ' по ' . $dateTo);

        $worksheet->mergeCells('A1:E2');
        $worksheet->setCellValue('A1', 'Федеральное государственное бюджетное научное учреждение');

        $worksheet->mergeCells('A3:E4');
        $worksheet->setCellValue('A3', 'Научный центр проблем здоровья семьи и репродукции человека');

        $worksheet->mergeCells('A5:E5');
        $worksheet->setCellValue('A5', 'г.Иркутск, ул.Карла Маркса,3');

        $worksheet->mergeCells('A6:E6');
        $worksheet->setCellValue('A6', 'Тел/факс: 333-445, Е-mail: karla_marksa-3@mail.ru');

        $worksheet->mergeCells('A8:E8');
        $worksheet->setCellValue('A8', 'Сведения  о профилактических прививках с ' . $dateFrom . ' по ' . $dateTo . 'гг.');

        $worksheet->mergeCells('A9:E9');
        $worksheet->setCellValue('A9', '(Ф-5)');

        $worksheet->mergeCells('A10:B11');
        $worksheet->setCellValue('A10', 'Всего привито против КЭ взрослых');

        $worksheet->mergeCells('C10:D11');
        $worksheet->setCellValue('C10', 'дети до 14 лет');

        $worksheet->mergeCells('E10:E12');
        $worksheet->setCellValue('E10', 'Применяемая вакцина');

        $worksheet->setCellValue('A12', 'Вакцинировано');
        $worksheet->setCellValue('B12', 'Реакцинировано');
        $worksheet->setCellValue('C12', 'Вакцинировано');
        $worksheet->setCellValue('D12', 'Реакцинировано');

        $vaccinationQuery = Vaccination::whereDate('date', '>=', $dateFrom)
        					->whereDate('date', '<=', $dateTo)
        					->where('age', '>', '14');

        $vaccinatedV1 = (clone $vaccinationQuery)->where('vaccine_level_id', '1')->count();
        $vaccinatedV2 = (clone $vaccinationQuery)->where('vaccine_level_id', '2')->count();
        $vaccinatedV3 = (clone $vaccinationQuery)->where('vaccine_level_id', '3')->count();
        $vaccinatedRV = (clone $vaccinationQuery)->where('vaccine_level_id', '4')->count();

        $worksheet->setCellValue('A13', 'V1 - ' . $vaccinatedV1);
        $worksheet->setCellValue('A14', 'V2 - ' . $vaccinatedV2);
        $worksheet->setCellValue('A15', 'V3 - ' . $vaccinatedV3);
        $worksheet->mergeCells('B13:B17');
        $worksheet->setCellValue('B13', $vaccinatedRV);
        $worksheet->mergeCells('A16:A17');
        $adultTotal = $vaccinatedV1 + $vaccinatedV2 + $vaccinatedV3 + $vaccinatedRV;
        $worksheet->setCellValue('A16', 'Всего - ' . $adultTotal);

        $vaccinationKidQuery = Vaccination::whereDate('date', '>=', $dateFrom)
        					->whereDate('date', '<=', $dateTo)
        					->where('age', '<=', '14');

        $vaccinatedV1 = (clone $vaccinationKidQuery)->where('vaccine_level_id', '1')->count();
        $vaccinatedV2 = (clone $vaccinationKidQuery)->where('vaccine_level_id', '2')->count();
        $vaccinatedV3 = (clone $vaccinationKidQuery)->where('vaccine_level_id', '3')->count();
        $vaccinatedRV = (clone $vaccinationKidQuery)->where('vaccine_level_id', '4')->count();

        $worksheet->setCellValue('C13', 'V1 - ' . $vaccinatedV1);
        $worksheet->setCellValue('C14', 'V2 - ' . $vaccinatedV2);
        $worksheet->setCellValue('C15', 'V3 - ' . $vaccinatedV3);
        $worksheet->mergeCells('D13:D17');
        $worksheet->setCellValue('D13', $vaccinatedRV);
        $worksheet->mergeCells('C16:C17');
        $kidTotal = $vaccinatedV1 + $vaccinatedV2 + $vaccinatedV3 + $vaccinatedRV;
        $worksheet->setCellValue('C16', 'Всего - ' . $kidTotal);

        $vaccines = Vaccine::where('id', '<>', '1')->where('enabled', '1')->get();

        $row = '13';
        foreach ($vaccines as $vaccine) {
        	$vaccineTotal = Vaccination::whereDate('date', '>=', $dateFrom)
        					->whereDate('date', '<=', $dateTo)
        					->where('vaccine_id', $vaccine->id)
        					->count();

        	$worksheet->setCellValue('E' . $row, $vaccine->name . ' - ' . $vaccineTotal);
        	$row++;
        }

        $total = $adultTotal + $kidTotal;
        $worksheet->mergeCells('A19:E20');
        $worksheet->setCellValue('A19', 'Итого получили профилактические прививки против клещевого энцефалита - ' . $total . ' человек.');

        $worksheet->mergeCells('A22:C22');
        $worksheet->setCellValue('A22', 'Руководитель центра диагностики профилактики клещевых инфекций');

        $worksheet->setCellValue('E22', 'Петрова И.В.');

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Отчёт по вакцинации по Ф-5 в ФГУЗ.xlsx';

        $writer->save($fileName);

        return $fileName;
    }

    public function generate_vaccination_agreement_report(VaccinationAgreement $agreement)
    {

    	$vaccinations = $agreement->vaccinations;

    	$spreadsheet = new Spreadsheet();

		$worksheet = $spreadsheet->getActiveSheet();
        
        //автоматически прописывать название месяца
        $worksheet->setTitle($this->months[Carbon::Parse($agreement->date)->month - 1]);

        $worksheet->mergeCells('A1:G1');
        $worksheet->setCellValue('A1', 'Реестр на медицинские услуги для ' . $agreement->company_name);

        $worksheet->mergeCells('A2:G2');
        $worksheet->setCellValue('A2', 'Лечебное учреждение:ФГБНУ НЦ ПЗСРЧ Центр диагностики и профилактики клещевых инфекций');

        $worksheet->mergeCells('A3:G3');
        $worksheet->setCellValue('A3', 'г.Иркутск, ул.Карла Маркса,3 телефон регистратуры 33-34-45, E-mail:Karla_Marksa-3@mail.ru');

        $worksheet->setCellValue('A5', '№');
        $worksheet->setCellValue('B5', 'Дата вакцинации');
        $worksheet->setCellValue('C5', 'ФИО клиента');
        $worksheet->setCellValue('D5', 'Возраст');
        $worksheet->setCellValue('E5', 'Наименование вакцины');
        $worksheet->setCellValue('F5', 'Кратность');
        $worksheet->setCellValue('G5', 'Стоимость услуги');

        $row = 6;

        foreach ($vaccinations as $key => $vaccination) {
        	$worksheet->setCellValue('A' . $row, $key + 1);
        	$worksheet->setCellValue('B' . $row, Carbon::parse($vaccination->date)->format('d/m/Y'));
        	$worksheet->setCellValue('C' . $row, $vaccination->name);
        	$worksheet->setCellValue('D' . $row, $vaccination->age);
        	$worksheet->setCellValue('E' . $row, $vaccination->vaccine->name);
        	$worksheet->setCellValue('F' . $row, $vaccination->vaccine_level->name);
        	$worksheet->setCellValue('G' . $row, $vaccination->vaccine->price->price);

        	$row++;
        }

        $worksheet->setCellValue('G' . $row, '=SUM(G6:G' . ($row - 1) . ')');
        $totalCell = 'G' . $row;
        $row += 2;

        $worksheet->mergeCells('A' . $row . ':G' . $row);

        $worksheet->setCellValue('A' . $row, '=CONCATENATE("Итого предоставлено медицинских услуг для организации ' . $agreement->company_name . ' на общую сумму ", ' . $totalCell . ', "рублей, 00 копеек")');

        $row += 2;
		$worksheet->setCellValue('A' . $row, 'Директор ФГБНУ НЦ ПЗСРЧ,  д.м.н.');
		$worksheet->setCellValue('G' . $row, 'Рычкова Л.В.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Гл. бухгалтер');
		$worksheet->setCellValue('G' . $row, 'Степанова В.П.');

		$row++;
		$worksheet->setCellValue('A' . $row, 'Зав. центром');
		$worksheet->setCellValue('G' . $row, 'Петрова И.В.');

        $writer = new Xlsx($spreadsheet);

        $fileName = 'Реестр вакцинаций – ' . $agreement->company_name . ' .xlsx';

        $writer->save($fileName);

        return response()->download($fileName)->deleteFileAfterSend();
	}
	
/////////////////////
	public function testReport(Request $request) {

		$dateFrom = request()->date_from;

		$dateTo = request()->date_to;

		$spreadsheet = new SpreadSheet();

		$sheet = $spreadsheet->getActiveSheet();

		$row_id = 3;

		$insuranceCompanies = InsuranceCompany::all();

		foreach($insuranceCompanies as $company) {

			$sheet->setCellValue('B'.$row_id, $company->name);

			$admissions = Admission::where('insurance_company_id', $company->id)
						->where(function($query) use ($dateFrom, $dateTo) {
							$query->where(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('date', '>=', $dateFrom)->whereDate('date', '<=', $dateTo);
							})
							->orWhereHas('result', function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('issue_date', '>=', $dateFrom)->whereDate('issue_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('refund', '>=', $dateFrom)->whereDate('refund', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('ig_date', '>=', $dateFrom)->whereDate('ig_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('consult_date', '>=', $dateFrom)->whereDate('consult_date', '<=', $dateTo);
							})
							->orWhere(function($q) use ($dateFrom, $dateTo) {
								$q->whereDate('measures_date', '>=', $dateFrom)->whereDate('measures_date', '<=', $dateTo);
							});
						})
						->where('to_skip', false)
						->get();

						// if($company->id == 12) {
						// 	dd($company);
						// }			

			foreach($admissions as $admission) {

				// if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

				// 	//консультация врача по факту укуса

				// 	if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {

				// 		$price = Price::where('code', 'К022')->first();

				// 		$company['doctor_visit'] += $price->price;

				// 	}
				// }

				// if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

				// 	//консультация инфекциониста

				// 	if ($admission->infect_consult) {

				// 		$price = Price::where('code', 'В036')->first();

				// 		$company['infection_consult'] += $price->price;	
						

				// 	}

				// }

				if ($admission->refund || ($admission->date >= $dateFrom && $admission->date <= $dateTo)) {

					//сумма первичных приемов

					if($admissions->count() != 0) {

						$company['admissionCount'] += Price::where('code', 'К014')->first()->price; 
					
					}

					//удаление клеща
					
					if($admission->removal_institution_id == 1) {

						$company['removalCount'] += Price::where('code', 'К015')->first()->price;											
						
					}

					//сумма забора крови

					if ($admission->material_id == '2') {

						$price = Price::where('code', 'А2331196')->first();

						$company['bloodCount'] += $price->price;
									
					}
				}
				

				if ($admission->refund || ($admission->result->issue_date >= $dateFrom && $admission->result->issue_date <= $dateTo)) {
					
					//ПЦР и остальные исследования
					$policyResNum = $admission->policy_research_num;

					$pcrNum = $admission->researches->where('type', 'pcr')->count();

					if ($pcrNum >= $policyResNum) {

						$pcrFinalNum = $policyResNum;

					} else {

						$pcrFinalNum = $pcrNum;

						$restResNum = $policyResNum - $pcrNum;

						$restRes = $admission->researches->where('type', '<>', 'pcr')->take($restResNum);

						foreach ($restRes as $research) {
							
							$company[$research->price->code] += $research->price->price;
							
						}

					}

					if ($pcrFinalNum > 0) {

						$price = Price::where('type', 'pcr')->where('research_num', $pcrFinalNum)->first();

						if ($admission->material_id == '1') {
							
							$company[$pcrFinalNum . 'Pcr'] += $price->price * $admission->material_amount;

						} else {

							$company[$pcrFinalNum . 'Pcr'] += $price->price;

						}

					}

				}
				
				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo)) {

					//введение Ig

					if ($admission->ig_dose) {

						$price = Price::where('code', 'К019')->first();

						$company['Ig_injections'] += $admission->ig_dose * $price->price;			

						//внутремышечная инъекция

						$priceIG = Price::where('code', 'П002')->first();

						$company['intramuscular_injections'] += $priceIG->price;
					}

				}
				
				if ($admission->refund || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//профилактика КБ

					if ($admission->bacterial_method_id && $admission->bacterial_method_id != '3') {

						$price = Price::where('code', 'К021')->first();

						$company['KB_prevention'] += $price->price;				

					}
				}

				if ($admission->refund || ($admission->ig_date >= $dateFrom && $admission->ig_date <= $dateTo) || ($admission->measures_date >= $dateFrom && $admission->measures_date <= $dateTo)) {

					//консультация врача по факту укуса

					if ($admission->encephalitis_method_id || $admission->bacterial_method_id || $admission->ig_dose) {

						$price = Price::where('code', 'К022')->first();

						$company['doctor_visit'] += $price->price;

					}
				}
				
				if ($admission->refund || ($admission->consult_date >= $dateFrom && $admission->consult_date <= $dateTo)) {

					//консультация инфекциониста

					if ($admission->infect_consult) {

						$price = Price::where('code', 'В036')->first();

						$company['infection_consult'] += $price->price;	
						

					}

				}

				
				// if($admission->id == 12) {
				// 	dd($admission->consult_date >= $dateFrom);
				// }

			}

			// приемы по антителам, которые по дате попадают в обозначенный период

			$antibodies = Antibody::where('insurance_company_id', $company->id)
			->whereDate('date', '>=', $dateFrom)
			->whereDate('date', '<=', $dateTo)->get();

			foreach($antibodies as $antibody) {

				// забор крови
				$price = Price::where('code', 'А2331196')->first();

				$company['bloodCount'] += $price->price;				

				if ($antibody->infect_consult) {

					$price = Price::where('code', 'В036')->first();

					$company['infection_consult'] += $price->price;
					
				}

				// ПЦР и другие исследования

				// количество ПЦР исследований

				$pcrNum = $antibody->researches->where('type', 'pcr')->count();

				if ($pcrNum > 0) {

					$price = Price::where('type', 'pcr')->where('research_num', $pcrNum)->first();

					$company[$pcrNum . 'Pcr'] += $price->price;				
				}

				$restRes = $antibody->researches->where('type', '<>', 'pcr');

				foreach ($restRes as $research) {
					
					$company[$research->price->code] += $research->price->price;

				}

			}

			$sheet->setCellValue('E'.$row_id, $company['2Pcr']);

			$sheet->setCellValue('F'.$row_id, $company['4Pcr']);

			$sheet->setCellValue('G'.$row_id, $company['5Pcr']);

			$sheet->setCellValue('H'.$row_id, $company['6Pcr']);

			$sheet->setCellValue('J'.$row_id, $company['К018']);

			$sheet->setCellValue('K'.$row_id, $company['К023']);

			$sheet->setCellValue('L'.$row_id, $company['К025']);

			$sheet->setCellValue('M'.$row_id, $company['К024']);

			$sheet->setCellValue('N'.$row_id, $company['К026']);

			$sheet->setCellValue('C'.$row_id, $company['admissionCount']);
			
			$sheet->setCellValue('D'.$row_id, $company['removalCount']);

			$sheet->setCellValue('I'.$row_id, $company['bloodCount'] );

			$sheet->setCellValue('O'.$row_id, $company['Ig_injections'] );

			$sheet->setCellValue('P'.$row_id, $company['intramuscular_injections'] );

			$sheet->setCellValue('Q'.$row_id, $company['KB_prevention']);

			$sheet->setCellValue('R'.$row_id, $company['doctor_visit']);

			$sheet->setCellValue('S'.$row_id, $company['infection_consult']);
			// if($company->id == 8) {
			// 	dd($company);
			// }

			$sheet->setCellValue('T'.$row_id,'=SUM(C'.$row_id.':S'.$row_id.')');

			$row_id++;

		}

		$sheet->mergeCells('B1:J1');

		$sheet->setCellValue('B1', 'Работа центра профилактики клещевых инфекций со страховыми компаниями за промежуток : '.Carbon::parse($dateFrom)->format('d/m/Y').' - '.Carbon::parse($dateTo)->format('d/m/Y'));

		$sheet->setCellValue('B2', 'СК');

		$sheet->setCellValue('C2', 'Первичный прием');

		$sheet->setCellValue('D2', 'Удаление клеща');

		$sheet->setCellValue('E2', 'ПЦР 2 инф');

		$sheet->setCellValue('F2', 'ПЦР 4 инф');

		$sheet->setCellValue('G2', '5 инф');

		$sheet->setCellValue('H2', 'ПЦР 6 инф');

		$sheet->setCellValue('I2', 'Забор крови');

		$sheet->setCellValue('J2', 'Кровь на Ag КЭ');

		$sheet->setCellValue('K2', 'Кровь на IgM КЭ');

		$sheet->setCellValue('L2', 'Кровь на IgM КБ');

		$sheet->setCellValue('M2', 'Кровь на IgG КЭ');

		$sheet->setCellValue('N2', 'Кровь на IgG КБ');

		$sheet->setCellValue('O2', 'Введение Ig');

		$sheet->setCellValue('P2', 'В/м');

		$sheet->setCellValue('Q2', 'Профилактика КБ');

		$sheet->setCellValue('R2', 'Прием врача по факту укуса');

		$sheet->setCellValue('S2', 'Инфекционист');

		$sheet->setCellValue('T2', 'Итого');

		$companiesAmountColumnCount = $insuranceCompanies->count()+4;

		$companiesTotalColomnCount = $insuranceCompanies->count()+3;
				
		$sheet->setCellValue('B'.$companiesTotalColomnCount, 'Итого');

		$sheet->setCellValue('B'.$companiesAmountColumnCount, 'Кол-во');

		$sheet->setCellValue('C'.$companiesAmountColumnCount, '=C'.$companiesTotalColomnCount.'/'.Price::where('code', 'К014')->first()->price);

		$sheet->setCellValue('D'.$companiesAmountColumnCount, '=D'.$companiesTotalColomnCount.'/'.Price::where('code', 'К015')->first()->price);

		$sheet->setCellValue('E'.$companiesAmountColumnCount, '=E'.$companiesTotalColomnCount.'/'.Price::where('code', 'К016')->first()->price);

		$sheet->setCellValue('F'.$companiesAmountColumnCount, '=F'.$companiesTotalColomnCount.'/'.Price::where('code', 'К035')->first()->price);

		$sheet->setCellValue('G'.$companiesAmountColumnCount, '=G'.$companiesTotalColomnCount.'/'.Price::where('code', 'К039')->first()->price);

		$sheet->setCellValue('H'.$companiesAmountColumnCount, '=H'.$companiesTotalColomnCount.'/'.Price::where('code', 'К042')->first()->price);

		$sheet->setCellValue('I'.$companiesAmountColumnCount, '=I'.$companiesTotalColomnCount.'/'.Price::where('code', 'А2331196')->first()->price);

		$sheet->setCellValue('J'.$companiesAmountColumnCount, '=J'.$companiesTotalColomnCount.'/'.Price::where('code', 'К018')->first()->price);

		$sheet->setCellValue('K'.$companiesAmountColumnCount, '=K'.$companiesTotalColomnCount.'/'.Price::where('code', 'К023')->first()->price);

		$sheet->setCellValue('L'.$companiesAmountColumnCount, '=L'.$companiesTotalColomnCount.'/'.Price::where('code', 'К025')->first()->price);

		$sheet->setCellValue('M'.$companiesAmountColumnCount, '=M'.$companiesTotalColomnCount.'/'.Price::where('code', 'К024')->first()->price);

		$sheet->setCellValue('N'.$companiesAmountColumnCount, '=N'.$companiesTotalColomnCount.'/'.Price::where('code', 'К026')->first()->price);

		$sheet->setCellValue('O'.$companiesAmountColumnCount, '=O'.$companiesTotalColomnCount.'/'.Price::where('code', 'К019')->first()->price);

		$sheet->setCellValue('P'.$companiesAmountColumnCount, '=P'.$companiesTotalColomnCount.'/'.Price::where('code', 'П002')->first()->price);

		$sheet->setCellValue('Q'.$companiesAmountColumnCount, '=Q'.$companiesTotalColomnCount.'/'.Price::where('code', 'К021')->first()->price);

		$sheet->setCellValue('R'.$companiesAmountColumnCount, '=R'.$companiesTotalColomnCount.'/'.Price::where('code', 'К022')->first()->price);

		$sheet->setCellValue('S'.$companiesAmountColumnCount, '=S'.$companiesTotalColomnCount.'/'.Price::where('code', 'В036')->first()->price);

		$sheet->getDefaultColumnDimension()->setWidth(9);

		$sheet->getColumnDimension('B')->setWidth(25);

		$sheet->getColumnDimension('A')->setWidth(5);

		$sheet->getRowDimension('2')->setRowHeight(60);

		$sheet->getStyle('C2:T2')->getAlignment()->setWrapText(true);

		foreach(range('B', 'T') as $letter) {

			for($i = 2; $i <= $row_id + 1; $i++) {

				$sheet->getStyle($letter . $i)->applyFromArray($this->cellStyleArray);

			}

		}

		foreach(range('C', 'T') as $letter) {

			$sheet->getStyle($letter . $companiesTotalColomnCount)->applyFromArray($this->totalStyleArray);

		}

		for($i = 3; $i <= 19; $i++) {
			
			$column = $this->getColumnFromNum($i);
			$companiesTotalRowCount = $insuranceCompanies->count()+2;
			$sheet->setCellValue($column.$companiesTotalColomnCount,'=SUM('.$column.'3:'.$column.$companiesTotalRowCount.')');

		}

		for($i=2; $i<=$insuranceCompanies->count()+2; $i++) {

			$sheet->getStyle('I' . $i)->applyFromArray($this->totalStyleArray);

		}
		
		$sheet->setCellValue('T'.$companiesTotalColomnCount,'=SUM(C'.$companiesTotalColomnCount.':S'.$companiesTotalColomnCount.')');

		$sheet->getStyle('C3:T40')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);		
	
        $fileName = 'Отчет по СК за'.Carbon::parse($dateFrom)->format('d/m/Y').'-'.Carbon::parse($dateTo)->format('d/m/Y').'.xlsx';

		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
		
		header('Cache-Control: max-age=0');

		$writer = new Xlsx($spreadsheet);

		$writer->save('php://output');

	}









}
