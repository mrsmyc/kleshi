<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PaymentType;

use App\ProfContingent;

use App\Vaccine;

use App\VaccinationAgreement;

use App\VaccineSerie;

use App\VaccineSpot;

use App\Group;

class VaccinationAgreementController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $agreements = VaccinationAgreement::orderBy('id', 'desc')->paginate('20');

        return view('vaccination_agreements.index', compact('agreements'));
    }

    public function create()
    {
    	$paymentTypes = PaymentType::where('name', 'Безналичный')->get();

        $vaccines = Vaccine::all();

        $vaccineSeries = VaccineSerie::all();

    	$vaccineSpots = VaccineSpot::all();

        //$profContingents = ProfContingent::all();
    	
        $profContingents = Group::where('type', 'prof_contingent')->get();

        $riskGroups = Group::where('type', 'risk_group')->get();

        $socialGroups = Group::where('type', 'social_group')->get();

    	return view('vaccination_agreements.create', compact('paymentTypes', 'vaccines', 'vaccineSeries', 'vaccineSpots', 'profContingents', 'riskGroups', 'socialGroups'));

    }

    public function store(Request $request)
    {
        $agreement = VaccinationAgreement::create($request->all());

        return redirect('vaccination_agreements/' . $agreement->id . '/edit');
    }

    public function edit(VaccinationAgreement $agreement)
    {
        $paymentTypes = PaymentType::where('name', 'Безналичный')->get();

        $vaccines = Vaccine::all();

        $vaccineSeries = $agreement->vaccine->series->where('enabled', 1);

        $vaccineSpots = VaccineSpot::all();

        $profContingents = Group::where('type', 'prof_contingent')->get();

        $riskGroups = Group::where('type', 'risk_group')->get();

        $socialGroups = Group::where('type', 'social_group')->get();

        $vaccinations = $agreement->vaccinations;

        return view('vaccination_agreements.edit', compact('agreement', 'paymentTypes', 'vaccines', 'vaccineSeries', 'vaccineSpots', 'profContingents', 'riskGroups', 'socialGroups', 'vaccinations'));
    }

    public function update(VaccinationAgreement $agreement, Request $request)
    {
        $agreement->update($request->all());

        return redirect()->back();
    }

    public function search(Request $request)
    {
        $id = $request->input('id');

        $name = $request->input('name');

        $filter = [

            'id' => $id,

            'name' => $name,

        ];

        $agreements = VaccinationAgreement::when($id, function ($query) use ($id) {
                        return $query->where('id', $id);
                    })
                    ->when($name, function ($query) use ($name){
                        return $query->orWhere('company_name', 'like', '%' . $name . '%');
                    })
                    ->paginate('20');

        $agreements->appends($request->except('page'));

        return view('vaccination_agreements.index', compact('agreements', 'filter'));
    }
}
