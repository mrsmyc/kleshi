<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaccination extends Model
{
    //protected $guarded = ['id'];
    protected $guarded = [];

    public function vaccination_agreement()
    {
    	return $this->belongsTo(VaccinationAgreement::class);
    }

    public function vaccine()
    {
    	return $this->belongsTo(Vaccine::class);
    }

    public function vaccine_level()
    {
        return $this->belongsTo(VaccineLevel::class);
    }

    public function group()
    {
    	return $this->hasOne(Group::class);
    }

    public function responsible()
    {
        return $this->belongsTo(User::class);
    }
    
}
