<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VaccineSerie extends Model
{
	protected $guarded = ['id'];
	
    public function vaccine()
    {
    	return $this->belongsTo(Vaccine::class);
    }
}
