<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vaccine extends Model
{
	protected $guarded = ['id'];
	
	public function series()
    {
    	return $this->hasMany(VaccineSerie::class);
    }

    public function price()
    {
        return $this->belongsTo(Price::class);
    }
}
